package NL.martijnpu.ChunkDefence.Arenas.Schematics;

import NL.martijnpu.ChunkDefence.Data.FileHandler;
import NL.martijnpu.ChunkDefence.Utils.Messages;

import java.util.ArrayList;
import java.util.List;

public final class SchematicManager {
    private static SchematicManager instance;
    private final List<String> allSchematics = new ArrayList<>();

    private SchematicManager() {
        allSchematics.addAll(FileHandler.get().getAllSchematics());
    }

    public static SchematicManager getInstance() {
        if (instance == null)
            instance = new SchematicManager();
        return instance;
    }

    public static void resetInstance() {
        if (instance != null)
            instance = null;
        SchematicEditor.resetInstance();
    }

    public void printInitData() {
        if (allSchematics.isEmpty())
            Messages.WARN.sendConsole("Loading none schematics. This is an error or first startup.");
        else
            Messages.PLUGIN.sendConsole("Loading " + allSchematics.size() + " schematic files.");
    }

    public boolean isInvalidSchematic(String name) {
        return name == null || !getAllSchematics().contains(name);
    }

    public List<String> getAllSchematics() {
        return allSchematics;
    }

    public void addSchematicName(String name) {
        if (!allSchematics.contains(name))
            allSchematics.add(name);
    }
}
