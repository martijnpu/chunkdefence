package NL.martijnpu.ChunkDefence.Arenas.Schematics;

import NL.martijnpu.ChunkDefence.Arenas.ArenaData;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Worlds.WorldManager;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.extent.clipboard.BlockArrayClipboard;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.*;
import com.sk89q.worldedit.function.operation.ForwardExtentCopy;
import com.sk89q.worldedit.function.operation.Operation;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.session.ClipboardHolder;
import com.sk89q.worldedit.world.block.BlockTypes;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;

public final class SchematicEditor {
    private static SchematicEditor instance;
    private final HashMap<String, Clipboard> clipboards = new HashMap<>();
    private final String folderName = "schematics/";
    private final String fileExtension = ".schem";

    private SchematicEditor() {
    }

    public static SchematicEditor getInstance() {
        if (instance == null)
            instance = new SchematicEditor();
        return instance;
    }

    public static void resetInstance() {
        instance = null;
    }

    @Nullable
    public Vector getDimensions(String schematicName) {
        if (!loadSchematic(schematicName))
            return null;

        BlockVector3 vector = clipboards.get(schematicName).getDimensions();
        return new Vector(vector.getX(), vector.getY(), vector.getZ());
    }

    public void saveSchematic(Player player, String schematicName) {
        try {
            WorldEditPlugin worldEditPlugin = (WorldEditPlugin) ChunkDefence.get().getServer().getPluginManager().getPlugin("WorldEdit");
            if (worldEditPlugin == null)
                return;

            File file = new File(ChunkDefence.get().getDataFolder().getPath(), folderName + schematicName + fileExtension);

            //Copy to clipboard
            Region region = worldEditPlugin.getSession(player).getSelection(worldEditPlugin.getSession(player).getSelectionWorld());
            Clipboard clipboard = new BlockArrayClipboard(region);
            EditSession editSession = WorldEdit.getInstance().newEditSession(new BukkitWorld(player.getWorld()));

            Messages.SCHEM_PROCESS.send(player, clipboard.getDimensions().toString());

            ForwardExtentCopy forwardExtentCopy = new ForwardExtentCopy(editSession, region, clipboard, region.getMinimumPoint());
            forwardExtentCopy.setCopyingEntities(true);
            Operations.complete(forwardExtentCopy);
            editSession.close();

            double x = player.getLocation().getX() - region.getMinimumPoint().getX();
            double y = player.getLocation().getY() - region.getMinimumPoint().getY();
            double z = player.getLocation().getZ() - region.getMinimumPoint().getZ();

            ArenaManager.getInstance().setArenaDelta(schematicName, new Location(null, x, y, z, player.getLocation().getYaw(), player.getLocation().getPitch()));

            //Save from clipboard to file
            if (file.delete())
                Messages.SCHEM_REMOVED.send(player);

            if (!file.getParentFile().exists())
                file.getParentFile().mkdir();

            try (ClipboardWriter writer = BuiltInClipboardFormat.SPONGE_SCHEMATIC.getWriter(Files.newOutputStream(file.toPath()))) {
                writer.write(clipboard);

                clipboards.put(schematicName, clipboard);
                Messages.SCHEM_SAVED.send(player, file.getName());
                SchematicManager.getInstance().addSchematicName(schematicName);
            }

        } catch (WorldEditException | NullPointerException ex) {
            Messages.SCHEM_SELECT.send(player);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public boolean pasteArenaAtLocation(Location location, String schematicName) {
        if (!loadSchematic(schematicName))
            return false;

        Location delta = ArenaManager.getInstance().getArenaDelta(schematicName);
        delta.setWorld(location.getWorld());
        Location tmpLocation = location.clone().subtract(delta);

        try (EditSession editSession = WorldEdit.getInstance().newEditSession(new BukkitWorld(tmpLocation.getWorld()))) {
            Clipboard clipboard = clipboards.get(schematicName);
            Operation operation = new ClipboardHolder(clipboard)
                    .createPaste(editSession)
                    .to(BlockVector3.at(tmpLocation.getBlockX(), tmpLocation.getBlockY(), tmpLocation.getBlockZ()))
                    .ignoreAirBlocks(false)
                    .copyEntities(true)
                    .build();
            Operations.complete(operation);
        } catch (WorldEditException | NullPointerException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void deleteArena(ArenaData arenaData) {
        if (WorldManager.getInstance().getArenaWorld() == null || !loadSchematic(arenaData.SCHEMATIC.get()))
            return;

        Vector base = arenaData.getArenaLoc();
        Vector dimensions = getDimensions(arenaData.SCHEMATIC.get());
        if (dimensions == null) {
            dimensions = new Vector();
            dimensions.setX(ConfigData.ARENA_SPACE.get() / 2.0);
            dimensions.setZ(dimensions.getX());

            int top = WorldManager.getInstance().getArenaWorld().getMaxHeight() - ConfigData.ARENA_HEIGHT.get();
            int bottom = -(WorldManager.getInstance().getArenaWorld().getMinHeight() + ConfigData.ARENA_HEIGHT.get());
            dimensions.setY(Math.min(top, bottom));
        }

        BlockVector3 minimum = BlockVector3.at(
                base.getBlockX() - dimensions.getBlockX(),
                base.getBlockY() - dimensions.getBlockY(),
                base.getBlockZ() - dimensions.getBlockZ());
        BlockVector3 maximum = BlockVector3.at(
                base.getBlockX() + dimensions.getBlockX(),
                base.getBlockY() + dimensions.getBlockY(),
                base.getBlockZ() + dimensions.getBlockZ());


        try (EditSession editSession = WorldEdit.getInstance().newEditSession(new BukkitWorld(WorldManager.getInstance().getArenaWorld()))) {
            CuboidRegion cuboidRegion = new CuboidRegion(minimum, maximum);

            editSession.getEntities(cuboidRegion).forEach(x -> {
                if (!(x instanceof Player))
                    x.remove();
            });

            editSession.setBlocks(cuboidRegion, BlockTypes.AIR.getDefaultState());
        } catch (WorldEditException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    private boolean loadSchematic(String schematicName) {
        if (clipboards.get(schematicName) != null) //Schematic already loaded
            return true;

        File file = new File(ChunkDefence.get().getDataFolder().getPath(), folderName + schematicName + fileExtension);
        WorldEditPlugin worldEditPlugin = (WorldEditPlugin) ChunkDefence.get().getServer().getPluginManager().getPlugin("WorldEdit");
        if (worldEditPlugin == null) {
            Messages.WARN.sendConsole("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
            Messages.WARN.sendConsole(Messages.SCHEM_NO_WE.get());
            Messages.WARN.sendConsole("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
            return false;
        }

        if (!file.exists()) {
            Messages.WARN.sendConsole("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
            Messages.WARN.sendConsole(Messages.SCHEM_UNKNOWN.get(schematicName));
            Messages.WARN.sendConsole("File: " + folderName + schematicName + fileExtension);
            Messages.WARN.sendConsole("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
            return false;
        }
        //Copy from file to clipboard
        ClipboardFormat format = ClipboardFormats.findByFile(file);
        if (format == null) {
            return false;
        }

        try (ClipboardReader reader = format.getReader(Files.newInputStream(file.toPath()))) {
            clipboards.put(schematicName, reader.read());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
