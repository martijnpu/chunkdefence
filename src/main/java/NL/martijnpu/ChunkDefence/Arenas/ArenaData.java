package NL.martijnpu.ChunkDefence.Arenas;

import NL.martijnpu.ChunkDefence.Arenas.Schematics.SchematicManager;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyInstance;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import NL.martijnpu.ChunkDefence.Worlds.WorldManager;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static NL.martijnpu.ChunkDefence.Data.FileHandler.ARENA_FILE;
import static NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyAdapter.*;

public final class ArenaData {
    private final ConfigKeyInstance<String> LOCATION;
    private final ConfigKeyInstance<List<String>> BOUGHT_ARENAS;
    private final ConfigKeyInstance<Boolean> IN_DELETION;
    private final ConfigKeyInstance<List<String>> MEMBERS;
    public final ConfigKeyInstance<Integer> WAVE;
    private final ConfigKeyInstance<Integer> DIFFICULTY;
    public final ConfigKeyInstance<String> GAMEMODE;
    public final ConfigKeyInstance<String> SCHEMATIC;
    final ConfigKeyInstance<Boolean> ENABLED;
    private final String ARENA_PATH_ = "arena.%";

    private final String arenaID;
    private Vector arenaLoc;
    public final List<Location> spawnLocations = new ArrayList<>(); //Runtime saved list to save calculating each time again

    /**
     * Create new ArenaData on the given location
     */
    ArenaData(Vector arenaLoc, String schematicName) {
        arenaID = getFreeID();
        LOCATION = new ConfigKeyInstance<>(stringKey(ARENA_FILE, ARENA_PATH_ + ".location", null), arenaID);
        BOUGHT_ARENAS = new ConfigKeyInstance<>(stringListKey(ARENA_FILE, ARENA_PATH_ + ".bought-arenas", new String[]{}), arenaID);
        IN_DELETION = new ConfigKeyInstance<>(booleanKey(ARENA_FILE, ARENA_PATH_ + ".is-in-deletion", false).hide(), arenaID);
        MEMBERS = new ConfigKeyInstance<>(stringListKey(ARENA_FILE, ARENA_PATH_ + ".members", new String[]{}), arenaID);
        WAVE = new ConfigKeyInstance<>(integerKey(ARENA_FILE, ARENA_PATH_ + ".wave", 1), arenaID);
        DIFFICULTY = new ConfigKeyInstance<>(integerKey(ARENA_FILE, ARENA_PATH_ + ".difficulty", 100), arenaID);
        GAMEMODE = new ConfigKeyInstance<>(stringKey(ARENA_FILE, ARENA_PATH_ + ".gamemode", "default"), arenaID);
        SCHEMATIC = new ConfigKeyInstance<>(stringKey(ARENA_FILE, ARENA_PATH_ + ".schematic", "defaultSchematic"), arenaID);
        ENABLED = new ConfigKeyInstance<>(booleanKey(ARENA_FILE, ARENA_PATH_ + ".enabled", true), arenaID);

        setLocation(arenaLoc);
        SCHEMATIC.set(schematicName);

        setAllData();
    }

    /**
     * Create ArenaData from existing config
     */
    ArenaData(String key) throws IllegalArgumentException {
        arenaID = key;
        LOCATION = new ConfigKeyInstance<>(stringKey(ARENA_FILE, ARENA_PATH_ + ".location", null), arenaID);
        BOUGHT_ARENAS = new ConfigKeyInstance<>(stringListKey(ARENA_FILE, ARENA_PATH_ + ".bought-arenas", new String[]{}), arenaID);
        IN_DELETION = new ConfigKeyInstance<>(booleanKey(ARENA_FILE, ARENA_PATH_ + ".is-in-deletion", false).hide(), arenaID);
        MEMBERS = new ConfigKeyInstance<>(stringListKey(ARENA_FILE, ARENA_PATH_ + ".members", new String[]{}), arenaID);
        WAVE = new ConfigKeyInstance<>(integerKey(ARENA_FILE, ARENA_PATH_ + ".wave", 1), arenaID);
        DIFFICULTY = new ConfigKeyInstance<>(integerKey(ARENA_FILE, ARENA_PATH_ + ".difficulty", 100), arenaID);
        GAMEMODE = new ConfigKeyInstance<>(stringKey(ARENA_FILE, ARENA_PATH_ + ".gamemode", "default"), arenaID);
        SCHEMATIC = new ConfigKeyInstance<>(stringKey(ARENA_FILE, ARENA_PATH_ + ".schematic", "defaultSchematic"), arenaID);
        ENABLED = new ConfigKeyInstance<>(booleanKey(ARENA_FILE, ARENA_PATH_ + ".enabled", true), arenaID);

        arenaLoc = Statics.getXYZFromString(LOCATION.get());
        if (arenaLoc == null)
            throw new IllegalArgumentException("Location is null or invalid for ArenaID '" + arenaID + "': " + LOCATION.get());

        String schematic = SCHEMATIC.get();
        if (schematic == null || SchematicManager.getInstance().isInvalidSchematic(schematic))
            throw new IllegalArgumentException("Schematic is null or invalid for ArenaID '" + arenaID + "': " + SCHEMATIC.get());

        setAllData();
    }

    private void setAllData() {
        if (!DIFFICULTY.isSet())
            DIFFICULTY.set(ConfigData.GM_DIFFICULTY_DEFAULT.get(GAMEMODE.get()));
        if (!SCHEMATIC.isSet())
            SCHEMATIC.set(ConfigData.GM_SCHEMATIC.get(GAMEMODE.get()));
    }

    void remove() {
        ARENA_FILE.get().set(ARENA_PATH_.replace("%", arenaID), null);
        ARENA_FILE.save();
    }

    void prepareForDeletion() {
        IN_DELETION.set(true);
        MEMBERS.set(null);
    }

    boolean needsDeletion() {
        return IN_DELETION.get();
    }

    public Vector getArenaLoc() {
        return arenaLoc == null ? null : arenaLoc.clone();
    }

    private void setLocation(Vector location) {
        arenaLoc = location;
        LOCATION.set(Statics.getStringFromXYZ(location));
    }

    public Location getSpawnLocation() {
        Location delta = ArenaManager.getInstance().getArenaDelta(SCHEMATIC.get());
        Location spawnLocation = arenaLoc.toLocation(WorldManager.getInstance().getArenaWorld(), delta.getYaw(), delta.getPitch());
        spawnLocation.setX(spawnLocation.getX() + (delta.getX() % 1));
        spawnLocation.setY(spawnLocation.getY() + (delta.getY() % 1));
        spawnLocation.setZ(spawnLocation.getZ() + (delta.getZ() % 1));
        return spawnLocation;
    }

    public List<UUID> getMembers() {
        return MEMBERS.get().stream().map(UUID::fromString).toList();
    }

    void addMember(UUID uuid) {
        List<String> list = MEMBERS.get();
        list.add(uuid.toString());
        MEMBERS.set(list);
    }

    boolean removeMember(UUID uuid) {
        List<String> list = MEMBERS.get();
        if (list.remove(uuid.toString())) {
            MEMBERS.set(null);
            MEMBERS.set(list);
            return true;
        }
        return false;
    }

    public int getDifficulty() {
        return DIFFICULTY.get();
    }

    public void editDifficulty(boolean isWin) {
        int newDiff = getDifficulty() + (isWin ? ConfigData.GM_DIFFICULTY_ON_WIN.get(GAMEMODE.get()) : -ConfigData.GM_DIFFICULTY_ON_LOSE.get(GAMEMODE.get()));
        newDiff = Math.max(newDiff, ConfigData.GM_DIFFICULTY_MINIMUM.get(GAMEMODE.get()));
        DIFFICULTY.set(newDiff);
    }

    public boolean hasBoughtArena(String schematic) {
        if (ConfigData.GM_ARENA_SAVE_BOUGHT.get(GAMEMODE.get()))
            return BOUGHT_ARENAS.get().contains(schematic) || SCHEMATIC.get().equals(schematic);
        return SCHEMATIC.get().equals(schematic);
    }

    void addBoughtArena(String schematic) {
        List<String> list = BOUGHT_ARENAS.get();
        list.add(schematic);
        BOUGHT_ARENAS.set(list);
    }

    /**
     * Swap the location and schematic data with another Arena.
     * All other data will be preserved.
     */
    void swapArenaData(ArenaData otherData) {
        Vector locationCopy = arenaLoc.clone();
        setLocation(otherData.arenaLoc);
        otherData.setLocation(locationCopy);

        String schematicCopy = SCHEMATIC.get();
        SCHEMATIC.set(otherData.SCHEMATIC.get());
        otherData.SCHEMATIC.set(schematicCopy);

        spawnLocations.clear();
        otherData.spawnLocations.clear();

        addBoughtArena(otherData.SCHEMATIC.get());
    }

    /**
     * Swap the data of a new Arena with the current one.
     * The Free arena will now be occupied
     * <B>The current arena needs to be scheduled for deletion!</B>
     */
    void changeArena(ArenaData free) {
        Vector oldLocation = arenaLoc.clone();
        String oldSchematic = SCHEMATIC.get();
        List<String> boughtArenas = BOUGHT_ARENAS.get();

        if (!boughtArenas.contains(SCHEMATIC.get()))
            boughtArenas.add(SCHEMATIC.get());
        if (!boughtArenas.contains(free.SCHEMATIC.get()))
            boughtArenas.add(free.SCHEMATIC.get());

        this.setLocation(free.arenaLoc);
        free.setLocation(oldLocation);

        this.SCHEMATIC.set(free.SCHEMATIC.get());
        free.SCHEMATIC.set(oldSchematic);

        this.spawnLocations.clear();
        free.spawnLocations.clear();

        this.BOUGHT_ARENAS.set(boughtArenas);
        free.BOUGHT_ARENAS.setDefault(true);
    }

    private String getFreeID() {
        int i = 1;
        while (ARENA_FILE.get().isSet(ARENA_PATH_.replace("%", i + "")))
            i++;
        return i + "";
    }
}
