package NL.martijnpu.ChunkDefence.Arenas;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Players.EconomyManager;
import NL.martijnpu.ChunkDefence.Players.PlayerDataManager;
import NL.martijnpu.ChunkDefence.Scores.ScoreManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import NL.martijnpu.ChunkDefence.Utils.TimeHandler;
import NL.martijnpu.ChunkDefence.Waves.WaveController;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public final class Arena {
    private final ArenaData arenaData;
    private final List<Player> onlinePlayers = new ArrayList<>();
    private final List<Player> playerQueue = new ArrayList<>();
    @Nullable
    private WaveController waveController; //Null if no players online
    private long arenaTime;
    private long lastSwitchTime = 0;

    /**
     * Create a new Arena from a location
     */
    Arena(Vector location, String schematicName) {
        arenaData = new ArenaData(location, schematicName);
        Messages.DEBUG.sendConsole("Creating new Arena '" + schematicName + "' at " + location);
    }

    /**
     * Create Arena from existing config
     */
    Arena(String key) throws IllegalArgumentException {
        arenaData = new ArenaData(key);
        ScoreManager.getInstance().updateArenaScore(this);
    }

    public long getSecondsSinceLastSwitch() {
        return TimeHandler.getCurrSec() - lastSwitchTime;
    }

    public ArenaData getArenaData() {
        return arenaData;
    }

    @Nullable
    public WaveController getWaveController() {
        return waveController;
    }

    public List<Player> getOnlinePlayers() {
        return onlinePlayers;
    }

    public Player getRandomOnlinePlayer() {
        if (onlinePlayers.isEmpty())
            return null;
        if (onlinePlayers.size() == 1)
            return onlinePlayers.get(0);
        return onlinePlayers.get(new Random().nextInt(onlinePlayers.size() - 1));
    }

    /**
     * Swap the location and type of 2 arenas.
     * Keeps all player/wave data intact
     */
    void swapArenaData(Arena newArena) {
        for (Player onlinePlayer : onlinePlayers) {
            Messages.ARENA_CHANGE.send(onlinePlayer);
            if (isInsideArena(onlinePlayer.getLocation()))
                Statics.teleportToSpawn(onlinePlayer, true);
        }
        for (Player onlinePlayer : newArena.onlinePlayers) {
            Messages.ARENA_CHANGE.send(onlinePlayer);
            if (newArena.isInsideArena(onlinePlayer.getLocation()))
                Statics.teleportToSpawn(onlinePlayer, true);
        }

        lastSwitchTime = TimeHandler.getCurrSec();
        arenaData.swapArenaData(newArena.arenaData);
    }

    /**
     * Mark arena for deletion and remove all active elements
     */
    void prepareForDeletion() {
        for (Player queuedPlayer : playerQueue) {
            Messages.ARENA_DELETE.send(queuedPlayer);
        }

        List<Player> clone = new ArrayList<>(onlinePlayers);
        for (Player onlinePlayer : clone) {
            Messages.ARENA_DELETE.send(onlinePlayer);
            playerRemove(onlinePlayer, false);
        }

        playerQueue.clear();
        onlinePlayers.clear();

        if (waveController != null)
            waveController.shutdown();
        arenaData.prepareForDeletion();
    }

    /**
     * Should be called AFTER prepareForDeletion
     */
    void delete() {
        if (!arenaData.needsDeletion())
            prepareForDeletion();

        Messages.DEBUG.sendConsole("Removing Arena '" + arenaData.SCHEMATIC.get() + "' at " + arenaData.getArenaLoc());
        arenaData.remove();
    }

    /**
     * Stop for server shutdown/restart
     */
    void shutDown() {
        if (waveController != null)
            waveController.shutdown();
        waveController = null;

        List<Player> copy = new ArrayList<>(onlinePlayers);
        copy.forEach(this::playerQuit);
    }

    /**
     * Cancel current wave and start a new one
     */
    public void restartWave() {
        if (waveController != null)
            waveController.shutdown();
        if (arenaData.ENABLED.get())
            waveController = new WaveController(this);
    }

    /**
     * Retrieve the saved progress of all players and the arena and enable the arena
     */
    public void activateArena() {
        List<String> unableToJoin = new ArrayList<>();
        List<Player> ableToJoin = new ArrayList<>();

        String currGamemode = arenaData.GAMEMODE.get();
        for (UUID member : arenaData.getMembers()) {
            OfflinePlayer offPlayer = ChunkDefence.get().getServer().getOfflinePlayer(member);

            PlayerDataManager.getInstance().getUserData(member).retrieveInventoryGamemode(currGamemode);
            EconomyManager.getInstance().retrieveBalance(offPlayer, currGamemode);

            if (offPlayer.isOnline()) {
                Player player = (Player) offPlayer;

                if (ArenaManager.getInstance().getArena(player) == null) {
                    Messages.ARENA_ACTIVATE.send(player, arenaData.GAMEMODE.get());
                    ableToJoin.add(player);
                } else {
                    Messages.CUSTOM.send(player, "Arena xx is activated, but since you are currently in another Arena you cannot join."); //Todo create new message
                    arenaData.removeMember(player.getUniqueId());
                    unableToJoin.add(player.getName());
                }
            }
        }

        arenaData.ENABLED.set(true);
        ableToJoin.forEach(this::playerJoin); //Moved down. setEnabled needs to be after player.getArena check

        if (!unableToJoin.isEmpty())
            sendMessageToAll(Messages.CUSTOM, "The following players are unable to join: " + unableToJoin); //Todo create new message
    }

    /**
     * Save the current progress of all players and the arena and disable the arena till it's reactivated
     */
    public void deactivateArena() {
        shutDown();
        arenaData.ENABLED.set(false);

        String currGamemode = arenaData.GAMEMODE.get();
        for (UUID member : arenaData.getMembers()) {
            OfflinePlayer offPlayer = ChunkDefence.get().getServer().getOfflinePlayer(member);

            EconomyManager.getInstance().saveBalance(offPlayer, currGamemode);
            PlayerDataManager.getInstance().getUserData(member).saveInventoryGamemode(currGamemode);

            if (offPlayer.isOnline()) {
                Player player = (Player) offPlayer;
                Messages.ARENA_DEACTIVATE.send(player, arenaData.GAMEMODE.get());

                Statics.teleportToSpawn(player, false);
                player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(20);
                player.setHealth(20);
                player.setExhaustion(20);
            }
        }
    }

    //region Player Events

    /**
     * Calls when a player joins the Arena
     */
    public void playerJoin(Player player) {
        if (!arenaData.ENABLED.get()) {
            Messages.EXCEPT_ADMIN.send(player);
            return;
        }

        boolean newMember = !arenaData.getMembers().contains(player.getUniqueId());

        if (newMember) {
            List<UUID> oldMembers = arenaData.getMembers();
            arenaData.addMember(player.getUniqueId());
            //ScoreManager.getInstance().updateMembers(this, oldMembers);
            sendMessageToAll(Messages.ARENA_ADD, player.getName());
        } else {
            sendMessageToAll(Messages.ARENA_JOIN, player.getName());
        }

        if (!arenaData.getMembers().contains(player.getUniqueId())) {
            Messages.EXCEPT_ADMIN.send(player);
            Messages.WARN.sendConsole("Error occured while adding player " + player.getName() + " to arena " + arenaData.SCHEMATIC.get() + ". NewMember: " + newMember);
            throw new NullPointerException();
        }

        int health = ConfigData.GM_MAX_HEALTH.get(arenaData.GAMEMODE.get());
        player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(health);
        player.setHealth(health);

        //Player can not join active wave. Needs to wait till next one
        if (waveController != null && waveController.isInWave()) {
            playerQueue.add(player);

            Statics.teleportToSpawn(player, false);
            Messages.WAVE_PROGRESS.send(player);
            return;
        }

        onlinePlayers.add(player);
        teleportHome(player, true);

        if (waveController == null)
            waveController = new WaveController(this);
        else
            waveController.updatePlayers();
    }

    /**
     * Remove the player from the arena
     *
     * @param notify Notify the other online players
     */
    public void playerRemove(Player player, boolean notify) {
        if (!arenaData.ENABLED.get()) {
            Messages.EXCEPT_ADMIN.send(player);
            return;
        }

        List<UUID> oldMembers = arenaData.getMembers();
        player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(20);
        player.setHealth(20);
        player.setExhaustion(20);

        if (arenaData.removeMember(player.getUniqueId())) {
            playerQuit(player);
            player.setPlayerTime(12000, false);

            if (notify)
                sendMessageToAll(Messages.ARENA_QUIT, player.getName());

            if (isInsideArena(player.getLocation()))
                Statics.teleportToSpawn(player, false);

            if (arenaData.getMembers().isEmpty()) {
                ArenaManager.getInstance().removeArena(this);
                Messages.ARENA_DELETE.send(player);
            }// else if (!notify)
            //ScoreManager.getInstance().updateMembers(this, oldMembers);

            if (ConfigData.ARENA_RESET_ON_LEAVE.get()) {
                EconomyManager.getInstance().setBalance(player, 0);
                player.getInventory().clear();
                player.getActivePotionEffects().forEach(x -> player.removePotionEffect(x.getType()));
            }
        }
    }

    /**
     * Call when a player goes offline
     */
    public void playerQuit(Player player) {
        if (!arenaData.ENABLED.get()) {
            Messages.EXCEPT_ADMIN.send(player);
            return;
        }

        if (onlinePlayers.remove(player) && waveController != null) {
            if (onlinePlayers.isEmpty()) {
                waveController.shutdown();
                waveController = null;
            } else {
                sendMessageToAll(Messages.ARENA_LEFT, player.getName());
                waveController.updatePlayers();
            }
        }
    }

    /**
     * Call when a player dies
     */
    public void playerDeath(Player player) {
        if (waveController == null || !onlinePlayers.contains(player))
            return;

        //If players are death they are not in the arena anymore
        boolean allDeath = onlinePlayers.stream().noneMatch(onlinePlayer -> onlinePlayer != player && isInsideArena(onlinePlayer.getLocation()));

        if (allDeath) {
            if (ConfigData.GM_DEATH_RESET_ARENA.get(arenaData.GAMEMODE.get())) {
                ArenaManager.getInstance().removeArena(this);
                player.setBedSpawnLocation(Statics.getLocationFromString(ConfigData.SPAWN_LOCATION.get()), true);
            } else {
                if (ConfigData.GM_DEATH_RESET_WAVES.get(arenaData.GAMEMODE.get())) {
                    //ScoreManager.getInstance().removeArena(this);
                    arenaData.WAVE.setDefault(true);
                    sendMessageToAll(Messages.WAVE_RESET);
                }

                if (ConfigData.GM_DEATH_RESTART_WAVE.get(arenaData.GAMEMODE.get())) {
                    restartWave();
                    sendMessageToAll(Messages.WAVE_CANCEL);
                }
            }
        } else {
            waveController.getWaveData().updateTargets(player);
        }
    }
    //endregion

    public void sendMessageToAll(Messages message, String... replacements) {
        onlinePlayers.forEach(player -> message.send(player, replacements));
    }

    public void onWaveMobKill(Player killer, boolean isTrap, int coins, String mobName) {
        if (isTrap)
            sendMessageToAll(Messages.ECO_ADD_TRAP, killer.getName(), mobName, String.valueOf(coins));
        else
            sendMessageToAll(Messages.ECO_ADD_KILL, killer.getName(), mobName, String.valueOf(coins));

        getOnlinePlayers().forEach(x -> EconomyManager.getInstance().depositPlayer(x, coins));
        ScoreManager.getInstance().addMobKill(killer.getUniqueId());
    }

    public void teleportHome(Player player, boolean forced) {
        if (onlinePlayers.contains(player)) {
            if (lastSwitchTime == 0 || lastSwitchTime <= TimeHandler.getCurrSec() - 14) { //No teleport in 15s change time
                if (forced || (waveController != null && !waveController.isInWave())) {
                    if (forced || !isInsideArena(player.getLocation())) {
                        player.teleport(arenaData.getSpawnLocation(), PlayerTeleportEvent.TeleportCause.PLUGIN);
                        if (!forced) //If teleport from outside message is handled by teleportEvent
                            Messages.ARENA_TELEPORT.send(player);
                    } else
                        Messages.ARENA_INSIDE.send(player);
                } else
                    Messages.WAVE_PROGRESS.send(player);
            } else
                Messages.ARENA_CHANGE.send(player);
        } else if (!forced)
            Messages.ARENA_NONE.send(player);
    }

    /**
     * Check if the given location is inside the boundaries of the arena slot
     */
    public boolean isInsideArena(Location location) {
        float half = ConfigData.ARENA_SPACE.get() / 2f;
        return location.getX() >= (arenaData.getArenaLoc().getX() - half)
                && location.getX() <= (arenaData.getArenaLoc().getX() + half)
                && location.getZ() >= (arenaData.getArenaLoc().getZ() - half)
                && location.getZ() <= (arenaData.getArenaLoc().getZ() + half);
    }

    /**
     * Ticks from TimeHandler
     */
    void pingSec() {
        if (!playerQueue.isEmpty() && (waveController == null || !waveController.isInWave())) {
            for (Player player : playerQueue) {
                if (arenaData.getMembers().contains(player.getUniqueId())) {
                    onlinePlayers.add(player);
                    Messages.WAVE_FINISH.send(player);
                }
            }
            playerQueue.clear();
        }

        if (waveController != null)
            waveController.pingSec();

        updateSunAndMoon();
    }

    private void updateSunAndMoon() {
        for (Player onlinePlayer : onlinePlayers) {
            long playerTime = onlinePlayer.getPlayerTime();
            if (playerTime - arenaTime > 10 || playerTime - arenaTime < -10)
                onlinePlayer.setPlayerTime(arenaTime, false);
        }
    }

    public void setArenaTime(long arenaTime) {
        this.arenaTime = arenaTime;
    }

    public String getArenaMembers() {
        StringBuilder members = new StringBuilder();
        for (UUID member : arenaData.getMembers()) {
            Player player = ChunkDefence.get().getServer().getPlayer(member);
            if (player != null) {
                members.append(player.getName());
            } else {
                OfflinePlayer oPlayer = ChunkDefence.get().getServer().getOfflinePlayer(member);
                members.append(oPlayer.getName() == null ? "Unknown Player" : oPlayer.getName());
            }
            members.append(", ");
        }
        if (members.length() >= 2) //remove last ", "
            members.delete(members.length() - 2, members.length());

        return members.toString();
    }
}
