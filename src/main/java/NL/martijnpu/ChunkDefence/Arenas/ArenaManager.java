package NL.martijnpu.ChunkDefence.Arenas;

import NL.martijnpu.ChunkDefence.Arenas.Schematics.SchematicEditor;
import NL.martijnpu.ChunkDefence.Arenas.Schematics.SchematicManager;
import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Data.FileHandler;
import NL.martijnpu.ChunkDefence.Players.PlayerDataManager;
import NL.martijnpu.ChunkDefence.Scores.ScoreManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import NL.martijnpu.ChunkDefence.Utils.TimeHandler;
import NL.martijnpu.ChunkDefence.Worlds.WorldManager;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.util.*;

public final class ArenaManager {
    private static ArenaManager instance;
    private final String DELTA_PATH = "delta-player-spawn";
    private final List<Arena> arenas = new ArrayList<>();
    private final List<Location> invalidArenas = new ArrayList<>();
    private final Map<String, Location> arena_deltas = new HashMap<>();

    private ArenaManager() {
        if (WorldManager.getInstance().getArenaWorld() == null) {
            Messages.WARN.sendConsole("Unable to load any Arena's since the Arenaworld '" + ConfigData.ARENA_WORLD.get() + "' is not loaded.");
            return;
        }
        load();
    }

    public static ArenaManager getInstance() {
        if (instance == null)
            instance = new ArenaManager();
        return instance;
    }

    public static void resetInstance() {
        if (instance != null)
            instance.shutDown();
        instance = null;
    }

    public void printInitData() {
        if (arenas.isEmpty())
            Messages.WARN.sendConsole("Loading an empty arena file. This is an error or nobody has an arena yet :(.");
        else
            Messages.PLUGIN.sendConsole("Loading " + arenas.size() + " player arena's.");
    }

    public void shutDown() {
        arenas.forEach(Arena::shutDown);
        arenas.clear();
        invalidArenas.clear();
        arena_deltas.clear();
    }

    public void updateSec() {
        for (Arena arena : arenas)
            arena.pingSec();
    }

    /**
     * Get an unmodifiable list of all arenas. If you need to search use any of the other methods!
     */
    public List<Arena> getArenas() {
        return Collections.unmodifiableList(arenas);
    }

    @Nullable
    public Arena getArenaCreateIfNotExist(Player player, String gamemode) {
        Arena arena = getArena(player);
        if (arena != null)
            return arena;

        //Check cooldown
        long delta = PlayerDataManager.getInstance().getUserData(player.getUniqueId()).lastArenaCreatePassed(ConfigData.ARENA_BUY_COOLDOWN.get());
        if (delta > 0) {
            Messages.SHOP_BUY_COOLDOWN.send(player, String.valueOf(delta + 1));
            return null;
        }

        //Check free Arenas
        String schematic = ConfigData.GM_SCHEMATIC.get(gamemode);

        arena = createArena(player, schematic);
        if (arena == null) //Messages are already handled
            return null;

        final Arena finalArena = arena;
        arena.getArenaData().GAMEMODE.set(gamemode);
        Messages.ARENA_CREATE.send(player, ConfigData.ARENA_TELEPORT_DELAY.get() + "");

        ChunkDefence.get().getServer().getScheduler().runTaskLater(ChunkDefence.get(),
                () -> finalArena.playerJoin(player),
                TimeHandler.SEC * ConfigData.ARENA_TELEPORT_DELAY.get());
        return arena;
    }

    /**
     * Return the Arena a player is actively playing regardless of the gamemode. Null if no Arena found
     */
    @Nullable
    public Arena getArena(Player player) {
        if (player == null)
            return null;
        return arenas.stream().filter(arena -> arena.getArenaData().ENABLED.get() && arena.getArenaData().getMembers().contains(player.getUniqueId())).findFirst().orElse(null);
    }

    /**
     * Return the Arena a player based on the gamemode. This can be either an enabled or disable arena. Null if no Arena found
     */
    @Nullable
    public Arena getArena(Player player, String gamemode) {
        if (player == null || gamemode.isBlank())
            return null;
        return arenas.stream().filter(arena -> gamemode.equals(arena.getArenaData().GAMEMODE.get()) && arena.getArenaData().getMembers().contains(player.getUniqueId())).findFirst().orElse(null);
    }

    /**
     * Return the Arena where the location is inside. Null if no Arena found
     */
    @Nullable
    public Arena getArena(Location location) {
        if (location == null || location.getWorld() != WorldManager.getInstance().getArenaWorld())
            return null;
        return arenas.stream().filter(arena -> arena.isInsideArena(location)).findFirst().orElse(null);
    }

    /**
     * Change the arena to a new form
     *
     * @return Whether the change is successful
     */
    public boolean changeArena(Player player, String schematic) {
        Arena originalArena = getArena(player);
        if (originalArena == null) {
            Messages.ARENA_NONE.send(player);
            return false;
        }

        if (originalArena.getWaveController() != null && originalArena.getWaveController().isInWave()) {
            Messages.WAVE_PROGRESS.send(player);
            return false;
        }

        Arena newArena = createArena(player, schematic);
        if (newArena == null)
            return false;

        Messages.ARENA_CREATE.send(player, ConfigData.ARENA_TELEPORT_DELAY.get() + "");
        originalArena.swapArenaData(newArena); //Swap location, meaning the new Arena will be the original

        ChunkDefence.get().getServer().getScheduler().runTaskLater(ChunkDefence.get(), () -> {
            for (Player onlinePlayer : originalArena.getOnlinePlayers())
                originalArena.teleportHome(onlinePlayer, true);

            newArena.shutDown();
            removeArena(newArena);
        }, TimeHandler.SEC * ConfigData.ARENA_TELEPORT_DELAY.get());

        return true;
    }

    /**
     * Mark the arena to be deleted. Deletion will take place 60s later to prevent any issues.
     */
    public void removeArena(Arena arena) {
        //ScoreManager.getInstance().removeArena(arena);
        arena.prepareForDeletion();
        SchematicEditor.getInstance().deleteArena(arena.getArenaData());

        ChunkDefence.get().getServer().getScheduler().runTaskLater(ChunkDefence.get(), () -> {
            arena.delete();
            arenas.remove(arena);
        }, TimeHandler.SEC * 60);
    }

    public void setArenaDelta(String name, Location location) {
        Location tempLoc = Statics.roundToHalf(location);
        tempLoc.setWorld(null);
        arena_deltas.put(name, tempLoc);

        FileHandler.ARENA_FILE.get().set(DELTA_PATH + "." + name, Statics.getStringFromLocationExcludeWorld(tempLoc));
        FileHandler.ARENA_FILE.save();
    }

    /**
     * Get the delta from the center of an Arena (Player spawn) to the minimum location for the schematic
     */
    public Location getArenaDelta(String schematic) {
        Location location = arena_deltas.get(schematic);
        if (location != null)
            return location.clone();
        Messages.WARN.sendConsole("Unable to read delta location of " + schematic);
        return new Location(null, 0, 0, 0, 90, 0);
    }

    private void load() {
        String path = "arena";
        arenas.clear();
        invalidArenas.clear();
        arena_deltas.clear();

        ConfigurationSection csArena = FileHandler.ARENA_FILE.get().getConfigurationSection(path);

        if (csArena != null) {
            for (String key : csArena.getKeys(false)) {
                try {
                    Arena arena = new Arena(key);
                    if (arena.getArenaData().needsDeletion()) {
                        removeArena(arena);
                        Messages.DEBUG.sendConsole("Found Arena scheduled to delete at index '" + key + "'. Removing it now...");
                    } else if (arena.getArenaData().getMembers().isEmpty()) {
                        removeArena(arena);
                        Messages.DEBUG.sendConsole("Found empty Arena at index '" + key + "'. Removing it now...");
                    } else {
                        arenas.add(arena);
                        ScoreManager.getInstance().updateArenaScore(arena);
                    }
                } catch (IllegalArgumentException ex) {
                    Messages.WARN.sendConsole(ex.getMessage() + ". Skip loading this arena...");
                    String fileLoc = FileHandler.ARENA_FILE.get().getString(path + "." + key + ".location", "");
                    Location loc = Statics.getLocationFromString(fileLoc);
                    if (loc != null)
                        invalidArenas.add(loc);
                }
            }
        }


        ConfigurationSection csDelta = FileHandler.ARENA_FILE.get().getConfigurationSection(DELTA_PATH);
        if (csDelta != null)
            csDelta.getKeys(false).forEach(key -> arena_deltas.put(key, Statics.getLocationFromString(csDelta.getString(key, null))));
    }

    /**
     * Create & register a new Arena on an empty space.
     * In order to be sure the Arena is created properly it's recommended to add a delay before interacting with the Arena
     */
    private Arena createArena(Player player, String schematic) {
        if (SchematicManager.getInstance().isInvalidSchematic(schematic)) {
            Messages.SCHEM_UNKNOWN.send(player, schematic);
            return null;
        }

        Vector location = getNextLocation();
        if (location == null) {
            Messages.EXCEPT_ADMIN.send(player);
            Messages.WARN.sendConsole("Unable to get a valid free location. Please contact the plugin author.");
            return null;
        }

        Arena newArena = new Arena(location, schematic);

        if (SchematicEditor.getInstance().pasteArenaAtLocation(newArena.getArenaData().getSpawnLocation(), schematic)) {
            newArena.getArenaData().SCHEMATIC.set(schematic);
            arenas.add(newArena);
        } else {
            Messages.WARN.sendConsole("&cSomething went wrong while creating an arena!");
            Messages.EXCEPT_ADMIN.send(player);
        }

        return newArena;
    }

    /**
     * This method will get the next available location for an Arena.
     * The search is in an outwards spiral.
     */
    @Nullable
    private Vector getNextLocation() {
        // (dirX, dirZ) is a vector - direction in which we move right now
        int dirX = 1;
        int dirZ = 0;
        // length of current segment
        int segment_length = 1;

        // current position (x, z) and how much of current segment we passed
        Vector location = new Vector(0, ConfigData.ARENA_HEIGHT.get(), 0);
        int segment_passed = 0;
        for (int k = 0; k < arenas.size() + 2; k++) {
            // make a step, add 'direction' vector (dirX, dirZ) to current position (x, z)
            location.setX(location.getX() + dirX * ConfigData.ARENA_SPACE.get());
            location.setZ(location.getZ() + dirZ * ConfigData.ARENA_SPACE.get());

            if (locationAvailable(location))
                return location;

            segment_passed++;
            if (segment_passed == segment_length) {
                // done with current segment
                segment_passed = 0;

                // 'rotate' directions
                int buffer = dirX;
                dirX = -dirZ;
                dirZ = buffer;

                // increase segment length if necessary
                if (dirZ == 0) {
                    segment_length++;
                }
            }
        }
        Messages.WARN.sendConsole("Unable to find free spot. Stopped checking at location '" + location + "'. Passed " + segment_passed + " segments in direction X:" + dirX + " - Z:" + dirZ);
        return null;
    }

    /**
     * Loops through all existing arenas to request their position
     * Location 0, 0 is always reserved as shop area
     *
     * @return Whether the requested location is not in use
     */
    private boolean locationAvailable(Vector location) {
        if (location.getBlockX() == 0 && location.getBlockZ() == 0)
            return false;

        Vector loc;
        for (Arena arena : arenas) {
            loc = arena.getArenaData().getArenaLoc();
            if (loc != null && loc.getBlockX() == location.getBlockX() && loc.getBlockZ() == location.getBlockZ())
                return false;
        }

        for (Location invalidLoc : invalidArenas) {
            if (invalidLoc.getBlockX() == location.getBlockX() && invalidLoc.getBlockZ() == location.getBlockZ())
                return false;
        }
        return true;
    }
}
