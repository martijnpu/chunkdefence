package NL.martijnpu.ChunkDefence.Mobs;

import NL.martijnpu.ChunkDefence.Data.FileHandler;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static NL.martijnpu.ChunkDefence.Data.FileHandler.MOBS_FILE;

public class MobManager {
    private final String MOBS_PATH = "mobs";
    private final String WAVES_PATH = "defined-waves";
    private final List<DefinedMobData> generatedMobs = new ArrayList<>();
    private final Map<Integer, List<DefinedMobData>> waves = new HashMap<>();
    private static MobManager instance;

    private MobManager() {
        boolean save = FileHandler.setNotExists(MOBS_FILE.get(), MOBS_PATH, "[]");
        save |= FileHandler.setNotExists(MOBS_FILE.get(), WAVES_PATH, "[]");
        if (save) MOBS_FILE.save();

        loadMobs();
        loadWaves();
    }

    public static MobManager getInstance() {
        if (instance == null)
            instance = new MobManager();
        return instance;
    }

    public static void resetInstance() {
        instance = null;
    }

    public void printInitData() {
        if (generatedMobs.isEmpty())
            Messages.WARN.sendConsole("Loading none mobs. This is an error or you have none mobs in the waves :(.");
        else Messages.PLUGIN.sendConsole("Loading " + generatedMobs.size() + " mobs");

        if (waves.isEmpty())
            Messages.PLUGIN.sendConsole("Loading none defined waves. This is an error or you have none waves :(.");
        else Messages.PLUGIN.sendConsole("Loading " + waves.size() + " defined waves");
    }

    public boolean isWaveDefined(int wave) {
        return waves.containsKey(wave);
    }

    public List<DefinedMobData> getDefinedMobs() {
        return generatedMobs;
    }

    public Map<Integer, DefinedMobData> getDefinedWave(int wave) {
        Map<Integer, DefinedMobData> map = new HashMap<>();

        if (isWaveDefined(wave))
            waves.get(wave).forEach(x -> map.put(x.getAmount(), x));
        return map;
    }

    private void loadMobs() {
        ConfigurationSection cs = MOBS_FILE.get().getConfigurationSection(MOBS_PATH);

        generatedMobs.addAll(getDefinedMobsFromConfig(cs));
    }

    private void loadWaves() {
        waves.clear();
        ConfigurationSection cs = MOBS_FILE.get().getConfigurationSection(WAVES_PATH);

        if (cs == null)
            return;

        for (String key : cs.getKeys(false)) {
            try {
                ConfigurationSection inner_cs = cs.getConfigurationSection(key);
                if (inner_cs != null)
                    waves.put(Integer.valueOf(key), getDefinedMobsFromConfig(inner_cs));
            } catch (NumberFormatException ex) {
                Messages.WARN.sendConsole(ex.toString());
            }
        }
    }

    private List<DefinedMobData> getDefinedMobsFromConfig(ConfigurationSection cs) {
        List<DefinedMobData> list = new ArrayList<>();

        if (cs != null) {
            for (String key : cs.getKeys(false)) {
                try {
                    list.add(new DefinedMobData(cs.getConfigurationSection(key)));
                } catch (IllegalArgumentException | NullPointerException ex) {
                    Messages.WARN.sendConsole("Unable to process a defined mob '" + cs.getCurrentPath() + "." + key + "'. Skipping.");
                }
            }
        }
        return list;
    }
}
