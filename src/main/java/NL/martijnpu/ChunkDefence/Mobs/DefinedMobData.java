package NL.martijnpu.ChunkDefence.Mobs;

import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;

public final class DefinedMobData {
    private final ConfigurationSection config;
    private final EntityType entityType;
    private final DefinedMobData passenger;

    public DefinedMobData(@Nullable ConfigurationSection config) {
        this.config = config;
        if (config == null)
            throw new NullPointerException("Passenger data is null");

        entityType = EntityType.valueOf(config.getString("type", "NoEntityTypeDefined"));
        if (!entityType.isAlive() || !entityType.isSpawnable()) {
            Messages.WARN.sendConsole(config.getName() + " is not an LivingEntity");
            throw new IllegalArgumentException(config.getName() + " is not an LivingEntity");
        }
        DefinedMobData data = null;
        if (config.isSet("passenger")) {
            try {
                data = new DefinedMobData(config.getConfigurationSection("passenger"));
            } catch (IllegalArgumentException | NullPointerException ex) {
                Messages.WARN.sendConsole("Unable to process " + config.getCurrentPath() + ".passenger! Mob has no passenger."
                        + "\nError: " + ex.getLocalizedMessage());
            }
        }
        passenger = data;
    }

    public int getCoins() {
        return config.getInt("coins", 10);
    }

    public int getWeight() {
        return config.getInt("weight", 1);
    }

    public int getAmount() {
        return config.getInt("amount", 1);
    }

    public int getSize() {
        return config.getInt("size", 3);
    }

    public int getExplosionRadius() {
        return config.getInt("explosion-radius", 3);
    }

    public boolean getPowered() {
        return config.getBoolean("powered", false);
    }

    public boolean getSilent() {
        return config.getBoolean("silent", false);
    }

    public boolean getGlowing() {
        return config.getBoolean("glowing", false);
    }

    public boolean getGliding() {
        return config.getBoolean("gliding", false);
    }

    public String getColor() {
        return config.getString("color", null);
    }

    public String getCustomName() {
        return config.getString("customname");
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public int getEntityHealth() {
        return config.getInt("health", -1);
    }

    public double getAdditionalDamage() {
        return config.getDouble("additional-damage", 0);
    }

    public boolean getEntityAdult() {
        return config.getBoolean("adult", true);
    }

    public ItemStack getEntityMainHand() {
        return config.getItemStack("equipment.mainhand");
    }

    public ItemStack getEntityOffHand() {
        return config.getItemStack("equipment.offhand");
    }

    public ItemStack getEntityHelmet() {
        return config.getItemStack("equipment.helmet");
    }

    public ItemStack getEntityChestPlate() {
        return config.getItemStack("equipment.chestplate");
    }

    public ItemStack getEntityLeggings() {
        return config.getItemStack("equipment.leggings");
    }

    public ItemStack getEntityBoots() {
        return config.getItemStack("equipment.boots");
    }

    public DefinedMobData getPassenger() {
        return passenger;
    }
}
