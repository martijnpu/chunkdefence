package NL.martijnpu.ChunkDefence.Gui.Types;

import NL.martijnpu.ChunkDefence.Gui.GuiData;
import NL.martijnpu.ChunkDefence.Gui.GuiInventory;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiUtils;
import NL.martijnpu.ChunkDefence.Traps.Effects.DamageType;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapData;
import NL.martijnpu.ChunkDefence.Utils.Keys;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static NL.martijnpu.ChunkDefence.Gui.Util.GuiUtils.openSign;

public final class GuiTrapEditDamage implements GuiData {
    public Component getTitle() {
        return Component.text("Edit Trap Damage properties");
    }

    public InventoryType getType() {
        return InventoryType.CHEST;
    }

    public int getSize() {
        return 9 * 4;
    }

    public Map<Integer, GuiSlot> renderSlots(Player player) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();
        TrapData trapData = (TrapData) player.getMetadata(Keys.GUI_TRAP_EDIT_KEY).get(0).value();
        if (trapData == null) {
            Messages.WARN.sendConsole("Unable to get GUI for this player");
            return itemSlots;
        }
        AtomicBoolean damageIndex = new AtomicBoolean(DamageType.PHYSICAL.equals(trapData.DAMAGE_TYPE.get()));

        itemSlots.put(0, new GuiSlot(Material.IRON_SWORD, "&6Damage type: &f" + trapData.DAMAGE_TYPE.get().name().toLowerCase())
                .setLore("&eThis is the mode which the trap uses to attack.", "It can either be damage or a potion effect")
                .setAnimated(1, 0).addAnimation(Material.IRON_SWORD).addAnimation(Material.DRAGON_BREATH));
        itemSlots.put(9, new GuiSlot(getTypeSelector(damageIndex.get()))
                .setLore("Click to toggle")
                .setLeftClick(() -> {
                    damageIndex.set(!damageIndex.get());
                    setTypeSelector(player, damageIndex.get());

                })
                .setRightClick(() -> {
                    damageIndex.set(!damageIndex.get());
                    setTypeSelector(player, damageIndex.get());
                }));

        switch (trapData.DAMAGE_TYPE.get()) {
            case EFFECT -> itemSlots.putAll(renderPotions(trapData, player));
            case PHYSICAL -> itemSlots.putAll(renderDamage(trapData, player));
        }

        itemSlots.put(35, new GuiSlot(Material.RED_CONCRETE, "&cBack")
                .setLeftClick(() -> GuiManager.getInstance().createInventory(player, GuiTrapEditMain.class).openGui()));

        itemSlots.putAll(GuiUtils.getLine(18, 26));

        return itemSlots;
    }


    /**
     * Regen the item in slot 9 as DamageType selector
     **/
    private void setTypeSelector(Player player, boolean isPhysical) {
        GuiInventory inventory = GuiManager.getInstance().getGui(player);
        if (inventory == null || inventory.getView() == null)
            return;

        inventory.getView().setItem(9, getTypeSelector(isPhysical));
        player.playSound(player.getEyeLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1, 1);
    }

    private ItemStack getTypeSelector(boolean isPhysical) {
        ItemStack itemStack = new ItemStack(isPhysical ? Material.IRON_SWORD : Material.DRAGON_BREATH);
        ItemMeta meta = itemStack.getItemMeta();
        List<Component> lore = new ArrayList<>();

        lore.add(Component.text(":::::::::::::::::::::::::::::::::::::::::::::::", NamedTextColor.GOLD, TextDecoration.STRIKETHROUGH));
        lore.add(Component.text("Select Damage Type:", NamedTextColor.GREEN));
        if (isPhysical) {
            lore.add(Component.text("->  Physical", NamedTextColor.DARK_PURPLE, TextDecoration.BOLD));
            lore.add(Component.text("Effect", NamedTextColor.AQUA));
        } else {
            lore.add(Component.text("Physical", NamedTextColor.AQUA));
            lore.add(Component.text("->  Effect", NamedTextColor.DARK_PURPLE, TextDecoration.BOLD));
        }

        meta.displayName(Component.text(isPhysical ? DamageType.PHYSICAL.name() : DamageType.EFFECT.name()));
        meta.lore(lore);
        itemStack.setItemMeta(meta);
        return itemStack;
    }

    private void toggleFireStatus(Player player) {
        int slot = 8;

        GuiInventory gui = GuiManager.getInstance().getGui(player);
        if (gui == null) {
            Messages.EXCEPT_ADMIN.send(player);
            Messages.WARN.sendConsole("Unable to get GUI for this player");
        } else {
            player.playSound(player.getEyeLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1, 1);
            if (gui.getType(slot).equals(Material.FIRE_CORAL_FAN))
                gui.changeItem(slot, new GuiSlot(Material.DEAD_FIRE_CORAL_FAN, "&6Set fire: &ffalse"));
            else if (gui.getType(slot).equals(Material.DEAD_FIRE_CORAL_FAN))
                gui.changeItem(slot, new GuiSlot(Material.FIRE_CORAL_FAN, "&6Set fire: &ftrue"));
            else
                throw new UnsupportedOperationException("Something went wrong while selecting setFire.");
        }
    }

    private Map<Integer, GuiSlot> renderDamage(TrapData trapData, Player player) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();

        if (Statics.isProtocolLibAvailable) {
            Sound sound = trapData.ATTACK_SOUND.get();
            itemSlots.put(2, new GuiSlot(Material.BELL, "&6Attack sound: &f" + sound)
                    .setLore("&eThis value is the time in between each attack.", "", "Click below to edit."));
            itemSlots.put(11, new GuiSlot(Material.PAPER, "&f" + sound)
                    .setLore("Value will be changed to:", "", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Attack sound", 11, Statics.toCapital(sound.name()), "sound")));

            double interval = trapData.INTERVAL.get();
            itemSlots.put(4, new GuiSlot(Material.CLOCK, "&6Interval: &f" + interval)
                    .setLore("&eThis value is the time in between each attack.", "", "Click below to edit."));
            itemSlots.put(13, new GuiSlot(Material.PAPER, "&f" + interval)
                    .setLore("Value will be changed to:", " ", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Interval", 13, interval + "", "double")));

            double damage = trapData.DAMAGE_AMOUNT.get();
            itemSlots.put(6, new GuiSlot(Material.REDSTONE, "&6Damage: &f" + damage)
                    .setLore("&eThis value is the amount of damage the trap will deal", "&eeach time it attacks. it is counted in half harts.", "", "Click below to edit."));
            itemSlots.put(15, new GuiSlot(Material.PAPER, "&f" + damage)
                    .setLore("Value will be changed to:", "", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Damage", 15, damage + "", "int")));
        }

        itemSlots.put(8, new GuiSlot(trapData.SET_FIRE.get() ? Material.FIRE_CORAL_FAN : Material.DEAD_FIRE_CORAL_FAN, "&6Set fire: &f" + trapData.SET_FIRE.get())
                .setLore("&eWhether this trap will set fire to the attacked mobs.", "", "Click to toggle.")
                .setLeftClick(() -> toggleFireStatus(player)));

        itemSlots.put(27, new GuiSlot(Material.GREEN_CONCRETE, "&aSave changes")
                .setLeftClick(() -> saveTrapDamage(trapData, player)));

        return itemSlots;
    }

    private void saveTrapDamage(TrapData trapData, Player player) {
        GuiInventory inventory = GuiManager.getInstance().getGui(player);
        if (inventory == null || inventory.getView() == null) {
            Messages.WARN.sendConsole("Unable to get GUI for this player");
            return;
        }

        try {
            if (Statics.isProtocolLibAvailable) {
                trapData.ATTACK_SOUND.set(Sound.valueOf(GuiUtils.getStrippedDisplayName(inventory, 11)));
                trapData.INTERVAL.set(Double.valueOf(GuiUtils.getStrippedDisplayName(inventory, 13)));
                trapData.DAMAGE_AMOUNT.set(Double.valueOf(GuiUtils.getStrippedDisplayName(inventory, 15)));
                trapData.DAMAGE_TYPE.set(DamageType.PHYSICAL);
            }
            trapData.SET_FIRE.set(inventory.getType(8).equals(Material.FIRE_CORAL_FAN));
        } catch (NullPointerException | NumberFormatException ex) {
            Messages.EXCEPT_ADMIN.send(player);
            Messages.WARN.sendConsole("An error occurred in a gui: " + ex.getLocalizedMessage());
        }

        GuiManager.getInstance().createInventory(player, GuiTrapEditMain.class).openGui();
        player.playSound(player.getEyeLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
    }

    private Map<Integer, GuiSlot> renderPotions(TrapData trapData, Player player) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();

        itemSlots.put(8, new GuiSlot(trapData.SET_FIRE.get() ? Material.FIRE_CORAL_FAN : Material.DEAD_FIRE_CORAL_FAN, "&6Set fire: &f" + trapData.SET_FIRE.get())
                .setLore("&eWhether this trap will set fire to the attacked mobs.", "", "Click to toggle.")
                .setLeftClick(() -> toggleFireStatus(player)));

        if (Statics.isProtocolLibAvailable) {
            Sound sound = trapData.ATTACK_SOUND.get();
            itemSlots.put(2, new GuiSlot(Material.BELL, "&6Attack sound: &f" + sound)
                    .setLore("&eThis value is the time in between each attack.", "", "Click below to edit."));
            itemSlots.put(11, new GuiSlot(Material.PAPER, "&f" + sound)
                    .setLore("Value will be changed to:", "", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Attack sound", 11, Statics.toCapital(sound.name()), "sound")));

            double interval = trapData.INTERVAL.get();
            itemSlots.put(4, new GuiSlot(Material.CLOCK, "&6Interval: &f" + interval)
                    .setLore("&eThis value is the time in between each attack.", "", "Click below to edit."));
            itemSlots.put(13, new GuiSlot(Material.PAPER, "&f" + interval)
                    .setLore("Value will be changed to:", "", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Interval", 13, interval + "", "double")));

            String effect = trapData.POTION_EFFECT.get();
            itemSlots.put(5, new GuiSlot(Material.POTION, "&6Potion effect: &f" + Statics.toCapital(effect))
                    .setLore("&eThis value is the effect type of this trap.", "", "Click below to edit."));
            itemSlots.put(14, new GuiSlot(Material.PAPER, "&f" + Statics.toCapital(effect))
                    .setLore("Value will be changed to:", "", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Potion effect", 14, Statics.toCapital(effect), "potion")));

            int amplifier = trapData.POTION_AMPLIFIER.get();
            itemSlots.put(6, new GuiSlot(Material.BREWING_STAND, "&6Potion amplifier: &f" + amplifier)
                    .setLore("&eThis value is the amplifier the effect will have.", "", "Click below to edit."));
            itemSlots.put(15, new GuiSlot(Material.PAPER, "&f" + amplifier)
                    .setLore("Value will be changed to:", "", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Potion amplifier", 15, amplifier + "", "int")));

            double time = trapData.POTION_TIME.get();
            itemSlots.put(7, new GuiSlot(Material.CLOCK, "&6Potion time: &f" + time)
                    .setLore("&eThis value is the time the effect will last.", "", "Click below to edit."));
            itemSlots.put(16, new GuiSlot(Material.PAPER, "&f" + time)
                    .setLore("Value will be changed to:", "", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Potion time", 16, time + "", "double")));
        }

        itemSlots.put(27, new GuiSlot(Material.GREEN_CONCRETE, "&aSave changes")
                .setLeftClick(() -> saveTrapPotions(trapData, player)));

        return itemSlots;
    }

    private void saveTrapPotions(TrapData trapData, Player player) {
        GuiInventory inventory = GuiManager.getInstance().getGui(player);
        if (inventory == null || inventory.getView() == null) {
            Messages.WARN.sendConsole("Unable to get GUI for this player");
            return;
        }

        try {
            trapData.ATTACK_SOUND.set(Sound.valueOf(GuiUtils.getStrippedDisplayName(inventory, 11)));
            trapData.INTERVAL.set(Double.valueOf(GuiUtils.getStrippedDisplayName(inventory, 13)));
            trapData.SET_FIRE.set(inventory.getType(8).equals(Material.FIRE_CORAL_FAN));
            trapData.POTION_EFFECT.set(GuiUtils.getStrippedDisplayName(inventory, 15));
            trapData.POTION_AMPLIFIER.set(Integer.valueOf(GuiUtils.getStrippedDisplayName(inventory, 15)));
            trapData.POTION_TIME.set(Double.valueOf(GuiUtils.getStrippedDisplayName(inventory, 16)));
            trapData.DAMAGE_TYPE.set(DamageType.EFFECT);
        } catch (NullPointerException | NumberFormatException ex) {
            Messages.EXCEPT_ADMIN.send(player);
            Messages.WARN.sendConsole("An error occurred in a gui: " + ex.getLocalizedMessage());
        }

        GuiManager.getInstance().createInventory(player, GuiTrapEditMain.class).openGui();
        player.playSound(player.getEyeLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
    }
}