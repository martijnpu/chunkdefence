package NL.martijnpu.ChunkDefence.Gui.Types;

import NL.martijnpu.ChunkDefence.Arenas.Schematics.SchematicManager;
import NL.martijnpu.ChunkDefence.Gui.GuiData;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopArena;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopItem;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopTrap;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapDataManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import java.util.HashMap;
import java.util.Map;

public final class GuiShopAdd implements GuiData {
    public Component getTitle() {
        return Messages.GUI_SHOP_NAME.getMiniMessageFormat();
    }

    public InventoryType getType() {
        return InventoryType.HOPPER;
    }

    public Map<Integer, GuiSlot> renderSlots(Player player) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();
        Block location = ShopManager.lookingAt(player);

        itemSlots.put(0, new GuiSlot(Material.IRON_AXE, "&aItem shop")
                .setLeftClick(() -> {
                    new ShopItem(location, player.getItemOnCursor());
                    GuiManager.getInstance().createInventory(player, GuiShopEditItem.class)
                            .changeItem(35, new GuiSlot(Material.AIR)) //Remove cancel button
                            .openGui();
                }));

        itemSlots.put(2, new GuiSlot(Material.BLAZE_POWDER, "&9Trap shop")
                .setLeftClick(() -> {
                    try {
                        new ShopTrap(location, TrapDataManager.getInstance().getAllTrapBases().get(0));
                        GuiManager.getInstance().createInventory(player, GuiShopEditTrap.class)
                                .changeItem(35, new GuiSlot(Material.AIR)) //Remove cancel button
                                .openGui();
                    } catch (IllegalAccessException ex) {
                        Messages.PLUGIN.send(player, "Unable to place trap due to invalid underground or surrounding blocks. Check the console for specifications.");
                    }
                }));

        itemSlots.put(4, new GuiSlot(Material.GRASS_BLOCK, "&dArena shop")
                .setLeftClick(() -> {
                    new ShopArena(location, SchematicManager.getInstance().getAllSchematics().get(0));
                    GuiManager.getInstance().createInventory(player, GuiShopEditArena.class)
                            .changeItem(35, new GuiSlot(Material.AIR)) //Remove cancel button
                            .openGui();
                }));

        return itemSlots;
    }
}
