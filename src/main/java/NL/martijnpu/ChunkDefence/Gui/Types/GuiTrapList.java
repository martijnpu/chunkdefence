package NL.martijnpu.ChunkDefence.Gui.Types;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Gui.GuiData;
import NL.martijnpu.ChunkDefence.Gui.GuiInventory;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapData;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapDataManager;
import NL.martijnpu.ChunkDefence.Utils.Keys;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public final class GuiTrapList implements GuiData {
    private final int TRAP_SIZE = 18;

    public Component getTitle() {
        return Component.text("Traps");
    }

    public InventoryType getType() {
        return InventoryType.CHEST;
    }

    public int getSize() {
        return 9 * 4;
    }

    public Map<Integer, GuiSlot> renderSlots(Player player) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();
        AtomicInteger index = new AtomicInteger(0);

        for (int i = TRAP_SIZE; i <= 26; i++)
            itemSlots.put(i, new GuiSlot(Material.BLACK_STAINED_GLASS_PANE, " ")
                    .setLore("Left click to get trap", "Left shift click to get a stack", "Right click to edit.")
                    .setAnimated(0.1, i)
                    .addAnimation(Material.BLACK_STAINED_GLASS_PANE, 20)
                    .addAnimation(Material.WHITE_STAINED_GLASS_PANE)
                    .addAnimation(Material.LIGHT_GRAY_STAINED_GLASS_PANE, 2)
                    .addAnimation(Material.GRAY_STAINED_GLASS_PANE, 3));

        itemSlots.put(31, new GuiSlot(Material.STRUCTURE_VOID, "&2Add new trap.")
                .setLeftClick(() -> {
                    player.setMetadata(Keys.GUI_TRAP_EDIT_KEY, new FixedMetadataValue(ChunkDefence.get(), TrapDataManager.getInstance().createNewTrapBase()));
                    GuiInventory inventory = GuiManager.getInstance().createInventory(player, GuiTrapEditMain.class);
                    inventory.changeItem(35, null); //remove cancel button
                    inventory.openGui();
                }));
        itemSlots.put(35, new GuiSlot(Material.RED_CONCRETE, Messages.GUI_MAIN_EXIT.get())
                .setLeftClick(() -> GuiManager.getInstance().closeInventory(player)));

        itemSlots.putAll(getTrapPage(player, index));

        return itemSlots;
    }

    private Map<Integer, GuiSlot> getTrapPage(Player player, AtomicInteger page) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();

        for (int slotIndex = 0; slotIndex < TRAP_SIZE; slotIndex++) {
            int trapIndex = slotIndex + (TRAP_SIZE * page.get());
            if (trapIndex >= TrapDataManager.getInstance().getAllTrapBases().size())
                itemSlots.put(slotIndex, null);
            else {
                TrapData trapData = TrapDataManager.getInstance().getAllTrapBases().get(trapIndex);
                if (trapData != null)
                    itemSlots.put(slotIndex, new GuiSlot(trapData.generateTrapItem(1))
                            .setName(trapData.NAME.get())
                            .setLeftClick(() -> GuiManager.getInstance().getGui(player).getView().setCursor(trapData.generateTrapItem(1)))
                            .setLeftShiftClick(() -> GuiManager.getInstance().getGui(player).getView().setCursor(trapData.generateTrapItem(64)))
                            .setRightClick(() -> {
                                player.setMetadata(Keys.GUI_TRAP_EDIT_KEY, new FixedMetadataValue(ChunkDefence.get(), trapData));
                                GuiManager.getInstance().createInventory(player, GuiTrapEditMain.class).openGui();
                            }));
                else
                    itemSlots.put(slotIndex, new GuiSlot(Material.BARRIER, "Unknown trap"));
            }
        }

        if (page.get() < TrapDataManager.getInstance().getAllTrapBases().size() / TRAP_SIZE) {
            itemSlots.put(33, new GuiSlot(Material.PAPER, "&6Next page")
                    .setLeftClick(() -> {
                        page.getAndIncrement();
                        GuiInventory inventory = GuiManager.getInstance().getGui(player);
                        getTrapPage(player, page).forEach(inventory::changeItem);
                    }));
        }

        if (page.get() > 0) {
            itemSlots.put(29, new GuiSlot(Material.PAPER, "&6Previous page")
                    .setLeftClick(() -> {
                        page.getAndDecrement();
                        GuiInventory inventory = GuiManager.getInstance().getGui(player);
                        getTrapPage(player, page).forEach(inventory::changeItem);
                    }));
        }

        return itemSlots;
    }
}