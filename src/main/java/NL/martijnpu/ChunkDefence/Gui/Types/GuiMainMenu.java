package NL.martijnpu.ChunkDefence.Gui.Types;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommands.ArenaDeleteCmd;
import NL.martijnpu.ChunkDefence.Commands.SubCommands.ArenaLeaveCmd;
import NL.martijnpu.ChunkDefence.Commands.SubCommands.ArenaWaveInfoCmd;
import NL.martijnpu.ChunkDefence.Commands.SubCommands.ShopTeleportCmd;
import NL.martijnpu.ChunkDefence.Gui.Book.Books;
import NL.martijnpu.ChunkDefence.Gui.GuiData;
import NL.martijnpu.ChunkDefence.Gui.GuiInventory;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiUtils;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import java.util.HashMap;
import java.util.Map;

public final class GuiMainMenu implements GuiData {
    public Component getTitle() {
        return Messages.GUI_MAIN_NAME.getMiniMessageFormat();
    }

    public InventoryType getType() {
        return InventoryType.CHEST;
    }

    public int getSize() {
        return 36;
    }

    public Map<Integer, GuiSlot> renderSlots(Player player) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();
        Arena arena = ArenaManager.getInstance().getArena(player);

        itemSlots.putAll(GuiUtils.getLine(0, 8));
        itemSlots.putAll(GuiUtils.getLine(27, 34));

        int highscoreSlot;
        int helpSlot;

        if (arena == null) {
            highscoreSlot = 10;

            itemSlots.put(13, new GuiSlot(Material.CRAFTING_TABLE, Messages.GUI_MAIN_CREATE.get())
                    .setLeftClick(() -> GuiManager.getInstance().createInventory(player, GuiGameModes.class).openGui()));

            helpSlot = 16;
        } else {
            itemSlots.put(9, new GuiSlot(Material.PAINTING, "&5" + arena.getArenaData().SCHEMATIC.get())
                    .setLore("&e" + Messages.WAVE_CURRENT.get("&f" + arena.getArenaData().WAVE.get()),
                            "&e" + Messages.WAVE_MEMBERS.get("&f" + arena.getArenaMembers())));

            highscoreSlot = 11;

            itemSlots.put(13, new GuiSlot(Material.SPECTRAL_ARROW, Messages.GUI_MAIN_SHOP.get())
                    .setLeftClick(() -> ShopTeleportCmd.teleport(player)));

            itemSlots.put(15, new GuiSlot(Material.ENDER_PEARL, Messages.GUI_MAIN_ARENA.get())
                    .setLeftClick(() -> arena.teleportHome(player, true)));

            itemSlots.put(17, new GuiSlot(Material.FEATHER, Messages.GUI_MAIN_START.get())
                    .setLeftClick(() -> arena.getWaveController().start()));

            helpSlot = 19;

            itemSlots.put(21, new GuiSlot(Material.BOOK, Messages.GUI_MAIN_WAVE_INFO.get())
                    .setLeftClick(() -> {
                        GuiManager.getInstance().closeInventory(player);
                        ArenaWaveInfoCmd.onCommand(player);
                    }));

            if (arena.getArenaData().getMembers().size() > 1)
                itemSlots.put(23, new GuiSlot(Material.DARK_OAK_DOOR, Messages.GUI_MAIN_LEAVE.get())
                        .setLeftClick(() -> {
                            GuiManager.getInstance().closeInventory(player);
                            arena.playerRemove(player, true);
                        }));
            else
                itemSlots.put(23, new GuiSlot(Material.DARK_OAK_DOOR, Messages.GUI_MAIN_SAVE.get())
                        .setLeftClick(() -> {
                            GuiManager.getInstance().closeInventory(player);
                            ArenaLeaveCmd.arenaLeaveConfirm(player);
                        }));

            itemSlots.put(25, new GuiSlot(Material.BARRIER, Messages.GUI_MAIN_DELETE.get())
                    .setLeftClick(() -> {
                        GuiManager.getInstance().closeInventory(player);
                        ArenaDeleteCmd.arenaDeleteConfirm(player);
                    }));
        }


        itemSlots.put(highscoreSlot, new GuiSlot(Material.SMOOTH_QUARTZ_STAIRS, Messages.GUI_MAIN_TOP.get())
                .setLeftClick(() -> {
                    GuiInventory inventory = GuiManager.getInstance().createInventory(player, GuiTop.class);
                    inventory.changeItem(53, new GuiSlot(Material.OAK_FENCE_GATE, Messages.GUI_MAIN_BACK.get())
                            .setLeftClick(() -> GuiManager.getInstance().createInventory(player, getClass()).openGui())).openGui();
                }));

        itemSlots.put(helpSlot, new GuiSlot(Material.REDSTONE_TORCH, Messages.GUI_MAIN_HELP.get())
                .setLeftClick(() -> {
                    GuiManager.getInstance().closeInventory(player);
                    Books.openBook(player, "welcome");
                }));

        itemSlots.put(35, new GuiSlot(Material.OAK_FENCE_GATE, Messages.GUI_MAIN_BACK.get())
                .setLeftClick(() -> GuiManager.getInstance().closeInventory(player)));


        return itemSlots;
    }
}
