package NL.martijnpu.ChunkDefence.Gui.Types;

import NL.martijnpu.ChunkDefence.Gui.GuiData;
import NL.martijnpu.ChunkDefence.Gui.GuiInventory;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiUtils;
import NL.martijnpu.ChunkDefence.Traps.Effects.AttackPattern;
import NL.martijnpu.ChunkDefence.Traps.Effects.ParticlePattern;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapData;
import NL.martijnpu.ChunkDefence.Utils.Keys;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static NL.martijnpu.ChunkDefence.Gui.Util.GuiUtils.openSign;

public final class GuiTrapEditParticle implements GuiData {
    public Component getTitle() {
        return Component.text("Edit Trap Particle properties");
    }

    public InventoryType getType() {
        return InventoryType.CHEST;
    }

    public int getSize() {
        return 9 * 4;
    }

    public Map<Integer, GuiSlot> renderSlots(Player player) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();
        TrapData trapData = (TrapData) player.getMetadata(Keys.GUI_TRAP_EDIT_KEY).get(0).value();
        if (trapData == null) {
            Messages.WARN.sendConsole("Unable to get GUI for this player");
            return itemSlots;
        }

        if (Statics.isProtocolLibAvailable) {
            Particle particle = trapData.PARTICLE_TYPE.get();
            itemSlots.put(0, new GuiSlot(Material.BLAZE_POWDER, "&6Particle type: &f" + particle.name().toLowerCase())
                    .setLore("&eThis is the type of particles around the trap.", "", "Click below to edit."));
            itemSlots.put(9, new GuiSlot(Material.PAPER, "&f" + particle.name().toLowerCase())
                    .setLore("Value will be changed to:", "", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&cParticle", 9, Statics.toCapital(particle.name()), "particle")));

            double speed = trapData.PARTICLE_SPEED.get();
            itemSlots.put(1, new GuiSlot(Material.CLOCK, "&6Speed: &f" + speed)
                    .setLore("&eThis value is the speed of each individual particle.", "", "Click below to edit."));
            itemSlots.put(10, new GuiSlot(Material.PAPER, "&f" + speed)
                    .setLore("Value will be changed to:", " ", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Particle speed", 10, speed + "", "double")));

            int amount = trapData.PARTICLE_AMOUNT.get();
            itemSlots.put(2, new GuiSlot(Material.WHITE_DYE, "&6Amount: &f" + amount)
                    .setLore("&eThis value is the amount of particles each game tick.", " ", "Click below to edit."));
            itemSlots.put(11, new GuiSlot(Material.PAPER, "&f" + amount)
                    .setLore("Value will be changed to:", " ", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Particle amount", 11, String.valueOf(amount), "int")));

            List<String> loreMovement = new ArrayList<>();
            loreMovement.add("&eThis value is the way particles will move.");
            loreMovement.add(" ");
            loreMovement.add("Values:");
            for (ParticlePattern particleOnAttack : ParticlePattern.values())
                loreMovement.add("&e- " + Statics.toCapital(particleOnAttack.toString()));
            loreMovement.add(" ");
            loreMovement.add("Click below to edit.");

            ParticlePattern movementMode = trapData.PARTICLE_PATTERN.get(); //Todo make selectable
            itemSlots.put(4, new GuiSlot(Material.COCOA_BEANS, "&6Movement mode: &f" + movementMode)
                    .setLore(loreMovement)
                    .setAnimated(1, 0)
                    .addAnimation(Material.COCOA_BEANS)
                    .addAnimation(Material.MELON_SEEDS));
            itemSlots.put(13, new GuiSlot(Material.PAPER, "&f" + movementMode)
                    .setLore("Value will be changed to:", " ", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Movement mode", 13, Statics.toCapital(movementMode.name()), "particleMovement")));

            List<String> loreMovement2 = new ArrayList<>();
            loreMovement.add("&eThis value is the way particles will move when attacking.");
            loreMovement2.add(" ");
            loreMovement2.add("Values:");
            for (AttackPattern value : AttackPattern.values())
                loreMovement2.add("&e- " + Statics.toCapital(value.toString()));
            loreMovement2.add(" ");
            loreMovement2.add("Click below to edit.");

            AttackPattern attack = trapData.ATTACK_PATTERN.get(); //Todo make selectable
            itemSlots.put(5, new GuiSlot(Material.IRON_SWORD, "&6Attack particle: &f" + attack)
                    .setLore(loreMovement2)
                    .setAnimated(1, 0)
                    .addAnimation(Material.IRON_SWORD, 1)
                    .addAnimation(Material.IRON_AXE)
                    .addAnimation(Material.IRON_HOE)
                    .addAnimation(Material.IRON_SHOVEL)
                    .addAnimation(Material.IRON_PICKAXE));
            itemSlots.put(14, new GuiSlot(Material.PAPER, "&f" + attack)
                    .setLore("Value will be changed to:", " ", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Attack particle", 14, Statics.toCapital(attack.name()), "particleAttack")));

            double size = trapData.PARTICLE_SIZE.get();
            itemSlots.put(6, new GuiSlot(Material.BOWL, "&6Movement size: &f" + size)
                    .setLore("&eThis value is The size of the movement the particles make.", " ", "Click below to edit."));
            itemSlots.put(15, new GuiSlot(Material.PAPER, "&f" + size)
                    .setLore("Value will be changed to:", " ", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Movement size", 15, size + "", "double")));

            double modeSpeed = trapData.PARTICLE_PATTERN_SPEED.get();
            itemSlots.put(7, new GuiSlot(Material.FEATHER, "&6Movement speed: &f" + modeSpeed)
                    .setLore("&eThis value is The speed of the movement the particles make.", " ", "Click below to edit."));
            itemSlots.put(16, new GuiSlot(Material.PAPER, "&f" + modeSpeed)
                    .setLore("Value will be changed to:", " ", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Movement speed", 16, modeSpeed + "", "double")));
        }

        itemSlots.put(27, new GuiSlot(Material.GREEN_CONCRETE, "&aSave changes")
                .setLeftClick(() -> saveTrapParticle(trapData, player)));
        itemSlots.put(35, new GuiSlot(Material.RED_CONCRETE, "&cBack")
                .setLeftClick(() -> GuiManager.getInstance().createInventory(player, GuiTrapEditMain.class).openGui()));

        itemSlots.putAll(GuiUtils.getLine(18, 26));

        return itemSlots;
    }

    private void saveTrapParticle(TrapData trapData, Player player) {
        GuiInventory inventory = GuiManager.getInstance().getGui(player);
        if (inventory == null || inventory.getView() == null) {
            Messages.WARN.sendConsole("Unable to get GUI for this player");
            return;
        }

        try {
            if (Statics.isProtocolLibAvailable) {
                trapData.PARTICLE_TYPE.set(Particle.valueOf(GuiUtils.getStrippedDisplayName(inventory, 9)));
                trapData.PARTICLE_SPEED.set(Double.valueOf(GuiUtils.getStrippedDisplayName(inventory, 10)));
                trapData.PARTICLE_AMOUNT.set(Integer.valueOf(GuiUtils.getStrippedDisplayName(inventory, 11)));
                trapData.ATTACK_PATTERN.set(AttackPattern.valueOf(GuiUtils.getStrippedDisplayName(inventory, 14)));
                trapData.PARTICLE_SIZE.set(Double.valueOf(GuiUtils.getStrippedDisplayName(inventory, 15)));
                trapData.PARTICLE_PATTERN.set(ParticlePattern.valueOf(GuiUtils.getStrippedDisplayName(inventory, 13)));
                trapData.PARTICLE_PATTERN_SPEED.set(Double.valueOf(GuiUtils.getStrippedDisplayName(inventory, 16)));
            }
        } catch (NullPointerException | NumberFormatException ex) {
            Messages.EXCEPT_ADMIN.send(player);
            Messages.WARN.sendConsole("An error occurred in a gui: " + ex.getLocalizedMessage());

        }

        GuiManager.getInstance().createInventory(player, GuiTrapEditMain.class).openGui();
        player.playSound(player.getEyeLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
    }
}