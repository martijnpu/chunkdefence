package NL.martijnpu.ChunkDefence.Gui.Types;

import NL.martijnpu.ChunkDefence.Gui.GuiData;
import NL.martijnpu.ChunkDefence.Gui.GuiInventory;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiUtils;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopTrap;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapData;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapDataManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public final class GuiShopEditTrap implements GuiData {
    public Component getTitle() {
        return Messages.GUI_SHOP_EDIT.getMiniMessageFormat();
    }

    public InventoryType getType() {
        return InventoryType.CHEST;
    }

    public int getSize() {
        return 9 * 4;
    }

    public Map<Integer, GuiSlot> renderSlots(Player player) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();
        ShopTrap shop = (ShopTrap) ShopManager.getInstance().getShop(ShopManager.lookingAt(player));
        AtomicInteger arenaIndex = new AtomicInteger(shop.getTrapData() == null ? -1 : TrapDataManager.getInstance().getAllTrapBases().indexOf(shop.getTrapData()));

        itemSlots.put(1, new GuiSlot(Material.IRON_AXE, "&6Selling trap: " + shop.getTrapData().NAME.get())
                .setLore("Click the item below with to cycle trough the available traps."));
        itemSlots.put(10, new GuiSlot(getTrapSelector(arenaIndex.get()))
                .setLeftClick(() -> {
                    arenaIndex.getAndIncrement();
                    if (arenaIndex.get() >= TrapDataManager.getInstance().getAllTrapBases().size())
                        arenaIndex.set(0);
                    setTrapSelector(player, arenaIndex);
                })
                .setRightClick(() -> {
                    arenaIndex.getAndDecrement();
                    if (arenaIndex.get() < 0)
                        arenaIndex.set(TrapDataManager.getInstance().getAllTrapBases().size() - 1);
                    setTrapSelector(player, arenaIndex);
                }));

        if (Statics.isProtocolLibAvailable) {
            String name = shop.getName();
            itemSlots.put(3, new GuiSlot(Material.NAME_TAG, "&6Current display name: &f" + name)
                    .setLore("Click below to edit the display name.")
                    .setLeftClick(() -> GuiUtils.openSign(player, "&6Display name", 12, name, "")));
            itemSlots.put(12, new GuiSlot(Material.PAPER, name)
                    .setLore("Click to edit the new display name")
                    .setLeftClick(() -> GuiUtils.openSign(player, "&6Display name", 12, name, "")));

            String cost = String.valueOf(shop.getCost());
            itemSlots.put(5, new GuiSlot(Material.SUNFLOWER, "&6Current price: &f" + cost)
                    .setLore("Click below to edit the price of this shop.")
                    .setLeftClick(() -> GuiUtils.openSign(player, "&6Price", 14, cost, "double")));
            itemSlots.put(14, new GuiSlot(Material.PAPER, cost)
                    .setLore("Click to edit the new price")
                    .setLeftClick(() -> GuiUtils.openSign(player, "&6Price", 14, cost, "double")));
        }

        itemSlots.putAll(GuiUtils.getLine(18, 26));

        itemSlots.put(27, new GuiSlot(Material.GREEN_CONCRETE, "&aSave changes")
                .setLeftClick(() -> saveTrapArena(shop, player)));
        itemSlots.put(31, new GuiSlot(Material.BARRIER, "&cRemove shop")
                .setLeftClick(() -> {
                    shop.remove();
                    GuiManager.getInstance().closeInventory(player);
                }));
        itemSlots.put(35, new GuiSlot(Material.RED_CONCRETE, "&cCancel")
                .setLeftClick(() -> GuiManager.getInstance().closeInventory(player)));

        return itemSlots;
    }

    private void saveTrapArena(ShopTrap shop, Player player) {
        GuiInventory inventory = GuiManager.getInstance().getGui(player);
        if (inventory == null || inventory.getView() == null) {
            Messages.SHOP_INVALID.send(player);
            return;
        }

        if (inventory.getView().getItem(10) == null) {
            Messages.PLUGIN.send(player, "&cShop must sell an item.");
            return;
        }

        if (Statics.isProtocolLibAvailable) {
            String name = inventory.getDisplayName(12);
            String cost = inventory.getDisplayName(14);
            try {
                if (!name.isBlank())
                    shop.setName(name);

                if (!cost.isBlank())
                    shop.setCost(Double.parseDouble(cost));
            } catch (NumberFormatException e) {
                Messages.PLUGIN.send(player, "&c'&f" + cost + "&c' is not a valid price.");
            }
        }

        //set Trap AFTER name
        TrapData trap = TrapDataManager.getInstance().getTrapBase(inventory.getView().getItem(10).getItemMeta().getDisplayName());
        if (trap != null)
            shop.setTrapData(trap);

        GuiManager.getInstance().closeInventory(player);

//        ShopManager.resetInstance();
//        ShopManager.getInstance().printInitData();
        player.playSound(player.getEyeLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
    }

    /**
     * Regen the item in slot 10 as trap selector
     **/
    private void setTrapSelector(Player player, AtomicInteger arenaIndex) {
        GuiInventory inventory = GuiManager.getInstance().getGui(player);
        if (inventory == null || inventory.getView() == null)
            return;

        inventory.getView().setItem(10, getTrapSelector(arenaIndex.get()));
        player.playSound(player.getEyeLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1, 1);
    }

    private ItemStack getTrapSelector(int index) {
        ItemStack itemStack = new ItemStack(Material.PAINTING);
        TrapData selectedTrap = index == -1 ? null : TrapDataManager.getInstance().getAllTrapBases().get(index);
        ItemMeta meta = itemStack.getItemMeta();
        List<Component> lore = new ArrayList<>();

        lore.add(Component.text(":::::::::::::::::::::::::::::::::::::::::::::::", NamedTextColor.GOLD, TextDecoration.STRIKETHROUGH));
        lore.add(Component.text("Select trap:", NamedTextColor.GREEN));
        for (TrapData trap : TrapDataManager.getInstance().getAllTrapBases()) {
            if (index != -1 && trap.equals(selectedTrap))
                lore.add(Component.text("->  ", NamedTextColor.DARK_PURPLE, TextDecoration.BOLD).append(Messages.formatLegacy(trap.NAME.get())));
            else
                lore.add(Messages.formatLegacy(trap.NAME.get()));
        }

        if (selectedTrap != null)
            meta.displayName(Component.text(selectedTrap.key));
        meta.lore(lore);
        itemStack.setItemMeta(meta);
        return itemStack;
    }
}
