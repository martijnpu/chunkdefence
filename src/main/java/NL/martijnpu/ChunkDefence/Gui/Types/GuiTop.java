package NL.martijnpu.ChunkDefence.Gui.Types;

import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Data.MessageData;
import NL.martijnpu.ChunkDefence.Gui.GuiData;
import NL.martijnpu.ChunkDefence.Gui.GuiInventory;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiUtils;
import NL.martijnpu.ChunkDefence.Scores.ScoreManager;
import NL.martijnpu.ChunkDefence.Scores.ScoreTypes.ScoreValue;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public final class GuiTop implements GuiData {
    public Component getTitle() {
        return Messages.GUI_TOP_ARENA_NAME.getMiniMessageFormat();
    }

    public InventoryType getType() {
        return InventoryType.CHEST;
    }

    public int getSize() {
        return 9 * 6;
    }

    public Map<Integer, GuiSlot> renderSlots(Player player) { //Todo Improve Top
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();
        AtomicInteger index = new AtomicInteger();
/*
        //Random items
        final List<Material> itemList = Arrays.asList(Material.EMERALD, Material.DIAMOND, Material.EMERALD, Material.DIAMOND, Material.GOLD_INGOT, Material.GOLD_INGOT, Material.GOLD_INGOT, Material.GOLD_INGOT, Material.GOLD_INGOT, Material.GOLD_INGOT, Material.GOLD_NUGGET, Material.GOLD_NUGGET, Material.GOLD_NUGGET, Material.IRON_INGOT, Material.QUARTZ, Material.NETHER_STAR, Material.HONEYCOMB, Material.POTATO);
        Random rand = new Random();

        for (int i = 0; i <= 35; i++) {
            if (i == 13 || i == 21 || i == 23 || i == 29 || i == 31 || i == 33)
                continue;
            if (rand.nextInt(4) == 0)
                itemSlots.put(i, new GuiSlot()
                        .setAnimated(rand.nextInt(1) + rand.nextDouble(), 0)
                        .addAnimation(itemList.get(rand.nextInt(itemList.size())), rand.nextInt(3) + 1)
                        .addAnimation(itemList.get(rand.nextInt(itemList.size())), rand.nextInt(3) + 1)
                        .addAnimation(itemList.get(rand.nextInt(itemList.size())), rand.nextInt(3) + 1));
        }*/

        //Divider
        itemSlots.putAll(GuiUtils.getLine(36, 44));
        itemSlots.put(40, new GuiSlot(Material.BARRIER, "Select below what you want to view"));

        //Bottom bar
        itemSlots.put(46, new GuiSlot(Material.IRON_SWORD, "&6Click to view:")
                .addLore(Messages.GUI_TOP_KILL_NAME.get())
                .setLeftClick(() -> {
                    changeTop(player, "KILLS");

                    GuiInventory gui = GuiManager.getInstance().getGui(player);
                    if (gui != null)
                        gui.changeItem(40, new GuiSlot(Material.IRON_SWORD, "&5Now viewing:")
                                .setLore(Messages.GUI_TOP_KILL_NAME.get())
                                .setShiny());
                }));

        itemSlots.put(49, new GuiSlot(Material.LECTERN, "&6Click to view highest waves per gamemode")
                .setLore(ConfigData.GAMEMODES.get())
                .setLeftClick(() -> editTopIndex(player, index, true))
                .setRightClick(() -> editTopIndex(player, index, false)));

        itemSlots.put(53, new GuiSlot(Material.BARRIER, Messages.GUI_MAIN_EXIT.get())
                .setLeftClick(() -> GuiManager.getInstance().closeInventory(player)));

        //Create pyramid
        //changeTop(player, "KILLS"); //Doesn't work due to inventory being constructed atm
        return itemSlots;
    }

    private void changeTop(Player player, String change) {
        GuiInventory gui = GuiManager.getInstance().getGui(player);
        if (gui == null)
            return;
        player.playSound(player.getEyeLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1, 1);

        gui.changeItem(4, getTopItem(1, change));
        gui.changeItem(12, getTopItem(2, change));
        gui.changeItem(14, getTopItem(3, change));
        gui.changeItem(20, getTopItem(4, change));
        gui.changeItem(22, getTopItem(5, change));
        gui.changeItem(24, getTopItem(6, change));
        gui.changeItem(28, getTopItem(7, change));
        gui.changeItem(30, getTopItem(8, change));
        gui.changeItem(32, getTopItem(9, change));
        gui.changeItem(34, getTopItem(10, change));
    }

    /**
     * Regen the item in slot 10 as top selector
     **/
    private void editTopIndex(Player player, AtomicInteger index, boolean add) {
        GuiInventory gui = GuiManager.getInstance().getGui(player);
        if (gui == null)
            return;

        index.getAndAdd(add ? 1 : -1);
        if (index.get() >= ConfigData.GAMEMODES.get().size())
            index.set(0);
        if (index.get() < 0)
            index.set(ConfigData.GAMEMODES.get().size() - 1);


        ItemStack itemStack = new ItemStack(Material.LECTERN);
        ItemMeta meta = itemStack.getItemMeta();
        List<Component> lore = new ArrayList<>();
        lore.add(Component.text("Click to select"));
        for (String name : ConfigData.GAMEMODES.get()) {
            if (ConfigData.GAMEMODES.get().get(index.get()).equals(name))
                lore.add(Component.text("->  ", NamedTextColor.DARK_PURPLE, TextDecoration.BOLD).append(Messages.formatLegacy(ConfigData.GM_NAME.get(name))));
            else
                lore.add(Messages.formatLegacy(ConfigData.GM_NAME.get(name)).color(NamedTextColor.AQUA));
        }
        lore.add(Component.text(":::::::::::::::::::::::::::::::::::::::::::::::", NamedTextColor.GOLD, TextDecoration.STRIKETHROUGH));

        if (meta != null) {
            meta.lore(lore);
            itemStack.setItemMeta(meta);
        }

        String gamemode_index = ConfigData.GAMEMODES.get().get(index.get());
        gui.changeItem(40, new GuiSlot(Material.getMaterial(ConfigData.GM_MATERIAL.get(gamemode_index)), "&6Now viewing:")
                .setLore(Messages.GUI_TOP_GM_NAME.get(ConfigData.GM_NAME.get(gamemode_index)))
                .setShiny());
        gui.changeItem(49, gui.getItemSlots().get(49).setLoreComponent(lore));
        changeTop(player, ConfigData.GAMEMODES.get().get(index.get()));

        player.playSound(player.getEyeLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1, 1);
    }

    private GuiSlot getTopItem(int i, String topType) {
        if ("KILLS".equals(topType)) {
            ScoreValue score = ScoreManager.getInstance().mobKills.getForever(i);
            if (score == null)
                return new GuiSlot(Material.ZOMBIE_HEAD, Messages.GUI_TOP_EMPTY.get());

            OfflinePlayer player = score.getOfflinePlayer();
            List<String> lore = MessageData.getMessageList(Messages.GUI_TOP_KILL_LORE.getPath());
            List<Component> lore2 = new ArrayList<>();
            for (String s : lore) {
                int kills = score.getValue();
                int deaths = 0; //Todo calc
                String replace = s.replace("%KILLS%", String.valueOf(kills))
                        .replace("%DEATHS%", String.valueOf(deaths))
                        .replace("%DAMAGE_DEAL%", String.valueOf(player.getStatistic(Statistic.DAMAGE_DEALT)))
                        .replace("%KD_RATE%", deaths == 0 ? "∞" : String.valueOf(kills / deaths))
                        .replace("%TIME_SINCE_DEATH%", String.valueOf(player.getStatistic(Statistic.TIME_SINCE_DEATH)))
                        .replace("%JUMP%", String.valueOf(player.getStatistic(Statistic.JUMP)));
                lore2.add(Messages.formatLegacy(replace));
            }

            return new GuiSlot()
                    .setPlayerHead(player)
                    .setLoreComponent(lore2)
                    .setName(Messages.GUI_TOP_FORMAT.get(String.valueOf(i), Objects.requireNonNull(player.getName())));
        }

        ScoreValue score = ScoreManager.getInstance().arenaWaveHighscore.get(topType).getForever(i);
        if (score == null)
            return new GuiSlot(Material.ZOMBIE_HEAD, Messages.GUI_TOP_EMPTY.get());

        List<Component> lore2 = new ArrayList<>();
        for (String s : MessageData.getMessageList(Messages.GUI_TOP_GM_LORE.getPath())) {
            String replace = s.replace("%MEMBERS%", score.getOfflinePlayer().getName())
                    .replace("%WAVE%", String.valueOf(score.getValue()));
            lore2.add(Messages.formatLegacy(replace));
        }
        //Todo support members in arenaScore
        /*if (!arena.getMemberList().isEmpty())
            return new GuiSlot()
                    .setPlayerHead(Bukkit.getOfflinePlayer(arena.getMemberList().stream().toList().get(0)))
                    .setLoreComponent(lore2)
                    .setName(Messages.GUI_TOP_FORMAT.get(String.valueOf(i), arena.getMembers()));*/
        return new GuiSlot(Material.SKELETON_SKULL)
                .setLoreComponent(lore2)
                .setName(Messages.GUI_TOP_FORMAT.get(String.valueOf(i), score.getOfflinePlayer().getName()));
    }
}
