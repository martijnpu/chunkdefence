package NL.martijnpu.ChunkDefence.Gui.Types;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommands.ArenaStartGamemodeCmd;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Data.MessageData;
import NL.martijnpu.ChunkDefence.Gui.GuiData;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class GuiGameModes implements GuiData {
    public Component getTitle() {
        return Messages.GUI_GM_NAME.getMiniMessageFormat();
    }

    public InventoryType getType() {
        return InventoryType.CHEST;
    }

    public int getSize() {
        return 9 * 2;
    }

    public Map<Integer, GuiSlot> renderSlots(Player player) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();
        int position = 0;

        for (String gamemode : ConfigData.GAMEMODES.get()) {
            Arena savedArena = ArenaManager.getInstance().getArena(player, gamemode);

            List<String> lore = MessageData.getMessageList(Messages.GUI_GM_LORE.getPath());
            lore.replaceAll(s -> s
                    //.replace("%MAX_PLAYERS%", ConfigData.GM_ARENA_ALLOW_INVITE.get(gamemode) ? "Everyone" : "1") //Todo fix
                    .replace("%MAX_HEALTH%", ConfigData.GM_MAX_HEALTH.get(gamemode) * 0.5 + " hearts")
                    .replace("%NAME%", ConfigData.GM_NAME.get(gamemode))
                    .replace("%TIME_INBETWEEN%", String.valueOf(ConfigData.GM_WAVE_TIME_BETWEEN.get(gamemode)))
                    .replace("%RESTART_ARENA_ON_DEATH%", String.valueOf(ConfigData.GM_DEATH_RESET_ARENA.get(gamemode)))
                    .replace("%RESTART_WAVE_ON_DEATH%", String.valueOf(ConfigData.GM_DEATH_RESTART_WAVE.get(gamemode)))
                    .replace("%DIFFICULTY%", String.valueOf(ConfigData.GM_DIFFICULTY_DEFAULT.get(gamemode)))
                    .replace("%ADAPTIVE_DIFFICULTY%", String.valueOf(ConfigData.GM_DIFFICULTY_ADAPTIVE.get(gamemode))));

            if (savedArena != null)
                lore.add(0, Messages.ARENA_RESUME.get());

            itemSlots.put(position, new GuiSlot(Material.getMaterial(ConfigData.GM_MATERIAL.get(gamemode)), ConfigData.GM_NAME.get(gamemode))
                    .setLore(lore)
                    .setLeftClick(() -> {
                        GuiManager.getInstance().closeInventory(player);
                        if (savedArena == null)
                            ArenaStartGamemodeCmd.arenaStartGamemodeCmd(player, gamemode);
                        else
                            savedArena.activateArena();
                    }));
            position++;
        }

        return itemSlots;
    }
}