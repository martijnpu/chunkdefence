package NL.martijnpu.ChunkDefence.Gui.Types;

import NL.martijnpu.ChunkDefence.Arenas.Schematics.SchematicManager;
import NL.martijnpu.ChunkDefence.Gui.GuiData;
import NL.martijnpu.ChunkDefence.Gui.GuiInventory;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiUtils;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopArena;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public final class GuiShopEditArena implements GuiData {
    public Component getTitle() {
        return Messages.GUI_SHOP_EDIT.getMiniMessageFormat();
    }

    public InventoryType getType() {
        return InventoryType.CHEST;
    }

    public int getSize() {
        return 9 * 4;
    }

    public Map<Integer, GuiSlot> renderSlots(Player player) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();
        ShopArena shop = (ShopArena) ShopManager.getInstance().getShop(ShopManager.lookingAt(player));
        AtomicInteger arenaIndex = new AtomicInteger(SchematicManager.getInstance().getAllSchematics().indexOf(shop.getSchematic()));

        itemSlots.put(1, new GuiSlot(Material.IRON_AXE, "&6Current arena: &f" + shop.getSchematic())
                .setLore("Click the item below with to cycle trough the available arenas."));
        itemSlots.put(10, new GuiSlot(getSchematicSelector(arenaIndex.get()))
                .setLeftClick(() -> {
                    arenaIndex.getAndIncrement();
                    if (arenaIndex.get() >= SchematicManager.getInstance().getAllSchematics().size())
                        arenaIndex.set(0);
                    setSchematicSelector(player, arenaIndex);
                })
                .setRightClick(() -> {
                    arenaIndex.getAndDecrement();
                    if (arenaIndex.get() < 0)
                        arenaIndex.set(SchematicManager.getInstance().getAllSchematics().size() - 1);
                    setSchematicSelector(player, arenaIndex);
                }));

        if (Statics.isProtocolLibAvailable) {
            String name = shop.getName();
            itemSlots.put(3, new GuiSlot(Material.NAME_TAG, "&6Current display name: &f" + name)
                    .setLore("Click below to edit the display name.")
                    .setLeftClick(() -> GuiUtils.openSign(player, "&6Display name", 12, name, "")));
            itemSlots.put(12, new GuiSlot(Material.PAPER, name)
                    .setLore("Click to edit the new display name")
                    .setLeftClick(() -> GuiUtils.openSign(player, "&6Display name", 12, name, "")));

            String cost = String.valueOf(shop.getCost());
            itemSlots.put(5, new GuiSlot(Material.SUNFLOWER, "&6Current price: &f" + cost)
                    .setLore("Click below to edit the price of this shop.")
                    .setLeftClick(() -> GuiUtils.openSign(player, "&6Price", 14, cost, "double")));
            itemSlots.put(14, new GuiSlot(Material.PAPER, cost)
                    .setLore("Click to edit the new price")
                    .setLeftClick(() -> GuiUtils.openSign(player, "&6Price", 14, cost, "double")));
        }

        itemSlots.put(7, new GuiSlot(Material.ARMOR_STAND, "&6Display item")
                .setLore("Replace the item below with the item you want to be displayed."));
        itemSlots.put(16, new GuiSlot(new ItemStack(shop.DISPLAY_ITEM.get()))
                .setLeftClick(() -> GuiUtils.swapItemsNull(player, 16, Material.GLASS_PANE)));

        itemSlots.putAll(GuiUtils.getLine(18, 26));

        itemSlots.put(27, new GuiSlot(Material.GREEN_CONCRETE, "&aSave changes")
                .setLeftClick(() -> saveShopArena(shop, player)));
        itemSlots.put(31, new GuiSlot(Material.BARRIER, "&cRemove shop")
                .setLeftClick(() -> {
                    shop.remove();
                    GuiManager.getInstance().closeInventory(player);
                }));
        itemSlots.put(35, new GuiSlot(Material.RED_CONCRETE, "&cCancel")
                .setLeftClick(() -> GuiManager.getInstance().closeInventory(player)));

        return itemSlots;
    }

    private void saveShopArena(ShopArena shop, Player player) {
        GuiInventory inventory = GuiManager.getInstance().getGui(player);
        if (inventory == null || inventory.getView() == null) {
            Messages.SHOP_INVALID.send(player);
            return;
        }

        if (inventory.getView().getItem(10) == null) {
            Messages.PLUGIN.send(player, "&cShop must sell an item.");
            return;
        }

        if (Statics.isProtocolLibAvailable) {
            String name = inventory.getDisplayName(12);
            String cost = inventory.getDisplayName(14);
            try {
                if (!name.isBlank())
                    shop.setName(name);

                if (!cost.isBlank())
                    shop.setCost(Double.parseDouble(cost));
            } catch (NumberFormatException e) {
                Messages.PLUGIN.send(player, "&c'&f" + cost + "&c' is not a valid price.");
            }
        }

        //set Schematic AFTER name
        String schematic = inventory.getDisplayName(10);
        if (!schematic.isBlank())
            shop.setSchematic(schematic);

        ItemStack item = inventory.getView().getItem(16);
        shop.setDisplayItem(item == null ? Material.GLASS_PANE : item.getType());

        GuiManager.getInstance().closeInventory(player);

//        ShopManager.resetInstance();
//        ShopManager.getInstance().printInitData();
        player.playSound(player.getEyeLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
    }

    /**
     * Regen the item in slot 10 as schematic selector
     **/
    private void setSchematicSelector(Player player, AtomicInteger arenaIndex) {
        GuiInventory inventory = GuiManager.getInstance().getGui(player);
        if (inventory == null || inventory.getView() == null)
            return;

        inventory.getView().setItem(10, getSchematicSelector(arenaIndex.get()));
        player.playSound(player.getEyeLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1, 1);
    }

    private ItemStack getSchematicSelector(int index) {
        List<Component> lore = new ArrayList<>();
        ItemStack itemStack = new ItemStack(Material.PAINTING);
        String selectedSchematic = index == -1 ? "Select arena" : SchematicManager.getInstance().getAllSchematics().get(index);
        ItemMeta meta = itemStack.getItemMeta();

        lore.add(Component.text(":::::::::::::::::::::::::::::::::::::::::::::::", NamedTextColor.GOLD, TextDecoration.STRIKETHROUGH));
        for (String name : SchematicManager.getInstance().getAllSchematics()) {
            if (selectedSchematic.equals(name))
                lore.add(Component.text("->  " + name, NamedTextColor.DARK_PURPLE, TextDecoration.BOLD));
            else
                lore.add(Component.text(name, NamedTextColor.AQUA));
        }
        meta.displayName(Component.text(selectedSchematic));
        meta.lore(lore);
        itemStack.setItemMeta(meta);
        return itemStack;
    }
}
