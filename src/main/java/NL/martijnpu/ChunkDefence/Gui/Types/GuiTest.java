package NL.martijnpu.ChunkDefence.Gui.Types;

import NL.martijnpu.ChunkDefence.Gui.GuiData;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import java.util.HashMap;
import java.util.Map;

public final class GuiTest implements GuiData {
    public Component getTitle() {
        return Component.text("Gui Test");
    }

    public InventoryType getType() {
        return InventoryType.HOPPER;
    }

    public Map<Integer, GuiSlot> renderSlots(Player player) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();
        itemSlots.put(0, new GuiSlot(Material.GRAY_STAINED_GLASS_PANE, "Time: 0.5")
                .setAnimated(0.5, 0)
                .addAnimation(Material.GREEN_STAINED_GLASS_PANE)
                .addAnimation(Material.RED_STAINED_GLASS_PANE));
        itemSlots.put(1, new GuiSlot(Material.GRAY_STAINED_GLASS_PANE, "Time: 1")
                .setAnimated(1, 0)
                .addAnimation(Material.GREEN_STAINED_GLASS_PANE)
                .addAnimation(Material.RED_STAINED_GLASS_PANE));
        itemSlots.put(2, new GuiSlot(Material.GRAY_STAINED_GLASS_PANE, "Time: 2")
                .setAnimated(2, 0)
                .addAnimation(Material.GREEN_STAINED_GLASS_PANE)
                .addAnimation(Material.RED_STAINED_GLASS_PANE));
        itemSlots.put(3, new GuiSlot(Material.GRAY_STAINED_GLASS_PANE, "Time: 5")
                .setAnimated(5, 0)
                .addAnimation(Material.GREEN_STAINED_GLASS_PANE)
                .addAnimation(Material.RED_STAINED_GLASS_PANE));
        itemSlots.put(4, new GuiSlot(Material.BARRIER, "Close")
                .setLeftClick(() -> GuiManager.getInstance().closeInventory(player)));

        return itemSlots;
    }
}
