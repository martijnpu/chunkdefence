package NL.martijnpu.ChunkDefence.Gui.Types;

import NL.martijnpu.ChunkDefence.Gui.GuiData;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import java.util.HashMap;
import java.util.Map;

public final class GuiArenaBuy implements GuiData {
    public Component getTitle() {
        return Messages.GUI_BUY_ARENA_NAME.getMiniMessageFormat();
    }

    public InventoryType getType() {
        return InventoryType.HOPPER;
    }

    public Map<Integer, GuiSlot> renderSlots(Player player) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();
        itemSlots.put(0, new GuiSlot(Material.GRAY_STAINED_GLASS_PANE, Messages.GUI_BUY_ARENA_QUESTION.get()));
        itemSlots.put(2, new GuiSlot(Material.GRAY_STAINED_GLASS_PANE, Messages.GUI_BUY_ARENA_QUESTION.get()));
        itemSlots.put(4, new GuiSlot(Material.GRAY_STAINED_GLASS_PANE, Messages.GUI_BUY_ARENA_QUESTION.get()));
        itemSlots.put(3, new GuiSlot(Material.RED_CONCRETE_POWDER, Messages.GUI_BUY_ARENA_QUESTION.get())
                .setLore(Messages.GUI_CANCEL.get())
                .setLeftClick(() -> GuiManager.getInstance().closeInventory(player)));

        return itemSlots;
    }
}
