package NL.martijnpu.ChunkDefence.Gui.Types;

import NL.martijnpu.ChunkDefence.Gui.GuiData;
import NL.martijnpu.ChunkDefence.Gui.GuiInventory;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiUtils;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopItem;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public final class GuiShopEditItem implements GuiData {
    public Component getTitle() {
        return Messages.GUI_SHOP_EDIT.getMiniMessageFormat();
    }

    public InventoryType getType() {
        return InventoryType.CHEST;
    }

    public int getSize() {
        return 9 * 4;
    }

    public Map<Integer, GuiSlot> renderSlots(Player player) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();
        ShopItem shop = (ShopItem) ShopManager.getInstance().getShop(ShopManager.lookingAt(player));

        itemSlots.put(1, new GuiSlot(Material.IRON_AXE, "&6Selling item")
                .setLore("Replace the item below with the item you want to sell."));
        itemSlots.put(10, new GuiSlot(shop.getItem())
                .setLeftClick(() -> GuiUtils.swapItemsNull(player, 10, Material.GLASS_PANE)));

        if (Statics.isProtocolLibAvailable) {
            String name = shop.getName();
            itemSlots.put(3, new GuiSlot(Material.NAME_TAG, "&6Current display name: &f" + name)
                    .setLore("Click below to edit the display name.")
                    .setLeftClick(() -> GuiUtils.openSign(player, "&6Display name", 12, name, "")));
            itemSlots.put(12, new GuiSlot(Material.PAPER, name)
                    .setLore("Click to edit the new display name")
                    .setLeftClick(() -> GuiUtils.openSign(player, "&6Display name", 12, name, "")));

            String cost = String.valueOf(shop.getCost());
            itemSlots.put(5, new GuiSlot(Material.SUNFLOWER, "&6Current price: &f" + cost)
                    .setLore("Click below to edit the price of this shop.")
                    .setLeftClick(() -> GuiUtils.openSign(player, "&6Price", 14, cost, "double")));
            itemSlots.put(14, new GuiSlot(Material.PAPER, cost)
                    .setLore("Click to edit the new price")
                    .setLeftClick(() -> GuiUtils.openSign(player, "&6Price", 14, cost, "double")));
        }

        itemSlots.put(7, new GuiSlot(Material.ARMOR_STAND, "&6Display item")
                .setLore("Display item will be the same as the selling type."));
        itemSlots.put(16, new GuiSlot(new ItemStack(Material.BARRIER))
                .setLore("Display item will be the same as the selling type."));

        itemSlots.putAll(GuiUtils.getLine(18, 26));

        itemSlots.put(27, new GuiSlot(Material.GREEN_CONCRETE, "&aSave changes")
                .setLeftClick(() -> saveShopItem(shop, player)));
        itemSlots.put(31, new GuiSlot(Material.BARRIER, "&cRemove shop")
                .setLeftClick(() -> {
                    shop.remove();
                    GuiManager.getInstance().closeInventory(player);
                }));
        itemSlots.put(35, new GuiSlot(Material.RED_CONCRETE, "&cCancel")
                .setLeftClick(() -> GuiManager.getInstance().closeInventory(player)));

        return itemSlots;
    }

    private void saveShopItem(ShopItem shop, Player player) {
        GuiInventory inventory = GuiManager.getInstance().getGui(player);
        if (inventory == null || inventory.getView() == null) {
            Messages.SHOP_INVALID.send(player);
            return;
        }

        if (inventory.getItemSlots().get(10) == null) {
            Messages.PLUGIN.send(player, "&cShop must sell an item.");
            return;
        }

        if (Statics.isProtocolLibAvailable) {
            String name = inventory.getDisplayName(12);
            String cost = inventory.getDisplayName(14);
            try {
                if (!name.isBlank())
                    shop.setName(name);

                if (!cost.isBlank())
                    shop.setCost(Double.parseDouble(cost));
            } catch (NumberFormatException e) {
                Messages.PLUGIN.send(player, "&c'&f" + cost + "&c' is not a valid price.");
            }
        }

        //set item AFTER name
        shop.changeItem(inventory.getView().getItem(10));

        GuiManager.getInstance().closeInventory(player);

//        ShopManager.resetInstance();
//        ShopManager.getInstance().printInitData();
        player.playSound(player.getEyeLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
    }
}
