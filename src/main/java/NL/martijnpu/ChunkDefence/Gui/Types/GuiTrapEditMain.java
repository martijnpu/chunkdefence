package NL.martijnpu.ChunkDefence.Gui.Types;

import NL.martijnpu.ChunkDefence.Gui.GuiData;
import NL.martijnpu.ChunkDefence.Gui.GuiInventory;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiUtils;
import NL.martijnpu.ChunkDefence.Traps.Effects.DamageType;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapData;
import NL.martijnpu.ChunkDefence.Utils.Keys;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import net.kyori.adventure.text.Component;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static NL.martijnpu.ChunkDefence.Gui.Util.GuiUtils.openSign;

public final class GuiTrapEditMain implements GuiData {
    public Component getTitle() {
        return Component.text("Edit Trap properties");
    }

    public InventoryType getType() {
        return InventoryType.CHEST;
    }

    public int getSize() {
        return 9 * 4;
    }

    public Map<Integer, GuiSlot> renderSlots(Player player) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();
        TrapData trapData = (TrapData) player.getMetadata(Keys.GUI_TRAP_EDIT_KEY).get(0).value();
        if (trapData == null) {
            Messages.WARN.sendConsole("Unable to get GUI for this player");
            itemSlots.put(0, new GuiSlot(Material.BARRIER, "Error while loading Trap"));
            return itemSlots;
        }

        itemSlots.put(7, new GuiSlot(Material.BRICK, "&6Display item")
                .setLore("Replace the item below with the item you want to be displayed in the inventory."));
        itemSlots.put(16, new GuiSlot(trapData.DISPLAY_ITEM.get())
                .setLeftClick(() -> GuiUtils.swapItemsNull(player, 16, Material.DEAD_BUSH)));

        itemSlots.put(8, new GuiSlot(Material.BRICKS, "&6Display block")
                .setLore("Replace the item below with the item you want to be placed on the ground."));
        itemSlots.put(17, new GuiSlot(trapData.BLOCK.get())
                .setLeftClick(() -> GuiUtils.swapItemsNull(player, 17, Material.DIRT)));

        if (Statics.isProtocolLibAvailable) {
            String name = trapData.NAME.get();
            itemSlots.put(0, new GuiSlot(Material.NAME_TAG, "&6Name: &f" + name)
                    .setLore("&eThis is the name shown to players.", "", "Click below to edit"));
            itemSlots.put(9, new GuiSlot(Material.PAPER, "&f" + name)
                    .setLore("Value will be changed to:", "", "Click to edit")
                    .setLeftClick(() -> openSign(player, "&6Name", 9, name, "")));

            String loreData = trapData.LORE.get();
            itemSlots.put(1, new GuiSlot(Material.ACACIA_SIGN, "&6Lore")
                    .setLore("&eThis is the lore shown to players.", "", "Click below to edit"));
            itemSlots.put(10, new GuiSlot(Material.PAPER, "&f" + loreData)
                    .setLore("Value will be changed to:", "", "Click to edit")
                    .setLeftClick(() -> openSign(player, "&6Lore", 10, loreData, "lore")));

            double setupTime = trapData.SETUP_TIME.get();
            itemSlots.put(2, new GuiSlot(Material.CLOCK, "&6Current setup time: &f" + setupTime)
                    .setLore("&eThis value is the amount of time it takes for this trap to become active.", "", "Click below to edit."));
            itemSlots.put(11, new GuiSlot(Material.PAPER, "&f" + setupTime)
                    .setLore("Value will be changed to:", "", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Setup time", 11, setupTime + "", "double")));

            double range = trapData.RANGE.get();
            itemSlots.put(3, new GuiSlot(Material.CROSSBOW, "&6Target range: &f" + range)
                    .setLore("&eThis value is the range in which the trap will attack entities.", "&eSetting the value to -1 will make the trap have no limit.", "", "Click below to edit."));
            itemSlots.put(12, new GuiSlot(Material.PAPER, "&f" + range)
                    .setLore("Value will be changed to:", "", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Target range", 12, range + "", "double")));

            int targetAmount = trapData.TARGET_AMOUNT.get();
            itemSlots.put(4, new GuiSlot(Material.PRISMARINE_CRYSTALS, "&6Target amount: &f" + targetAmount)
                    .setLore("&eThis value is the amount of entities the trap can target at once.", "&eSetting the value to -1 will make the trap target all mobs in his range.", "", "Click below to edit."));
            itemSlots.put(13, new GuiSlot(Material.PAPER, "&f" + targetAmount)
                    .setLore("Value will be changed to:", "", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Target amount", 13, targetAmount + "", "int")));

            List<String> lore = new ArrayList<>();
            lore.add("&eThe trap will only attack these entities. Leave empty to target");
            lore.add("&eall entities except players. Players can be added to the list.");
            lore.add("");
            lore.add("Current list:");
            trapData.TARGET_ENTITIES.get().forEach(entityType -> lore.add("&e- " + Statics.toCapital(entityType)));
            lore.add("");
            lore.add("Click below to edit");

            itemSlots.put(5, new GuiSlot(Material.ZOMBIE_HEAD, "&6Target entities")
                    .setLore(lore)
                    .setAnimated(1.5, 0)
                    .addAnimation(Material.WITHER_SKELETON_SKULL).addAnimation(Material.CREEPER_HEAD).addAnimation(Material.ZOMBIE_HEAD).addAnimation(Material.PLAYER_HEAD).addAnimation(Material.SKELETON_SKULL));
            itemSlots.put(14, new GuiSlot(Material.PAPER, "&6Entities:")
                    .setLore(trapData.TARGET_ENTITIES.get().stream().map(Statics::toCapital).collect(Collectors.toList()))
                    .setLeftClick(() -> GuiUtils.openEntityLoreSign(player, 14, true))
                    .setRightClick(() -> GuiUtils.openEntityLoreSign(player, 14, false)));

            int durability = trapData.DURABILITY.get();
            itemSlots.put(6, new GuiSlot(Material.CHIPPED_ANVIL, "&6Durability: &f" + durability)
                    .setLore("&eThis value is the maximum durability for this trap. When it reaches 0, the trap breaks. ", "&eSetting this value to -1 will give it infinite durability.", "", "Click below to edit."));
            itemSlots.put(15, new GuiSlot(Material.PAPER, "&f" + durability)
                    .setLore("Value will be changed to:", "", "Click to edit.")
                    .setLeftClick(() -> openSign(player, "&6Durability", 15, durability + "", "int")));
        }

        itemSlots.put(27, new GuiSlot(Material.GREEN_CONCRETE, "&aSave changes")
                .setLeftClick(() -> saveTrapInfo(trapData, player)));

        itemSlots.put(30, new GuiSlot(Material.BARRIER, "&cRemove trap")
                .setLeftClick(() -> {
                    GuiInventory inventory = GuiManager.getInstance().createInventory(player, GuiConfirm.class);
                    inventory.changeItem(1, new GuiSlot(Material.LIME_CONCRETE_POWDER, "Remove Trap")
                            .setLeftClick(() -> {
                                trapData.removeTrap();
                                GuiManager.getInstance().closeInventory(player);
                            }));
                    inventory.openGui();
                }));

        itemSlots.put(32, new GuiSlot(Material.BLAZE_POWDER, "&6Particle settings")
                .setLore("&eParticle: &f" + trapData.PARTICLE_TYPE.get().name(), "&eMode: &f" + trapData.PARTICLE_PATTERN.get(), "&eOn attack: &f" + trapData.ATTACK_PATTERN.get().name(), "&eAmount: &f" + trapData.PARTICLE_AMOUNT.get(), "", "Left click to open the Particle settings")
                .setLeftClick(() -> GuiManager.getInstance().createInventory(player, GuiTrapEditParticle.class).openGui()));

        DamageType type = trapData.DAMAGE_TYPE.get(); //Todo Right click to change type,
        itemSlots.put(33, new GuiSlot(Material.IRON_SWORD, "&6Attack settings")
                .setAnimated(1, 0)
                .addAnimation(Material.IRON_SWORD).addAnimation(Material.EXPERIENCE_BOTTLE).addAnimation(Material.IRON_AXE).addAnimation(Material.HONEY_BOTTLE)
                .setLore("&eDamage type: &f" + type.name().toLowerCase(), "&eInterval: &f" + trapData.INTERVAL.get(), "&eSet fire: &f" + trapData.SET_FIRE.get(), "", "Left click to open the Damage settings", "Right click to toggle the damage type (todo)")
                .setLeftClick(() -> GuiManager.getInstance().createInventory(player, GuiTrapEditDamage.class).openGui()));

        itemSlots.put(35, new GuiSlot(Material.RED_CONCRETE, "&cCancel")
                .setLeftClick(() -> GuiManager.getInstance().closeInventory(player)));

        itemSlots.putAll(GuiUtils.getLine(18, 26));

        return itemSlots;
    }

    private void saveTrapInfo(TrapData trapData, Player player) {
        GuiInventory inventory = GuiManager.getInstance().getGui(player);
        if (inventory == null || inventory.getView() == null) {
            Messages.WARN.sendConsole("Unable to get GUI for this player");
            return;
        }

        try {
            trapData.DISPLAY_ITEM.set(inventory.getType(16));
            trapData.BLOCK.set(inventory.getType(17));
            if (Statics.isProtocolLibAvailable) {
                trapData.NAME.set(inventory.getDisplayName(9).replace(ChatColor.COLOR_CHAR, '&'));
                trapData.LORE.set(GuiUtils.getStrippedDisplayName(inventory, 10));
                trapData.SETUP_TIME.set(Double.valueOf(ChatColor.stripColor(GuiUtils.getStrippedDisplayName(inventory, 11))));
                trapData.RANGE.set(Double.valueOf(GuiUtils.getStrippedDisplayName(inventory, 12)));
                trapData.TARGET_AMOUNT.set(Integer.valueOf(GuiUtils.getStrippedDisplayName(inventory, 13)));
                trapData.TARGET_ENTITIES.set(inventory.getView().getItem(14).getItemMeta().getLore());
                trapData.DURABILITY.set(Integer.valueOf(GuiUtils.getStrippedDisplayName(inventory, 15)));
            }
        } catch (NullPointerException | NumberFormatException ex) {
            Messages.EXCEPT_ADMIN.send(player);
            Messages.WARN.sendConsole("An error occurred in a gui: " + ex.getLocalizedMessage());
            ex.printStackTrace();
        }

        //todo move out of method (also for ShopEdit GUI's)
        GuiManager.getInstance().createInventory(player, GuiTrapList.class);
        player.playSound(player.getEyeLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
    }
}