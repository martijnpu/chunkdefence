package NL.martijnpu.ChunkDefence.Gui.Book;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Data.FileHandler;
import NL.martijnpu.ChunkDefence.Data.MessageData;
import NL.martijnpu.ChunkDefence.Scores.ScoreManager;
import NL.martijnpu.ChunkDefence.Scores.ScoreTypes.ScoreValue;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import net.kyori.adventure.inventory.Book;
import org.bukkit.entity.Player;

import java.util.List;

public final class Books {

    public static void openBook(Player player, String bookName) {
        Messages.DEBUG.sendConsole(player.getDisplayName() + " is trying to open book '" + bookName + "'");
        String bookPath = "books." + bookName;

        if (!FileHandler.MESSAGES_FILE.get().contains("messages." + bookPath)) {
            Messages.WARN.send(player, "A book named " + bookName + " doesn't exists!");
            return;
        }

        String bookAuthor = MessageData.getMessage(bookPath + ".author");
        String bookTitle = MessageData.getMessage(bookPath + ".title");
        List<String> pages = MessageData.getMessageList(bookPath + ".pages");

        Book.Builder bookBuilder = Book.builder();
        bookBuilder.author(Messages.formatLegacy(bookAuthor));
        bookBuilder.title(Messages.formatLegacy(bookTitle));

        try {
            Arena arena = ArenaManager.getInstance().getArena(player);

            for (String page : pages) {
                if (page.contains("%SCORE_")) {
                    //todo remove loop and get gamemode + place from placeholder
                    for (String gamemode : ConfigData.GAMEMODES.get()) {
                        StringBuilder builder = new StringBuilder();

                        for (int i = 1; i < 11; i++) {
                            ScoreValue score = ScoreManager.getInstance().arenaWaveHighscore.get(gamemode).getForever(i);
                            if (score != null)
                                builder.append(Messages.format(MessageData.getMessage("books.booklines.score-" + gamemode.toLowerCase()) //Todo rewrite book scoremanager
                                        .replace("%INDEX%", String.valueOf(i))
                                        //.replace("%PLAYERS?%", highscore.getMembers())
                                        .replace("%GAMEMODE%", gamemode)
                                        .replace("%WAVE%", String.valueOf(score.getValue()))));
                            builder.append("\n");
                        }

                        page = page.replace("%SCORE_" + gamemode.toUpperCase() + "%", Messages.format(builder.toString()));
                    }
                }
                page = page.replace("%PLAYER%", player.getDisplayName())
                        .replace("%WAVE%", arena == null ? "0" : String.valueOf(arena.getArenaData().WAVE.get()))
                        .replace("%TEAM%", arena == null ? "---" : arena.getArenaMembers())
                        .replace("%COUNTDOWN_TIME%", String.valueOf(ConfigData.GM_WAVE_TIME_BETWEEN.get(arena == null ? "default" : arena.getArenaData().GAMEMODE.get())));

                bookBuilder.addPage(Messages.formatLegacy(page));
            }

        } catch (Exception ex) {
            bookBuilder.addPage(Messages.WARN.getMiniMessageFormat("&4Unable to generate welcome message. See console for the full error.\n&0" + ex.getMessage()));
            ex.printStackTrace();
        }

        player.openBook(bookBuilder);
    }
}
