package NL.martijnpu.ChunkDefence.Gui.Sign;

import NL.martijnpu.ChunkDefence.Events.ProtocolLib.Packages.WrapperPlayServerOpenSignEditor;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.TimeHandler;
import com.comphenix.protocol.wrappers.BlockPosition;
import net.kyori.adventure.text.Component;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.sign.Side;
import org.bukkit.entity.Player;

import java.util.stream.IntStream;

public final class GuiSign {
    private final String JSON_FORMAT = "{\"text\":\"%s\"}";

    public final Player player;
    private String[] text = new String[4];
    private Runnable onCloseRunnable;
    private Location signLocation;

    public GuiSign(String[] lines, Player player) {
        GuiManager.getInstance().register(this, player);
        this.player = player;
        for (int i = 0; i < lines.length && i < text.length; i++)
            text[i] = Messages.format(lines[i]);
    }

    public void closeSign() {
        //todo send packet to force close
    }

    public void setCloseRunnable(Runnable runnable) {
        onCloseRunnable = runnable;
    }

    public String[] getText() {
        return text;
    }

    /**
     * Triggered on close event.
     *
     * @param lines Current (updated) lines
     */
    public void onClose(String[] lines) {
        text = lines;
        GuiManager.getInstance().unregister(this);
        if (onCloseRunnable != null)
            TimeHandler.getInstance().addRunnableQueue(onCloseRunnable);

        replaceOldBlock(player);
    }

    public void replaceOldBlock(Player player) {
        // If the Player is in another world, the fake Block will be fixed automatically when the Player loads its world.
        if (signLocation != null && signLocation.getWorld() != null && player.getWorld().equals(signLocation.getWorld()))
            player.sendBlockChange(signLocation, signLocation.getBlock().getBlockData());
    }

    public void openGui() {
        Messages.DEBUG.sendConsole("Opening fake sign for " + player.getName());
        try {
            signLocation = player.getLocation().clone();
            BlockData blockData = Material.OAK_SIGN.createBlockData();
            BlockState blockState = blockData.createBlockState();
            BlockPosition blockPosition = new BlockPosition(signLocation.toVector());

            //Change fake block
            player.sendBlockChange(signLocation, blockState.getBlockData());

            //Change sign text
            if (blockState instanceof Sign sign) {
                IntStream.range(0, text.length).forEach(i -> sign.getSide(Side.BACK).line(i, Component.text(text[i])));
                player.sendBlockUpdate(signLocation, sign);
            }

            //Open sign - Paper can only open real signs
            WrapperPlayServerOpenSignEditor wrapperOpenSign = new WrapperPlayServerOpenSignEditor();
            wrapperOpenSign.setLocation(blockPosition);
            wrapperOpenSign.sendPacket(player);

            /* old - 100% Packets based
            BlockPosition blockPosition = new BlockPosition(signLocation.toVector());

            //Change fake block
            WrapperPlayServerBlockChange wrapperBlockChange = new WrapperPlayServerBlockChange();
            wrapperBlockChange.setLocation(blockPosition);
            wrapperBlockChange.setBlockData(WrappedBlockData.createData(Material.OAK_SIGN));

            //Change sign text
            NbtCompound signNBT = NbtFactory.ofCompound("");
            IntStream.range(0, text.length).forEach(i -> signNBT.put("Text" + (i + 1), String.format(JSON_FORMAT, text[i])));
            signNBT.put("x", signLocation.getX());
            signNBT.put("y", signLocation.getY());
            signNBT.put("z", signLocation.getZ());
            signNBT.put("id", "minecraft:sign");

            WrapperPlayServerTileEntityData wrapperSignData = new WrapperPlayServerTileEntityData();
            wrapperSignData.setLocation(blockPosition);
            wrapperSignData.setAction(9);
            wrapperSignData.setNbtData(signNBT);

            //Open sign
            WrapperPlayServerOpenSignEditor wrapperOpenSign = new WrapperPlayServerOpenSignEditor();
            wrapperOpenSign.setLocation(blockPosition);

            //Send all packets
            wrapperBlockChange.sendPacket(player);
            wrapperSignData.sendPacket(player);
            wrapperOpenSign.sendPacket(player);
            */
        } catch (Exception ex) {
            Messages.EXCEPT_ADMIN.send(player);
            ex.printStackTrace();
            onClose(text);
        }
    }
}
