package NL.martijnpu.ChunkDefence.Gui;

import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import net.kyori.adventure.text.Component;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import java.util.Map;

public interface GuiData {
    default Component getTitle() {
        return Component.text("Unknown");
    }

    default InventoryType getType() {
        return InventoryType.BARREL;
    }

    /**
     * If getType() equals CHEST, define the custom size. Must be a multiple of 9
     */
    default int getSize() {
        return 9; //9, 18, 27, 36, 45, 54
    }

    Map<Integer, GuiSlot> renderSlots(Player player);
}
