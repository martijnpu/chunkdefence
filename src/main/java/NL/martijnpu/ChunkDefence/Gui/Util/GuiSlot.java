package NL.martijnpu.ChunkDefence.Gui.Util;

import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Data.MessageData;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.TimeHandler;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public final class GuiSlot {
    private final ItemStack itemStack;
    private final List<Material> animationList = new ArrayList<>();
    private Runnable onLeftClick, onRightClick, onLeftShiftClick, onRightShiftClick, onMiddleClick;
    private String name = "";
    private List<Component> lore = new ArrayList<>();
    private boolean canEdit;
    private double animationSpeed; //1 frame per x seconds
    private double animatedState;

    public GuiSlot() {
        itemStack = new ItemStack(Material.AIR);
        name = " ";
        defaultMeta();
    }

    public GuiSlot(@Nullable ItemStack itemStack) {
        this.itemStack = itemStack;
        if (itemStack != null && itemStack.hasItemMeta()) {
            name = itemStack.getItemMeta().getDisplayName();
            lore = itemStack.getItemMeta().lore();
        }
        defaultMeta();
    }

    public GuiSlot(Material material) {
        itemStack = new ItemStack(material);
        defaultMeta();
    }

    public GuiSlot(Material material, String name) {
        itemStack = new ItemStack(material);
        checkMaterial();
        this.name = MessageData.getMessage(name);
        defaultMeta();
    }

    private void checkMaterial() {
        if (itemStack.getType().equals(Material.AIR))
            itemStack.setType(Material.LIGHT_GRAY_STAINED_GLASS_PANE);
    }

    private void defaultMeta() {
        ItemMeta meta = itemStack.getItemMeta();
        if (meta != null) {
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES,
                    ItemFlag.HIDE_DESTROYS,
                    ItemFlag.HIDE_ENCHANTS,
                    ItemFlag.HIDE_PLACED_ON,
                    ItemFlag.HIDE_ITEM_SPECIFICS,
                    ItemFlag.HIDE_UNBREAKABLE);
            itemStack.setItemMeta(meta);
        }
    }

    public GuiSlot setLeftClick(Runnable onLeftClick) {
        this.onLeftClick = onLeftClick;
        return this;
    }

    public GuiSlot setRightClick(Runnable onRightClick) {
        this.onRightClick = onRightClick;
        return this;
    }

    public GuiSlot setLeftShiftClick(Runnable onLeftShiftClick) {
        this.onLeftShiftClick = onLeftShiftClick;
        return this;
    }

    public GuiSlot setRightShiftClick(Runnable onRightShiftClick) {
        this.onRightShiftClick = onRightShiftClick;
        return this;
    }

    public GuiSlot setMiddleClick(Runnable onMiddleClick) {
        this.onMiddleClick = onMiddleClick;
        return this;
    }

    public GuiSlot setEdible(boolean canEdit) {
        this.canEdit = canEdit;
        return this;
    }

    public GuiSlot setShiny() {
        checkMaterial();
        ItemMeta meta = itemStack.getItemMeta();
        meta.addEnchant(Enchantment.DAMAGE_ALL, 0, false);
        itemStack.setItemMeta(meta);
        return this;
    }

    public GuiSlot setLoreComponent(List<Component> lore) {
        checkMaterial();
        this.lore = lore;
        return this;
    }

    public GuiSlot setLore(List<String> lore) {
        List<Component> newLore = new ArrayList<>();
        for (String string : lore)
            newLore.add(Messages.formatLegacy(string));
        return setLoreComponent(newLore);
    }

    public GuiSlot setLore(String... lore) {
        List<Component> newLore = new ArrayList<>();
        for (String string : lore)
            newLore.add(Messages.formatLegacy(string));
        return setLoreComponent(newLore);
    }

    public GuiSlot setMaterial(Material material) {
        itemStack.setType(material);
        return this;
    }

    public GuiSlot setAnimated(double speed, double offset) {
        checkMaterial();
        animationSpeed = speed;
        animatedState = offset;
        return this;
    }

    public GuiSlot addAnimation(Material material, int amount) {
        for (int i = 0; i < amount; i++)
            animationList.add(material);
        return this;
    }

    public GuiSlot addAnimation(Material material) {
        animationList.add(material);
        return this;
    }

    public void onLeftClick() {
        if (onLeftClick != null)
            onLeftClick.run();
    }

    public void onRightClick() {
        if (onRightClick != null)
            onRightClick.run();
        else
            onLeftClick();
    }

    public void onShiftRightClick() {
        if (onRightShiftClick != null)
            onRightShiftClick.run();
        else if (onRightClick != null)
            onRightClick();
        else
            onLeftShiftClick();
    }

    public void onLeftShiftClick() {
        if (onLeftShiftClick != null)
            onLeftShiftClick.run();
        else
            onLeftClick();
    }

    public void onMiddleClick() {
        if (onMiddleClick != null)
            onMiddleClick.run();
    }

    public ItemStack getItem() {
        ItemMeta meta = itemStack.getItemMeta();
        if (meta != null) {
            meta.lore(lore);
            meta.displayName(Messages.formatLegacy(name));
            itemStack.setItemMeta(meta);
        }
        return itemStack;
    }

    public String getName() {
        return name;
    }

    public GuiSlot setName(String name) {
        checkMaterial();
        this.name = MessageData.getMessage(name);
        return this;
    }

    public boolean canEdit() {
        return canEdit;
    }

    public boolean isAnimated() {
        return animationList.size() > 1 && animationSpeed > 0;
    }

    public Material tickAnimation() {
        animatedState += ConfigData.PARTICLE_FREQUENCY.get() / animationSpeed / TimeHandler.SEC;
        animatedState %= animationList.size();
        return animationList.get((int) animatedState);
    }

    public GuiSlot addLore(String s) {
        checkMaterial();
        lore.add(Component.text(s));
        return this;
    }

    public GuiSlot removeLore(String s) {
        checkMaterial();
        lore.remove(Component.text(s));
        return this;
    }

    public GuiSlot setAmount(int i) {
        checkMaterial();
        itemStack.setAmount(i);
        return this;
    }

    public GuiSlot setPlayerHead(OfflinePlayer offlinePlayer) {
        itemStack.setType(Material.PLAYER_HEAD);
        SkullMeta meta = (SkullMeta) itemStack.getItemMeta();
        if (meta != null && offlinePlayer != null) {
            meta.setOwningPlayer(offlinePlayer);
        }
        itemStack.setItemMeta(meta);
        itemStack.addItemFlags(ItemFlag.HIDE_ITEM_SPECIFICS, ItemFlag.HIDE_ATTRIBUTES);
        return this;
    }
}
