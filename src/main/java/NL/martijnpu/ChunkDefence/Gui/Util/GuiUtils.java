package NL.martijnpu.ChunkDefence.Gui.Util;

import NL.martijnpu.ChunkDefence.Gui.GuiInventory;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Sign.GuiSign;
import NL.martijnpu.ChunkDefence.Traps.Effects.AttackPattern;
import NL.martijnpu.ChunkDefence.Traps.Effects.ParticlePattern;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import net.kyori.adventure.text.serializer.plain.PlainTextComponentSerializer;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionType;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public final class GuiUtils {

    public static void openSign(Player player, String valueName, int replaceSlot, String oldValue, String dataType) {
        GuiInventory inventory = GuiManager.getInstance().getGui(player);
        oldValue = PlainTextComponentSerializer.plainText().serialize(Messages.formatLegacy(oldValue));

        if (inventory == null)
            throw new NullPointerException("Player '" + player + "' doesn't have a CD-inventory open!");

        String[] signText;
        if ("lore".equals(dataType))
            signText = new String[]{"&f&o", "&f&o", "&f&o", "&a&l↑ lore ↑"};
        else
            signText = new String[]{"§f§o" + oldValue, "&a&l↑ ↑ ↑ ↑", valueName, "&eType on first line."};

        GuiSign sign = new GuiSign(signText, player);
        sign.setCloseRunnable(() -> {
            inventory.openGui();
            String name = PlainTextComponentSerializer.plainText().serialize(Messages.formatLegacy(sign.getText()[0]));

            if (name.isEmpty()) {
                player.playSound(player.getEyeLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                return;
            }

            try {
                String temp = name.toUpperCase().replace(' ', '_');
                Object value = switch (dataType) {
                    case "double" -> Double.parseDouble(temp);
                    case "int" -> Integer.parseInt(temp);
                    case "potion" -> PotionType.valueOf(temp);
                    case "sound" -> Sound.valueOf(temp);
                    case "particle" -> Particle.valueOf(temp);
                    case "particleAttack" -> AttackPattern.valueOf(temp);
                    case "particleMovement" -> ParticlePattern.valueOf(temp);
                    default -> null; //String
                };
                Messages.DEBUG.sendConsole("Received signchange with first line: '" + name + "'. Replacing slotIndex " + replaceSlot);

                if ("double".equals(dataType) || "int".equals(dataType))
                    name = "§f" + value;
                else if ("lore".equals(dataType))
                    name = "§f" + signText[0] + signText[1] + signText[2];
                else
                    name = "§f" + name;

                GuiSlot slot = inventory.getItemSlots().get(replaceSlot);
                slot.setName(name);
                inventory.changeItem(replaceSlot, slot);

            } catch (IllegalArgumentException ex) {
                player.playSound(player.getEyeLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                Messages.PLUGIN.send(player, "&f" + name + " &cis not a valid " + dataType + ".");
                inventory.openGui();
            }
        });

        inventory.hideGui();
        sign.openGui();
    }

    public static String getStrippedDisplayName(GuiInventory inventory, int slot) {
        return PlainTextComponentSerializer.plainText().serialize(Messages.formatLegacy(inventory.getDisplayName(slot)));
    }

    /**
     * Get a <i>black</i> animated line from slot @from to @to
     */
    public static Map<Integer, GuiSlot> getLine(int from, int to) {
        Map<Integer, GuiSlot> itemSlots = new HashMap<>();
        for (int i = from; i <= to; i++)
            itemSlots.put(i, new GuiSlot(Material.BLACK_STAINED_GLASS_PANE, " ")
                    .setAnimated(0.1, i)
                    .addAnimation(Material.BLACK_STAINED_GLASS_PANE, 20)
                    .addAnimation(Material.WHITE_STAINED_GLASS_PANE)
                    .addAnimation(Material.LIGHT_GRAY_STAINED_GLASS_PANE, 2)
                    .addAnimation(Material.GRAY_STAINED_GLASS_PANE, 3));
        return itemSlots;
    }

    /**
     * ClickEvent to allow players to swap the Cursor item with a Gui Slot.
     * .
     * <b>!! WARNING !!</b><br>
     * <i>canEdit</i> must be false when using this method
     **/
    public static void swapItemsNull(Player player, int slot, Material empty) {
        GuiInventory gui = GuiManager.getInstance().getGui(player);
        if (gui != null)
            gui.swapItemsNull(slot, empty);
    }

    public static void openEntityLoreSign(Player player, int replaceSlot, boolean add) {
        GuiInventory inventory = GuiManager.getInstance().getGui(player);
        if (inventory == null) {
            Messages.EXCEPT_ADMIN.send(player);
            Messages.WARN.sendConsole("Old GUI is closed. Report this to the developer!");
            return;
        }

        GuiSign sign = new GuiSign(new String[]{"§f§o", "&a&l↑ ↑ ↑ ↑", add ? "&aAdd target" : "&cRemove target", "&eType on first line."}, player);

        sign.setCloseRunnable(() -> {
            inventory.openGui();
            String name = PlainTextComponentSerializer.plainText().serialize(Messages.formatLegacy(sign.getText()[0]));

            if (name.isEmpty()) {
                player.playSound(player.getEyeLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                return;
            }

            if (Arrays.stream(EntityType.values()).anyMatch(x -> x.name().equalsIgnoreCase(name.replace(' ', '_')))) {
                player.playSound(player.getEyeLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                Messages.PLUGIN.send(player, "&f" + name + "&c is not a valid entity.");
                return;
            }

            GuiSlot slot = inventory.getItemSlots().get(replaceSlot);
            if (add)
                slot.addLore(name);
            else
                slot.removeLore(name);
            inventory.changeItem(replaceSlot, slot);
        });

        inventory.hideGui();
        sign.openGui();
    }
}
