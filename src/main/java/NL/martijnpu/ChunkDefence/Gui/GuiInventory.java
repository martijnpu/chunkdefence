package NL.martijnpu.ChunkDefence.Gui;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import net.kyori.adventure.text.serializer.plain.PlainTextComponentSerializer;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class GuiInventory {
    public final Player player;
    private final Map<Integer, GuiSlot> itemSlots = new HashMap<>();
    private final Inventory inventory;
    private final GuiData guiData;
    private InventoryView inventoryView;
    public boolean isHidden = false;

    public GuiInventory(Player player, GuiData guiData) {
        Messages.DEBUG.sendConsole("Opening GUI of " + player.getName() + ": " + PlainTextComponentSerializer.plainText().serialize(guiData.getTitle()));
        this.player = player;
        this.guiData = guiData;

        if (InventoryType.CHEST.equals(guiData.getType()))
            inventory = ChunkDefence.get().getServer().createInventory(player, guiData.getSize(), guiData.getTitle());
        else
            inventory = ChunkDefence.get().getServer().createInventory(player, guiData.getType(), guiData.getTitle());

        guiData.renderSlots(player).forEach(this::changeItem);
        Messages.DEBUG.sendConsole("ItemSlots contains " + itemSlots.size() + " keys");

        GuiManager.getInstance().register(player, this);
    }

    public Map<Integer, GuiSlot> getItemSlots() {
        return itemSlots;
    }

    @Nullable
    public InventoryView getView() {
        return inventoryView;
    }

    public void openGui() {
        isHidden = false;
        inventoryView = player.openInventory(inventory);
    }

    /**
     * Close GUI, but do not destroy the object/data
     */
    public void hideGui() {
        isHidden = true;
        inventoryView.close();
    }

    /**
     * Close gui with sound
     **/
    public void closeGuiSound() {
        closeGui();
        player.playSound(player.getEyeLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1, 1);
    }

    /**
     * Close GUI in silence
     */
    void closeGui() {
        GuiManager.getInstance().unregister(this); //Before closing
        if (inventoryView != null)
            inventoryView.close();
    }

    public GuiInventory changeItem(int slot, GuiSlot guiSlot) {
        itemSlots.put(slot, guiSlot);
        inventory.setItem(slot, guiSlot == null ? null : guiSlot.getItem());
        return this;
    }

    public void resetItems() {
        itemSlots.clear();
        guiData.renderSlots(player).forEach(this::changeItem);
        Messages.DEBUG.sendConsole("ItemSlots contains " + itemSlots.size() + " keys");
    }

    /**
     * <b>!! WARNING !!</b><br>
     * <i>canEdit</i> must be false when using this method
     **/
    public void swapItemsNull(int slot, Material empty) {
        if (inventory == null)
            return;

        ItemStack cursorItem = inventoryView.getCursor();
        ItemStack slotItem = inventoryView.getItem(slot);

        if (slotItem != null && slotItem.getType() == empty)
            inventoryView.setCursor(new ItemStack(Material.AIR));
        else
            inventoryView.setCursor(slotItem);

        if (cursorItem.getType() == Material.AIR)
            inventoryView.setItem(slot, new ItemStack(empty));
        else
            inventoryView.setItem(slot, cursorItem);
    }

    /**
     * Get the material Type of the item in the targeted slot.
     *
     * @param slot -1 means item in Cursor, > 0 equals inventorySlot
     */
    public Material getType(int slot) {
        if (inventoryView != null) {
            ItemStack itemStack = slot == -1 ? inventoryView.getCursor() : inventoryView.getItem(slot);
            if (itemStack != null)
                return itemStack.getType();
        } else {
            GuiSlot guiSlot = itemSlots.get(slot);
            if (guiSlot != null && guiSlot.getItem() != null)
                return guiSlot.getItem().getType();
        }
        return Material.GLASS_PANE;
    }

    public boolean canEdit(int slot) {
        if (itemSlots.containsKey(slot) && itemSlots.get(slot) != null)
            return itemSlots.get(slot).canEdit();
        return false;
    }

    public void onClick(int slot, ClickType clickType) {
        if (itemSlots.containsKey(slot)) {
            switch (clickType) {
                case LEFT:
                    itemSlots.get(slot).onLeftClick();
                    break;
                case RIGHT:
                    itemSlots.get(slot).onRightClick();
                    break;
                case SHIFT_LEFT:
                    itemSlots.get(slot).onLeftShiftClick();
                    break;
                case SHIFT_RIGHT:
                    itemSlots.get(slot).onShiftRightClick();
                    break;
                case MIDDLE:
                    itemSlots.get(slot).onMiddleClick();
                    break;
                default:
                    //Nothing
            }
        }
    }

    public void inventoryTick() {
        if (inventoryView == null) //inventory is not opened yet
            return;

        itemSlots.forEach((slot, item) -> {
            if (item != null && item.isAnimated()) {
                ItemStack itemStack = inventoryView.getItem(slot);
                if (itemStack != null) {
                    itemStack.setType(item.tickAnimation());
                    inventory.setItem(slot, itemStack);
                }
            }
        });
    }


    /**
     * Get the display name of the item in the targeted slot.
     *
     * @param slot -1 means item in Cursor, > 0 equals inventorySlot
     */
    public String getDisplayName(int slot) {
        ItemStack itemStack = null;
        if (inventoryView != null) {
            if (slot == -1)
                itemStack = inventoryView.getCursor();
            else
                itemStack = inventoryView.getItem(slot);
        } else {
            if (itemSlots.get(slot) != null)
                itemStack = itemSlots.get(slot).getItem();
        }

        if (itemStack != null && itemStack.getItemMeta() != null)
            return itemStack.getItemMeta().getDisplayName();
        return "";
    }
}
