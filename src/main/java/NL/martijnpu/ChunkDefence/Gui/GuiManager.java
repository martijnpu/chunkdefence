package NL.martijnpu.ChunkDefence.Gui;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Gui.Sign.GuiSign;
import NL.martijnpu.ChunkDefence.Utils.Keys;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class GuiManager {
    private static GuiManager instance;
    private final Map<Player, GuiInventory> activeGui = new HashMap<>();
    private final Map<Player, GuiSign> activeSign = new HashMap<>();

    private GuiManager() {}

    public static GuiManager getInstance() {
        if (instance == null)
            instance = new GuiManager();
        return instance;
    }

    public static void resetInstance() {
        if (instance != null)
            instance.shutdown();
        instance = null;
    }

    private void shutdown() {
        Map<Player, GuiInventory> cloneBase = new HashMap<>(activeGui);
        cloneBase.forEach((p, inv) -> inv.closeGuiSound());

        try {
            Map<Player, GuiSign> cloneSign = new HashMap<>(activeSign);
            cloneSign.forEach((p, sign) -> sign.closeSign());
        } catch (NoClassDefFoundError ex) {
            Messages.WARN.sendConsole("Exception occurred. This one is safe to ignore: " + ex.getMessage());
        }
    }

    /**
     * Tick the gui's for animations and more
     */
    public void guiTick() {
        activeGui.forEach((x, y) -> y.inventoryTick());
    }

    void register(Player player, GuiInventory inv) {
        player.setMetadata(Keys.GUI_INV_KEY, new FixedMetadataValue(ChunkDefence.get(), inv));
        activeGui.put(player, inv);
    }

    void unregister(GuiInventory inventory) {
        inventory.player.removeMetadata(Keys.GUI_INV_KEY, ChunkDefence.get());
        //inventory.player.removeMetadata(Keys.GUI_TRAP_EDIT_KEY, ChunkDefence.get());
        activeGui.remove(inventory.player);
    }

    public GuiInventory createInventory(Player player, Class<? extends GuiData> guiData) {
        GuiInventory inventory = getGui(player);
        GuiData gui;
        try {gui = guiData.getDeclaredConstructor().newInstance();} catch (Exception ignored) {return null;}

        if (inventory != null)
            inventory.closeGui();

        inventory = new GuiInventory(player, gui);

        register(player, inventory);
        return inventory;
    }

    public void closeInventory(Player player) {
        GuiInventory inv = getGui(player);
        if (inv != null)
            inv.closeGuiSound();
    }

    @Nullable
    public GuiInventory getGui(Player player) {
        if (player.hasMetadata(Keys.GUI_INV_KEY))
            return (GuiInventory) player.getMetadata(Keys.GUI_INV_KEY).get(0).value();
        return null;
    }

    public void register(GuiSign sign, Player player) {
        GuiSign old = getSign(player);
        if (old != null)
            unregister(sign);

        player.setMetadata(Keys.GUI_SIGN_KEY, new FixedMetadataValue(ChunkDefence.get(), sign));
        activeSign.put(player, sign);
    }

    public void unregister(GuiSign sign) {
        if (sign.player != null) {
            sign.player.removeMetadata(Keys.GUI_SIGN_KEY, ChunkDefence.get());
            activeSign.remove(sign.player);
        }
    }

    @Nullable
    public GuiSign getSign(Player player) {
        return activeSign.get(player);
    }
}
