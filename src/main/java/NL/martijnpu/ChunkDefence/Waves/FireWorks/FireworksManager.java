package NL.martijnpu.ChunkDefence.Waves.FireWorks;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Waves.SpawnWaves;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public final class FireworksManager {
    private static final List<FireworkTimer> fireworkQueue = new ArrayList<>();
    private static final int MAX_DELAY = 20; //ParticleEngine Ticks
    private static final Random RANDOM = new Random();

    /**
     * Update all queued fireworks
     */
    public static void updateTick() {
        fireworkQueue.forEach(FireworkTimer::countdown);
        fireworkQueue.removeIf(FireworkTimer::isExpired);
    }

    public static void spawnFireworkArena(Arena arena) {
        SpawnWaves.getSpawnLocations(arena).forEach(x -> fireworkQueue.add(new FireworkTimer(x, 1, RANDOM.nextInt(MAX_DELAY))));
    }

    public static void spawnFireworkLocation(Location location, int amount, int spread) {
        for (int i = 0; i < amount; i++)
            fireworkQueue.add(new FireworkTimer(location, spread, RANDOM.nextInt(MAX_DELAY)));
    }
}
