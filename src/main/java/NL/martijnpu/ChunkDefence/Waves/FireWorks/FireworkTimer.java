package NL.martijnpu.ChunkDefence.Waves.FireWorks;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.Random;

final class FireworkTimer {
    private final Random RANDOM = new Random();
    private final Location location;
    private final int spread;
    private int delayTime;

    FireworkTimer(Location location, int spread, int delayTime) {
        this.location = location.clone();
        this.delayTime = delayTime;
        this.spread = spread;
    }

    boolean isExpired() {
        return delayTime <= -1;
    }

    void countdown() {
        delayTime--;
        if (delayTime == 0)
            spawn(location, spread);
    }

    private void spawn(Location location, int spread) {
        Location fireLocation = location.clone();
        fireLocation.add(RANDOM.nextInt(spread) - 0.5 * spread, 2, RANDOM.nextInt(spread) - 0.5 * spread);
        Firework firework = (Firework) fireLocation.getWorld().spawnEntity(fireLocation, EntityType.FIREWORK);

        FireworkEffect effect = FireworkEffect.builder()
                .flicker(RANDOM.nextBoolean())
                .trail(RANDOM.nextBoolean())
                .with(getRandomType())
                .withColor(getRandomColor())
                .withFade(getRandomColor())
                .build();

        FireworkMeta meta = firework.getFireworkMeta();
        meta.addEffect(effect);
        meta.setPower(RANDOM.nextInt(2) + 1);
        firework.setFireworkMeta(meta);
    }

    private FireworkEffect.Type getRandomType() {
        return switch (new Random().nextInt(5)) {
            case 1 -> FireworkEffect.Type.BALL;
            case 2 -> FireworkEffect.Type.BALL_LARGE;
            case 3 -> FireworkEffect.Type.BURST;
            case 4 -> FireworkEffect.Type.CREEPER;
            default -> FireworkEffect.Type.STAR;
        };
    }

    private Color getRandomColor() {
        return switch (new Random().nextInt(17)) {
            case 1 -> Color.AQUA;
            case 2 -> Color.BLACK;
            case 3 -> Color.YELLOW;
            case 4 -> Color.BLUE;
            case 5 -> Color.FUCHSIA;
            case 6 -> Color.GRAY;
            case 7 -> Color.GREEN;
            case 8 -> Color.LIME;
            case 9 -> Color.MAROON;
            case 10 -> Color.NAVY;
            case 11 -> Color.OLIVE;
            case 12 -> Color.ORANGE;
            case 13 -> Color.PURPLE;
            case 14 -> Color.RED;
            case 15 -> Color.SILVER;
            case 16 -> Color.TEAL;
            default -> Color.WHITE;
        };
    }
}
