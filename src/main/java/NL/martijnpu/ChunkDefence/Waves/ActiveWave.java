package NL.martijnpu.ChunkDefence.Waves;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class ActiveWave {
    private final Arena arena;
    private final List<LivingEntity> waveEntities = new ArrayList<>();
    private final BossBar bossBar; //Wave progress
    private int deaths = 0;
    private boolean isSpawned = false;

    ActiveWave(Arena arena) {
        this.arena = arena;
        bossBar = ChunkDefence.get().getServer().createBossBar(Messages.BOSS_ACTIVE.get(String.valueOf(arena.getArenaData().WAVE.get())), BarColor.RED, BarStyle.SOLID);
        arena.getOnlinePlayers().forEach(bossBar::addPlayer);
        updateBossBar();
        Messages.DEBUG.sendConsole("Starting wave " + arena.getArenaData().WAVE.get() + "...");
    }

    int getWave() {
        return arena.getArenaData().WAVE.get();
    }

    boolean isStarted() {
        return isSpawned;
    }

    int getRemainingEntities() {
        return waveEntities.size();
    }

    /**
     * Add existing entities to the current wave
     */
    public void addEntity(LivingEntity entity) {
        waveEntities.add(entity);
        isSpawned = true;
        updateBossBar();

        if (entity instanceof Mob mob)
            mob.setTarget(arena.getRandomOnlinePlayer());
    }

    /**
     * Freeze the current entities and make them invulnerable
     */
    void pause() {
        PotionEffect potion_weak = new PotionEffect(PotionEffectType.WEAKNESS, 9999, 9999, false, false);

        waveEntities.forEach(x -> {
            x.setAI(false);
            x.setSilent(true);
            x.setInvisible(true);
            x.setCollidable(false);
            x.setInvulnerable(true);
            x.setGlowing(true);
            x.addPotionEffect(potion_weak);
            removeTargets();
        });
    }

    /**
     * Unfreeze the current entities
     */
    void resume() {
        waveEntities.forEach(x -> {
            x.setAI(true);
            x.setSilent(false);
            x.setInvisible(false);
            x.setCollidable(true);
            x.setInvulnerable(false);
            x.setGlowing(false);
            x.removePotionEffect(PotionEffectType.WEAKNESS);
            updateTargets(null);
        });
    }

    /**
     * Force end the current wave
     */
    void killWave() {
        bossBar.removeAll();
        for (LivingEntity waveEntity : waveEntities)
            if (!waveEntity.isDead())
                waveEntity.remove();
    }

    /**
     * Update the targets of the mobs in the wave
     *
     * @param target Only update mobs with target.<br><i>Null = All mobs</i>
     */
    public void updateTargets(@Nullable Player target) {
        List<Player> alive = arena.getOnlinePlayers();
        alive.removeIf((x) -> !arena.isInsideArena(x.getLocation()));

        for (LivingEntity waveEntity : waveEntities) {
            if (!waveEntity.isDead() && (target == null || ((Mob) waveEntity).getTarget() == target)) {
                Player newTarget = (alive.size() == 1) ? alive.get(0) : alive.get(new Random().nextInt(alive.size() - 1));
                ((Mob) waveEntity).setTarget(newTarget);
            }
        }
    }

    /**
     * Remove the targets of all mobs
     */
    private void removeTargets() {
        for (LivingEntity waveEntity : waveEntities)
            if (!waveEntity.isDead())
                ((Mob) waveEntity).setTarget(null);
    }

    /**
     * Check all waveEntities if they're still alive
     */
    void checkDeaths() {
        int size = waveEntities.size();
        waveEntities.removeIf(x -> !x.isValid());
        deaths += size - waveEntities.size();
        updateBossBar();
    }

    /**
     * Check if the wave is finished
     */
    boolean isDone() {
        return waveEntities.isEmpty() && isSpawned;
    }

    private void updateBossBar() {
        bossBar.setVisible(true);
        if (waveEntities.isEmpty())
            bossBar.setProgress(0);
        else {
            bossBar.setProgress((waveEntities.size() * 1.0) / (waveEntities.size() + deaths));
            arena.setArenaTime(21000 - (6000L * waveEntities.size() / (waveEntities.size() + deaths)));//Todo improve time (bossbar works correct, sun needs to work differently)
        }
        if (isDone())
            bossBar.removeAll();
    }

    /**
     * Highlight all wave entities
     */
    public void highlightEntities() {
        PotionEffect potionEffect = new PotionEffect(PotionEffectType.GLOWING, 200, 0, false, false, false);
        waveEntities.forEach(livingEntity -> livingEntity.addPotionEffect(potionEffect));
    }
}
