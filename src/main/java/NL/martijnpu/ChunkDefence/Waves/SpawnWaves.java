package NL.martijnpu.ChunkDefence.Waves;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaData;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Arenas.Schematics.SchematicEditor;
import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Mobs.DefinedMobData;
import NL.martijnpu.ChunkDefence.Mobs.MobManager;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Utils.Keys;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import NL.martijnpu.ChunkDefence.Worlds.WorldManager;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.loot.Lootable;
import org.bukkit.material.Colorable;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.Vector;

import java.util.*;

public final class SpawnWaves {
    /**
     * Spawn the given wave in the specified Arena
     */
    public static void spawnWave(int wave, Arena arena) {
        Map<Integer, DefinedMobData> mobs = MobManager.getInstance().isWaveDefined(wave) ? MobManager.getInstance().getDefinedWave(wave) : getGeneratedWave(wave);
        List<Location> spawnLocations = getSpawnLocations(arena);

        if (spawnLocations.isEmpty())
            arena.sendMessageToAll(Messages.EXCEPT_ARENA_SPAWN_LOC);
        else {
            Random random = new Random();
            mobs.forEach((amount, mobData) -> {
                for (int i = amount % 100; i > 0; i--)
                    spawnEntity(mobData, arena, spawnLocations.get(random.nextInt(spawnLocations.size())));
            });
        }
    }

    /**
     * Get the mobs of a generated wave
     */
    public static Map<Integer, DefinedMobData> getGeneratedWave(int wave) {
        Map<Integer, DefinedMobData> list = new HashMap<>();
        if (wave <= 0)
            return list;

        Random random = new Random(wave); //Todo review calculation
        int pointsToSpawn = (int) (Math.pow(wave - 1, 2.12) * 0.1 + 2);
        int groups = (int) Math.ceil(Math.cos(wave) + Math.pow(wave, 0.5));
        int groupsize = pointsToSpawn / groups;
        int maxGroup = (int) (wave * 0.2 + 5);

        while (pointsToSpawn > 0) {
            List<DefinedMobData> possibleMobs = new ArrayList<>();

            int group_points = groupsize;

            if (group_points * 2 > pointsToSpawn)
                group_points = pointsToSpawn;

            pointsToSpawn = pointsToSpawn - group_points;

            for (DefinedMobData mobData : MobManager.getInstance().getDefinedMobs()) {
                if (mobData.getWeight() <= group_points && (group_points / mobData.getWeight()) <= maxGroup)
                    possibleMobs.add(mobData);
            }
            DefinedMobData dmd = possibleMobs.get(random.nextInt(possibleMobs.size()));
            random.setSeed(dmd.getEntityType().hashCode() + dmd.getWeight() + wave);
            list.put((int) Math.round(group_points * 1.0 / dmd.getWeight()) + (100 * list.size()), dmd);
        }
        return list;
    }

    /**
     * Get realtime all available spawnlocation within the given arena
     */
    public static List<Location> getSpawnLocations(Arena arena) {
        if (arena == null)
            return new ArrayList<>();

        if (arena.getArenaData().spawnLocations.isEmpty()) {
            Vector center = arena.getArenaData().getArenaLoc();
            Vector delta = ArenaManager.getInstance().getArenaDelta(arena.getArenaData().SCHEMATIC.get()).toVector();
            Vector dimensions = SchematicEditor.getInstance().getDimensions(arena.getArenaData().SCHEMATIC.get());
            if (dimensions == null)
                return new ArrayList<>();

            Block startBlock = center.toLocation(WorldManager.getInstance().getArenaWorld()).subtract(delta).getBlock();
            Material spawnMaterial = Material.getMaterial(ConfigData.ARENA_MOB_SPAWN.get());

            for (int x = 0; x <= dimensions.getBlockX(); x++) {
                for (int y = 0; y <= dimensions.getBlockY(); y++) {
                    for (int z = 0; z <= dimensions.getBlockZ(); z++) {
                        Block tmpBlock = startBlock.getRelative(x, y, z);
                        if (tmpBlock.getType().equals(spawnMaterial) && tmpBlock.getRelative(0, 1, 0).isEmpty())
                            arena.getArenaData().spawnLocations.add(tmpBlock.getLocation().add(0.5, 1, 0.5)); //Spawn on top of the block
                    }
                }
            }
            Messages.DEBUG.sendConsole("Found " + arena.getArenaData().spawnLocations.size() + " new spawnlocations from " + spawnMaterial.name() + " between '" + Statics.getStringFromXYZ(startBlock.getLocation().toVector()) + "' and '" + Statics.getStringFromXYZ(startBlock.getRelative(dimensions.getBlockX(), dimensions.getBlockY(), dimensions.getBlockZ()).getLocation().toVector()) + "'");
        }
        return arena.getArenaData().spawnLocations;
    }

    /**
     * Spawn the DefinedMob at the given location and set all default values
     */
    private static LivingEntity spawnEntity(DefinedMobData mobData, Arena arena, Location location) {
        LivingEntity entity = (LivingEntity) location.getWorld().spawnEntity(location, mobData.getEntityType());
        setDefault(entity, arena.getArenaData().getDifficulty(), mobData, arena.getArenaData());

        if (mobData.getPassenger() != null) {
            LivingEntity passenger = spawnEntity(mobData.getPassenger(), arena, location);
            entity.addPassenger(passenger);
        }
        return entity;
    }

    /**
     * Set the defaults of an entity according DefinedMobData
     */
    private static void setDefault(LivingEntity entity, int difficulty, DefinedMobData mobData, ArenaData arenaData) {
        entity.setMetadata(Keys.MOB_KILLCOINS_KEY, new FixedMetadataValue(ChunkDefence.get(), mobData.getCoins()));
        entity.getPersistentDataContainer().set(Keys.SHOP_KEY, PersistentDataType.INTEGER, ShopManager.getRandomEntityID());
        entity.setSilent(mobData.getSilent());
        entity.setGlowing(mobData.getGlowing());
        entity.setGliding(mobData.getGliding());

        // To disable remove when far away
        entity.setRemoveWhenFarAway(false);

        if (entity.getEquipment() != null) {
            entity.getEquipment().clear();
            entity.getEquipment().setItemInMainHand(mobData.getEntityMainHand());
            entity.getEquipment().setItemInOffHand(mobData.getEntityOffHand());
            entity.getEquipment().setHelmet(mobData.getEntityHelmet());
            entity.getEquipment().setChestplate(mobData.getEntityChestPlate());
            entity.getEquipment().setLeggings(mobData.getEntityLeggings());
            entity.getEquipment().setBoots(mobData.getEntityBoots());
        }

        entity.setCanPickupItems(false);
        if (entity instanceof Lootable lootable)
            lootable.setLootTable(null);
        if (entity instanceof Zombie zombie)
            zombie.setBaby(!mobData.getEntityAdult());
        if (entity instanceof Rabbit rabbit)
            rabbit.setRabbitType(Rabbit.Type.THE_KILLER_BUNNY);
        if (entity instanceof Wolf wolf)
            wolf.setAngry(true);
        if (entity instanceof Slime slime)
            slime.setSize(mobData.getSize());
        if ((entity instanceof Skeleton || entity instanceof Illusioner) && mobData.getEntityMainHand() == null)
            entity.getEquipment().setItemInMainHand(new ItemStack(Material.BOW));
        if (entity instanceof Pillager && mobData.getEntityMainHand() == null)
            entity.getEquipment().setItemInMainHand(new ItemStack(Material.CROSSBOW));
        if (entity instanceof Hoglin hoglin)
            hoglin.setImmuneToZombification(false);
        if (entity instanceof Creeper creeper) {
            creeper.setExplosionRadius(mobData.getExplosionRadius());
            creeper.setPowered(mobData.getPowered());
        }
        if (mobData.getCustomName() != null) {
            entity.customName(Messages.formatLegacy(mobData.getCustomName()));
            entity.setCustomNameVisible(true);
        }
        if (entity instanceof Colorable colorable) {
            if (mobData.getColor() != null) {
                try {
                    colorable.setColor(DyeColor.valueOf(mobData.getColor().toUpperCase()));
                } catch (IllegalArgumentException ex) {
                    Messages.WARN.sendConsole("Unknown DyeColor: " + mobData.getColor() + ". Setting to purple.");
                    colorable.setColor(DyeColor.PURPLE);
                }
            }
        }
        if (entity instanceof Ageable ageable) {
            if (mobData.getEntityAdult())
                ageable.setAdult();
            else
                ageable.setBaby();
        }

        if (ConfigData.GM_DIFFICULTY_ADAPTIVE.get(arenaData.GAMEMODE.get())) {
            int newHealth = mobData.getEntityHealth() > -1 ? mobData.getEntityHealth() : (int) (entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue() * (difficulty / (new Random().nextInt(20) + 90.0)));
            entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(newHealth);
            entity.setHealth(entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());
        }

        if (mobData.getAdditionalDamage() != 0) {
            double base = entity.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).getBaseValue();
            entity.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(base + mobData.getAdditionalDamage());
        }
    }
}
