package NL.martijnpu.ChunkDefence.Waves;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Players.EconomyManager;
import NL.martijnpu.ChunkDefence.Scores.ScoreManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Utils.TimeHandler;
import NL.martijnpu.ChunkDefence.Waves.FireWorks.FireworksManager;
import net.kyori.adventure.text.Component;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.attribute.Attribute;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

public final class WaveController {
    private final Arena arena;
    private int countdownTaskID = 0;
    private int countdownTimer = 0;
    private long threeSecCheck = 0; //Wait 3 sec before declaring a wave finished
    final BossBar bossBar = ChunkDefence.get().getServer().createBossBar(Messages.BOSS_INACTIVE.get(), BarColor.GREEN, BarStyle.SOLID); //Countdown timer
    private ActiveWave activeWave = null; //Active wave
    private boolean isPaused;
    private final int DEFAULT_COUNTDOWN = 10;

    public WaveController(Arena arena) {
        this.arena = arena;
        updatePlayers();
        Messages.DEBUG.sendConsole("Creating WaveController for " + arena.getArenaMembers());

        //Start wave if no Bypass & auto-start configured
        if (!Permission.BYPASS_DIRECTSTART.hasPermission(arena.getOnlinePlayers().get(0)) && ConfigData.GM_WAVE_TIME_BETWEEN.get(arena.getArenaData().GAMEMODE.get()) > 0) //todo Doc
            start();
    }

    public ActiveWave getWaveData() {
        return activeWave;
    }

    /**
     * Update if the players are changed
     */
    public void updatePlayers() {
        bossBar.removeAll();
        arena.getOnlinePlayers().forEach(bossBar::addPlayer);
    }

    /**
     * Shutdown WaveController and waves
     */
    public void shutdown() {
        Messages.DEBUG.sendConsole("WaveController shutting down for " + arena.getArenaMembers() + "...");

        arena.setArenaTime(12000);
        bossBar.setVisible(false);
        bossBar.removeAll();
        if (countdownTaskID != 0)
            ChunkDefence.get().getServer().getScheduler().cancelTask(countdownTaskID);
        if (activeWave != null) {
            activeWave.killWave();
            arena.getArenaData().editDifficulty(false); //Todo check for better options when server shutdown since this method is called at different places
        }
    }

    /**
     * Check if there is a wave currently in progress
     */
    public boolean isInWave() {
        return activeWave != null;
    }

    /**
     * Check if there is a countdown running for a new wave
     */
    public boolean isInCountDown() {
        return countdownTaskID != 0;
    }

    /**
     * Start the Waves if possible.
     * If the wave is already starting force the countdown
     */
    public void start() {
        if (isPaused) {
            arena.sendMessageToAll(Messages.WAVE_PAUSE);
            return;
        }
        Messages.DEBUG.sendConsole("Wave starting for " + arena.getArenaMembers() + "...");

        if (activeWave != null && activeWave.isStarted()) {
            arena.sendMessageToAll(Messages.WAVE_FORCE_WAVE);
            return;
        }

        if (countdownTaskID == 0) {
            countdownTimer = ConfigData.GM_WAVE_TIME_BETWEEN.get(arena.getArenaData().GAMEMODE.get());
            if (countdownTimer <= -1)
                countdownTimer = DEFAULT_COUNTDOWN;

            arena.getOnlinePlayers().forEach(Messages.WAVE_SKIP_TIMER::send);
            countdownTaskID = ChunkDefence.get().getServer().getScheduler().scheduleSyncRepeatingTask(ChunkDefence.get(), this::countdown, TimeHandler.SEC, TimeHandler.SEC);
            bossBar.setVisible(true);
            arena.getOnlinePlayers().forEach(onlinePlayer -> onlinePlayer.setPlayerTime(-700, false)); //todo Make config option to disable timeSync
            return;
        }

        if (countdownTimer > 4) { //Force counter to 3 seconds (timer will withdraw 1)
            arena.sendMessageToAll(Messages.WAVE_FORCE_SUCCESS);
            countdownTimer = 4;
        } else if (countdownTimer > 0) //Cannot be forced more
            arena.sendMessageToAll(Messages.WAVE_FORCE_ALREADY);
    }

    /**
     * Freeze the current wave or pause the countdown
     *
     * @return whether it's applied
     */
    public boolean pause() {
        if (isPaused)
            return false;
        isPaused = true;

        Messages.DEBUG.sendConsole("Paused WaveController for " + arena.getArenaMembers());

        if (activeWave == null || countdownTaskID != 0)
            ChunkDefence.get().getServer().getScheduler().cancelTask(countdownTaskID);
        else
            activeWave.pause();
        return true;
    }

    /**
     * Resume the automatic countdown or unfreeze the wave
     *
     * @return whether it's applied
     */
    public boolean resume() {
        if (!isPaused)
            return false;
        isPaused = false;

        Messages.DEBUG.sendConsole("Resumed WaveController for " + arena.getArenaMembers());

        if (activeWave == null)
            startCountdown();
        else
            activeWave.resume();
        return true;
    }

    private void countdown() {
        int timeBetween = ConfigData.GM_WAVE_TIME_BETWEEN.get(arena.getArenaData().GAMEMODE.get());
        if (timeBetween <= -1)
            timeBetween = DEFAULT_COUNTDOWN;

        if (countdownTimer == timeBetween)
            arena.getOnlinePlayers().forEach(onlinePlayer -> onlinePlayer.setPlayerTime(-700, false));

        countdownTimer--;
        arena.setArenaTime((long) (12786 - 11002 * (countdownTimer * 1.0 / timeBetween)));

        bossBar.setProgress(Math.max(countdownTimer, 0) * 1f / timeBetween);
        if (countdownTimer >= 1) {
            bossBar.setTitle(Messages.BOSS_COUNTDOWN.get(String.valueOf(countdownTimer)));
            bossBar.setColor(BarColor.GREEN);
        }

        if ((countdownTimer == 120) || (countdownTimer == 90) || (countdownTimer == 60) || (countdownTimer == 30) || (countdownTimer == 10) || ((countdownTimer <= 3) && (countdownTimer > 1))) {
            arena.sendMessageToAll(Messages.WAVE_NEXT_MULTI, String.valueOf(countdownTimer));
        } else if (countdownTimer == 1) {
            for (Player player : arena.getOnlinePlayers()) {
                if (!arena.isInsideArena(player.getLocation()))
                    player.teleport(arena.getArenaData().getSpawnLocation(), PlayerTeleportEvent.TeleportCause.PLUGIN);
            }
            arena.sendMessageToAll(Messages.WAVE_NEXT_SINGLE, String.valueOf(countdownTimer));
            bossBar.setTitle(Messages.BOSS_START.get());
            bossBar.setColor(BarColor.RED);
        } else if (countdownTimer == 0) {
            for (Player player : arena.getOnlinePlayers()) { //Double check to be sure
                if (!arena.isInsideArena(player.getLocation()))
                    player.teleport(arena.getArenaData().getSpawnLocation(), PlayerTeleportEvent.TeleportCause.PLUGIN);
            }
            arena.sendMessageToAll(Messages.WAVE_START);
            bossBar.setVisible(false);
            activeWave = new ActiveWave(arena);
            SpawnWaves.spawnWave(arena.getArenaData().WAVE.get(), arena);
            ChunkDefence.get().getServer().getScheduler().cancelTask(countdownTaskID);
            countdownTaskID = 0;
            threeSecCheck = 0;
        }
    }

    /**
     * Get the current status of the WaveController.
     * Possible options: Pause, Empty, Countdown, Spawning, Entities remaining
     */
    public String getStatus() {
        if (isPaused)
            return Messages.PLACEHOLDER_WAVE_PAUSE.get();

        if (countdownTimer > 0)
            return Messages.PLACEHOLDER_WAVE_COUNT.get(String.valueOf(countdownTimer));

        if (activeWave == null)
            return Messages.PLACEHOLDER_WAVE_NONE.get();

        if (!activeWave.isStarted())
            return Messages.PLACEHOLDER_WAVE_SPAWN.get();

        return Messages.PLACEHOLDER_WAVE_REMAIN.get(String.valueOf(activeWave.getRemainingEntities()));
    }

    /**
     * Time-engine. Recommended to tick once a second
     */
    public void pingSec() {
        if (activeWave != null) {
            activeWave.checkDeaths();
            checkWave();
        }
    }

    /**
     * Check the condition of the current wave
     * If nobody is in the Arena, pause the wave
     * If the current wave is finished and wrap up
     */
    private void checkWave() {
        if (activeWave == null)
            return;

        //Check if players are available and act accordingly
        boolean present = arena.getOnlinePlayers().stream().anyMatch(p -> arena.isInsideArena(p.getLocation()));
        if (present && isPaused) {
            resume();
            arena.sendMessageToAll(Messages.WAVE_START);
        } else if (!present && !isPaused) {
            pause();
            arena.sendMessageToAll(Messages.ARENA_AWAY);
        }

        //Ignore further checks
        if (isPaused)
            return;

        //Wait 3 seconds before declaring the wave finished
        if (!activeWave.isDone())
            threeSecCheck = TimeHandler.getCurrSec();
        if (TimeHandler.getCurrSec() < threeSecCheck + 3)
            return;

        //Finish wave
        bossBar.setTitle(Messages.BOSS_END.get());
        bossBar.setColor(BarColor.GREEN);
        bossBar.setVisible(true);
        arena.getArenaData().editDifficulty(true);
        arena.getArenaData().WAVE.set(arena.getArenaData().WAVE.get() + 1);
        countdownTimer = ConfigData.GM_WAVE_TIME_BETWEEN.get(arena.getArenaData().GAMEMODE.get());
        FireworksManager.spawnFireworkArena(arena);

        if (countdownTaskID != 0) { //todo check if required
            ChunkDefence.get().getServer().getScheduler().cancelTask(countdownTaskID);
            countdownTaskID = 0;
        }

        if (ConfigData.GM_WAVE_HEAL_ON_SUCCEED.get(arena.getArenaData().GAMEMODE.get()))
            arena.getOnlinePlayers().forEach(player -> player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue()));

        for (Player player : arena.getOnlinePlayers()) {
            player.playSound(player.getLocation(), Sound.ENTITY_ENDER_DRAGON_GROWL, SoundCategory.PLAYERS, 1, 1);
            FireworksManager.spawnFireworkLocation(player.getLocation(), 2, 5);
            EconomyManager.getInstance().depositPlayer(player, ConfigData.GM_WAVE_COINS_SUCCEED.get(arena.getArenaData().GAMEMODE.get()));

            Component component = Messages.WAVE_SURVIVE.getMiniMessageFormat(String.valueOf(activeWave.getWave()));
            if (countdownTimer > -1)
                component = component.append(Messages.WAVE_NEXT_MULTI.getMiniMessageFormat(String.valueOf(countdownTimer)));

            Messages.sendBigMessage(player.getPlayer(), component);
        }

        activeWave.killWave();
        activeWave = null;

        ScoreManager.getInstance().updateArenaScore(arena);
        arena.setArenaTime(12000); //todo add 10s timer on bossbar reset (start wave with arena start & arenatime)
        startCountdown();
    }

    private void startCountdown() {
        if (countdownTimer <= -1) // No auto start
            return;

        countdownTaskID = ChunkDefence.get().getServer().getScheduler().scheduleSyncRepeatingTask(ChunkDefence.get(), this::countdown, TimeHandler.SEC * 3, TimeHandler.SEC);
        bossBar.setVisible(true);
        arena.getOnlinePlayers().forEach(Messages.WAVE_SKIP_TIMER::send);
    }
}
