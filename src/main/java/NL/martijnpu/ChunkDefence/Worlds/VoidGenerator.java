package NL.martijnpu.ChunkDefence.Worlds;

import org.bukkit.World;
import org.bukkit.generator.BiomeProvider;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.generator.WorldInfo;
import org.jetbrains.annotations.NotNull;

public final class VoidGenerator extends ChunkGenerator {

    //All methods are default false, no need to override them

    @Override
    public boolean canSpawn(@NotNull World world, int x, int z) {
        return false;
    }

    @Override
    public BiomeProvider getDefaultBiomeProvider(@NotNull WorldInfo worldInfo) {
        return new CustomBiomeProvider();
    }
}
