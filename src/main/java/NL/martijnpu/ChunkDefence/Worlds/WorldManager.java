package NL.martijnpu.ChunkDefence.Worlds;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Data.FileHandler;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import org.bukkit.*;

import javax.annotation.Nullable;

public final class WorldManager {
    private static WorldManager instance;
    private World world;

    private WorldManager() {
        loadWorld();
    }

    public static void resetInstance() {
        if (instance != null) {
            instance = null;
        }
    }

    public static WorldManager getInstance() {
        if (instance == null)
            instance = new WorldManager();
        return instance;
    }

    @Nullable
    public World getArenaWorld() {
        return world;
    }

    private void loadWorld() {
        String worldName = ConfigData.ARENA_WORLD.get();
        world = ChunkDefence.get().getServer().getWorld(worldName);
        boolean firstTime = !FileHandler.get().isWorldAvailable();

        try {
            if (world == null) { //Meaning world isn't loaded yet
                Messages.PLUGIN.sendConsole("");
                Messages.PLUGIN.sendConsole("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Plugin continues after the world loading =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
                Messages.PLUGIN.sendConsole("Registering the arena-world named " + worldName);
                WorldCreator w = new WorldCreator(worldName);
                w.generateStructures(false);
                w.generator(new VoidGenerator());
                w.environment(World.Environment.NORMAL);
                world = ChunkDefence.get().getServer().createWorld(w);
                if (world == null) {
                    Messages.WARN.sendConsole("Unable to register world " + worldName);
                    return;
                }

                if (firstTime) {
                    Location location = new Location(world, 0.5, ConfigData.ARENA_HEIGHT.get(), 0.5, 90, 0);
                    world.setSpawnLocation(location);
                    world.getBlockAt(location).getRelative(0, -1, 0).setType(Material.STONE);
                    ConfigData.SHOP_LOCATION.set(Statics.getStringFromLocation(location));
                    Messages.CUSTOM.sendConsole("&fSince this is the first setup, a shop location is create in the arena-world at " + Statics.getStringFromLocationExcludeWorld(location));
                }
                Messages.PLUGIN.sendConsole("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= World loading done =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
                Messages.PLUGIN.sendConsole("");
            } else
                Messages.PLUGIN.sendConsole("Loading the arena-world named " + worldName);

            world.setGameRule(GameRule.DO_ENTITY_DROPS, false);
            world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
            world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);
            world.setGameRule(GameRule.DO_FIRE_TICK, false);
            world.setGameRule(GameRule.DO_MOB_LOOT, false);
            world.setGameRule(GameRule.DO_MOB_SPAWNING, false);
            world.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
            world.setGameRule(GameRule.SPAWN_RADIUS, 0);
            world.setGameRule(GameRule.DISABLE_RAIDS, true);
            world.setGameRule(GameRule.DO_IMMEDIATE_RESPAWN, true);
            world.setGameRule(GameRule.DO_INSOMNIA, false);
            world.setGameRule(GameRule.DO_PATROL_SPAWNING, false);
            world.setGameRule(GameRule.DO_TRADER_SPAWNING, false);
            world.setGameRule(GameRule.KEEP_INVENTORY, true);
            world.setGameRule(GameRule.MOB_GRIEFING, false);
            world.setGameRule(GameRule.NATURAL_REGENERATION, true);
            world.setGameRule(GameRule.RANDOM_TICK_SPEED, 0);
            world.setGameRule(GameRule.REDUCED_DEBUG_INFO, !ConfigData.DEBUG.get());
            world.setGameRule(GameRule.SPECTATORS_GENERATE_CHUNKS, false);
            world.setTime(18000);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
