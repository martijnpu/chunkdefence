package NL.martijnpu.ChunkDefence.Events.ProtocolLib;


import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Events.ProtocolLib.Packages.WrapperPlayClientUpdateSign;
import NL.martijnpu.ChunkDefence.Events.SubEvents.PlayerInteractEvents;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Sign.GuiSign;
import NL.martijnpu.ChunkDefence.Shops.FloatingText.ShopDisplayManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class PackagesListener implements Listener {
    private static final List<Player> playerQueue = new ArrayList<>(); //Transform async actions to main thread

    public static void initialize() {
        if (!Statics.isProtocolLibAvailable)
            return;

        ProtocolManager manager = ProtocolLibrary.getProtocolManager();
        if (manager == null) {
            Messages.WARN.sendConsole("ProtocolLib is enabled but no ProtocolManager found. Disabling features!");
            Statics.isProtocolLibAvailable = false;
            return;
        }

        manager.addPacketListener(new PacketAdapter(ChunkDefence.get(), ListenerPriority.NORMAL, PacketType.Play.Client.LOOK) {
            @Override
            public void onPacketReceiving(PacketEvent event) {
                ShopDisplayManager.getInstance().checkPlayerLook(event.getPlayer());
            }
        });

        /* //Not required when ArmorStands are used as Marker
        manager.addPacketListener(new PacketAdapter(ChunkDefence.get(), ListenerPriority.NORMAL, PacketType.Play.Client.USE_ENTITY) {
            @Override
            public void onPacketReceiving(PacketEvent event) {
                Messages.DEBUG.sendConsole("Received USE_ENTITY event with packet: " + event.getPacket().getType().name());

                try {
                    if (!event.getPacket().getType().equals(PacketType.Play.Client.USE_ENTITY))
                        return;

                    EnumWrappers.EntityUseAction action = event.getPacket().getEnumEntityUseActions().readSafely(0).getAction();

                    if (action.equals(EnumWrappers.EntityUseAction.INTERACT_AT))
                        playerQueue.add(event.getPlayer());
                } catch (Exception e) {
                    Messages.WARN.sendConsole("ChunkDefence encountered an exception regarding ProtocolLib interaction.");
                    e.printStackTrace();
                }
            }
        });*/

        manager.addPacketListener(new PacketAdapter(ChunkDefence.get(), PacketType.Play.Client.UPDATE_SIGN) {
            @Override
            public void onPacketReceiving(PacketEvent event) {
                WrapperPlayClientUpdateSign packet = new WrapperPlayClientUpdateSign(event.getPacket());
                Player player = event.getPlayer();
                GuiSign menu = GuiManager.getInstance().getSign(player);

                if (menu == null)
                    return;

                Messages.DEBUG.sendConsole("Received sign update for " + player.getName() + " with lines: " + Arrays.toString(packet.getLines()));

                event.setCancelled(true);
                menu.onClose(packet.getLines());
            }
        });
    }

    public static void destruct() {
        if (!Statics.isProtocolLibAvailable)
            return;

        playerQueue.clear();
        ProtocolManager manager = ProtocolLibrary.getProtocolManager();
        if (manager != null)
            manager.removePacketListeners(ChunkDefence.get());
    }

    public static void checkQue() {
        playerQueue.forEach(PlayerInteractEvents::interactShop);
        playerQueue.clear();
    }
}
