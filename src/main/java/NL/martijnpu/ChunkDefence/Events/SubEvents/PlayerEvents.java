package NL.martijnpu.ChunkDefence.Events.SubEvents;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Gui.Book.Books;
import NL.martijnpu.ChunkDefence.Shops.FloatingText.ShopDisplayManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import NL.martijnpu.ChunkDefence.Utils.Updater;
import NL.martijnpu.ChunkDefence.Worlds.WorldManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;

public final class PlayerEvents implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent e) {
        Updater.onPlayerJoin(e.getPlayer());

        if (ConfigData.SERVER_PROTECT_ALL.get() || e.getPlayer().getWorld().equals(WorldManager.getInstance().getArenaWorld()))
            playerJoin(e.getPlayer());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent e) {
        playerQuit(e.getPlayer());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerMove(PlayerMoveEvent event) {
        ShopDisplayManager.getInstance().checkPlayerLook(event.getPlayer());
    }


    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerWorldChange(PlayerChangedWorldEvent event) {
        if (ConfigData.SERVER_PROTECT_ALL.get()) //Ignore check in bungee mode
            return;

        boolean fromArenaWorld = event.getFrom().equals(WorldManager.getInstance().getArenaWorld());
        boolean toArenaWorld = event.getPlayer().getWorld().equals(WorldManager.getInstance().getArenaWorld());

        Messages.DEBUG.sendConsole("Received worldChange from arena: " + fromArenaWorld + " to arena " + toArenaWorld);
        if (fromArenaWorld == toArenaWorld)
            return;

        if (fromArenaWorld)
            playerQuit(event.getPlayer());

        if (toArenaWorld)
            playerJoin(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        Arena arena = ArenaManager.getInstance().getArena(event.getPlayer());

        if (arena == null) {
            Messages.DEBUG.sendConsole("Received teleport of '" + event.getPlayer().getName() + "' without arena");
            return;
        }

        boolean fromInside = arena.isInsideArena(event.getFrom());
        boolean toInside = arena.isInsideArena(event.getTo());
        Messages.DEBUG.sendConsole("Received teleport of '" + event.getPlayer().getName() + "' fromInside: " + fromInside + " toInside " + toInside);

        if (fromInside != toInside) { //Only track teleportation when joining/leaving an arena
            //Deny join & leaving during a wave, but allow join during countdown
            if (arena.getWaveController() != null && !Permission.BYPASS_TELEPORT.hasPermission(event.getPlayer()) && (arena.getWaveController().isInWave() || (arena.getWaveController().isInCountDown() && !toInside))) {
                Messages.WAVE_PROGRESS.send(event.getPlayer());
                event.setCancelled(true);
                return;
            }

            if (toInside)
                Messages.ARENA_WELCOME.send(event.getPlayer());
        }
    }

    public static void playerJoin(Player player) {
        player.setPlayerTime(12000, false);

        if (player.getFirstPlayed() == 0) {
            Messages.SPAWN_FIRST.send(player);
            Statics.teleportToSpawn(player, true);
            Books.openBook(player, "welcome");
        } else {
            Messages.SPAWN_WELCOME.send(player);
            Arena arena = ArenaManager.getInstance().getArena(player);

            if (arena != null && ConfigData.SERVER_TELEPORT_JOIN.get())
                arena.playerJoin(player);
            else
                Statics.teleportToSpawn(player, true);
        }

        if (!Permission.BYPASS_GAMEMODE.hasPermission(player))
            player.setGameMode(ConfigData.DEFAULT_GAMEMODE.get());
    }

    public static void playerQuit(Player player) {
        Arena arena = ArenaManager.getInstance().getArena(player);
        if (arena != null)
            arena.playerQuit(player);
    }

}
