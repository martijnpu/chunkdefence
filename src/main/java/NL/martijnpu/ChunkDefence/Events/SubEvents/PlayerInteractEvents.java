package NL.martijnpu.ChunkDefence.Events.SubEvents;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Shops.Shop;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapData;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapDataManager;
import NL.martijnpu.ChunkDefence.Traps.TrapLocation;
import NL.martijnpu.ChunkDefence.Traps.TrapManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Worlds.WorldManager;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerPickupArrowEvent;
import org.bukkit.inventory.EquipmentSlot;

public final class PlayerInteractEvents implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerItemDrop(PlayerDropItemEvent e) {
        if (ConfigData.SERVER_PROTECT_ALL.get() || e.getPlayer().getWorld().equals(WorldManager.getInstance().getArenaWorld()))
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerConsume(PlayerItemConsumeEvent event) {
        //Cancel if Trap
        if (event.getItem().hasItemMeta() && TrapDataManager.getInstance().getTrapBase(event.getItem().getItemMeta()) != null)
            event.setCancelled(true);

        if ((ConfigData.SERVER_PROTECT_ALL.get() || event.getPlayer().getWorld().equals(WorldManager.getInstance().getArenaWorld()))) {
            if (ConfigData.REMOVE_FOOD_CONTAINER.get()) {
                if (event.getItem().getType() == Material.MILK_BUCKET || event.getItem().getType().name().contains("STEW")) {
                    event.setItem(event.getItem().subtract());

                    //for (PotionEffect effect : event.getPlayer().getActivePotionEffects())
                    //    event.getPlayer().removePotionEffect(effect.getType());
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onFoodLevelChange(FoodLevelChangeEvent e) {
        if (e.getItem() != null) // eating is allowed
            return;

        Arena arena = ArenaManager.getInstance().getArena((Player) e.getEntity());

        if (arena == null || arena.getWaveController() == null || !arena.getWaveController().isInWave())
            e.setCancelled(true); //Food decay outside the waves will be blocked
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerArrowPickup(PlayerPickupArrowEvent e) {
        if (!ConfigData.SERVER_PROTECT_ALL.get() && !e.getArrow().getWorld().equals(WorldManager.getInstance().getArenaWorld()))
            return;

        if (ConfigData.ANTI_ARROW_PICKUP.get())
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent event) {
        boolean cancelAction = false;
        boolean cancelInteract = false;

        if (!ConfigData.SERVER_PROTECT_ALL.get() && !event.getPlayer().getWorld().equals(WorldManager.getInstance().getArenaWorld()))
            return;

        if (!Permission.BYPASS_BUILD.hasPermission(event.getPlayer()) && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) { //Todo documentation
            cancelAction = true; // allow perms
            cancelInteract = true;
        }

        // Allow eating
        if (event.getItem() != null
                && event.getItem().getType().isEdible()
                && event.getAction() != Action.LEFT_CLICK_BLOCK) {
            cancelAction = false;
        }

        // Allow bell ring
        else if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getClickedBlock().getType().equals(Material.BELL)) {
            Arena arena = ArenaManager.getInstance().getArena(event.getClickedBlock().getLocation());
            if (arena != null && arena.getWaveController() != null && arena.getWaveController().getWaveData() != null)
                arena.getWaveController().getWaveData().highlightEntities();
            cancelAction = false;
            cancelInteract = false;
        }

        // Allow weapons
        else if (event.getItem() != null &&
                (event.getItem().getType().name().contains("HELMET")
                        || event.getItem().getType().name().contains("CHESTPLATE")
                        || event.getItem().getType().name().contains("LEGGINGS")
                        || event.getItem().getType().name().contains("BOOTS")
                        || event.getItem().getType().name().contains("SWORD")
                        || event.getItem().getType().name().contains("BOW")
                        || event.getItem().getType() == Material.SHIELD
                        || event.getItem().getType() == Material.TRIDENT)) {
            cancelAction = false;
        }

        // Allow Shop Right Click
        else if (event.getHand() == EquipmentSlot.HAND && event.getAction() == Action.RIGHT_CLICK_BLOCK && interactShop(event.getPlayer())) {
            event.setCancelled(true);
            event.setUseInteractedBlock(Event.Result.DENY);
            return;
        }

        // Allow Trap Destroy
        else if (event.getAction() == Action.LEFT_CLICK_BLOCK
                && TrapManager.getInstance().getTrapLocation(event.getClickedBlock()) != null) {
            TrapManager.getInstance().destroyTrap(event.getPlayer(), event.getClickedBlock());
            event.setCancelled(true);
            return;
        }

        // Allow Trap Place
        else if (event.getAction() == Action.RIGHT_CLICK_BLOCK
                && event.getItem() != null
                && TrapManager.isTrap(event.getItem().getItemMeta())) {
            TrapData trapData = TrapDataManager.getInstance().getTrapBase(event.getItem().getItemMeta());

            if (trapData != null && event.getClickedBlock() != null) {
                int durability = TrapManager.getDurability(event.getItem().getItemMeta());
                try {
                    TrapManager.getInstance().placeTrap(event.getPlayer(), event.getClickedBlock().getRelative(event.getBlockFace()), trapData, durability);
                } catch (IllegalAccessException ignored) {} //Already handled
                event.setCancelled(true);
                return;
            }
        }

        // Deny Trap GUI open
        else if (event.getAction() == Action.RIGHT_CLICK_BLOCK
                && TrapManager.getInstance().getTrapLocation(event.getClickedBlock()) != null) { //todo improve get trap (to the top of this method?)
            TrapLocation trapLocation = TrapManager.getInstance().getTrapLocation(event.getClickedBlock());

            //todo send trap information (Name - Owner - Uses left)
            Messages.CUSTOM.send(event.getPlayer(), "rightclick on a trap");
            event.setCancelled(true);
            return;
        }

        if (cancelAction)
            event.setCancelled(true);
        if (cancelInteract)
            event.setUseInteractedBlock(Event.Result.DENY);

    }

    public static boolean interactShop(Player player) {
        Shop shop = ShopManager.getInstance().getShop(player);
        if (shop != null) {
            shop.buyShop(player);
            return true;
        }
        return false;
    }
}
