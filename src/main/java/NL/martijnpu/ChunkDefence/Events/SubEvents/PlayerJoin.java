package NL.martijnpu.ChunkDefence.Events.SubEvents;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Gui.Book.Books;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import NL.martijnpu.ChunkDefence.Utils.Updater;
import NL.martijnpu.ChunkDefence.Worlds.WorldManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public final class PlayerJoin implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent e) {
        Updater.onPlayerJoin(e.getPlayer());

        if (ConfigData.SERVER_PROTECT_ALL.get() || e.getPlayer().getWorld().equals(WorldManager.getInstance().getArenaWorld()))
            playerJoin(e.getPlayer());
    }

    public static void playerJoin(Player player) {
        player.setPlayerTime(12000, false);

        if (player.getFirstPlayed() == 0) {
            Messages.SPAWN_FIRST.send(player);
            Statics.teleportToSpawn(player, true);
            Books.openBook(player, "welcome");
        } else {
            Messages.SPAWN_WELCOME.send(player);
            Arena arena = ArenaManager.getInstance().getArena(player);

            if (arena != null && ConfigData.SERVER_TELEPORT_JOIN.get())
                arena.playerJoin(player);
            else
                Statics.teleportToSpawn(player, true);
        }

        if (!Permission.BYPASS_GAMEMODE.hasPermission(player))
            player.setGameMode(ConfigData.DEFAULT_GAMEMODE.get());
    }
}
