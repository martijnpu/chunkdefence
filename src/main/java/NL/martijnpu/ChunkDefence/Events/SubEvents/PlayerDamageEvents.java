package NL.martijnpu.ChunkDefence.Events.SubEvents;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import NL.martijnpu.ChunkDefence.Worlds.WorldManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

public final class PlayerDamageEvents implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDamage(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player player))
            return;

        if (ConfigData.SERVER_PROTECT_ALL.get() || player.getWorld().equals(WorldManager.getInstance().getArenaWorld())) {
            Arena arena = ArenaManager.getInstance().getArena(player);
            if (arena == null || arena.getWaveController() == null || !arena.getWaveController().isInWave())
                e.setCancelled(true); //Deny damage when no wave is active
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerDeath(PlayerDeathEvent e) {
        Arena arena = ArenaManager.getInstance().getArena(e.getEntity());

        if (arena != null) { //Only protect is players does have an Arena
            arena.playerDeath(e.getEntity());
            e.getEntity().setBedSpawnLocation(Statics.getLocationFromString(ConfigData.SPAWN_LOCATION.get()), true);

            if ((ConfigData.GM_DEATH_RESET_INVENTORY.get(arena.getArenaData().GAMEMODE.get()))) {
                e.setKeepInventory(false);
                Messages.INV_RESET.send(e.getEntity());
            } else
                e.setKeepInventory(true);

        }
    }
}
