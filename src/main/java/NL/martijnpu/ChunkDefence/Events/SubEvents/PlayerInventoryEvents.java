package NL.martijnpu.ChunkDefence.Events.SubEvents;

import NL.martijnpu.ChunkDefence.Gui.GuiInventory;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;

public final class PlayerInventoryEvents implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInventoryClick(InventoryClickEvent e) {
        if (e.getViewers().isEmpty())
            return;

        Player player = (Player) e.getViewers().get(0);
        GuiInventory gui = GuiManager.getInstance().getGui(player);

        if (gui != null) {

            boolean isShift = e.getClick() == ClickType.SHIFT_RIGHT || e.getClick() == ClickType.SHIFT_LEFT || e.getClick() == ClickType.DOUBLE_CLICK || e.getClick() == ClickType.UNKNOWN || e.getClick() == ClickType.WINDOW_BORDER_LEFT || e.getClick() == ClickType.WINDOW_BORDER_RIGHT;

            if (!gui.canEdit(e.getSlot()) && (e.getClickedInventory() == null || e.getClickedInventory().getType() != InventoryType.PLAYER || isShift))
                e.setCancelled(true);

            if (e.getClickedInventory() != null && !e.getClickedInventory().getType().equals(InventoryType.PLAYER))
                gui.onClick(e.getSlot(), e.getClick());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInventoryClose(InventoryCloseEvent e) {
        if (e.getViewers().isEmpty())
            return;

        Player player = (Player) e.getViewers().get(0);
        GuiInventory inv = GuiManager.getInstance().getGui(player);

        if (inv != null && !inv.isHidden)
            inv.closeGuiSound();
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInvClick(InventoryInteractEvent e) {
        //Todo
    }
}
