package NL.martijnpu.ChunkDefence.Events.SubEvents;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapDataManager;
import NL.martijnpu.ChunkDefence.Utils.Keys;
import NL.martijnpu.ChunkDefence.Worlds.WorldManager;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.persistence.PersistentDataType;

import java.util.UUID;

public final class EntityEvents implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onCreatureSpawn(CreatureSpawnEvent e) {
        if (e.getEntity().getType() == EntityType.ARMOR_STAND)
            return;

        //Add Entity to existing wave
        Arena arena = ArenaManager.getInstance().getArena(e.getLocation());
        if (arena != null && arena.getWaveController() != null && arena.getWaveController().getWaveData() != null)
            arena.getWaveController().getWaveData().addEntity(e.getEntity());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onMobKill(EntityDeathEvent event) {
        if (!event.getEntity().hasMetadata(Keys.MOB_KILLCOINS_KEY)) // Check if it's an arena entity
            return;

        int coins = event.getEntity().getMetadata(Keys.MOB_KILLCOINS_KEY).get(0).asInt();
        if (coins == 0)
            return;

        Player player = event.getEntity().getKiller();
        boolean isTrap = player == null;

        if (isTrap) {
            String lastAttacked = event.getEntity().getPersistentDataContainer().get(Keys.ENTITY_LAST_ATTACKED, PersistentDataType.STRING);
            if (lastAttacked == null)
                return;

            player = ChunkDefence.get().getServer().getPlayer(UUID.fromString(lastAttacked));
            if (player == null)
                return;
        }

        Arena arena = ArenaManager.getInstance().getArena(player);
        if (arena != null)
            arena.onWaveMobKill(player, isTrap, coins, event.getEntity().getName());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onShootBow(EntityShootBowEvent event) {
        if (event.getBow() != null && event.getBow().hasItemMeta() && TrapDataManager.getInstance().getTrapBase(event.getBow().getItemMeta()) != null)
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onArrowHit(ProjectileHitEvent event) {
        if (!ConfigData.SERVER_PROTECT_ALL.get() && !event.getEntity().getWorld().equals(WorldManager.getInstance().getArenaWorld()))
            return;

        //Hit Bell Event
        if (event.getHitBlock() != null && event.getHitBlock().getType().equals(Material.BELL)) {
            Arena arena = ArenaManager.getInstance().getArena(event.getHitBlock().getLocation());
            if (arena != null && arena.getWaveController() != null && arena.getWaveController().getWaveData() != null)
                arena.getWaveController().getWaveData().highlightEntities();
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityTarget(EntityTargetEvent e) {
        if (e.getEntity().hasMetadata(Keys.MOB_KILLCOINS_KEY) && !(e.getTarget() instanceof Player)) {
            Arena arena = ArenaManager.getInstance().getArena(e.getEntity().getLocation());
            if (arena != null)
                e.setTarget(arena.getRandomOnlinePlayer());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityTeleport(EntityTeleportEvent event) {
        if (event.getEntity().getType().equals(EntityType.SHULKER) && event.getFrom().getWorld().equals(WorldManager.getInstance().getArenaWorld()))
            event.setCancelled(true);
    }
}
