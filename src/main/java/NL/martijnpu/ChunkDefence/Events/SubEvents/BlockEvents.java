package NL.martijnpu.ChunkDefence.Events.SubEvents;

import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Traps.TrapManager;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Worlds.WorldManager;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.world.StructureGrowEvent;

public final class BlockEvents implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreakEvent(BlockBreakEvent event) {
        if (ShopManager.getInstance().getShop(event.getBlock()) != null)
            event.setCancelled(true);

        if (Permission.BYPASS_BUILD.hasPermission(event.getPlayer()) && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) //Todo documentation
            return;

        if (ConfigData.SERVER_PROTECT_ALL.get() || event.getBlock().getWorld().equals(WorldManager.getInstance().getArenaWorld()))
            event.setCancelled(true);
        else if (TrapManager.getInstance().getTrapLocation(event.getBlock()) != null)
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPlaceEvent(BlockPlaceEvent event) {
        if (Permission.BYPASS_BUILD.hasPermission(event.getPlayer()) && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) //Todo documentation
            return;

        if (ConfigData.SERVER_PROTECT_ALL.get() || event.getBlock().getWorld().equals(WorldManager.getInstance().getArenaWorld()))
            event.setBuild(false);
        else if (TrapManager.getInstance().getTrapLocation(event.getBlock()) != null)
            event.setBuild(false);
        else if (ShopManager.getInstance().getShop(event.getBlock()) != null)
            event.setBuild(false);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockFade(BlockFadeEvent event) {
        if (ConfigData.SERVER_PROTECT_ALL.get() || event.getBlock().getWorld().equals(WorldManager.getInstance().getArenaWorld()))
            event.setCancelled(true);
        else if (TrapManager.getInstance().getTrapLocation(event.getBlock()) != null)
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onStructureGrow(StructureGrowEvent event) {
        if (ConfigData.SERVER_PROTECT_ALL.get() || event.getWorld().equals(WorldManager.getInstance().getArenaWorld())
                || TrapManager.getInstance().getTrapLocation(event.getLocation().getBlock()) != null)
            event.setCancelled(true);
    }
}
