package NL.martijnpu.ChunkDefence.Events.SubEvents;

import NL.martijnpu.ChunkDefence.Utils.Keys;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.GameMode;
import org.bukkit.entity.ArmorStand;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;

public final class ArmorStandEvents implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerArmorStandManipulateEvent(PlayerArmorStandManipulateEvent event) {
        //Protect Shop ArmorStands
        ArmorStand stand = event.getRightClicked();
        if (stand.getPersistentDataContainer().has(Keys.SHOP_KEY) || !(Permission.BYPASS_BUILD.hasPermission(event.getPlayer()) && event.getPlayer().getGameMode().equals(GameMode.CREATIVE))) //Todo documentation
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerArmorStandManipulate(PlayerArmorStandManipulateEvent event) {
        //Protect Shop ArmorStands
        ArmorStand stand = event.getRightClicked();
        if (stand.getPersistentDataContainer().has(Keys.SHOP_KEY) || !(Permission.BYPASS_BUILD.hasPermission(event.getPlayer()) && event.getPlayer().getGameMode().equals(GameMode.CREATIVE))) //Todo documentation
            event.setCancelled(true);
    }
}
