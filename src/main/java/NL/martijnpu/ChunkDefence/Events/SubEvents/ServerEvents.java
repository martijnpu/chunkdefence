package NL.martijnpu.ChunkDefence.Events.SubEvents;

import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Traps.TrapManager;
import NL.martijnpu.ChunkDefence.Worlds.WorldManager;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerLoadEvent;
import org.bukkit.event.world.ChunkLoadEvent;

public final class ServerEvents implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onServerLoad(ServerLoadEvent e) {
        ShopManager.getInstance().removeOldShopEntities();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onChunkLoad(ChunkLoadEvent e) {
        if (e.getWorld().equals(WorldManager.getInstance().getArenaWorld())) {
            for (Entity entity : e.getChunk().getEntities()) {
                TrapManager.getInstance().addEntity(entity);
                ShopManager.checkShopEntity(entity);
            }
        }
    }
}
