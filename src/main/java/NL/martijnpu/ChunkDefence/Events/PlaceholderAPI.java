package NL.martijnpu.ChunkDefence.Events;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Players.EconomyManager;
import NL.martijnpu.ChunkDefence.Scores.ScoreManager;
import NL.martijnpu.ChunkDefence.Scores.ScoreTypes.ScoreValue;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

/**
 * This class will automatically register as a placeholder expansion
 * when a jar including this class is added to the directory
 * {@code /plugins/PlaceholderAPI/expansions} on your server.
 * <p>
 * If you create such a class inside your own plugin, you have to
 * register it manually in your plugins {@code onEnable()} by using
 * {@code new YourExpansionClass().register();}
 */
public final class PlaceholderAPI extends PlaceholderExpansion {
    /**
     * Execute check for PlaceholderAPI before executing this method!
     * This is because of the extends
     */
    public static void registerPAPI() {
        if (ChunkDefence.get().getServer().getPluginManager().getPlugin("PlaceholderAPI") != null) {
            new PlaceholderAPI().register();
            Statics.isPAPIEnabled = true;
        }
    }

    /**
     * Because this is an internal class,
     * you must override this method to let PlaceholderAPI know to not unregister your expansion class when
     * PlaceholderAPI is reloaded
     *
     * @return true to persist through reloads
     */
    @Override
    public boolean persist() {
        return true;
    }

    /**
     * Because this is a internal class, this check is not needed
     * and we can simply return {@code true}
     *
     * @return Always true since it's an internal class.
     */
    @Override
    public boolean canRegister() {
        return true;
    }

    /**
     * The name of the person who created this expansion should go here.
     * <br>For convenience do we return the author from the plugin.yml
     *
     * @return The name of the author as a String.
     */
    @Override
    public @NotNull String getAuthor() {
        return ChunkDefence.get().getDescription().getAuthors().toString();
    }

    /**
     * The placeholder identifier should go here.
     * <br>This is what tells PlaceholderAPI to call our onRequest
     * method to obtain a value if a placeholder starts with our
     * identifier.
     * <br>This must be unique and can not contain % or _
     *
     * @return The identifier in {@code %<identifier>_<value>%} as String.
     */
    @Override
    public @NotNull String getIdentifier() {
        return "chunkdefence";
    }

    /**
     * This is the version of the expansion.
     * <br>You don't have to use numbers, since it is set as a String.
     * <p>
     * For convenience do we return the version from the plugin.yml
     *
     * @return The version as a String.
     */
    @Override
    public @NotNull String getVersion() {
        return ChunkDefence.get().getDescription().getVersion();
    }

    /**
     * This is the method called when a placeholder with our identifier
     * is found and needs a value.
     * <br>We specify the value identifier in this method.
     * <br>Since version 2.9.1 can you use OfflinePlayers in your requests.
     *
     * @param player     A {@link Player Player}.
     * @param identifier A String containing the identifier/value.
     * @return possibly-null String of the requested identifier.
     */
    @Override
    public String onPlaceholderRequest(Player player, @NotNull String identifier) {
        if (player == null)
            return "";

        String[] args = identifier.split("_");

        switch (args[0]) {
            // %chunkdefence_wave%
            case "wave": {
                Arena arena = ArenaManager.getInstance().getArena(player);
                if (arena == null)
                    return Messages.PLACEHOLDER_WAVE_NONE.get();
                return String.valueOf(arena.getArenaData().WAVE.get());
            }

            // %chunkdefence_info%
            case "info": {
                Arena arena = ArenaManager.getInstance().getArena(player);
                if (arena == null)
                    return Messages.PLACEHOLDER_NONE_ARENA.get();

                if (arena.getWaveController() == null)
                    return Messages.PLACEHOLDER_WAVE_NONE.get();
                return arena.getWaveController().getStatus();
            }

            // %chunkdefence_balance%
            case "balance": {
                return String.valueOf(Math.round(EconomyManager.getInstance().getBalance(player) * 100.0) / 100.0);
            }

            // %chunkdefence_gamemode%
            case "gamemode": {
                Arena arena = ArenaManager.getInstance().getArena(player);
                if (arena == null)
                    return Messages.PLACEHOLDER_NONE_GAMEMODE.get();
                return arena.getArenaData().GAMEMODE.get();
            }

            // %chunkdefence_wavetop_<gamemode>_<1..>_<daily/weekly/monthly/forever>%
            case "wavetop": {
                if (args.length <= 3)
                    return "&4Invalid format: %chunkdefence_wavetop_<gamemode>_<1..>_<daily/weekly/monthly/forever>%";

                if (!ScoreManager.getInstance().isValidGamemode(args[1]))
                    return Messages.format("&4Unknown gamemode &f" + args[1]);

                try {
                    int index = Integer.parseInt(args[2]);
                    ScoreValue score = switch (args[3].toLowerCase()) {
                        case "daily" -> ScoreManager.getInstance().arenaWaveHighscore.get(args[1]).getDaily(index);
                        case "weekly" -> ScoreManager.getInstance().arenaWaveHighscore.get(args[1]).getWeeky(index);
                        case "monthly" -> ScoreManager.getInstance().arenaWaveHighscore.get(args[1]).getMonthy(index);
                        case "forever" -> ScoreManager.getInstance().arenaWaveHighscore.get(args[1]).getForever(index);
                        default -> null;
                    };

                    if (score == null)
                        return Messages.PLACEHOLDER_TOP_EMPTY.get(String.valueOf(index));

                    OfflinePlayer op = score.getOfflinePlayer();

                    return Messages.PLACEHOLDER_TOP_WAVE.get(args[2], args[1], String.valueOf(score.getValue()), op.getName());
                } catch (NumberFormatException ex) {
                    return Messages.EXCEPT_NUMBER.get(args[2]);
                }
            }

            // %chunkdefence_killtop_<1..>_<daily/weekly/monthly/forever>%
            case "killtop": {
                if (args.length <= 2)
                    return "&4Invalid format: %chunkdefence_killtop_<1..>_<daily/weekly/monthly/forever>%";

                try {
                    int index = Integer.parseInt(args[1]);
                    ScoreValue score = switch (args[2].toLowerCase()) {
                        case "daily" -> ScoreManager.getInstance().mobKills.getDaily(index);
                        case "weekly" -> ScoreManager.getInstance().mobKills.getWeeky(index);
                        case "monthly" -> ScoreManager.getInstance().mobKills.getMonthy(index);
                        case "forever" -> ScoreManager.getInstance().mobKills.getForever(index);
                        default -> null;
                    };

                    if (score == null)
                        return Messages.PLACEHOLDER_TOP_EMPTY.get(args[1]);

                    OfflinePlayer op = score.getOfflinePlayer();

                    return Messages.PLACEHOLDER_TOP_KILL.get(args[1], op.getName() == null ? "Unnamed" : op.getName(), String.valueOf(score.getValue()));
                } catch (NumberFormatException ex) {
                    return Messages.EXCEPT_NUMBER.get(args[1]);
                }
            }

            // We return null if an invalid placeholder (f.e. %chunkdefence_placeholder3%) was provided
            default:
                return null;
        }
    }
}
