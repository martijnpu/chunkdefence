package NL.martijnpu.ChunkDefence.Events;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Events.SubEvents.*;

public final class EventManager {
    public static void setupEvents() {
        var manager = ChunkDefence.get().getServer().getPluginManager();

        manager.registerEvents(new ArmorStandEvents(), ChunkDefence.get());
        manager.registerEvents(new BlockEvents(), ChunkDefence.get());
        manager.registerEvents(new EntityEvents(), ChunkDefence.get());
        manager.registerEvents(new PlayerDamageEvents(), ChunkDefence.get());
        manager.registerEvents(new PlayerEvents(), ChunkDefence.get());
        manager.registerEvents(new PlayerInteractEvents(), ChunkDefence.get());
        manager.registerEvents(new PlayerInventoryEvents(), ChunkDefence.get());
        manager.registerEvents(new ServerEvents(), ChunkDefence.get());
    }
}
