package NL.martijnpu.ChunkDefence.Data;

import NL.martijnpu.ChunkDefence.Utils.Messages;

import java.util.List;

public final class MessageData {

    public static String getMessage(String path) {
        if (Messages.CUSTOM.getPath().equals(path)) //Skip file check for custom (and related)
            return path;
        return FileHandler.MESSAGES_FILE.get().getString(("prefix".equals(path) ? path : "messages." + path), path);
    }

    public static List<String> getMessageList(String path) {
        return FileHandler.MESSAGES_FILE.get().getStringList("messages." + path);
    }
}
