package NL.martijnpu.ChunkDefence.Data.Utils;

public final class ConfigKeyInstance<T> extends ConfigKey<T> {
    private final String key;

    public ConfigKeyInstance(ConfigKey<T> configKey, String key) {
        super(configKey.function, configKey.configFile, configKey.path, configKey.def);
        this.key = key;
    }

    public boolean isSet() {
        return super.isSet(key);
    }

    public void setDefault(boolean force) {
        super.setDefault(force, key);
    }

    public void set(T value) {
        super.set(value, key);
    }

    public T get() {
        return super.get(key);
    }

    public String getPath() {
        return super.getPath(key);
    }
}
