package NL.martijnpu.ChunkDefence.Data.Utils;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

public final class ConfigFile {
    private final File file;
    private YamlConfiguration configuration = null;
    private boolean save = false;

    public ConfigFile(String path) {
        file = new File(ChunkDefence.get().getDataFolder(), path);
    }

    public YamlConfiguration get() {
        if (configuration == null)
            configuration = loadConfig(file);
        return configuration;
    }

    /**
     * Delete the cache to retrieve it next time when it's called.
     */
    public void reload() {
        configuration = null;
    }

    /**
     * Mark the configFile as edited and ready to save
     */
    public void save() {
        save = true;
    }

    public void saveIfEdit() {
        if (save) {
            saveConfig(configuration, file);
            save = false;
        }
    }

    private YamlConfiguration loadConfig(File file) {
        if (!file.exists())
            ChunkDefence.get().saveResource(file.getName(), false);
        YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

        if (config.getKeys(false).isEmpty())
            config = backupConfig(file);
        return config;
    }

    private YamlConfiguration backupConfig(File originFile) {
        String backupName = "backups/" + originFile.getName().replace(".yml", "") + "_backup";
        File backupFile = new File(ChunkDefence.get().getDataFolder(), "backups");
        if (!backupFile.exists() && !backupFile.mkdirs())
            Messages.WARN.sendConsole("Failed to create backup directory");
        int i = 1;
        while (backupFile.exists()) {
            backupFile = new File(ChunkDefence.get().getDataFolder(), backupName + "_" + i + ".yml");
            i++;
        }
        Messages.WARN.sendConsole("Renaming to " + backupFile.getName());
        if (!originFile.renameTo(backupFile))
            Messages.WARN.sendConsole("Failed creating backup");
        originFile.delete();
        ChunkDefence.get().saveResource(originFile.getName(), true);
        return YamlConfiguration.loadConfiguration(originFile);
    }

    private void saveConfig(YamlConfiguration config, File file) {
        if (config == null)
            return;
        try {
            config.save(file);
        } catch (IOException ex) {
            ChunkDefence.get().getLogger().log(Level.SEVERE, "Could not save config to " + file, ex);
        }
    }
}
