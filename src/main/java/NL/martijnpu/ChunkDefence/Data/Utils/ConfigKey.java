package NL.martijnpu.ChunkDefence.Data.Utils;

import NL.martijnpu.ChunkDefence.Utils.Messages;

public class ConfigKey<T> {
    protected final TriFunction<ConfigFile, String, T, T> function;
    protected final ConfigFile configFile;
    protected final String path;
    protected final T def;
    private boolean hidden = false;

    ConfigKey(TriFunction<ConfigFile, String, T, T> function, ConfigFile configFile, String path, T def) {
        this.function = function;
        this.configFile = configFile;
        this.path = path;
        this.def = def;
    }

    /**
     * Builder method to hide the key in the config
     */
    public ConfigKey<T> hide() {
        hidden = true;
        return this;
    }

    /**
     * Check if the path is already set
     *
     * @param pathReplacement Optional to replace the % in the path. If % present but parameter not defined 'default' is used
     */
    public boolean isSet(String... pathReplacement) {
        return configFile.get().isSet(handleReplacements(path, pathReplacement));
    }

    /**
     * Write the default value is the key is not present yet
     *
     * @param pathReplacement Optional to replace the % in the path. If % present but parameter not defined 'default' is used
     * @param force           Force writing if the parameter is already present
     */
    public void setDefault(boolean force, String... pathReplacement) {
        if (!force) {
            if (hidden || isSet(pathReplacement))
                return;
            Messages.WARN.sendConsole("&4Adding missing key " + handleReplacements(path, pathReplacement) + " as '" + def + "'");
        }
        set(def, pathReplacement);
    }

    public void set(T value, String... pathReplacement) {
        if (value instanceof Enum<?> enumInstance)
            configFile.get().set(handleReplacements(path, pathReplacement), enumInstance.name().toUpperCase());
        else
            configFile.get().set(handleReplacements(path, pathReplacement), value);
        configFile.save();
    }

    /**
     * @param pathReplacement Optional to replace the % in the path. If % present but parameter not defined 'default' is used
     * @return Get config value. If not defined/invalid get default
     */
    public T get(String... pathReplacement) {
        if (!path.contains("%"))
            return function.apply(configFile, path, def);

        //Try getting the default config if the custom is not set
        return function.apply(configFile, handleReplacements(path, pathReplacement), function.apply(configFile, handleReplacements(path, "default"), def));
    }

    public String getPath(String... pathReplacement) {
        return handleReplacements(path, pathReplacement);
    }

    protected String handleReplacements(String path, String... pathReplacement) {
        if (pathReplacement.length == 0) //Todo used for gamemode
            pathReplacement = new String[]{"default"};

        for (String string : pathReplacement)
            path = path.replaceFirst("%", string);
        return path;
    }
}