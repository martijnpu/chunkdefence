package NL.martijnpu.ChunkDefence.Data.Utils;

import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public interface ConfigKeyAdapter {
    TriFunction<ConfigFile, String, Boolean, Boolean> GET_BOOLEAN = (a, b, c) -> a.get().getBoolean(b, c);
    TriFunction<ConfigFile, String, Integer, Integer> GET_INTEGER = (a, b, c) -> a.get().getInt(b, c);
    TriFunction<ConfigFile, String, Double, Double> GET_DOUBLE = (a, b, c) -> a.get().getDouble(b, c);
    TriFunction<ConfigFile, String, String, String> GET_STRING = (a, b, c) -> a.get().getString(b, c);
    TriFunction<ConfigFile, String, Object, Object> GET_OBJECT = (a, b, c) -> a.get().get(b, c);
    TriFunction<ConfigFile, String, List<String>, List<String>> GET_STRINGLIST = (a, b, c) -> a.get().getStringList(b);
    TriFunction<ConfigFile, String, List<String>, List<String>> GET_CONFIGKEYLIST = (a, b, c) -> {
        ConfigurationSection cfg = a.get().getConfigurationSection(b);
        return cfg == null ? c : new ArrayList<>(cfg.getKeys(false)).stream().sorted().collect(Collectors.toList());
    };

    static <T> ConfigKey<T> customKey(ConfigFile configFile, String path, T def, TriFunction<ConfigFile, String, T, T> function) {
        return new ConfigKey<>(function, configFile, path, def);
    }

    static ConfigKey<Boolean> booleanKey(ConfigFile configFile, String path, boolean def) {
        return customKey(configFile, path, def, GET_BOOLEAN);
    }

    static ConfigKey<Integer> integerKey(ConfigFile configFile, String path, int def) {
        return customKey(configFile, path, def, GET_INTEGER);
    }

    static ConfigKey<Double> doubleKey(ConfigFile configFile, String path, double def) {
        return customKey(configFile, path, def, GET_DOUBLE);
    }

    static ConfigKey<String> stringKey(ConfigFile configFile, String path, String def) {
        return customKey(configFile, path, def, GET_STRING);
    }

    static ConfigKey<List<String>> stringListKey(ConfigFile configFile, String path, String[] def) {
        List<String> list = Arrays.asList(def);
        return customKey(configFile, path, list, GET_STRINGLIST);
    }

    static ConfigKey<Object> objectKey(ConfigFile configFile, String path, Object def) {
        return customKey(configFile, path, def, GET_OBJECT);
    }

    static <T extends Enum<T>> ConfigKey<T> enumKey(ConfigFile configFile, String path, T def) {

        //Not possible to create constant outside of method scope
        TriFunction<ConfigFile, String, T, T> GET_ENUM = (a, b, c) -> {
            String string = a.get().getString(b, c.name().toUpperCase());

            for (Enum<T> candidate : c.getClass().getEnumConstants()) {
                if (candidate.name().equalsIgnoreCase(string)) {
                    return (T) candidate;
                }
            }
            return def;
        };


        return customKey(configFile, path, def, GET_ENUM);
    }

    static ConfigKey<List<String>> configListKey(ConfigFile configFile, String path) {
        return customKey(configFile, path, new ArrayList<>(), GET_CONFIGKEYLIST);
    }
}

