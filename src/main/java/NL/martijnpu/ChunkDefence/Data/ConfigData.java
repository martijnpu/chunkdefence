package NL.martijnpu.ChunkDefence.Data;


import NL.martijnpu.ChunkDefence.Data.Utils.ConfigKey;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Biome;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static NL.martijnpu.ChunkDefence.Data.FileHandler.*;
import static NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyAdapter.*;

public final class ConfigData {
    //region Config
    public static final ConfigKey<Boolean> DEBUG = booleanKey(SETTINGS_FILE, "debug", false).hide();
    public static final ConfigKey<Boolean> BSTATS = booleanKey(SETTINGS_FILE, "bstats", true).hide();
    public static final ConfigKey<Boolean> HIDE_WARNING = booleanKey(SETTINGS_FILE, "hide-warns", false).hide();
    public static final ConfigKey<Boolean> SHOP_RELATIVE_PLAYER = booleanKey(SETTINGS_FILE, "shop.relative-to-player", true);
    public static final ConfigKey<Integer> SHOP_COOLDOWN = integerKey(SETTINGS_FILE, "shop.cooldown", 1);
    public static final ConfigKey<String> SHOP_LOCATION = stringKey(SETTINGS_FILE, "shop.warp-location", "null");
    public static final ConfigKey<Integer> PARTICLE_FREQUENCY = integerKey(SETTINGS_FILE, "update-frequency.particle-ticks", 2);
    public static final ConfigKey<Boolean> VAULT = booleanKey(SETTINGS_FILE, "hooks.vault", true);
    public static final ConfigKey<Boolean> PROTOCOLLIB = booleanKey(SETTINGS_FILE, "hooks.protocollib", true);
    public static final ConfigKey<String> SPAWN_LOCATION = stringKey(SETTINGS_FILE, "server.spawn-location", "null");
    public static final ConfigKey<Boolean> ANTI_ARROW_PICKUP = booleanKey(SETTINGS_FILE, "server.anti-arrow-pickup", true);
    public static final ConfigKey<GameMode> DEFAULT_GAMEMODE = enumKey(SETTINGS_FILE, "server.default-gamemode", GameMode.ADVENTURE);
    public static final ConfigKey<Boolean> REMOVE_FOOD_CONTAINER = booleanKey(SETTINGS_FILE, "server.remove-food-container", true);
    public static final ConfigKey<Integer> FREQUENCY_SCOREBOARD = integerKey(SETTINGS_FILE, "update-frequency.scoreboard-seconds", 30);
    public static final ConfigKey<String> ARENA_MOB_SPAWN = stringKey(SETTINGS_FILE, "arena.mob-spawn-block", "OBSIDIAN");
    public static final ConfigKey<Integer> ARENA_BUY_COOLDOWN = integerKey(SETTINGS_FILE, "arena.cooldown", 60);
    public static final ConfigKey<Integer> ARENA_TELEPORT_DELAY = integerKey(SETTINGS_FILE, "arena.teleport-new-arena-delay", 15);
    public static final ConfigKey<Boolean> ARENA_RESET_ON_LEAVE = booleanKey(SETTINGS_FILE, "arena.reset-on-leave", true);
    public static final ConfigKey<Boolean> SERVER_TELEPORT_JOIN = booleanKey(SETTINGS_FILE, "server.teleport-on-join", true);
    public static final ConfigKey<Boolean> SERVER_PROTECT_ALL = booleanKey(SETTINGS_FILE, "server.protect-all-worlds", true);

    //Gamemodes
    public static final ConfigKey<List<String>> GAMEMODES = configListKey(SETTINGS_FILE, "gamemodes");
    public static final ConfigKey<String> GM_NAME = stringKey(SETTINGS_FILE, "gamemodes.%.name", "&6Default");
    public static final ConfigKey<Integer> GM_MAX_HEALTH = integerKey(SETTINGS_FILE, "gamemodes.%.max-health", 20);
    public static final ConfigKey<String> GM_MATERIAL = stringKey(SETTINGS_FILE, "gamemodes.%.material", "GRASS_BLOCK");
    public static final ConfigKey<String> GM_SCHEMATIC = stringKey(SETTINGS_FILE, "gamemodes.%.schematic.default", "defaultSchematic");
    public static final ConfigKey<Boolean> GM_ARENA_SAVE_BOUGHT = booleanKey(SETTINGS_FILE, "gamemodes.%.arena.save-bought", true);
    public static final ConfigKey<Boolean> GM_ARENA_ALLOW_CHANGE = booleanKey(SETTINGS_FILE, "gamemodes.%.arena.allow-change", true);
    public static final ConfigKey<Boolean> GM_ARENA_ALLOW_INVITE = booleanKey(SETTINGS_FILE, "gamemodes.%.arena.allow-invite", true);
    public static final ConfigKey<Boolean> GM_DEATH_RESTART_WAVE = booleanKey(SETTINGS_FILE, "gamemodes.%.player-death.restart-wave", true);
    public static final ConfigKey<Boolean> GM_DEATH_RESET_WAVES = booleanKey(SETTINGS_FILE, "gamemodes.%.player-death.reset-waves", false);
    public static final ConfigKey<Boolean> GM_DEATH_RESET_INVENTORY = booleanKey(SETTINGS_FILE, "gamemodes.%.player-death.reset-inventory", false);
    public static final ConfigKey<Boolean> GM_DEATH_RESET_ARENA = booleanKey(SETTINGS_FILE, "gamemodes.%.player-death.reset-arena", false);
    public static final ConfigKey<Integer> GM_DIFFICULTY_DEFAULT = integerKey(SETTINGS_FILE, "gamemodes.%.difficulty.default", 100);
    public static final ConfigKey<Integer> GM_DIFFICULTY_ON_WIN = integerKey(SETTINGS_FILE, "gamemodes.%.difficulty.on-win", 5);
    public static final ConfigKey<Integer> GM_DIFFICULTY_ON_LOSE = integerKey(SETTINGS_FILE, "gamemodes.%.difficulty.on-lose", 8);
    public static final ConfigKey<Boolean> GM_DIFFICULTY_ADAPTIVE = booleanKey(SETTINGS_FILE, "gamemodes.%.difficulty.adaptive", true);
    public static final ConfigKey<Integer> GM_DIFFICULTY_MINIMUM = integerKey(SETTINGS_FILE, "gamemodes.%.difficulty.minimum", 50);
    public static final ConfigKey<Integer> GM_WAVE_TIME_BETWEEN = integerKey(SETTINGS_FILE, "gamemodes.%.wave.time-between", 60);
    public static final ConfigKey<Integer> GM_WAVE_COINS_SUCCEED = integerKey(SETTINGS_FILE, "gamemodes.%.wave.coins-succeed", 10);
    public static final ConfigKey<Boolean> GM_WAVE_HEAL_ON_SUCCEED = booleanKey(SETTINGS_FILE, "gamemodes.%.wave.heal-on-succeed", true);
    //Arena Settings
    public static final ConfigKey<String> ARENA_WORLD = stringKey(ARENA_FILE, "settings.world", "arenaWorld");
    public static final ConfigKey<Biome> ARENA_BIOME = enumKey(ARENA_FILE, "settings.biome", Biome.JUNGLE);
    public static final ConfigKey<Integer> ARENA_SPACE = integerKey(ARENA_FILE, "settings.space-between", 500);
    public static final ConfigKey<Integer> ARENA_HEIGHT = integerKey(ARENA_FILE, "settings.height", 70);
    private static final ConfigData instance = new ConfigData();
    private static List<ConfigKey<?>> KEYS;

    private ConfigData() {
        KEYS = new ArrayList<>();
        try {
            for (Field declaredField : getClass().getDeclaredFields())
                if (declaredField.getType().equals(ConfigKey.class))
                    KEYS.add((ConfigKey<?>) declaredField.get(this));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static ConfigData getInstance() {
        return instance;
    }

    /**
     * reload all settings
     *
     * @param hard false is only the config, shops and mobs. Hard also includes Arena (full plugin restart needed)
     */
    public void reload(boolean hard) {
        SETTINGS_FILE.reload();
        TRAPS_FILE.reload();
        SHOPS_FILE.reload();
        MOBS_FILE.reload();
        if (hard)
            ARENA_FILE.reload();

        for (ConfigKey<?> key : KEYS)
            key.setDefault(false);

        checkForValidData();

        if (DEBUG.get())
            Messages.DEBUG.sendConsole("Debugmodus enabled. Be aware for spam!");

        if (!PROTOCOLLIB.get()) Statics.isProtocolLibAvailable = false;
    }

    /**
     * Check config for invalid fields and repair if needed
     */
    private void checkForValidData() {
        try {
            Material.getMaterial(ARENA_MOB_SPAWN.get());
        } catch (Exception ex) {
            Messages.WARN.sendConsole("&cThe config option " + ARENA_MOB_SPAWN.getPath() + " doesn't contain a valid material. Resetting to default value...");
            ARENA_MOB_SPAWN.setDefault(true);
        }

        //Make sure default is always run as first
        List<String> list = GAMEMODES.get();
        list.remove(" default");
        list.add(0, " default");

        for (String x : list) {
            try {
                Material.getMaterial(GM_MATERIAL.get(x));
            } catch (Exception ex) {
                Messages.WARN.sendConsole("&cThe config option " + GM_MATERIAL.getPath(x) + " doesn't contain a valid material. Resetting to default value...");
                GM_MATERIAL.setDefault(true);
            }
        }

        String shopLocation = SHOP_LOCATION.get();
        if (!"null".equalsIgnoreCase(shopLocation)) {
            Location location = Statics.getLocationFromString(shopLocation);
            if (location == null) {
                Messages.WARN.sendConsole("&cThe config option " + SHOP_LOCATION.getPath() + " doesn't contain a valid Location. Removing current value.");
                SHOP_LOCATION.setDefault(true);
            }
        }

        String spawnLocation = SPAWN_LOCATION.get();
        if (!"null".equalsIgnoreCase(spawnLocation)) {
            Location location = Statics.getLocationFromString(spawnLocation);
            if (location == null) {
                Messages.WARN.sendConsole("&cThe config option " + SPAWN_LOCATION.getPath() + " doesn't contain a valid Location. Removing current value.");
                SPAWN_LOCATION.setDefault(true);
            }
        }
    }

    public void printInitData() {
        Messages.PLUGIN.sendConsole("Loading " + KEYS.size() + " config values");
    }
}
