package NL.martijnpu.ChunkDefence.Data;


import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Data.Utils.ConfigFile;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class FileHandler {
    private static final FileHandler instance = new FileHandler();
    public static final ConfigFile MESSAGES_FILE = new ConfigFile("/messages.yml");
    public static final ConfigFile PLAYERDATA_FILE = new ConfigFile("/players.yml");
    public static final ConfigFile SCORES_FILE = new ConfigFile("/highscores.yml");
    public static final ConfigFile SHOPS_FILE = new ConfigFile("/shops.yml");
    public static final ConfigFile TRAPS_FILE = new ConfigFile("/traps.yml");
    public static final ConfigFile ARENA_FILE = new ConfigFile("/arenas.yml");
    public static final ConfigFile MOBS_FILE = new ConfigFile("/mobs.yml");
    public static final ConfigFile SETTINGS_FILE = new ConfigFile("/settings.yml");
    private final File schematicFolder = new File(ChunkDefence.get().getDataFolder(), "/schematics");


    private FileHandler() {}

    public static FileHandler get() {
        return instance;
    }

    public static boolean setNotExists(YamlConfiguration config, String path, Object defaultValue) {
        if (config.isSet(path))
            return false;
        Messages.WARN.sendConsole("Missing data at '" + path + "'. Setting the default now...");
        config.set(path, defaultValue);
        return true;
    }

    public List<String> getAllSchematics() {
        if (schematicFolder.isDirectory() && schematicFolder.list() != null)
            return Arrays.stream(schematicFolder.list()).map(s -> s.replaceAll(".[a-zA-Z]*$", "")).collect(Collectors.toList());
        return new ArrayList<>();
    }

    /**
     * Trigger all needed Files to save itself if edited.
     * Run this method async to prevent lag
     */
    public void saveAllFiles() {
        MESSAGES_FILE.saveIfEdit();
        PLAYERDATA_FILE.saveIfEdit();
        SCORES_FILE.saveIfEdit();
        SHOPS_FILE.saveIfEdit();
        TRAPS_FILE.saveIfEdit();
        ARENA_FILE.saveIfEdit();
        MOBS_FILE.saveIfEdit();
        SETTINGS_FILE.saveIfEdit();
    }

    public boolean isWorldAvailable() {
        return new File(ChunkDefence.get().getServer().getWorldContainer().getAbsolutePath() + "/" + ConfigData.ARENA_WORLD.get()).exists();
    }
}
