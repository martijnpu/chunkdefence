package NL.martijnpu.ChunkDefence.Players;

import NL.martijnpu.ChunkDefence.Data.FileHandler;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public final class PlayerDataManager {
    private static PlayerDataManager instance;
    private final List<PlayerData> players = new ArrayList<>();

    private PlayerDataManager() {
        ConfigurationSection cs = FileHandler.PLAYERDATA_FILE.get().getConfigurationSection("user");

        if (cs == null)
            return;

        for (String key : cs.getKeys(false)) {
            try {
                players.add(new PlayerData(UUID.fromString(key)));
            } catch (IllegalArgumentException ex) {
                Messages.WARN.sendConsole(ex.toString());
            }
        }
    }

    public static PlayerDataManager getInstance() {
        if (instance == null)
            instance = new PlayerDataManager();
        return instance;
    }

    public static void resetInstance() {
        instance = null;
        FileHandler.PLAYERDATA_FILE.reload();
    }

    public void printInitData() {
        if (players.isEmpty())
            Messages.WARN.sendConsole("Loading an empty user file. This is an error or first startup.");
        else
            Messages.PLUGIN.sendConsole("Loading " + players.size() + " users.");
    }

    public PlayerData getUserData(UUID uuid) {
        PlayerData pd = players.stream().filter(player -> player.uuid.equals(uuid)).findFirst().orElse(null);
        if (pd == null) {
            pd = new PlayerData(uuid);
            players.add(pd);
        }
        return pd;
    }

    public List<UUID> getAllUsers() {
        return players.stream().map(x -> x.uuid).collect(Collectors.toList());
    }
}
