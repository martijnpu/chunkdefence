package NL.martijnpu.ChunkDefence.Players;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Data.Utils.ConfigKey;
import NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyInstance;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.TimeHandler;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.IntStream;

import static NL.martijnpu.ChunkDefence.Data.FileHandler.PLAYERDATA_FILE;
import static NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyAdapter.*;

public final class PlayerData {
    final UUID uuid;
    public final ConfigKeyInstance<Double> BALANCE;

    private final ConfigKey<List<String>> SAVED_INVENTORY = configListKey(PLAYERDATA_FILE, "user.%.gamemode.%.inventory").hide();
    private final ConfigKey<Object> SAVED_INVENTORY_ITEM = objectKey(PLAYERDATA_FILE, "user.%.gamemode.%.inventory.%", null).hide();
    private final ConfigKey<Double> SAVED_BALANCE = doubleKey(PLAYERDATA_FILE, "user.%.gamemode.%.balance", 0.0).hide();

    private final Map<Arena, Long> invited = new HashMap<>();
    private long lastShopBuy = 0;
    private long lastArenaCreate = 0;

    PlayerData(UUID uuid) {
        this.uuid = uuid;
        BALANCE = new ConfigKeyInstance<>(doubleKey(PLAYERDATA_FILE, "user.%.balance", 0.0).hide(), uuid.toString());
    }

    void addBalance(double balance) {
        BALANCE.set(BALANCE.get() + balance);
    }

    void removeBalance(double balance) {
        BALANCE.set(BALANCE.get() - balance);
    }

    /**
     * Add an arena to the list of invites
     */
    public void addInvite(Arena arena) {
        invited.put(arena, TimeHandler.getCurrSec());
    }

    /**
     * Remove an arena to the list of invites
     *
     * @return Elapsed time since invite was send.
     * -1 = no invite found
     */
    public long removeInvite(Arena arena) {
        Long timestamp = invited.remove(arena);
        if (timestamp == null)
            return -1;
        return TimeHandler.getCurrSec() - timestamp;
    }

    public long getLastShopTime() {
        return lastShopBuy;
    }

    public void updateLastStopTime() {
        lastShopBuy = TimeHandler.getCurrSec();
    }

    /**
     * Check how many times still needed to wait since last arena created
     * If this is the case, the last time will be updated.
     *
     * @return Time still needed to wait before @timePassed
     */
    public long lastArenaCreatePassed(long timePassed) {
        long delta = timePassed - (TimeHandler.getCurrSec() - lastArenaCreate);
        if (delta <= 0)
            lastArenaCreate = TimeHandler.getCurrSec();
        return delta;
    }

    /**
     * Save the current inventory of a player
     *
     * @param gamemode The specified gamemode
     */
    public void saveInventoryGamemode(String gamemode) {
        Player player = ChunkDefence.get().getServer().getPlayer(uuid);
        if (player == null || gamemode.isBlank())
            return;

        SAVED_INVENTORY.set(null, uuid.toString(), gamemode);
        Messages.DEBUG.sendConsole("Saving inventory of " + player.getName() + " with " + player.getInventory().getSize() + " itemStacks");

        Inventory inventory = player.getInventory();
        for (int i = 0; i < inventory.getSize(); i++) {
            ItemStack itemStack = inventory.getItem(i);
            if (itemStack != null)
                SAVED_INVENTORY_ITEM.set(itemStack, uuid.toString(), gamemode, String.valueOf(i));
        }

        IntStream.range(0, player.getInventory().getSize()).forEach(i -> player.getInventory().setItem(i, null)); //Cannot be in the same loop
    }

    /**
     * Retrieve the saved inventory of a player
     *
     * @param gamemode The specified gamemode
     */
    public void retrieveInventoryGamemode(String gamemode) {
        Player player = ChunkDefence.get().getServer().getPlayer(uuid);
        if (player == null || gamemode.isBlank())
            return;

        IntStream.range(0, player.getInventory().getSize()).forEach(i -> player.getInventory().setItem(i, null));
        List<String> indexes = SAVED_INVENTORY.get(uuid.toString(), gamemode);

        Messages.DEBUG.sendConsole("Loading inventory of " + player.getName() + " with " + indexes.size() + " itemStacks");

        for (String index : indexes) {
            ItemStack itemStack = (ItemStack) SAVED_INVENTORY_ITEM.get(uuid.toString(), gamemode, index);

            if (itemStack == null) {
                Messages.WARN.sendConsole("Unable to process value '" + SAVED_INVENTORY_ITEM.getPath(uuid.toString(), gamemode, index) + "' to itemstack. Skipping...");
                continue;
            }

            try {
                int i = Integer.parseInt(index);
                player.getInventory().setItem(i, itemStack);

                SAVED_INVENTORY_ITEM.set(null, uuid.toString(), gamemode, index);
            } catch (NumberFormatException ex) {
                Messages.WARN.sendConsole("Unable to convert '" + SAVED_INVENTORY_ITEM.getPath(uuid.toString(), gamemode, index) + "' to indexNumber. Skip itemStack " + itemStack.getAmount() + " " + itemStack.getType());
            }
        }
    }

    /**
     * Get the saved balance of a specified gamemode
     *
     * @return Current balance. Null if not set
     */
    public Double getBalanceGamemode(String gamemode) {
        return SAVED_BALANCE.isSet(uuid.toString(), gamemode) ? SAVED_BALANCE.get(uuid.toString(), gamemode) : null;
    }

    /**
     * Set the current gamemode balance of a player
     *
     * @param balance  The new balance. This can be null
     * @param gamemode The specified gamemode
     */
    public void setBalanceGamemode(String gamemode, Double balance) {
        SAVED_BALANCE.set(balance, uuid.toString(), gamemode);
    }
}
