package NL.martijnpu.ChunkDefence.Players;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.RegisteredServiceProvider;

public final class EconomyManager {
    private static EconomyManager instance;
    private Economy economy;

    public static EconomyManager getInstance() {
        if (instance == null)
            instance = new EconomyManager();
        return instance;
    }

    public static void resetInstance() {
        instance = null;
    }

    private EconomyManager() {
        economy = null;

        if (ChunkDefence.get().getServer().getPluginManager().getPlugin("Vault") != null) {
            RegisteredServiceProvider<Economy> rsp = ChunkDefence.get().getServer().getServicesManager().getRegistration(Economy.class);
            if (rsp != null)
                economy = rsp.getProvider();
        }

        if (!ConfigData.VAULT.get())
            Messages.PLUGIN.sendConsole("Vault economy disabled. Using own economy.");
        else if (economy == null)
            Messages.PLUGIN.sendConsole("No Vault dependency found. Using own economy!");
        else
            Messages.PLUGIN.sendConsole("Successfully hooked into Vault");
    }

    public boolean usingVault() {
        return economy != null && ConfigData.VAULT.get();
    }

    /**
     * Deposit a positive amount of coins into a players account
     */
    public void depositPlayer(OfflinePlayer player, double coins) {
        if (coins <= 0)
            return;
        if (usingVault())
            economy.depositPlayer(player, coins);
        else
            PlayerDataManager.getInstance().getUserData(player.getUniqueId()).addBalance(coins);
    }

    /**
     * Withdraw a positive amount of coins from a players account
     */
    public void withdrawPlayer(OfflinePlayer player, double coins) {
        if (coins <= 0)
            return;
        if (usingVault())
            economy.withdrawPlayer(player, coins);
        else
            PlayerDataManager.getInstance().getUserData(player.getUniqueId()).removeBalance(coins);
    }

    /**
     * Set the amount of coins of a players account
     */
    public void setBalance(OfflinePlayer player, double coins) {
        double balance = getBalance(player);
        if (balance == coins)
            return;

        if (balance > coins)
            withdrawPlayer(player, balance - coins);
        else
            depositPlayer(player, coins - balance);
    }

    /**
     * Get the amount of coins of a players account
     */
    public double getBalance(OfflinePlayer player) {
        if (usingVault())
            return economy.getBalance(player);
        return PlayerDataManager.getInstance().getUserData(player.getUniqueId()).BALANCE.get();
    }

    public boolean exportToVault() {
        if (economy == null)
            return false;

        PlayerDataManager.getInstance().getAllUsers().forEach(uuid -> {
            PlayerData data = PlayerDataManager.getInstance().getUserData(uuid);
            economy.depositPlayer(ChunkDefence.get().getServer().getOfflinePlayer(uuid), data.BALANCE.get());
            data.BALANCE.set(null);
        });
        return true;
    }

    public boolean importFromVault() {
        if (economy == null)
            return false;

        PlayerDataManager.getInstance().getAllUsers().forEach(uuid -> {
            double balance = economy.getBalance(ChunkDefence.get().getServer().getOfflinePlayer(uuid));
            PlayerDataManager.getInstance().getUserData(uuid).addBalance(balance);
            if (balance > 0)
                economy.withdrawPlayer(ChunkDefence.get().getServer().getOfflinePlayer(uuid), balance);
            else if (balance < 0)
                economy.depositPlayer(ChunkDefence.get().getServer().getOfflinePlayer(uuid), balance);
        });
        return true;
    }

    /**
     * Save the current balance of a player and save it as a gamemode
     */
    public void saveBalance(OfflinePlayer player, String gamemode) {
        PlayerData playerData = PlayerDataManager.getInstance().getUserData(player.getUniqueId());

        double currBalance = getBalance(player);
        setBalance(player, 0);
        playerData.setBalanceGamemode(gamemode, currBalance);
    }

    /**
     * Retrieve the saved balance of a gamemode and restore the current balance of a player
     */
    public void retrieveBalance(OfflinePlayer player, String gamemode) {
        PlayerData playerData = PlayerDataManager.getInstance().getUserData(player.getUniqueId());

        Double savedBalance = playerData.getBalanceGamemode(gamemode);
        playerData.setBalanceGamemode(gamemode, null);
        setBalance(player, savedBalance == null ? 0.0 : savedBalance);
    }
}
