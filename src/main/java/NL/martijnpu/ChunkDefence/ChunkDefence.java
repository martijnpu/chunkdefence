package NL.martijnpu.ChunkDefence;

import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Arenas.Schematics.SchematicManager;
import NL.martijnpu.ChunkDefence.Commands.CommandManager;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Data.FileHandler;
import NL.martijnpu.ChunkDefence.Events.EventManager;
import NL.martijnpu.ChunkDefence.Events.PlaceholderAPI;
import NL.martijnpu.ChunkDefence.Events.ProtocolLib.PackagesListener;
import NL.martijnpu.ChunkDefence.Events.SubEvents.PlayerEvents;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Mobs.MobManager;
import NL.martijnpu.ChunkDefence.Players.PlayerDataManager;
import NL.martijnpu.ChunkDefence.Scores.ScoreManager;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Traps.TrapManager;
import NL.martijnpu.ChunkDefence.Utils.*;
import NL.martijnpu.ChunkDefence.Worlds.WorldManager;
import com.comphenix.protocol.ProtocolLibrary;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class ChunkDefence extends JavaPlugin {
    private static ChunkDefence instance;

    public static ChunkDefence get() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;

        Messages.PLUGIN.sendConsole("""


                &2=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                &6   ____ _                 _    ____        __
                &6  / ___| |__  _   _ _ __ | | _|  _ \\  ___ / _| ___ _ __   ___ ___
                &6 | |   | '_ \\| | | | '_ \\| |/ / | | |/ _ \\ |_ / _ \\ '_ \\ / __/ _ \\
                &6 | |___| | | | |_| | | | |   <| |_| |  __/  _|  __/ | | | (_|  __/
                &6  \\____|_| |_|\\__,_|_| |_|_|\\_\\____/ \\___|_|  \\___|_| |_|\\___\\___|
                                
                &2=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                """);

        checkServerVersion();      //Check valid version
        getServer().getScheduler().runTaskAsynchronously(this, Updater::checkForUpdate);

        if (ConfigData.BSTATS.get())
            new Metrics(this, 15114);

        EventManager.setupEvents();

        setup(true);

        if (!Bukkit.getOnlinePlayers().isEmpty()) {
            Messages.WARN.sendConsole("Server reload detected. Reloading is not recommended, please use '/cda restart' instead");
            Messages.WARN.sendConsole("Attempting to restore plugin state...");
            getServer().getOnlinePlayers().forEach(PlayerEvents::playerJoin);
        }

        Messages.PLUGIN.sendConsole("&2=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Done loading ChunkDefence =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        Messages.PLUGIN.sendConsole("&2                                   We're up and running");
    }

    @Override
    public void onDisable() {
        shutdown(true);
    }

    public boolean onReload(boolean forceAll) {
        try {
            Messages.PLUGIN.sendConsole("&6=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Start reloading ChunkDefence =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");
            if (forceAll)
                getServer().getOnlinePlayers().forEach(PlayerEvents::playerQuit);
            shutdown(forceAll);

            setup(forceAll);
            Messages.PLUGIN.sendConsole("&2=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Done reloading ChunkDefence =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            Messages.PLUGIN.sendConsole("&c=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Error while reloading ChunkDefence =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");
            return false;
        } finally {
            if (forceAll && WorldManager.getInstance().getArenaWorld() != null)
                getServer().getOnlinePlayers().forEach(PlayerEvents::playerJoin);
        }
    }

    /**
     * With the APIManager it's possible to retrieve the important Managers that are in charge of this plugin.
     */
    @SuppressWarnings("unused") //Not used internally
    public APIManager getAPIManager() {
        return new APIManager();
    }

    private void setup(boolean forceAll) {
        hookWorldEdit();
        hookProtocolLib();
        hookPlaceholderAPI();

        if (!getServer().getPluginManager().isPluginEnabled(this))
            return;

        ConfigData.getInstance().reload(forceAll);
        ConfigData.getInstance().printInitData();
        MobManager.getInstance().printInitData();
        SchematicManager.getInstance().printInitData();
        CommandManager.getInstance();
        PlayerDataManager.getInstance().printInitData();
        WorldManager.getInstance(); //Before trap & shop loading
        TrapManager.getInstance().printInitData();
        if (forceAll)
            ArenaManager.getInstance().printInitData();
        PackagesListener.initialize();
        ShopManager.getInstance().printInitData();
        ScoreManager.getInstance().printInitData();

        TimeHandler.getInstance().start(); //As last
    }

    private void shutdown(boolean forceAll) {
        if (forceAll)
            ArenaManager.resetInstance();
        TimeHandler.getInstance().stop();
        PackagesListener.destruct();

        //Resetting
        MobManager.resetInstance();
        SchematicManager.resetInstance();
        ShopManager.resetInstance();
        ScoreManager.resetInstance();
        PlayerDataManager.resetInstance();
        TrapManager.resetInstance();  // After Shops
        GuiManager.resetInstance();
        FileHandler.MESSAGES_FILE.reload();
        if (forceAll)
            WorldManager.resetInstance();

        Messages.PLUGIN.sendConsole("&6=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Unloading ChunkDefence Done =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");
    }

    private void hookWorldEdit() {
        if (getServer().getPluginManager().getPlugin("WorldEdit") == null) {
            Messages.WARN.sendConsole("Disabled due to no WorldEdit dependency found!");
            getServer().getPluginManager().disablePlugin(this);
        } else
            Messages.PLUGIN.sendConsole("Successfully hooked into WorldEdit");
    }

    private void hookProtocolLib() {
        if (!ConfigData.PROTOCOLLIB.get()) {
            Statics.isProtocolLibAvailable = false;
            Messages.PLUGIN.sendConsole("ProtocolLib hook disabled by the config. Disabling a lot of nice features!");
            return;
        }

        try {
            if (getServer().getPluginManager().getPlugin("ProtocolLib") != null) {
                Statics.isProtocolLibAvailable = ProtocolLibrary.getProtocolManager() != null;
            }

            if (Statics.isProtocolLibAvailable)
                Messages.PLUGIN.sendConsole("Successfully hooked into ProtocolLib");
            else
                Messages.WARN.sendConsole("No ProtocolLib dependency found. Disabling a lot of nice features!");
        } catch (Exception ex) {
            Messages.WARN.sendConsole("Error while trying to get ProtocolLib dependency. Disabling a lot of nice features!");
        }
    }

    private void hookPlaceholderAPI() {
        PlaceholderAPI.registerPAPI();

        if (Statics.isPAPIEnabled)
            Messages.PLUGIN.sendConsole("Successfully hooked into PlaceholderAPI");
        else
            Messages.WARN.sendConsole("No PlaceholderAPI dependency found. Disabling custom placeholders");
    }

    private void checkServerVersion() {
        try {
            Class.forName("org.bukkit.entity.Player$Spigot"); //Some Spigot class
        } catch (Throwable tr) {
            Messages.WARN.sendConsole("Disabled due server doesn't implement Spigot software! Use Spigot, Paper or another derivative");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        double LATEST_TESTED_MC_VERSION = 20.4; //1.20.4
        double FIRST_TESTED_MC_VERSION = 20.0; //1.20.0 due to Packages
        double currVersion;

        try {
            currVersion = Double.parseDouble(getServer().getBukkitVersion().split("-")[0].split("\\.", 2)[1]);
        } catch (NumberFormatException ex) {
            Messages.WARN.sendConsole("Unable to determine minecraft version. This plugin only works for 1." + FIRST_TESTED_MC_VERSION + " till 1." + LATEST_TESTED_MC_VERSION + "!");
            return;
        }

        if (currVersion < FIRST_TESTED_MC_VERSION) {
            Messages.WARN.sendConsole("Disabled due version 1." + currVersion + " is not supported. Use 1." + FIRST_TESTED_MC_VERSION + " till 1." + LATEST_TESTED_MC_VERSION + "!");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        if (currVersion > LATEST_TESTED_MC_VERSION) {
            Messages.WARN.sendConsole("Minecraft version 1." + currVersion + " is not yet tested. Please report any bug you find!");
            return;
        }

        Messages.PLUGIN.sendConsole("Running v" + getDescription().getVersion() + " on minecraft 1." + currVersion + ", this is a supported combination.");
    }
}
