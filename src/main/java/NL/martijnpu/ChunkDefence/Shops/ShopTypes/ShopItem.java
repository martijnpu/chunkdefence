package NL.martijnpu.ChunkDefence.Shops.ShopTypes;

import NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyInstance;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Types.GuiShopEditItem;
import NL.martijnpu.ChunkDefence.Shops.Shop;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import static NL.martijnpu.ChunkDefence.Data.FileHandler.SHOPS_FILE;
import static NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyAdapter.objectKey;

public final class ShopItem extends Shop {
    private final ConfigKeyInstance<Object> SAVED_ITEM;
    private ItemStack item;

    public ShopItem(String key) throws IllegalArgumentException {
        super(key);

        SAVED_ITEM = new ConfigKeyInstance<>(objectKey(SHOPS_FILE, "shop.%.item", null), key);
        item = (ItemStack) SAVED_ITEM.get();

        if (item == null) {
            Messages.WARN.sendConsole("Unable to load shop " + key + ". Invalid Item");
            throw new IllegalArgumentException();
        }

        spawnName();
        spawnDisplayItem();
        setDisplayItem(item.getType());
        loadFloatingText();
        shopLocation.getRelative(BlockFace.UP).setType(Material.BARRIER);
    }

    public ShopItem(Block block, ItemStack item) {
        super(block, ShopType.ITEM);

        SAVED_ITEM = new ConfigKeyInstance<>(objectKey(SHOPS_FILE, "shop.%.item", null), key);

        if (Material.AIR.equals(item.getType()))
            item = new ItemStack(Material.GLASS_PANE);

        this.item = item;
        spawnDisplayItem();
        changeItem(item);
        loadFloatingText();
        shopLocation.getRelative(BlockFace.UP).setType(Material.BARRIER);
    }

    @Override
    protected void processShopItem(Player player) {
        if (handleMoney(player))
            player.getInventory().addItem(item.clone());
    }

    public boolean hasPermission(Player player) {
        return hasPermission(player, item.getType().toString().toLowerCase());
    }

    public void editShop(Player player) {
        GuiManager.getInstance().createInventory(player, GuiShopEditItem.class).openGui();
    }

    protected void loadFloatingText() {
        super.loadFloatingText();
        textList.forEach(floatingText -> floatingText.text = floatingText.text
                .replace("%NAME%", getName())
                .replace("%AMOUNT%", item.getAmount() + ""));
    }

    public ItemStack getItem() {
        return item;
    }

    public void changeItem(ItemStack itemStack) {
        item = itemStack;

        String name = "&6";

        if (itemStack.getAmount() > 1)
            name += item.getAmount() + "x ";

        if (item.hasItemMeta() && item.getItemMeta().hasDisplayName())
            name += item.getItemMeta().getDisplayName();
        else
            name += Statics.toCapital(item.getType().name());

        setName(name);

        setDisplayItem(item.getType());
        SAVED_ITEM.set(item);

        Messages.DEBUG.sendConsole("Changing the item of shop '" + key + "' to: " + item.getType());
    }
}
