package NL.martijnpu.ChunkDefence.Shops.ShopTypes;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Arenas.Schematics.SchematicManager;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyInstance;
import NL.martijnpu.ChunkDefence.Gui.GuiInventory;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Types.GuiArenaBuy;
import NL.martijnpu.ChunkDefence.Gui.Types.GuiShopEditArena;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Shops.Shop;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import static NL.martijnpu.ChunkDefence.Data.FileHandler.SHOPS_FILE;
import static NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyAdapter.stringKey;

public final class ShopArena extends Shop {
    private final ConfigKeyInstance<String> SCHEMATIC_KEY;

    public ShopArena(String key) throws IllegalArgumentException {
        super(key);

        SCHEMATIC_KEY = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".arena.schematic", ""), key);

        if (SchematicManager.getInstance().isInvalidSchematic(SCHEMATIC_KEY.get())) {
            Messages.SCHEM_UNKNOWN.sendConsole(SCHEMATIC_KEY.get());
            throw new IllegalArgumentException();
        }

        spawnName();
        spawnDisplayItem();
        loadFloatingText();
        shopLocation.getRelative(BlockFace.UP).setType(Material.BARRIER);
    }

    public ShopArena(Block block, String schematic) {
        super(block, ShopType.ARENA);

        if (SchematicManager.getInstance().isInvalidSchematic(schematic)) {
            Messages.WARN.sendConsole("Unable to load shop " + key + ". '" + schematic + "' is an invalid Arena");
            throw new IllegalArgumentException();
        }

        SCHEMATIC_KEY = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".arena.schematic", schematic), key);
        SCHEMATIC_KEY.setDefault(true);

        setSchematic(schematic);
        setName(schematic);
        spawnDisplayItem();
        loadFloatingText();
        shopLocation.getRelative(BlockFace.UP).setType(Material.BARRIER);
    }

    public String getSchematic() {
        return SCHEMATIC_KEY.get();
    }

    public void setSchematic(String newSchematic) {
        if (getSchematic().equals(newSchematic))
            return;

        if (SchematicManager.getInstance().isInvalidSchematic(newSchematic)) {
            Messages.SCHEM_UNKNOWN.sendConsole(newSchematic);
            return;
        }
        setName(getName().replaceAll("(?i)" + getSchematic(), newSchematic)); //Only replace old SchematicName
        SCHEMATIC_KEY.set(newSchematic);

        Messages.DEBUG.sendConsole("Changing the schematic of shop '" + key + "' to: " + newSchematic);
    }

    public boolean alreadyBought(Player player) {
        Arena arena = ArenaManager.getInstance().getArena(player);
        if (arena == null)
            return false;

        return arena.getArenaData().hasBoughtArena(SCHEMATIC_KEY.get());
    }

    @Override
    protected void processShopItem(Player player) {
        if (ArenaManager.getInstance().getArena(player) == null) {
            Messages.ARENA_NONE.send(player);
            return;
        }

        GuiInventory inventory =
                GuiManager.getInstance().createInventory(player, GuiArenaBuy.class);
        inventory.changeItem(1, new GuiSlot(Material.LIME_CONCRETE_POWDER, Messages.GUI_BUY_ARENA_QUESTION.get())
                .setLore(Messages.GUI_BUY_ARENA_CONFIRM.get())
                .setLeftClick(() -> {
                    buyConfirmed(player);
                    GuiManager.getInstance().closeInventory(player);
                }));
        inventory.openGui();
    }

    public boolean hasPermission(Player player) {
        return hasPermission(player, SCHEMATIC_KEY.get());
    }

    public void editShop(Player player) {
        GuiManager.getInstance().createInventory(player, GuiShopEditArena.class).openGui();
    }

    protected void loadFloatingText() {
        super.loadFloatingText();
        textList.forEach(floatingText -> floatingText.text = floatingText.text
                .replace("%NAME%", getName()));
    }

    public void buyConfirmed(Player player) {
        Arena arena = ArenaManager.getInstance().getArena(player);
        if (arena == null) {
            Messages.ARENA_NONE.send(player);
            return;
        }

        if (!ConfigData.GM_ARENA_ALLOW_CHANGE.get(arena.getArenaData().GAMEMODE.get())) {
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.PLAYERS, 1f, 0.9f);
            Messages.EXCEPT_ARENA_CHANGE.send(player);
            return;
        }

        if (arena.getWaveController() != null && arena.getWaveController().isInWave()) {
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.PLAYERS, 1f, 0.9f);
            Messages.WAVE_PROGRESS.send(player);
            return;
        }

        if (SchematicManager.getInstance().isInvalidSchematic(SCHEMATIC_KEY.get())) {
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.PLAYERS, 1f, 0.9f);
            Messages.SCHEM_UNKNOWN.send(player, SCHEMATIC_KEY.get());
            return;
        }

        if (!Permission.BYPASS_COOLDOWN.hasPermission(player)
                && arena.getSecondsSinceLastSwitch() < ConfigData.ARENA_BUY_COOLDOWN.get()) {
            Messages.ARENA_COOLDOWN.send(player, String.valueOf(ConfigData.ARENA_BUY_COOLDOWN.get() - arena.getSecondsSinceLastSwitch()));
            return;
        }

        if (arena.getArenaData().hasBoughtArena(SCHEMATIC_KEY.get()) || handleMoney(player))
            ArenaManager.getInstance().changeArena(player, SCHEMATIC_KEY.get());
    }
}
