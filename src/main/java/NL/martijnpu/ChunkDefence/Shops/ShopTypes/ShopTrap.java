package NL.martijnpu.ChunkDefence.Shops.ShopTypes;

import NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyInstance;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Types.GuiShopEditTrap;
import NL.martijnpu.ChunkDefence.Shops.Shop;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapData;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapDataManager;
import NL.martijnpu.ChunkDefence.Traps.TrapManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import java.util.List;

import static NL.martijnpu.ChunkDefence.Data.FileHandler.SHOPS_FILE;
import static NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyAdapter.integerKey;
import static NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyAdapter.stringKey;

public final class ShopTrap extends Shop {
    private final ConfigKeyInstance<String> TRAP_KEY;
    private final ConfigKeyInstance<Integer> TRAP_AMOUNT;
    private TrapData trapData;

    public ShopTrap(String key) throws IllegalArgumentException, IllegalAccessException {
        super(key);

        TRAP_KEY = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".trap.index", ""), key);
        TRAP_AMOUNT = new ConfigKeyInstance<>(integerKey(SHOPS_FILE, PATH + ".trap.amount", 1), key);

        trapData = TrapDataManager.getInstance().getTrapBase(TRAP_KEY.get());

        if (trapData == null) {
            Messages.WARN.sendConsole("Unable to load shop " + key + ". Invalid Trap");
            throw new IllegalArgumentException();
        }

        loadFloatingText();
        removeTrap();
        setTrap();
        spawnName();
    }

    public ShopTrap(Block block, TrapData trapData) throws IllegalAccessException {
        super(block, ShopType.TRAP);
        this.trapData = trapData;

        TRAP_KEY = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".trap.index", trapData.key), key);
        TRAP_AMOUNT = new ConfigKeyInstance<>(integerKey(SHOPS_FILE, PATH + ".trap.amount", 1), key);

        TRAP_KEY.setDefault(true);
        TRAP_AMOUNT.setDefault(true);

        setName("&61x " + trapData.NAME.get());
        loadFloatingText();
        removeTrap();
        spawnName();
        setTrap();
    }

    @Override
    protected void processShopItem(Player player) {
        if (handleMoney(player))
            player.getInventory().addItem(trapData.generateTrapItem(TRAP_AMOUNT.get()));
    }

    public boolean hasPermission(Player player) {
        return hasPermission(player, trapData.key);
    }

    public void editShop(Player player) {
        GuiManager.getInstance().createInventory(player, GuiShopEditTrap.class).openGui();
    }

    @Override
    public void remove() {
        removeTrap();
        super.remove();
    }

    protected void loadFloatingText() {
        super.loadFloatingText();
        textList.forEach(floatingText -> floatingText.text = floatingText.text
                .replace("%NAME%", trapData.NAME.get())
                .replace("%AMOUNT%", TRAP_AMOUNT.get() + "")
                .replace("%FIRE%", trapData.SET_FIRE.get() + "")
                .replace("%EFFECT%", Statics.toCapital(trapData.POTION_EFFECT.get()))
                .replace("%EFFECT_AMPLIFIER%", Statics.toRoman(trapData.POTION_AMPLIFIER.get()))
                .replace("%DAMAGE_HEART%", trapData.DAMAGE_AMOUNT.get() + "")
                .replace("%DAMAGE_SECONDS%", trapData.INTERVAL.get() + "")
                .replace("%RANGE%", trapData.RANGE.get() + "")
                .replace("%TARGET_AMOUNT%", trapData.TARGET_AMOUNT.get() + "")
                .replace("%DURABILITY%", trapData.DURABILITY.get() == -1 ? Messages.TRAP_DURABILITY_INF_SHOP.get() : trapData.DURABILITY.get() + "")
                .replace("%BUILD_SECONDS%", trapData.SETUP_TIME.get() + "")
                .replace("%MOBS%", getAllMobs()));
    }

    private void removeTrap() {
        TrapManager.getInstance().destroyTrap(null, shopLocation.getRelative(BlockFace.UP));
        shopLocation.getRelative(BlockFace.UP).setType(Material.AIR);
    }

    private void setTrap() throws IllegalAccessException {
        TrapManager.getInstance().placeTrap(null, shopLocation.getRelative(BlockFace.UP), trapData, -1);
    }

    private String getAllMobs() {
        List<String> entityList = trapData.TARGET_ENTITIES.get();
        if (entityList.isEmpty())
            return Messages.TRAP_MOBS_ALL_SHOP.get();

        StringBuilder mobs = new StringBuilder();
        for (String schematicName : entityList) {
            mobs.append(Statics.toCapital(schematicName)).append(" ");
        }
        mobs.deleteCharAt(mobs.length() - 1); //Remove last space
        return mobs.toString();
    }

    public TrapData getTrapData() {
        return trapData;
    }

    public void setTrapData(TrapData newTrapData) {
        if (trapData.equals(newTrapData))
            return;

        removeTrap();

        setName(getName().replaceAll("(?i)" + trapData.NAME.get(), newTrapData.NAME.get())); //Only replace old TrapName
        trapData = newTrapData;
        TRAP_KEY.set(newTrapData.key);

        try {
            setTrap();
        } catch (IllegalAccessException ignored) {}

        Messages.DEBUG.sendConsole("Changing the trap of shop '" + key + "' to: " + newTrapData.key);
    }
}
