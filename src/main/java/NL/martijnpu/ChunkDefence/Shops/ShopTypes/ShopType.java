package NL.martijnpu.ChunkDefence.Shops.ShopTypes;

import org.jetbrains.annotations.Nullable;

public enum ShopType {
    ARENA,
    ITEM,
    TRAP;

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }

    @Nullable
    public static ShopType getEnum(String string) {
        for (ShopType candidate : ShopType.class.getEnumConstants()) {
            if (candidate.name().equalsIgnoreCase(string)) {
                return candidate;
            }
        }
        return null;
    }
}
