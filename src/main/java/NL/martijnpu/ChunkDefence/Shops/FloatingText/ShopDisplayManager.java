package NL.martijnpu.ChunkDefence.Shops.FloatingText;

import NL.martijnpu.ChunkDefence.Shops.Shop;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import org.bukkit.GameMode;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class ShopDisplayManager {
    private final Map<Player, ShopDisplayPlayer> playerMap = new HashMap<>();
    private static ShopDisplayManager instance;

    private ShopDisplayManager() {}

    public static ShopDisplayManager getInstance() {
        if (instance == null)
            instance = new ShopDisplayManager();
        return instance;
    }

    public static void resetInstance() {
        if (instance != null)
            instance.removeAllPlayerLooks();
        instance = null;
    }

    private void removeAllPlayerLooks() {
        for (Map.Entry<Player, ShopDisplayPlayer> entry : playerMap.entrySet()) {
            try {
                entry.getValue().remove();
            } catch (NoClassDefFoundError ex) {
                Messages.PLUGIN.sendConsole("Couldn't unload interactions with shops. This could be harmless, report otherwise");
            }
        }
    }

    /**
     * Check whether a player is looking to a shop and processes all actions
     */
    public void checkPlayerLook(Player player) {
        if (!Statics.isProtocolLibAvailable)
            return;

        Block lookAt = ShopManager.lookingAt(player);
        ShopDisplayPlayer playerData = playerMap.get(player);

        if (playerData == null) {
            Shop shop = ShopManager.getInstance().getShop(lookAt);
            if (shop != null && player.getGameMode() != GameMode.SPECTATOR)
                playerMap.put(player, new ShopDisplayPlayer(player, shop));
        } else {
            if ((playerData.getShop().shopLocation.equals(lookAt) || playerData.getShop().shopLocation.getRelative(BlockFace.UP).equals(lookAt)) && player.getGameMode() != GameMode.SPECTATOR) {
                playerData.update();
            } else {
                playerData.remove();
                playerMap.remove(player);
            }
        }
    }

}
