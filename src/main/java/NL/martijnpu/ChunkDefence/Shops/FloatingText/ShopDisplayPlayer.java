package NL.martijnpu.ChunkDefence.Shops.FloatingText;

import NL.martijnpu.ChunkDefence.Shops.Shop;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopArena;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public final class ShopDisplayPlayer {
    private final Player player;
    private final Shop shop;
    private final List<FloatingText> texts;

    ShopDisplayPlayer(Player player, Shop shop) {
        this.player = player;
        this.shop = shop;

        if (Statics.isProtocolLibAvailable) {
            texts = shop.getTextList();
            addPersonalTouch();
            texts.forEach(x -> x.spawnText(shop.shopLocation.getLocation(), player));
        } else
            texts = new ArrayList<>();

        update();
    }

    Shop getShop() {
        return shop;
    }

    void update() {
        texts.forEach(FloatingText::update);
        Messages.SHOP_ACTION_BAR.sendActionBarMessage(player, shop.getName());
    }

    void remove() {
        texts.forEach(FloatingText::remove);
        Messages.PLUGIN.sendActionBarMessage(player, "");
    }

    /**
     * Populate placeholders based on the player
     */
    private void addPersonalTouch() {
        if (shop instanceof ShopArena shopArena) {
            String message = shopArena.alreadyBought(player) ? Messages.SHOP_BUY_ARENA_ALREADY_OWN.get() : Messages.SHOP_BUY_ARENA_LACK.get();
            texts.forEach(text -> text.text = text.text.replace("%OWNED%", message)); //Todo Documentation
        }
    }
}
