package NL.martijnpu.ChunkDefence.Shops.FloatingText;

import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Events.ProtocolLib.Packages.WrapperPlayServerEntityDestroy;
import NL.martijnpu.ChunkDefence.Events.ProtocolLib.Packages.WrapperPlayServerEntityMetadata;
import NL.martijnpu.ChunkDefence.Events.ProtocolLib.Packages.WrapperPlayServerEntityTeleport;
import NL.martijnpu.ChunkDefence.Events.ProtocolLib.Packages.WrapperPlayServerSpawnEntityLiving;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedDataValue;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.google.common.collect.Lists;
import net.kyori.adventure.text.serializer.json.JSONComponentSerializer;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.UUID;

public final class FloatingText {
    private final Vector offset;
    private final int entityID = (int) (Math.random() * Integer.MAX_VALUE / 4);
    public String text;
    private Player player;
    private Location location;

    public FloatingText(@Nonnull String text, @Nonnull Vector offset) {
        this.text = text;
        this.offset = offset;
    }

    public FloatingText copy() {
        return new FloatingText(text, offset);
    }

    void spawnText(Location location, Player player) {
        this.player = player;
        this.location = location;
        spawnEntity();
    }

    void update() {
        WrapperPlayServerEntityTeleport updateEntity = new WrapperPlayServerEntityTeleport();
        Location locationUp = calcLocation();
        updateEntity.setEntityID(entityID);
        updateEntity.setX(locationUp.getX());
        updateEntity.setY(locationUp.getY());
        updateEntity.setZ(locationUp.getZ());
        updateEntity.setPitch(player.getEyeLocation().getPitch());
        updateEntity.setYaw(player.getEyeLocation().getYaw());
        updateEntity.sendPacket(player);
    }

    void remove() {
        WrapperPlayServerEntityDestroy destroyWrapper = new WrapperPlayServerEntityDestroy();
        destroyWrapper.setEntityIds(new int[]{entityID});
        destroyWrapper.sendPacket(player);
    }

    private void spawnEntity() {
        Location locationSpawn = calcLocation();
        String nameJSON = JSONComponentSerializer.json().serialize(Messages.formatLegacy(text));

        WrapperPlayServerSpawnEntityLiving spawnEntity = new WrapperPlayServerSpawnEntityLiving();
        spawnEntity.setEntityID(entityID);
        spawnEntity.setUniqueId(UUID.randomUUID());
        spawnEntity.setType(EntityType.ARMOR_STAND);
        spawnEntity.setX(locationSpawn.getX());
        spawnEntity.setY(locationSpawn.getY());
        spawnEntity.setZ(locationSpawn.getZ());
        spawnEntity.setPitch(player.getEyeLocation().getPitch());
        spawnEntity.setYaw(player.getEyeLocation().getYaw());
        spawnEntity.sendPacket(player);

        WrapperPlayServerEntityMetadata entityMetadata = new WrapperPlayServerEntityMetadata();
        entityMetadata.setEntityID(entityID);
        entityMetadata.setMetadata(Lists.newArrayList( //https://wiki.vg/Entity_metadata#Entity
                new WrappedDataValue(0, WrappedDataWatcher.Registry.get(Byte.class), (byte) 0x20), //Invisible
                new WrappedDataValue(2, WrappedDataWatcher.Registry.getChatComponentSerializer(true), Optional.of(WrappedChatComponent.fromJson(nameJSON).getHandle())), //Custom Name
                new WrappedDataValue(3, WrappedDataWatcher.Registry.get(Boolean.class), true), //Custom Name visible
                new WrappedDataValue(15, WrappedDataWatcher.Registry.get(Byte.class), (byte) 0x13) //ARMOR_STAND small & no base_plate & marker
                // new WrappedDataValue(23, WrappedDataWatcher.Registry.getChatComponentSerializer(false), WrappedChatComponent.fromText(text).getHandle()), //TEXT_DISPLAY
                // new WrappedDataValue(25, WrappedDataWatcher.Registry.get(Integer.class), -926416896)  //TEXT_DISPLAY Background color
        ));
        entityMetadata.sendPacket(player);
    }

    /**
     * Calculate the X, Y & Z location of the display relatively to the player.
     * It will always be in the direction the player is looking at.
     * The X and Y are multiplied by the configured offset.
     * The Z is if it's not relative at the same height, otherwise will be relative to the player looking up/down
     */
    private Location calcLocation() {
        Vector direction = player.getEyeLocation().getDirection();

        Vector locationYVector = direction.clone().multiply(offset.getY() * 0.1 * offset.getZ()).rotateAroundAxis(direction.clone().setY(0).rotateAroundY(0.5 * Math.PI), 0.5 * Math.PI).rotateAroundAxis(direction.clone(), Math.PI);
        Vector locationXVector = direction.clone().multiply(offset.getX() * 0.1 * offset.getZ()).rotateAroundAxis(direction.clone().setY(0).rotateAroundY(0.5 * Math.PI), 0.5 * Math.PI).rotateAroundAxis(direction.clone(), 1.5 * Math.PI);

        if (ConfigData.SHOP_RELATIVE_PLAYER.get()) {
            Vector vector = direction.clone().multiply(offset.getZ()).add(new Vector(0, -1.35, 0));
            return player.getEyeLocation().clone().add(vector).add(locationXVector).add(locationYVector);
        }
        Vector vector = player.getEyeLocation().toVector().subtract(location.clone().toVector().add(new Vector(0, 0.5, 0))).normalize().multiply(3.3 - offset.getZ());
        return location.clone().add(vector).add(locationXVector).add(locationYVector).add(0.5, 0.5, 0);
    }
}
