package NL.martijnpu.ChunkDefence.Shops;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Data.FileHandler;
import NL.martijnpu.ChunkDefence.Shops.FloatingText.ShopDisplayManager;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopArena;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopItem;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopTrap;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopType;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapData;
import NL.martijnpu.ChunkDefence.Utils.Keys;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.FluidCollisionMode;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class ShopManager {
    private static final String SHOPS_PATH = "shop";
    private static int randomEntityID;
    private final List<Shop> shops = new ArrayList<>();
    private static ShopManager instance;

    private ShopManager() {
        load();
        ShopDisplayManager.getInstance();
    }

    public static ShopManager getInstance() {
        if (instance == null)
            instance = new ShopManager();
        return instance;
    }

    public static void resetInstance() {
        randomEntityID = -1;
        if (instance != null)
            instance.removeOldShopEntities();

        ShopDisplayManager.resetInstance();
        instance = null;
    }

    /**
     * Generate a random number each startup to identify old and new entities
     */
    public static int getRandomEntityID() {
        if (randomEntityID <= 0)
            randomEntityID = new Random().nextInt(Integer.MAX_VALUE - 1) + 1;
        return randomEntityID;
    }

    public void printInitData() {
        if (shops.isEmpty())
            Messages.WARN.sendConsole("Loading none shops. This could be an error or you don't have any shops");
        else
            Messages.PLUGIN.sendConsole("Loading " + shops.size() + " shops");
    }

    public Shop addTrapShop(Player player, TrapData trapData) {
        Block block = lookingAt(player);
        if (block == null) {
            Messages.SHOP_LOOK.send(player);
            return null;
        }
        if (getShop(block) != null) {
            Messages.SHOP_ALREADY.send(player);
            return null;
        }

        try {
            Shop shop = new ShopTrap(block, trapData);
            shops.add(shop);
            Messages.SHOP_ADD.send(player);
            return shop;
        } catch (IllegalAccessException ignored) {
            return null;
        }
    }

    @Nullable
    public Shop addItemShop(Player player, ItemStack item) {
        Block block = lookingAt(player);
        if (block == null) {
            Messages.SHOP_LOOK.send(player);
            return null;
        }
        if (getShop(block) != null) {
            Messages.SHOP_ALREADY.send(player);
            return null;
        }
        Shop shop = new ShopItem(block, item);
        shops.add(shop);
        Messages.SHOP_ADD.send(player);
        return shop;
    }

    @Nullable
    public ShopArena addArenaShop(Player player, String schematic) {
        Block block = lookingAt(player);
        if (block == null) {
            Messages.SHOP_LOOK.send(player);
            return null;
        }
        if (getShop(block) != null) {
            Messages.SHOP_ALREADY.send(player);
            return null;
        }

        ShopArena shop = new ShopArena(block, schematic);
        shops.add(shop);
        Messages.SHOP_ADD.send(player);
        return shop;
    }

    /**
     * Add shop to shoplist.
     * All validity checks already needs to be done
     */
    void addShop(Shop shop) {
        shops.add(shop);
    }

    /**
     * Checks if a new shop can be placed at the location the player is looking at
     */
    public boolean canAddShop(Player player) {
        Block block = lookingAt(player);
        if (block == null) {
            Messages.SHOP_LOOK.send(player);
            return false;
        }
        if (getShop(block) != null) {
            Messages.SHOP_ALREADY.send(player);
            return false;
        }

        return true;
    }

    public void removeShop(Player player) {
        Shop shop = getShop(player);
        if (shop != null) {
            shop.remove();
            Messages.SHOP_REMOVED.send(player);
        } else
            Messages.SHOP_NOT_FOUND.send(player);
    }

    /**
     * Used by @Shop to remove itself from the list
     */
    void removeShop(Shop shop) {
        shops.remove(shop);
    }

    public void moveShop(Player player, String name) {
        Block look = lookingAt(player);
        if (getShop(look) != null) {
            Messages.SHOP_ALREADY.send(player);
            return;
        }

        Shop shop = shops.stream().filter(x -> x.key.equals(name)).findFirst().orElse(null);
        if (shop == null) {
            Messages.SHOP_INVALID.send(player);
            return;
        }

        shop.move(look);
        load();
        removeOldShopEntities();

        Messages.SHOP_MOVED.send(player);
    }

    @Nullable
    public Shop getShop(Block block) {
        return block == null ? null : shops.stream().filter(shop -> shop.shopLocation.equals(block) || shop.shopLocation.equals(block.getRelative(BlockFace.DOWN))).findFirst().orElse(null);
    }

    @Nullable
    public Shop getShop(Player player) {
        return getShop(lookingAt(player));
    }

    public static Block lookingAt(Player player) {
        return player.getTargetBlockExact(6, FluidCollisionMode.SOURCE_ONLY);
    }

    private void load() {
        randomEntityID = -1;
        ShopDisplayManager.resetInstance();

        shops.clear();

        ConfigurationSection cs = FileHandler.SHOPS_FILE.get().getConfigurationSection(SHOPS_PATH);

        if (cs == null)
            return;

        for (String key : cs.getKeys(false)) {
            try {
                ShopType type = ShopType.getEnum(cs.getString(key + ".type"));
                if (type == null) {
                    Messages.WARN.sendConsole("Illegal shop '" + SHOPS_PATH + "." + key + "' does not have a valid type! Skipping...");
                    continue;
                }

                switch (type) {
                    case ARENA -> shops.add(new ShopArena(key));
                    case ITEM -> shops.add(new ShopItem(key));
                    case TRAP -> shops.add(new ShopTrap(key));
                }
            } catch (IllegalArgumentException | IllegalAccessException ignored) {
                Messages.WARN.sendConsole("Illegal shop '" + SHOPS_PATH + "." + key + "' does have an error while loading! Skipping...");
            }
        }
    }

    /**
     * Remove all loaded entities with an old entity ID
     */
    public void removeOldShopEntities() {
        ChunkDefence.get().getServer().getWorlds().forEach(world -> world.getEntities().forEach(ShopManager::checkShopEntity));
    }

    /**
     * Check entity if it's a shop entity and remove if it contains an old entity ID
     */
    public static void checkShopEntity(Entity entity) {
        PersistentDataContainer container = entity.getPersistentDataContainer();
        if (container.has(Keys.SHOP_KEY, PersistentDataType.INTEGER))
            if (getRandomEntityID() != container.getOrDefault(Keys.SHOP_KEY, PersistentDataType.INTEGER, -1))
                entity.remove();
    }
}
