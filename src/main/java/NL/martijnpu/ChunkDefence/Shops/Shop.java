package NL.martijnpu.ChunkDefence.Shops;

import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyInstance;
import NL.martijnpu.ChunkDefence.Players.EconomyManager;
import NL.martijnpu.ChunkDefence.Players.PlayerData;
import NL.martijnpu.ChunkDefence.Players.PlayerDataManager;
import NL.martijnpu.ChunkDefence.Shops.FloatingText.FloatingText;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopItem;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopType;
import NL.martijnpu.ChunkDefence.Utils.*;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static NL.martijnpu.ChunkDefence.Data.FileHandler.SHOPS_FILE;
import static NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyAdapter.*;

public abstract class Shop {
    public final String key;
    public final ConfigKeyInstance<ShopType> TYPE;
    public final ConfigKeyInstance<Material> DISPLAY_ITEM;
    private final ConfigKeyInstance<Double> COST;
    protected final ConfigKeyInstance<String> DISPLAY_NAME;
    private final ConfigKeyInstance<String> LOCATION;
    private final ConfigKeyInstance<String> MESSAGE_1_TEXT;
    private final ConfigKeyInstance<String> MESSAGE_1_OFFSET;
    private final ConfigKeyInstance<String> MESSAGE_2_TEXT;
    private final ConfigKeyInstance<String> MESSAGE_2_OFFSET;
    protected final String PATH = "shop.%";

    protected final List<FloatingText> textList = new ArrayList<>();
    protected final List<ArmorStand> displayEntities = new ArrayList<>();
    public Block shopLocation;
    private AreaEffectCloud displayCloud;

    /**
     * Create a new shop from the file
     */
    protected Shop(String key) {
        this.key = key;

        TYPE = new ConfigKeyInstance<>(enumKey(SHOPS_FILE, PATH + ".type", ShopType.ARENA), key);
        COST = new ConfigKeyInstance<>(doubleKey(SHOPS_FILE, PATH + ".cost", 10), key);
        LOCATION = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".location", ""), key);
        DISPLAY_NAME = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".display.name", ""), key);
        DISPLAY_ITEM = new ConfigKeyInstance<>(enumKey(SHOPS_FILE, PATH + ".display.item", Material.GLASS_PANE), key);
        MESSAGE_1_TEXT = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".messages.1.text", "%NAME%"), key);
        MESSAGE_1_OFFSET = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".messages.1.offset", "0 7 1.8"), key);
        MESSAGE_2_TEXT = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".messages.2.text", "&ePrice: &f%PRICE%"), key);
        MESSAGE_2_OFFSET = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".messages.2.offset", "-0.5 4 2.2"), key);

        shopLocation = Statics.getLocationFromString(LOCATION.get()).getBlock();

        if (shopLocation.isEmpty())
            shopLocation.setType(Material.STONE);
    }

    protected Shop(Block location, ShopType type) {
        key = getFreePath(type);

        TYPE = new ConfigKeyInstance<>(enumKey(SHOPS_FILE, PATH + ".type", type), key);
        COST = new ConfigKeyInstance<>(doubleKey(SHOPS_FILE, PATH + ".cost", 10.0), key);
        LOCATION = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".location", Statics.getStringFromLocation(location.getLocation())), key);
        DISPLAY_NAME = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".display.name", ""), key);
        DISPLAY_ITEM = new ConfigKeyInstance<>(enumKey(SHOPS_FILE, PATH + ".display.item", Material.GLASS_PANE), key);
        MESSAGE_1_TEXT = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".messages.1.text", "%NAME%"), key);
        MESSAGE_1_OFFSET = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".messages.1.offset", "0 3 2.2"), key);
        MESSAGE_2_TEXT = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".messages.2.text", "[mm]<yellow>Price: </yellow><white>%PRICE%</white>"), key);
        MESSAGE_2_OFFSET = new ConfigKeyInstance<>(stringKey(SHOPS_FILE, PATH + ".messages.2.offset", "-0.5 1.3 2.8"), key);

        TYPE.setDefault(true);
        COST.setDefault(true);
        LOCATION.setDefault(true);
        shopLocation = location;
        DISPLAY_NAME.setDefault(true);
        DISPLAY_ITEM.setDefault(true);
        MESSAGE_1_TEXT.setDefault(true);
        MESSAGE_1_OFFSET.setDefault(true);
        MESSAGE_2_TEXT.setDefault(true);
        MESSAGE_2_OFFSET.setDefault(true);

        ShopManager.getInstance().addShop(this);
    }

    /**
     * Process the shop item if it's bought. All check are already handled before the call.
     * Method still needs to call <i>handleMoney(player)</i>
     */
    protected abstract void processShopItem(Player player);

    public abstract boolean hasPermission(Player player);

    public abstract void editShop(Player player);

    public List<FloatingText> getTextList() {
        return textList.stream().map(FloatingText::copy).collect(Collectors.toList());
    }

    public void remove() {
        shopLocation.getRelative(BlockFace.UP).setType(Material.AIR);
        displayEntities.forEach(Entity::remove);
        displayCloud.remove();
        SHOPS_FILE.get().set(PATH.replace("%", key), null);
        SHOPS_FILE.save();

        ShopManager.getInstance().removeShop(this);
    }

    public void move(Block newBlock) {
        // Removal of old entities is done by reloading the ShopManager afterwards
        shopLocation.getRelative(BlockFace.UP).setType(Material.AIR);
        shopLocation = newBlock;
        LOCATION.set(Statics.getStringFromLocation(shopLocation.getLocation()));
    }

    public double getCost() {
        return COST.get();
    }

    public void setCost(double cost) {
        if (getCost() != cost)
            COST.set(cost);
    }

    public String getName() {
        return LegacyComponentSerializer.legacyAmpersand().serialize(Messages.formatLegacy(DISPLAY_NAME.get()));
    }

    public void setName(String name) {
        DISPLAY_NAME.set(name);
        spawnName();
        Messages.DEBUG.sendConsole("Changing the name of shop '" + key + "' to: " + name);
    }

    protected void spawnName() {
        if (displayCloud != null && displayCloud.isValid())
            displayCloud.remove();

        displayCloud = (AreaEffectCloud) shopLocation.getWorld().spawnEntity(shopLocation.getLocation().clone().add(0.5, 1, 0.5), EntityType.AREA_EFFECT_CLOUD);
        displayCloud.setCustomNameVisible(true);
        displayCloud.customName(Messages.formatLegacy(DISPLAY_NAME.get()));
        displayCloud.setParticle(Particle.BLOCK_CRACK, Material.AIR.createBlockData());
        displayCloud.setRadius(0);
        displayCloud.setDuration(999999);
        displayCloud.getPersistentDataContainer().set(Keys.SHOP_KEY, PersistentDataType.INTEGER, ShopManager.getRandomEntityID());
    }

    public void buyShop(Player player) {
        if (canBuy(player))
            processShopItem(player);
    }

    private boolean canBuy(Player player) {
        if (!hasPermission(player)) {
            Messages.SHOP_BUY_NO_PERM.send(player);
            return false;
        }

        PlayerData playerData = PlayerDataManager.getInstance().getUserData(player.getUniqueId());
        long timeLeft = playerData.getLastShopTime() + ConfigData.SHOP_COOLDOWN.get() - TimeHandler.getCurrSec();

        if (timeLeft > 0) {
            Messages.SHOP_BUY_COOLDOWN.send(player, String.valueOf(timeLeft));
            return false;
        } else {
            playerData.updateLastStopTime();
            return true;
        }
    }

    public void setDisplayItem(Material material) {
        if (DISPLAY_ITEM.get().equals(material))
            return;

        DISPLAY_ITEM.set(material);
        spawnDisplayItem();
        Messages.DEBUG.sendConsole("Changing the displayItem of shop '" + key + "' to: " + material.name());
    }

    protected void spawnDisplayItem() {
        ConfigurationSection cs;

        displayEntities.forEach(Entity::remove);

        ItemStack itemStack;
        if (TYPE.get().equals(ShopType.ITEM))
            itemStack = ((ShopItem) this).getItem();
        else
            itemStack = new ItemStack(DISPLAY_ITEM.get());

        if (itemStack.getType().equals(Material.GLASS_PANE))
            itemStack = new ItemStack(Material.GLASS);

        if (itemStack.getType().isBlock())
            cs = SHOPS_FILE.get().getConfigurationSection("postures.block");
        else {
            if (itemStack.getType().getMaxDurability() < 1) {
                cs = SHOPS_FILE.get().getConfigurationSection("postures.item");
            } else {
                if (itemStack.getType().name().contains("SWORD") || itemStack.getType().name().contains("AXE") || itemStack.getType().name().contains("HOE") || itemStack.getType().name().contains("SHOVEL") || itemStack.getType().equals(Material.STICK))
                    cs = SHOPS_FILE.get().getConfigurationSection("postures.sword");
                else if (itemStack.getType().equals(Material.SHIELD))
                    cs = SHOPS_FILE.get().getConfigurationSection("postures.shield");
                else if (itemStack.getType().equals(Material.BOW))
                    cs = SHOPS_FILE.get().getConfigurationSection("postures.bow");
                else if (itemStack.getType().equals(Material.TRIDENT))
                    cs = SHOPS_FILE.get().getConfigurationSection("postures.trident");
                else
                    cs = SHOPS_FILE.get().getConfigurationSection("postures.armour");
            }
        }

        if (cs == null) {
            Messages.WARN.sendConsole("Loading no display items for shop '" + key + "'. This is an error.");
            ArmorStand displayStand = spawnArmorStand(new Vector(0.5, 0, 0.5));
            Objects.requireNonNull(displayStand.getEquipment()).setItemInMainHand(itemStack);
            displayStand.setRightArmPose(new EulerAngle(0, 0, 0));
            return;
        }

        for (String key : cs.getKeys(false)) {
            Vector location = Statics.getXYZFromString(cs.getString(key + ".location"));
            Vector rotation = Statics.getXYZFromString(cs.getString(key + ".rotation"));
            if (location == null)
                location = new Vector(0, 90, 0);
            if (rotation == null)
                rotation = new Vector(0, 2, 0);
            ArmorStand displayStand = spawnArmorStand(location);
            displayStand.setSmall(cs.getBoolean(key + ".small", false));
            if (cs.getBoolean(key + ".head", false)) {
                displayStand.setHeadPose(new EulerAngle(rotation.getX(), rotation.getY(), rotation.getZ()));
                Objects.requireNonNull(displayStand.getEquipment()).setHelmet(itemStack);
            } else {
                Objects.requireNonNull(displayStand.getEquipment()).setItemInMainHand(itemStack);
                displayStand.setRightArmPose(new EulerAngle(rotation.getX(), rotation.getY(), rotation.getZ()));
            }
        }
    }

    protected boolean hasPermission(Player player, String identifier) {
        return Permission.SHOP_NOLIMIT.hasPermission(player)
                || Permission.CUSTOM.hasPermission(player, "chunkdefence.shop." + TYPE.get() + ".*")
                || Permission.CUSTOM.hasPermission(player, "chunkdefence.shop." + TYPE.get() + "." + identifier);
    }

    protected boolean handleMoney(Player player) {
        if (player == null)
            return false;

        if (EconomyManager.getInstance().getBalance(player) < getCost()) {
            Messages.SHOP_BUY_NO_MONEY.send(player);
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, SoundCategory.NEUTRAL, 1, 1);
            return false;
        }
        if (player.getInventory().firstEmpty() == -1) {
            Messages.SHOP_BUY_NO_SPACE.send(player);
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, SoundCategory.NEUTRAL, 1, 1);
            return false;
        }
        EconomyManager.getInstance().withdrawPlayer(player, getCost());
        Messages.SHOP_BUY_SUCCESS.send(player);
        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.NEUTRAL, 1, 1);
        return true;
    }

    private ArmorStand spawnArmorStand(Vector vector) {
        Location location = shopLocation.getLocation().clone().add(vector);
        ArmorStand displayStand = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
        displayStand.setGravity(false);
        displayStand.setArms(true);
        displayStand.setVisible(false);
        displayStand.setCustomNameVisible(false);
        displayStand.getPersistentDataContainer().set(Keys.SHOP_KEY, PersistentDataType.INTEGER, ShopManager.getRandomEntityID());
        displayEntities.add(displayStand);
        return displayStand;
    }

    protected void loadFloatingText() {
        textList.clear();
        ConfigurationSection cs = SHOPS_FILE.get().getConfigurationSection((PATH + ".messages").replace("%", key));
        double cost = getCost();

        if (cs == null) {
            Messages.CUSTOM.sendConsole("cs is null of " + (PATH + ".messages").replace("%", key) + ".messages");
            return;
        }

        for (String index : cs.getKeys(false)) {
            Vector distance = Statics.getXYZFromString(cs.getString(index + ".offset"));
            String text = Objects.requireNonNull(cs.getString(index + ".text", ""))
                    .replace("%PRICE%", String.valueOf(cost % 1 == 0.0 ? ((int) cost) : cost)); //Todo add placeholders from ShopTypes too
            textList.add(new FloatingText(text, distance != null ? distance : new Vector(0, 0, 2)));
        }
    }

    private String getFreePath(ShopType type) {
        int i = 0;
        while (true) {
            String path = type.toString().toLowerCase() + "-" + i;
            if (!SHOPS_FILE.get().isSet("shop." + path))
                return path;
            i++;
        }
    }
}
