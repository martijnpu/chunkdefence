package NL.martijnpu.ChunkDefence.Traps;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapData;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapDataManager;
import NL.martijnpu.ChunkDefence.Utils.Keys;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Slab;
import org.bukkit.block.data.type.Stairs;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataHolder;
import org.bukkit.persistence.PersistentDataType;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class TrapManager {
    private static TrapManager instance;
    private final List<TrapLocation> trapLocations = new ArrayList<>();

    private TrapManager() {
        TrapDataManager.getInstance().printInitData();
        loadEntityTraps();
    }

    public static TrapManager getInstance() {
        if (instance == null)
            instance = new TrapManager();
        return instance;
    }

    public static void resetInstance() {
        TrapDataManager.resetInstance();
        instance = null;
    }

    /**
     * Get the durability of the item
     */
    public static int getDurability(ItemMeta itemMeta) {
        if (!isTrap(itemMeta))
            return -1;

        Integer durability = itemMeta.getPersistentDataContainer().get(Keys.TRAP_DURABILITY, PersistentDataType.INTEGER);
        return durability == null ? -1 : durability;
    }

    /**
     * Check if the <i>ItemStack</i> or <i>Entity</i> is a trap instance
     */
    public static boolean isTrap(PersistentDataHolder holder) {
        return holder != null && holder.getPersistentDataContainer().has(Keys.TRAP_KEY, PersistentDataType.STRING);

    }

    public void printInitData() {
        Messages.PLUGIN.sendConsole("Loading " + trapLocations.size() + " active traps.");
    }

    /**
     * Loop through all World and all entities and try to add them as Trap
     */
    public void loadEntityTraps() {
        ChunkDefence.get().getServer().getWorlds().forEach(world -> world.getEntities().forEach(this::addEntity));
    }

    /**
     * Add the entity to the TrapList if the entity is a Trap instance
     */
    public void addEntity(Entity entity) {
        if (entity.getType() == EntityType.AREA_EFFECT_CLOUD
                && trapLocations.stream().noneMatch(trapLocation -> trapLocation.display.equals(entity))
                && isTrap(entity)) {
            try {
                trapLocations.add(new TrapLocation((AreaEffectCloud) entity));
            } catch (IllegalArgumentException ignore) {
                // Already handled
            }
        }
    }

    /**
     * Place a Trap on a location
     *
     * @param player     Executing player, null if console
     * @param block      Location where the block will be placed
     * @param trapData   TrapData of the placed trap
     * @param durability Custom durability of the trap
     * @throws IllegalAccessException Thrown if the trap could not be placed. Exception can be ignored
     */
    public void placeTrap(@Nullable Player player, Block block, TrapData trapData, int durability) throws IllegalAccessException {
        //Check permission & location
        if (player != null && !Permission.ADMIN_TRAP_PLACE.hasPermission(player)) {
            Arena arena = ArenaManager.getInstance().getArena(player);
            if (arena == null || !arena.isInsideArena(block.getLocation())) {
                Messages.TRAP_INVALID_OUTSIDE.send(player);
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                throw new IllegalAccessException();
            }
        }

        //Can place down
        Material blockTypeBelow = block.getRelative(BlockFace.DOWN).getType();
        BlockData blockDataBelow = block.getRelative(BlockFace.DOWN).getBlockData();
        if (!(blockTypeBelow.isOccluding()
                || (blockDataBelow instanceof Slab && ((Slab) blockDataBelow).getType() == Slab.Type.TOP)
                || (blockDataBelow instanceof Stairs && ((Stairs) blockDataBelow).getHalf() == Bisected.Half.TOP))
                || !block.getType().isAir()
                || blockTypeBelow == Material.BARRIER) {
            Messages.TRAP_INVALID_PLACE.send(player);
            if (player != null) player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
            throw new IllegalAccessException();
        }

        //Has no traps around (+)
        if (getTrapLocation(block.getRelative(BlockFace.NORTH)) != null
                || getTrapLocation(block.getRelative(BlockFace.EAST)) != null
                || getTrapLocation(block.getRelative(BlockFace.SOUTH)) != null
                || getTrapLocation(block.getRelative(BlockFace.WEST)) != null
                || getTrapLocation(block.getRelative(BlockFace.DOWN)) != null) {
            Messages.TRAP_INVALID_NEXTTO.send(player);
            if (player != null) player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
            throw new IllegalAccessException();
        }

        trapLocations.add(new TrapLocation(block, trapData, (player == null) ? null : player.getUniqueId(), durability));

        if (player != null && !player.getGameMode().equals(GameMode.CREATIVE))
            player.getInventory().getItemInMainHand().setAmount(player.getInventory().getItemInMainHand().getAmount() - 1);
    }

    /**
     * Destroy the trap on the given location
     *
     * @param player Executing player, can be null if Console
     * @param block  Destroyed block
     */
    public void destroyTrap(@Nullable Player player, Block block) {
        TrapLocation trap = getTrapLocation(block);

        if (trap == null) {
            if (player != null)
                Messages.WARN.sendConsole("No trap found to destroy at location " + Statics.getStringFromLocation(block.getLocation()));
            return;
        }

        if (player != null && !Permission.ADMIN_DESTROY.hasPermission(player)) {
            if (trap.getOwner() == null) {
                Messages.TRAP_INVALID_OWNER.send(player, "ChunkDefence");
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                return;
            }
            if (!player.getUniqueId().toString().equals(trap.getOwner())) {
                String name = ChunkDefence.get().getServer().getOfflinePlayer(UUID.fromString(trap.getOwner())).getName();
                Messages.TRAP_INVALID_OWNER.send(player, (name == null ? "Unknown" : name));
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                return;
            }
        }

        if (player != null && !player.getGameMode().equals(GameMode.CREATIVE)) {
            if (player.getInventory().firstEmpty() == -1) {
                Messages.EXCEPT_FULL.send(player);
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                return;
            }
            player.getInventory().addItem(trap.trapData.generateTrapItem(1, trap.getDurability()));
        }

        trapLocations.remove(trap);
        block.getWorld().playSound(block.getLocation(), Sound.BLOCK_TRIPWIRE_DETACH, 1, 1);
        trap.remove();
    }

    /**
     * Get Trap at the blocklocation
     *
     * @param block Location
     * @return TrapLocation or null
     */
    @Nullable
    public TrapLocation getTrapLocation(Block block) {
        return trapLocations.stream().filter(traps -> traps.block.equals(block)).findFirst().orElse(null);
    }

    public void handleEngineTick() {
        List<TrapLocation> copyTrapLocations = new ArrayList<>(trapLocations);

        for (TrapLocation trap : copyTrapLocations) {
            trap.tickEngine();
            if (trap.canRemoveTrap())
                trapLocations.remove(trap);
        }
    }
}
