package NL.martijnpu.ChunkDefence.Traps;

import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapData;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapDataManager;
import NL.martijnpu.ChunkDefence.Utils.Keys;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import NL.martijnpu.ChunkDefence.Utils.TimeHandler;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Waterlogged;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.util.UUID;


public final class TrapLocation {
    public final TrapData trapData;
    public final Block block;
    public final AreaEffectCloud display;
    private final Vector DISPLAY_DELTA = new Vector(0.5, 0.2, 0.5);
    private final Vector CLOUD_DELTA = new Vector(0.5, 0.4, 0.5);
    private AreaEffectCloud setupCloud = null;
    private AreaEffectCloud durabilityCloud = null;
    private double timeTillBuild = -1;
    private long tickSinceAttack = 0;

    /**
     * Import a TrapLocation based on an existing AreaEffectCloud
     */
    TrapLocation(AreaEffectCloud display) {
        this.display = display;

//        if (getOwner() == null) {//Trap is shop
//            remove();
//            throw new IllegalArgumentException();
//        }

        block = display.getLocation().getBlock();
        trapData = TrapDataManager.getInstance().getTrapBase(display.getPersistentDataContainer().get(Keys.TRAP_KEY, PersistentDataType.STRING));
        if (trapData == null) {
            Messages.WARN.sendConsole("Couldn't load trap at location: " + Statics.getStringFromLocation(display.getLocation()) + ". Illegal name: " + display.getPersistentDataContainer().get(Keys.TRAP_KEY, PersistentDataType.STRING));
            remove();
            Messages.WARN.sendConsole("Removed unknown trap.");
            throw new IllegalArgumentException();
        }

        setTrapBlock();
        display.setCustomNameVisible(trapData.NAME_VISIBLE.get() && getOwner() != null); //Invisible if shop or config
        if (!display.getPersistentDataContainer().has(Keys.TRAP_DURABILITY))
            display.getPersistentDataContainer().set(Keys.TRAP_DURABILITY, PersistentDataType.INTEGER, trapData.DURABILITY.get());
    }

    /**
     * Create a new trapLocation and setup blocks & entities
     * Method also sets the full TrapLocation
     * <b><i>If the Owner is null, the trap is considered as shop and therefore not attacking</i></b>
     *
     * @param block      New BlockLocation
     * @param trapData   Defined TrapData
     * @param owner      Executing player, null is console.
     * @param durability Custom durability
     */
    TrapLocation(Block block, TrapData trapData, @Nullable UUID owner, int durability) {
        this.block = block;
        this.trapData = trapData;
        timeTillBuild = (owner == null) ? -1 : trapData.SETUP_TIME.get();

        display = generateCloud(DISPLAY_DELTA, Integer.MAX_VALUE);
        display.getPersistentDataContainer().set(Keys.TRAP_KEY, PersistentDataType.STRING, trapData.key);
        display.getPersistentDataContainer().set(Keys.TRAP_DURABILITY, PersistentDataType.INTEGER, durability);
        display.setCustomNameVisible(trapData.NAME_VISIBLE.get() && owner != null); //Invisible if shop or config
        if (owner == null)
            display.getPersistentDataContainer().set(Keys.SHOP_KEY, PersistentDataType.INTEGER, ShopManager.getRandomEntityID()); //Auto remove at reload
        else
            display.getPersistentDataContainer().set(Keys.TRAP_OWNER, PersistentDataType.STRING, owner.toString());

        setTrapBlock();
    }

    /**
     * Update TrapBlock according TrapData
     */
    private void setTrapBlock() {
        block.setType(trapData.BLOCK.get());
        BlockData blockdata = block.getType().createBlockData();
        if (blockdata instanceof Waterlogged waterData) {
            waterData.setWaterlogged(false);
            block.setBlockData(waterData);
        }
    }

    /**
     * Remove TrapLocation
     */
    public void remove() {
        if (!display.isValid() || block == null) //Already removed
            return;

        display.remove();
        if (durabilityCloud != null)
            durabilityCloud.remove();
        if (setupCloud != null)
            setupCloud.remove();
        block.setType(Material.AIR);
    }

    /**
     * Lower durability by 1
     */
    public void lowerDurability() {
        display.getPersistentDataContainer().set(Keys.TRAP_DURABILITY, PersistentDataType.INTEGER, getDurability() - 1);
    }

    public int getDurability() {
        Integer durability = display.getPersistentDataContainer().get(Keys.TRAP_DURABILITY, PersistentDataType.INTEGER);
        return durability == null ? -1 : durability;
    }

    @Nullable
    public String getOwner() {
        return display.getPersistentDataContainer().get(Keys.TRAP_OWNER, PersistentDataType.STRING);
    }

    /**
     * Reload the TrapLocation. All instances and blocks will be updated.
     * Not used at this moment
     */
    void reloadTrap() {
        setTrapBlock();
        display.setCustomNameVisible(trapData.NAME_VISIBLE.get() && getOwner() != null); //Invisible if shop or config
        if (setupCloud != null)
            setupCloud.remove();
        if (durabilityCloud != null)
            durabilityCloud.remove();
    }

    /**
     * Check whether the conditions are correct to remove this TrapLocation
     */
    boolean canRemoveTrap() {
        return !display.getLocation().getChunk().isLoaded() || getDurability() <= 0;
    }

    /**
     * Run the build-timer, damage and effects
     */
    void tickEngine() {
        if (!display.isValid()) {
            remove();
            return;
        }

        if (!display.getLocation().getChunk().isLoaded())
            return;

        updateSetupCloud();
        updateDurabilityCloud();
        updateParticle();
        attack();

        if (getDurability() <= 0 && getOwner() != null) { //Durability is always 0 as ShopTrap
            block.getWorld().spawnParticle(trapData.PARTICLE_TYPE.get(), block.getLocation(), trapData.PARTICLE_AMOUNT.get() * 5, 0.5, 0.5, 0.5, (double) trapData.PARTICLE_SPEED.get());
            display.getWorld().playSound(display.getLocation(), Sound.ENTITY_ZOMBIE_BREAK_WOODEN_DOOR, 1, 1);
            remove();
        }
    }

    /**
     * Update the status of the SetupCloud
     */
    private void updateSetupCloud() {
        if (!(timeTillBuild > 0)) {
            if (setupCloud != null && setupCloud.isValid())
                setupCloud.remove();
            return;
        }

        if (setupCloud == null || !setupCloud.isValid())
            setupCloud = generateCloud(CLOUD_DELTA, (int) (trapData.SETUP_TIME.get() * TimeHandler.SEC));

        timeTillBuild -= (1.0 / TimeHandler.SEC) * ConfigData.PARTICLE_FREQUENCY.get();
        setupCloud.customName(Component.text(Statics.roundToFull(timeTillBuild)).color(NamedTextColor.YELLOW));
    }

    /**
     * Update the status of the DurabilityCloud
     */
    private void updateDurabilityCloud() {
        int durability = getDurability();
        if (timeTillBuild > 0 || getOwner() == null) {
            if (durabilityCloud != null && durabilityCloud.isValid())
                durabilityCloud.remove();
            return;
        }

        if (durabilityCloud == null || !durabilityCloud.isValid())
            durabilityCloud = generateCloud(CLOUD_DELTA, -1);

        durabilityCloud.setDuration((int) (30 * TimeHandler.SEC));

        double percentage = 100.0 * durability / trapData.DURABILITY.get();
        durabilityCloud.customName(Component.text(durability).color(getColorByPercentage(percentage)));
    }


    private void updateParticle() {
        final int ACTIVATION_DISTANCE = 30;

        if (timeTillBuild > 0 || getOwner() == null)
            return;

        boolean playerFound = !display.getWorld().getNearbyEntities(display.getLocation(), ACTIVATION_DISTANCE, ACTIVATION_DISTANCE, ACTIVATION_DISTANCE, Player.class::isInstance).isEmpty();
        if (playerFound) //Only particles if player is near
            trapData.PARTICLE_PATTERN.get().spawnParticle(display, trapData);
    }

    private void attack() {
        if (timeTillBuild > 0 || getDurability() <= 0 || getOwner() == null)
            return;

        tickSinceAttack += ConfigData.PARTICLE_FREQUENCY.get();

        if (tickSinceAttack >= trapData.INTERVAL.get() * TimeHandler.SEC)
            if (trapData.DAMAGE_TYPE.get().attack(this))
                tickSinceAttack = 0;
    }

    private AreaEffectCloud generateCloud(Vector vector, int duration) {
        AreaEffectCloud cloud = (AreaEffectCloud) block.getWorld().spawnEntity(block.getLocation().clone().add(vector), EntityType.AREA_EFFECT_CLOUD);
        cloud.setParticle(Particle.BLOCK_CRACK, Material.AIR.createBlockData());
        cloud.setWaitTime(0);
        cloud.setSilent(true);
        cloud.setInvulnerable(true);
        cloud.setRadius(0);
        cloud.setDuration(duration);
        cloud.setCustomNameVisible(true);
        cloud.customName(Messages.formatLegacy(trapData.NAME.get()));
        return cloud;
    }

    /**
     * Get a color based on a percentage where 100% = Green and 0% = Red
     *
     * @param percentage 0..100%
     * @return Legacy hexadecimal color format <i>(&x&.&.&.&.&.&.)</i>
     */
    private TextColor getColorByPercentage(double percentage) {
        percentage /= 100.0;
        if (percentage > 1 || percentage < 0)
            return TextColor.color(204, 0, 204);
        return TextColor.color(limit(255 * 2 - (255 * (percentage * 2))), limit(255 * (percentage * 2)), 0);
    }

    /**
     * Limit double between 0 and 255 and convert to int
     */
    private int limit(double i) {
        return (int) Math.max(0, Math.min(255, i));
    }
}
