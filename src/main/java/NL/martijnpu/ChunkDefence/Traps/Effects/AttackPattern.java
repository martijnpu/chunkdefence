package NL.martijnpu.ChunkDefence.Traps.Effects;

import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapData;
import NL.martijnpu.ChunkDefence.Traps.TrapLocation;
import org.bukkit.Location;
import org.bukkit.SoundCategory;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public enum AttackPattern {
    NONE, //No Particles
    AREA_TRAP, //Particles in the area of the Trap
    AREA_ATTACK, //Particles in the area of the Entity
    AREA_BOTH, //Particles in the area of both the Trap as the Entity
    PROJECTILE; //Draw line from the Trap to the Entity

    public void summonAttackParticles(TrapLocation trapLocation, Entity entityAttacked) {
        switch (this) {
            case AREA_TRAP -> summonAreaTrap(trapLocation);
            case AREA_ATTACK -> summonAreaAttack(trapLocation.trapData, entityAttacked);
            case AREA_BOTH -> {
                summonAreaTrap(trapLocation);
                summonAreaAttack(trapLocation.trapData, entityAttacked);
            }
            case PROJECTILE ->
                    drawLine(trapLocation.display.getLocation(), entityAttacked.getLocation().add(0, 1, 0), trapLocation.display.getWorld(), trapLocation.trapData);
            default -> {}
        }
        trapLocation.display.getWorld().playSound(trapLocation.display.getLocation(), trapLocation.trapData.ATTACK_SOUND.get(), SoundCategory.PLAYERS, (float) 0.5, 1);
    }

    private void summonAreaTrap(TrapLocation trapLocation) {
        trapLocation.display.getWorld().spawnParticle(trapLocation.trapData.PARTICLE_TYPE.get(), trapLocation.display.getLocation(), trapLocation.trapData.PARTICLE_AMOUNT.get() * 2, 1, 1, 1, (double) trapLocation.trapData.PARTICLE_SPEED.get());
    }

    private void summonAreaAttack(TrapData trapData, Entity entityAttacked) {
        entityAttacked.getWorld().spawnParticle(trapData.PARTICLE_TYPE.get(), entityAttacked.getLocation(), trapData.PARTICLE_AMOUNT.get() * 4, 0.5, 0.5, 0.5, (double) trapData.PARTICLE_SPEED.get());
    }

    private void drawLine(Location point1, Location point2, World world, TrapData trapData) {
        double distance = point1.distance(point2);
        Vector p1 = point1.toVector();
        Vector p2 = point2.toVector();
        Vector vector = p2.clone().subtract(p1).normalize().multiply(0.2);
        for (double length = 0; length < distance; length += 0.2) {
            world.spawnParticle(trapData.PARTICLE_TYPE.get(), p1.getX(), p1.getY(), p1.getZ(), trapData.PARTICLE_AMOUNT.get() / 4, 0.1, 0.1, 0.1, (double) trapData.PARTICLE_SPEED.get());
            p1.add(vector);
        }
    }
}
