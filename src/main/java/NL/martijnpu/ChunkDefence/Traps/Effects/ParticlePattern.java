package NL.martijnpu.ChunkDefence.Traps.Effects;

import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapData;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public enum ParticlePattern {
    NONE, //No particles
    CIRCLE, //Rotating particles on the ground
    FLOOR, //Random particles on the ground
    SPREAD, //Random particles in the air
    SURROUND, //Two vertical perpendicular circles around the trap
    SPHERE, //Half a sphere around the trap
    CYLINDER, //Rotating hollow cylinder around the trap
    FIREFLY; //Slanted circles around the tra

    public void spawnParticle(Entity entity, TrapData trapData) {
        double size = trapData.PARTICLE_SIZE.get();

        switch (this) {
            case SPREAD ->
                    entity.getWorld().spawnParticle(trapData.PARTICLE_TYPE.get(), entity.getLocation(), (int) trapData.PARTICLE_AMOUNT.get(), size, size, size, (double) trapData.PARTICLE_SPEED.get());
            case FLOOR ->
                    entity.getWorld().spawnParticle(trapData.PARTICLE_TYPE.get(), entity.getLocation().clone().add(0, -0.2, 0), (int) trapData.PARTICLE_AMOUNT.get(), size, 0, size, (double) trapData.PARTICLE_SPEED.get());
            default -> {
                for (Vector vector : trapData.getParticleDelta())
                    entity.getWorld().spawnParticle(trapData.PARTICLE_TYPE.get(), entity.getLocation().clone().add(vector), 1, 0, 0, 0, (double) trapData.PARTICLE_SPEED.get());
            }
        }
    }

    public List<Vector> calculateNextDelta(TrapData trapData) {
        List<Vector> deltas = new ArrayList<>();

        if (trapData == null || trapData.PARTICLE_AMOUNT.get() <= 0)
            return deltas;

        int amount = trapData.PARTICLE_AMOUNT.get();
        double size = trapData.PARTICLE_SIZE.get();

        switch (this) {
            case SURROUND -> {
                for (int i = 0; i < amount; i++) {
                    float[] v = calcSinCos(trapData, i, false);

                    if (v[0] < -0.3) //underground
                        continue;

                    if (i % 2 == 0)
                        deltas.add(new Vector(v[1], v[0], v[2]));
                    else
                        deltas.add(new Vector(v[2], v[0], v[1]));
                }
            }
            case SPHERE -> {
                for (int i = 0; i < amount; i++) {
                    float[] v = calcSinCos(trapData, i, false);

                    if (v[0] < -0.3) //underground
                        continue;

                    if (i % 2 == 0)
                        deltas.add(new Vector(v[1], v[0], v[2]).rotateAroundY(trapData.getParticlePeriod()));
                    else
                        deltas.add(new Vector(v[2], v[0], v[1]).rotateAroundY(trapData.getParticlePeriod()));
                }
            }
            case CIRCLE -> {
                for (int i = 0; i < amount; i++) {
                    float[] v = calcSinCos(trapData, i, false);
                    deltas.add(new Vector(v[0], v[2], v[1]));
                }
            }
            case CYLINDER -> {
                for (int i = 0; i < amount; i++) {
                    float[] v = calcSinCos(trapData, i, true);
                    deltas.add(new Vector(v[0], v[2], v[1]));
                }
            }
            case FIREFLY -> {
                for (int i = 0; i < amount; i++) {
                    float vX = (float) (Math.sin(Math.toRadians(trapData.getParticlePeriod() * 2) + ((2 * Math.PI) / amount) * i) * size);
                    float vY = (float) (Math.sin(Math.toRadians(trapData.getParticlePeriod() * 3) + ((2 * Math.PI) / amount) * i) * size);
                    float vZ = (float) (Math.cos(Math.toRadians(trapData.getParticlePeriod()) + ((2 * Math.PI) / amount) * i) * size);

                    deltas.add(new Vector(vX, vY + size, vZ));
                }
            }

            default -> {} //Floor & Spread have a random spawning
        }
        return deltas;
    }

    private float[] calcSinCos(TrapData trapData, int t, boolean height) {
        float[] v = new float[3];
        int amount = trapData.PARTICLE_AMOUNT.get();
        double size = trapData.PARTICLE_SIZE.get();

        v[0] = (float) (Math.sin(Math.toRadians(trapData.getParticlePeriod()) + ((2 * Math.PI) / amount) * t) * size);
        v[1] = (float) (Math.cos(Math.toRadians(trapData.getParticlePeriod()) + ((2 * Math.PI) / amount) * t) * size);
        v[2] = height ? (float) (Math.sin(Math.toRadians(trapData.getParticlePeriod())) * 0.8 + 0.5 * size) : 0;
        return v;
    }
}
