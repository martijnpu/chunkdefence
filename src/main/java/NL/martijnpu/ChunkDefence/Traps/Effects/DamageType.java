package NL.martijnpu.ChunkDefence.Traps.Effects;


import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapData;
import NL.martijnpu.ChunkDefence.Traps.TrapLocation;
import NL.martijnpu.ChunkDefence.Utils.Keys;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.List;

public enum DamageType {
    NONE, //No interaction with the Target
    PHYSICAL, //Remove the health of the Target
    EFFECT; //Give the Target an effect

    /**
     * Execute an attack, ignores if no entities in sight
     *
     * @param trapLocation Trap the attack is coming from
     * @return Whether entities are attacked
     */
    public boolean attack(TrapLocation trapLocation) {
        if (this == NONE || trapLocation.getOwner() == null || trapLocation.trapData == null)
            return false;

        double range = trapLocation.trapData.RANGE.get();
        List<Entity> entities = trapLocation.display.getNearbyEntities(range, range, range);
        List<LivingEntity> targets = new ArrayList<>();

        for (Entity entity : entities) {
            if (isTarget(entity, trapLocation.trapData))
                targets.add((LivingEntity) entity);
            if (targets.size() >= trapLocation.trapData.TARGET_AMOUNT.get())
                break;
        }

        if (targets.isEmpty())
            return false;

        for (LivingEntity targetEntity : targets) {
            switch (this) {
                case PHYSICAL -> targetEntity.damage(trapLocation.trapData.DAMAGE_AMOUNT.get());
                case EFFECT -> targetEntity.addPotionEffect(trapLocation.trapData.getPotionEffect());
                default -> {}
            }

            if (this == PHYSICAL) //Lower Durability per individual attack
                trapLocation.lowerDurability();

            if (trapLocation.trapData.SET_FIRE.get())
                targetEntity.setFireTicks(100);

            targetEntity.getPersistentDataContainer().set(Keys.ENTITY_LAST_ATTACKED, PersistentDataType.STRING, trapLocation.getOwner());
            trapLocation.trapData.ATTACK_PATTERN.get().summonAttackParticles(trapLocation, targetEntity);
        }

        if (this == EFFECT) //Lower Durability once per active tick
            trapLocation.lowerDurability();

        return true;
    }

    private boolean isTarget(Entity entity, TrapData trapData) {
        List<String> stringList = trapData.TARGET_ENTITIES.get();
        List<EntityType> entityList = new ArrayList<>();
        stringList.forEach(x -> entityList.add(EntityType.valueOf(x.toUpperCase())));

        return entity instanceof LivingEntity
                && (entity.getType() != EntityType.ARMOR_STAND) //Ignore untouchable entities
                && !(entityList.isEmpty() && entity.getType() == EntityType.PLAYER) //Ignore players on all Mobs
                && (entityList.isEmpty() || entityList.contains(entity.getType())); //All Mobs or Entity is on the list
    }
}
