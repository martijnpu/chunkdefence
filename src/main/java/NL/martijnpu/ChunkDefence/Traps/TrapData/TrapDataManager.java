package NL.martijnpu.ChunkDefence.Traps.TrapData;

import NL.martijnpu.ChunkDefence.Data.FileHandler;
import NL.martijnpu.ChunkDefence.Traps.TrapManager;
import NL.martijnpu.ChunkDefence.Utils.Keys;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class TrapDataManager {
    private static TrapDataManager instance;
    private final List<TrapData> trapDataList = new ArrayList<>();

    private TrapDataManager() {
        loadTrapsFile();
    }

    public static TrapDataManager getInstance() {
        if (instance == null)
            instance = new TrapDataManager();
        return instance;
    }

    public static void resetInstance() {
        instance = null;
    }

    public void printInitData() {
        if (trapDataList.isEmpty())
            Messages.WARN.sendConsole("Loading none defined traps. This could be an error or first startup.");
        else
            Messages.PLUGIN.sendConsole("Loading " + trapDataList.size() + " defined traps.");
    }

    public List<String> getAllTrapNames() {
        return trapDataList.stream().map(x -> x.key).collect(Collectors.toList());
    }

    public List<TrapData> getAllTrapBases() {
        return trapDataList;
    }

    public TrapData createNewTrapBase() {
        TrapData trapData = new TrapData(null);
        trapDataList.add(trapData);
        return trapData;
    }

    public void removeTrapBase(TrapData trapData) {
        if (trapData == null)
            return;
        trapDataList.remove(trapData);
        trapData.removeTrap();
    }

    /**
     * Get TrapData based on ItemMeta.
     * Null if no key found
     */
    @Nullable
    public TrapData getTrapBase(@Nullable ItemMeta itemMeta) {
        if (!TrapManager.isTrap(itemMeta))
            return null;

        String key = itemMeta.getPersistentDataContainer().get(Keys.TRAP_KEY, PersistentDataType.STRING);
        return key == null ? null : getTrapBase(key);
    }

    /**
     * Get TrapData based on key/name
     * Return null if no TrapData found
     */
    @Nullable
    public TrapData getTrapBase(String index) {
        for (TrapData trapData : trapDataList) {
            if (index.equalsIgnoreCase(trapData.key) || index.equalsIgnoreCase(trapData.NAME.get()))
                return trapData;
        }
        return null;
    }

    public void trapDataTick() {
        trapDataList.forEach(TrapData::updateParticleLocation);
    }

    private void loadTrapsFile() {
        String path = "traps";
        trapDataList.clear();
        ConfigurationSection cs = FileHandler.TRAPS_FILE.get().getConfigurationSection(path);

        if (cs == null || cs.getKeys(false).isEmpty()) {
            return;
        }

        for (String key : cs.getKeys(false)) {
            try {
                trapDataList.add(new TrapData(key));
            } catch (Exception ex) {
                Messages.WARN.sendConsole("Unable to read defined trap: " + key);
                ex.printStackTrace();
            }
        }
    }
}
