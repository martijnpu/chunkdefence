package NL.martijnpu.ChunkDefence.Traps.TrapData;

import NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyInstance;
import NL.martijnpu.ChunkDefence.Traps.Effects.AttackPattern;
import NL.martijnpu.ChunkDefence.Traps.Effects.DamageType;
import NL.martijnpu.ChunkDefence.Traps.Effects.ParticlePattern;
import NL.martijnpu.ChunkDefence.Utils.Keys;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

import static NL.martijnpu.ChunkDefence.Data.FileHandler.TRAPS_FILE;
import static NL.martijnpu.ChunkDefence.Data.Utils.ConfigKeyAdapter.*;

public final class TrapData {
    public final ConfigKeyInstance<String> NAME;
    public final ConfigKeyInstance<Material> DISPLAY_ITEM; //Item in hand #Minecraft
    public final ConfigKeyInstance<Boolean> NAME_VISIBLE; //Name visible when placed
    public final ConfigKeyInstance<Boolean> ITEM_GLOW; //Item glowing in hand
    public final ConfigKeyInstance<String> LORE; //Item lore
    public final ConfigKeyInstance<Material> BLOCK; //Blocktype #Minecraft
    public final ConfigKeyInstance<Integer> DURABILITY; //Amount of attacks before breaking
    public final ConfigKeyInstance<Double> SETUP_TIME; //Time before Trap is active in seconds

    public final ConfigKeyInstance<DamageType> DAMAGE_TYPE; //Type of damage [PHYSICAL, EFFECT or NONE]
    public final ConfigKeyInstance<Double> DAMAGE_AMOUNT; //Damage per attack in hearts
    public final ConfigKeyInstance<Boolean> SET_FIRE; //Set targets on fire
    public final ConfigKeyInstance<Double> INTERVAL; //Interval between attacks in seconds
    public final ConfigKeyInstance<Double> RANGE; //Attack range in Blocks

    public final ConfigKeyInstance<String> POTION_EFFECT; //Potion effect #Minecraft
    public final ConfigKeyInstance<Integer> POTION_AMPLIFIER; //Potion amplifier
    public final ConfigKeyInstance<Double> POTION_TIME; //Potion duration in seconds

    public final ConfigKeyInstance<ParticlePattern> PARTICLE_PATTERN; //Particle pattern #Minecraft
    public final ConfigKeyInstance<Double> PARTICLE_PATTERN_SPEED; //Speed to follow the selected patterns [0.1 - 20.0]
    public final ConfigKeyInstance<Particle> PARTICLE_TYPE; //Particle Type #Minecraft
    public final ConfigKeyInstance<Double> PARTICLE_SPEED; //Particle Movementspeed (Shooting away) [0.0]
    public final ConfigKeyInstance<Double> PARTICLE_SIZE; //Distance from Trap in Blocks [0.1 - 2.0]
    public final ConfigKeyInstance<Integer> PARTICLE_AMOUNT; //Amount of particles rendered each ParticleTick [1 - 20]

    public final ConfigKeyInstance<AttackPattern> ATTACK_PATTERN; //Pattern during an attack
    public final ConfigKeyInstance<Sound> ATTACK_SOUND; //Sound during an attack #Minecraft

    public final ConfigKeyInstance<Integer> TARGET_AMOUNT; //Amount of targets per attack
    public final ConfigKeyInstance<List<String>> TARGET_ENTITIES; //List of available entities to attack #Minecraft
    public final String key;
    private List<Vector> particleDelta = new ArrayList<>();
    private long particlePeriod = 0;

    public TrapData(@Nullable String key) {
        if (key == null || key.isEmpty())
            key = getNextTrapKey();
        this.key = key;

        NAME = new ConfigKeyInstance<>(stringKey(TRAPS_FILE, "traps.%.name", "Unknown"), key);
        NAME_VISIBLE = new ConfigKeyInstance<>(booleanKey(TRAPS_FILE, "traps.%.display-name", true), key);
        DISPLAY_ITEM = new ConfigKeyInstance<>(enumKey(TRAPS_FILE, "traps.%.display-item", Material.DIRT), key);
        ITEM_GLOW = new ConfigKeyInstance<>(booleanKey(TRAPS_FILE, "traps.%.item-glow", false), key);
        LORE = new ConfigKeyInstance<>(stringKey(TRAPS_FILE, "traps.%.lore", ""), key);
        BLOCK = new ConfigKeyInstance<>(enumKey(TRAPS_FILE, "traps.%.trap-block", Material.DEAD_BUSH), key);
        DURABILITY = new ConfigKeyInstance<>(integerKey(TRAPS_FILE, "traps.%.durability", 0), key);
        SETUP_TIME = new ConfigKeyInstance<>(doubleKey(TRAPS_FILE, "traps.%.setup-time", 3), key);

        PARTICLE_PATTERN = new ConfigKeyInstance<>(enumKey(TRAPS_FILE, "traps.%.particle.pattern", ParticlePattern.NONE), key);
        PARTICLE_PATTERN_SPEED = new ConfigKeyInstance<>(doubleKey(TRAPS_FILE, "traps.%.particle.pattern-speed", 1), key);
        PARTICLE_TYPE = new ConfigKeyInstance<>(enumKey(TRAPS_FILE, "traps.%.particle.type", Particle.FLAME), key);
        PARTICLE_SPEED = new ConfigKeyInstance<>(doubleKey(TRAPS_FILE, "traps.%.particle.particle-speed", 1), key);
        PARTICLE_SIZE = new ConfigKeyInstance<>(doubleKey(TRAPS_FILE, "traps.%.particle.size", 0), key);
        PARTICLE_AMOUNT = new ConfigKeyInstance<>(integerKey(TRAPS_FILE, "traps.%.particle.amount", 1), key);

        ATTACK_PATTERN = new ConfigKeyInstance<>(enumKey(TRAPS_FILE, "traps.%.attack.type", AttackPattern.NONE), key);
        ATTACK_SOUND = new ConfigKeyInstance<>(enumKey(TRAPS_FILE, "traps.%.attack.sound", Sound.BLOCK_DISPENSER_LAUNCH), key);

        DAMAGE_TYPE = new ConfigKeyInstance<>(enumKey(TRAPS_FILE, "traps.%.damage.type", DamageType.NONE), key);
        INTERVAL = new ConfigKeyInstance<>(doubleKey(TRAPS_FILE, "traps.%.damage.interval", 1.0), key);
        SET_FIRE = new ConfigKeyInstance<>(booleanKey(TRAPS_FILE, "traps.%.damage.set-fire", false), key);
        RANGE = new ConfigKeyInstance<>(doubleKey(TRAPS_FILE, "traps.%.damage.range", 0), key);
        DAMAGE_AMOUNT = new ConfigKeyInstance<>(doubleKey(TRAPS_FILE, "traps.%.damage.damage", -1), key);
        POTION_EFFECT = new ConfigKeyInstance<>(stringKey(TRAPS_FILE, "traps.%.damage.effect", PotionEffectType.CONFUSION.getName().toUpperCase()), key);
        POTION_AMPLIFIER = new ConfigKeyInstance<>(integerKey(TRAPS_FILE, "traps.%.damage.amplify", 1), key);
        POTION_TIME = new ConfigKeyInstance<>(doubleKey(TRAPS_FILE, "traps.%.damage.time", 1), key);

        TARGET_AMOUNT = new ConfigKeyInstance<>(integerKey(TRAPS_FILE, "traps.%.target.amount", -1), key);
        TARGET_ENTITIES = new ConfigKeyInstance<>(stringListKey(TRAPS_FILE, "traps.%.target.entity", new String[]{}), key);

        //Checks
        if (POTION_EFFECT.isSet() && PotionEffectType.getByName(POTION_EFFECT.get()) == null)
            POTION_EFFECT.setDefault(true);

        StringBuilder invalidEntities = new StringBuilder();
        for (String entityString : TARGET_ENTITIES.get()) {
            try {
                EntityType.valueOf(entityString.toUpperCase());
            } catch (IllegalArgumentException ex) {
                if (!invalidEntities.isEmpty())
                    invalidEntities.append(", ");
                invalidEntities.append(entityString);
            }
        }
        if (!invalidEntities.isEmpty())
            Messages.WARN.sendConsole("Found invalid entities for Trap '" + key + "': " + invalidEntities);
    }

    /**
     * Create an itemstack of <i>[amount]</i> traps
     */
    public ItemStack generateTrapItem(int amount) {
        return generateTrapItem(amount, -1);
    }


    /**
     * Create an itemstack of <i>[amount]</i> traps with a custom <i>[durability]</i>
     */
    public ItemStack generateTrapItem(int amount, int durability) {
        ItemStack itemStack = new ItemStack(DISPLAY_ITEM.get(), amount);
        ItemMeta meta = itemStack.getItemMeta();
        if (durability == -1)
            durability = DURABILITY.get();
        if (meta != null)
            meta.displayName(Messages.formatLegacy(NAME.get()));
        List<Component> lore = new ArrayList<>();
        lore.add(Messages.formatLegacy(LORE.get()));
        lore.add(Component.empty());
        lore.add(Messages.TRAP_DIVIDER.getMiniMessageFormat());
        if (DAMAGE_TYPE.get() == DamageType.EFFECT) {
            lore.add(Messages.TRAP_EFFECT.getMiniMessageFormat(Statics.toCapital(POTION_EFFECT.get())));
            lore.add(Messages.TRAP_AMPLIFY.getMiniMessageFormat(Statics.toRoman(POTION_AMPLIFIER.get())));
        } else {
            if (DAMAGE_AMOUNT.get() >= 1)
                lore.add(Messages.TRAP_DAMAGE.getMiniMessageFormat(String.valueOf(DAMAGE_AMOUNT.get()), String.valueOf(INTERVAL.get())));
            else if (!SET_FIRE.get())
                lore.add(Messages.TRAP_DAMAGE_FIRE.getMiniMessageFormat());
        }
        if (SET_FIRE.get())
            lore.add(Messages.TRAP_FIRE.getMiniMessageFormat());

        lore.add(Messages.TRAP_RANGE.getMiniMessageFormat(String.valueOf(RANGE.get())));
        if (TARGET_AMOUNT.get() > 0)
            lore.add(Messages.TRAP_TARGET.getMiniMessageFormat(TARGET_AMOUNT.get() + ""));

        if (DURABILITY.get() == -1)
            lore.add(Messages.TRAP_DURABILITY_INF.getMiniMessageFormat());
        else
            lore.add(Messages.TRAP_DURABILITY.getMiniMessageFormat(String.valueOf(durability), String.valueOf(DURABILITY.get())));

        if (SETUP_TIME.get() > 0)
            lore.add(Messages.TRAP_BUILD_TIME.getMiniMessageFormat(String.valueOf(SETUP_TIME.get())));

        StringBuilder mobs = new StringBuilder(Messages.TRAP_MOBS.get());
        List<String> entityList = TARGET_ENTITIES.get();
        if (entityList.isEmpty())
            mobs.append(Messages.TRAP_MOBS_ALL.get());
        else {
            for (int i = 1; i < entityList.size() + 1; i++) {
                mobs.append(Statics.toCapital(entityList.get(i - 1))).append(" ");
                if (i % 4 == 0) {
                    lore.add(Messages.formatLegacy(mobs.toString()));
                    mobs = new StringBuilder(Messages.TRAP_MOBS_NL.get());
                }
            }
        }

        if (mobs.length() > 8)
            lore.add(Messages.formatLegacy(mobs.toString()));
        lore.add(Messages.TRAP_DIVIDER.getMiniMessageFormat());
        if (meta == null)
            return itemStack;

        meta.lore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        if (ITEM_GLOW.get())
            meta.addEnchant(Enchantment.LUCK, 1, false);
        meta.getPersistentDataContainer().set(Keys.TRAP_KEY, PersistentDataType.STRING, key);
        meta.getPersistentDataContainer().set(Keys.TRAP_DURABILITY, PersistentDataType.INTEGER, durability);

        itemStack.setItemMeta(meta);
        return itemStack;
    }

    public long getParticlePeriod() {
        return particlePeriod;
    }

    public List<Vector> getParticleDelta() {
        return particleDelta;
    }

    /**
     * Called from the ParticleEngine
     * Calculates the particle locations for this Particle Tick
     */
    public void updateParticleLocation() {
        particlePeriod = (int) ((particlePeriod + PARTICLE_PATTERN_SPEED.get()) % 360);
        particleDelta = PARTICLE_PATTERN.get().calculateNextDelta(this);
    }

    public PotionEffect getPotionEffect() {
        return new PotionEffect(PotionEffectType.getByName(POTION_EFFECT.get()), (int) (POTION_TIME.get() * 20), POTION_AMPLIFIER.get());
    }

    public void removeTrap() {
        TRAPS_FILE.get().set("traps." + key, null);
        TRAPS_FILE.save();
    }

    /**
     * Get the next available trap-key name
     */
    public String getNextTrapKey() {
        int lastKeyNumber = 0;
        while (true) {
            if (!TRAPS_FILE.get().contains("Traps-" + lastKeyNumber))
                return "Traps-" + lastKeyNumber;
            lastKeyNumber++;
        }
    }
}
