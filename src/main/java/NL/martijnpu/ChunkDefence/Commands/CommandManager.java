package NL.martijnpu.ChunkDefence.Commands;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Commands.SubCommands.*;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public final class CommandManager implements CommandExecutor {
    private static CommandManager instance;
    private final ArrayList<SubCommand> commands;

    private CommandManager() {
        TabComplete tabComplete = new TabComplete();

        Objects.requireNonNull(ChunkDefence.get().getCommand("shop")).setExecutor(this);
        Objects.requireNonNull(ChunkDefence.get().getCommand("spawn")).setExecutor(this);
        Objects.requireNonNull(ChunkDefence.get().getCommand("arena")).setExecutor(this);
        Objects.requireNonNull(ChunkDefence.get().getCommand("chunkdefence")).setExecutor(this);
        Objects.requireNonNull(ChunkDefence.get().getCommand("chunkdefenceadmin")).setExecutor(this);

        Objects.requireNonNull(ChunkDefence.get().getCommand("shop")).setTabCompleter(tabComplete);
        Objects.requireNonNull(ChunkDefence.get().getCommand("spawn")).setTabCompleter(tabComplete);
        Objects.requireNonNull(ChunkDefence.get().getCommand("arena")).setTabCompleter(tabComplete);
        Objects.requireNonNull(ChunkDefence.get().getCommand("chunkdefence")).setTabCompleter(tabComplete);
        Objects.requireNonNull(ChunkDefence.get().getCommand("chunkdefenceadmin")).setTabCompleter(tabComplete);

        commands = new ArrayList<>();
        commands.add(new AdminCmd()); //Base Admin command (fallback)
        if (ConfigData.DEBUG.get())
            commands.add(new AdminTestCmd());
        commands.add(new AdminArenaChangeCmd());
        commands.add(new AdminArenaDeleteCmd());
        commands.add(new AdminEcoAddCmd());
        commands.add(new AdminEcoExportCmd());
        commands.add(new AdminEcoImportCmd());
        commands.add(new AdminEcoInfoCmd());
        commands.add(new AdminEcoRemoveCmd());
        commands.add(new AdminFixWorldCmd());
        commands.add(new AdminGenerateDataCmd());
        commands.add(new AdminHelpCmd());
        commands.add(new AdminReloadCmd());
        if (ConfigData.DEBUG.get())
            commands.add(new AdminRestartCmd()); //Only for debugging
        commands.add(new AdminSchematicListCmd());
        commands.add(new AdminSchematicLoadCmd());
        commands.add(new AdminSchematicSaveCmd());
        commands.add(new AdminShopAddCmd());
        commands.add(new AdminShopEditCmd());
        commands.add(new AdminShopEditDisplayCmd()); //Above base ShopEdit
        commands.add(new AdminShopEditItemCmd()); //Above base ShopEdit
        commands.add(new AdminShopEditNameCmd()); //Above base ShopEdit
        commands.add(new AdminShopEditSchematicCmd()); //Above base ShopEdit
        commands.add(new AdminShopEditTrapCmd()); //Above base ShopEdit
        commands.add(new AdminShopEditCostCmd()); //Above base ShopEdit
        commands.add(new AdminShopMoveCmd());
        commands.add(new AdminShopRemoveCmd());
        commands.add(new AdminShopSetLocationCmd());
        commands.add(new AdminSpawnSetCmd());
        commands.add(new AdminSpawnTeleportCmd());
        commands.add(new AdminTrapsCmd());
        commands.add(new AdminTrapsGiveCmd()); //Above base Info
        commands.add(new AdminTrapsListCmd()); //Above base Info
        commands.add(new AdminUpdatePlaceholdersCmd());
        commands.add(new AdminVersionCmd());
        commands.add(new AdminWaveFileCmd());
        commands.add(new AdminWaveListCmd());
        commands.add(new AdminWaveSpawnCmd());
        commands.add(new ArenaCmd()); //Base Arena command (fallback)
        commands.add(new ArenaDeleteCmd());
        commands.add(new ArenaHelpCommandsCmd());
        commands.add(new ArenaInfoCmd());
        commands.add(new ArenaInfoOthersCmd()); //Above base Info
        commands.add(new ArenaInviteCmd());
        commands.add(new ArenaInviteAcceptCmd()); //Above base Invite
        commands.add(new ArenaInviteCancelCmd()); //Above base Invite
        commands.add(new ArenaLeaveCmd());
        //commands.add(new ArenaPauseCmd()); //todo removed manual pause command
        //commands.add(new ArenaResumeCmd()); //todo removed manual resume command
        commands.add(new ArenaStartCmd());
        commands.add(new ArenaStartGamemodeCmd()); //Above base Start
        commands.add(new ArenaTeleportCmd());
        commands.add(new ArenaTeleportOthersCmd()); //Above base Teleport
        commands.add(new ArenaTopCmd());
        commands.add(new ArenaWaveInfoCmd());
        commands.add(new ChunkDefenceCmd()); //Base ChunkDefence command (fallback)
        commands.add(new ChunkDefenceBook());
        commands.add(new ChunkDefenceCommandsCmd());
        commands.add(new ShopTeleportCmd());
        commands.add(new SpawnTeleportCmd());
    }

    public static CommandManager getInstance() {
        if (instance == null)
            instance = new CommandManager();
        return instance;
    }

    @Override
    public boolean onCommand(@Nonnull CommandSender s, @Nonnull Command command, @Nonnull String label, @Nonnull String[] args) {
        SubCommand bestMatch = null;
        Player player = null;

        if (s instanceof Player)
            player = ((Player) s).getPlayer();

        loop:
        for (SubCommand subCommand : commands) {
            if (!subCommand.getCommand().equalsIgnoreCase(command.getName()))
                continue;

            if (args.length < subCommand.getArgs().length)
                continue;

            for (int i = 0; i < subCommand.getArgs().length; i++) {
                if (!subCommand.getArgs()[i].equalsIgnoreCase(args[i]))
                    continue loop;
            }

            if (bestMatch == null || subCommand.getArgs().length > bestMatch.getArgs().length)
                bestMatch = subCommand;
        }

        if (bestMatch != null) {
            if (!(s instanceof Player) && bestMatch.playerOnly()) {
                Messages.CMD_PLAYER.sendConsole();
                return true;
            }

            if (!bestMatch.getPermission().hasPermissionMessage(s)) {
                return true;
            }

            if (args.length < bestMatch.getArgs().length + bestMatch.getPlayerArguments()) {
                Messages.CMD_FORMAT.send(player, bestMatch.getUsage());
                return true;
            }

            bestMatch.onCommand(player, Arrays.copyOfRange(args, bestMatch.getArgs().length, args.length));
            return true;
        }

        Messages.CMD_UNKNOWN.send(player, label);
        return true;
    }

    ArrayList<SubCommand> getCommands() {
        return commands;
    }

    public ArrayList<String> onHelpCommand(Player player, String commandName, int page) {
        int page_size = 15;
        ArrayList<String> listBuilder = new ArrayList<>();
        ArrayList<String> listFinal = new ArrayList<>();
        page--;

        for (SubCommand command : commands) {
            if (!command.getCommand().equalsIgnoreCase(commandName) && !commandName.isEmpty())
                continue;

            if (player != null && !command.getPermission().hasPermission(player))
                continue;

            listBuilder.add(Messages.CMD_HELP_CMD.get(command.getUsage()));
            listBuilder.add(Messages.CMD_HELP_DESC.get(command.getDescription()));
            listBuilder.add("");
        }

        int pages = listBuilder.size() / page_size;

        if (listBuilder.size() / page_size != 0)
            pages++;

        listFinal.add(Messages.CMD_HELP_PAGE.get(String.valueOf(page + 1), String.valueOf(pages)));

        int index = (page + 1) * page_size;

        if (((page + 1) * page_size) > listBuilder.size())
            index = listBuilder.size();

        if (page * page_size <= listBuilder.size())
            listFinal.addAll(listBuilder.subList(page * page_size, index));

        return listFinal;
    }
}
