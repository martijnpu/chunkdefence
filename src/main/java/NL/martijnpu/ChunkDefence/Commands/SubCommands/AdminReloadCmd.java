package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

public final class AdminReloadCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Reload the config and user files";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"reload"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_RELOAD;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        if (ChunkDefence.get().onReload(false)) {
            Messages.CONFIG_SUCCESS.send(player);
            Messages.CONFIG_EXCEPTION.send(player);
        } else
            Messages.CONFIG_ERROR.send(player);
    }
}
