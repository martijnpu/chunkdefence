package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Mobs.DefinedMobData;
import NL.martijnpu.ChunkDefence.Mobs.MobManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Waves.SpawnWaves;
import org.bukkit.entity.Player;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public final class AdminWaveFileCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Generates a file with a list of waves.";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"wave", "file"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_WAVE;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        String path = "backups/generatedWaves.txt";
        File file = new File(ChunkDefence.get().getDataFolder(), path);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, false))) {
            for (int wave = 1; wave < 200; wave++) {
                Map<Integer, DefinedMobData> spawnEntities = MobManager.getInstance().getDefinedWave(wave);
                if (spawnEntities.isEmpty())
                    spawnEntities = SpawnWaves.getGeneratedWave(wave);

                writer.newLine();
                writer.append("Wave ").append(String.valueOf(wave));
                writer.newLine();

                for (Map.Entry<Integer, DefinedMobData> entry : spawnEntities.entrySet()) {
                    writer.append("  - ").append(String.valueOf(entry.getKey() % 100)).append(" ").append(entry.getValue().getEntityType().name().toLowerCase().replace("_", " "));
                    writer.newLine();
                    writer.append("Total coins: ").append(entry.getValue().getCoins() + "");
                    writer.newLine();
                }
            }
            Messages.PLUGIN.send(player, "File generated as: " + path);
            Messages.CMD_SUCCESS.send(player);
        } catch (IOException ex) {
            Messages.WARN.send(player, "Unable to generate waves: " + ex.getMessage());
        }

    }
}
