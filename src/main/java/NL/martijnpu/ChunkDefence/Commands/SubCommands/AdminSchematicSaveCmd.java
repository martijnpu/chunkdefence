package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Schematics.SchematicEditor;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

public final class AdminSchematicSaveCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Save an Arena";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"schematic", "save"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_SCHEMATIC;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected int getPlayerArguments() {
        return 1;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + " <name>";
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        SchematicEditor.getInstance().saveSchematic(player, args[0]);
    }
}
