package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Types.GuiTop;
import org.bukkit.entity.Player;

public final class ArenaTopCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "arena";
    }

    @Override
    protected String getDescription() {
        return "Shows the arena top.";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"top"};
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        GuiManager.getInstance().createInventory(player, GuiTop.class).openGui();
    }
}
