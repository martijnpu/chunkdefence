package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Data.FileHandler;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public final class AdminShopMoveCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Moves the shop to where you look";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"shop", "move"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_SHOP;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected int getPlayerArguments() {
        return 1;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + " <Index of shop>";
    }

    @Override
    protected List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        List<String> temp = new ArrayList<>();
        ConfigurationSection cs = FileHandler.SHOPS_FILE.get().getConfigurationSection("shop");
        if (cs != null)
            temp.addAll(cs.getKeys(false));
        bigList.add(temp);
        return bigList;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        String name = String.join(" ", args);

        if (!FileHandler.SHOPS_FILE.get().getConfigurationSection("shop").getKeys(false).contains(name)) {
            Messages.SHOP_INVALID.send(player);
            return;
        }

        ShopManager.getInstance().moveShop(player, name);
    }
}
