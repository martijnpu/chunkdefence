package NL.martijnpu.ChunkDefence.Commands.SubCommands;


import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import NL.martijnpu.ChunkDefence.Utils.Updater;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public final class AdminVersionCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Show plugin information";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"version"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        List<String> messages = new ArrayList<>();
        messages.add("&fAuthor            :&a martijnpu & ThijsSmeel");
        messages.add("&fPlaceholderAPI :&a " + (Statics.isPAPIEnabled ? "Yes" : "No"));
        messages.add("&fProtocolLib      :&a " + (Statics.isProtocolLibAvailable ? "Yes" : "No"));
        messages.add("&fServer           :&a " + ChunkDefence.get().getServer().getVersion());
        messages.add("&fType              :&a " + (ConfigData.SERVER_PROTECT_ALL.get() ? "Server-wide" : "World-wide"));
        messages.add("&fYour Version    :&a " + Updater.getCurrVersion() + (Updater.getNewVersion() > Updater.getCurrVersion() ? " &c(outdated)" : "") + (Updater.getNewVersion() < Updater.getCurrVersion() ? " &3(Dev Version)" : ""));
        messages.add("&fNewest version :&a " + (Updater.getNewVersion() == 0 ? "N/A" : Updater.getNewVersion()));
        messages.add("");
        messages.add("&fWiki: &a" + ChunkDefence.get().getDescription().getWebsite());
        messages.add("&fSupport:&a https://discord.gg/2RHYvNy");
        if (ConfigData.DEBUG.get()) messages.add("&cDEBUG MODE      &f:&4 ENABLED");
        Messages.sendBigMessage(player, messages);
    }
}
