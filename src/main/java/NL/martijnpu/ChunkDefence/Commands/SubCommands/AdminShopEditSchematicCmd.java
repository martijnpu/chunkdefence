package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Schematics.SchematicManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Shops.Shop;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopArena;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public final class AdminShopEditSchematicCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Edit the shop schematic you're looking at.";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"shop", "edit", "schematic"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_SHOP;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected int getPlayerArguments() {
        return 1;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + "<schematic>";
    }

    @Override
    protected List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(SchematicManager.getInstance().getAllSchematics());
        return bigList;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Shop shop = ShopManager.getInstance().getShop(player);

        if (shop == null) {
            Messages.SHOP_NOT_FOUND.send(player);
            return;
        }
        //Todo only ArenaShop from editing

        if (SchematicManager.getInstance().isInvalidSchematic(args[0])) {
            Messages.SCHEM_UNKNOWN.send(player, args[0]);
            return;
        }
        ((ShopArena) shop).setSchematic(args[0]);
        Messages.SHOP_EDIT_SUCCESS.send(player);
    }
}
