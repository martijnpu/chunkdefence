package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Players.EconomyManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

public final class AdminEcoExportCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Export the local economy to Vault";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"economy", "export"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_SCHEMATIC;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        if (EconomyManager.getInstance().exportToVault()) {
            Messages.ECO_EXPORT.send(player);
        } else
            Messages.ECO_NO_VAULT.send(player);
    }
}
