package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Shops.Shop;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class AdminShopEditDisplayCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Edit the shop display you're looking at.";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"shop", "edit", "display"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_SHOP;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected int getPlayerArguments() {
        return 1;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + "<item>";
    }

    @Override
    protected List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(Arrays.stream(Material.values()).map(Enum::name).collect(Collectors.toList()));
        return bigList;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Shop shop = ShopManager.getInstance().getShop(player);

        if (shop == null) {
            Messages.SHOP_NOT_FOUND.send(player);
            return;
        }
        //Todo lock TrapShop from editing

        Material material = Material.getMaterial(args[0]);

        if (material == null) {
            Messages.EXCEPT_ARGUMENT.send(player, args[0]);
            return;
        }
        shop.setDisplayItem(material);
        Messages.SHOP_EDIT_SUCCESS.send(player);
    }
}
