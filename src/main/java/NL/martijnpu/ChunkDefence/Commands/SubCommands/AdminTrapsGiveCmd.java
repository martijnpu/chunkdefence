package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapData;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapDataManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.Sound;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class AdminTrapsGiveCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Opens the trap GUI.";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"traps", "give"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_TRAP;
    }

    @Override
    protected int getPlayerArguments() {
        return 2;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + " <name> <trap> [amount] [durability]";
    }

    @Override
    protected List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(ChunkDefence.get().getServer().getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList()));
        bigList.add(TrapDataManager.getInstance().getAllTrapNames());
        bigList.add(IntStream.rangeClosed(0, 9).mapToObj(Integer::toString).collect(Collectors.toList()));
        bigList.add(IntStream.rangeClosed(0, 9).mapToObj(Integer::toString).collect(Collectors.toList()));
        return bigList;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        try {
            Player target = ChunkDefence.get().getServer().getPlayer(args[0]);
            if (target == null) {
                Messages.EXCEPT_OFFLINE.send(player);
                return;
            }

            TrapData trap = TrapDataManager.getInstance().getTrapBase(args[1]);
            if (trap == null) {
                Messages.TRAP_UNKNOWN.send(player, args[1]);
                return;
            }

            int amount = args.length == 2 ? 1 : Integer.parseInt(args[2]);
            int durability = args.length <= 3 ? -1 : Integer.parseInt(args[3]);

            if (player.getInventory().firstEmpty() == -1) {
                Messages.EXCEPT_FULL.send(player);
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                return;
            }

            player.getInventory().addItem(trap.generateTrapItem(amount, durability));
            Messages.TRAP_RECEIVE.send(target, trap.NAME.get(), amount + "");
        } catch (NumberFormatException ex) {
            Messages.EXCEPT_NUMBER.send(player, args[1]);
        }
    }
}
