package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Data.FileHandler;
import NL.martijnpu.ChunkDefence.Gui.Book.Books;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public final class ChunkDefenceBook extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefence";
    }

    @Override
    protected String getDescription() {
        return "Shows a ChunkDefence book";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"book"};
    }

    @Override
    protected int getPlayerArguments() {
        return 1;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + " <bookname>";
    }

    @Override
    protected List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        if (FileHandler.MESSAGES_FILE.get().contains("messages.books"))
            bigList.add(new ArrayList<>(FileHandler.MESSAGES_FILE.get().getConfigurationSection("messages.books").getKeys(false)));
        return bigList;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Books.openBook(player, args[0]);
    }
}
