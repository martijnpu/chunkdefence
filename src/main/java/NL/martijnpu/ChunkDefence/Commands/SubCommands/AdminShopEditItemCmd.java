package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Shops.Shop;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopItem;
import NL.martijnpu.ChunkDefence.Shops.ShopTypes.ShopType;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public final class AdminShopEditItemCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Edit the shop item you're looking at.";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"shop", "edit", "item"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_SHOP;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Shop shop = ShopManager.getInstance().getShop(player);

        if (shop == null) {
            Messages.SHOP_NOT_FOUND.send(player);
            return;
        }

        if (!ShopType.ITEM.equals(shop.TYPE.get())) {
            Messages.SHOP_INVALID.send(player);
            return;
        }

        if (!player.getItemOnCursor().getType().equals(Material.AIR))
            ((ShopItem) shop).changeItem(player.getItemOnCursor());
        Messages.SHOP_EDIT_SUCCESS.send(player);
    }
}
