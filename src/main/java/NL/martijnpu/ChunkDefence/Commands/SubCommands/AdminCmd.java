package NL.martijnpu.ChunkDefence.Commands.SubCommands;


import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public final class AdminCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Plugin information";
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        List<String> messages = new ArrayList<>();
        messages.add("&a" + ChunkDefence.get().getDescription().getFullName());
        messages.add("&f" + ChunkDefence.get().getDescription().getDescription());
        messages.add("");
        messages.add("&aAuthors: &f" + ChunkDefence.get().getDescription().getAuthors());
        messages.add("&aWiki: &f" + ChunkDefence.get().getDescription().getWebsite());
        messages.add("");
        messages.add("&aAvailable commands: &f/cda commands");
        Messages.sendBigMessage(player, messages);
    }
}
