package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public final class ArenaStartGamemodeCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "arena";
    }

    @Override
    protected String getDescription() {
        return "Start or force-start the next wave with a specific gamemode (only for first wave)";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"start"};
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected int getPlayerArguments() {
        return 1;
    }

    @Override
    protected List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(ConfigData.GAMEMODES.get());
        return bigList;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        arenaStartGamemodeCmd(player, args[0]);
    }

    public static void arenaStartGamemodeCmd(Player player, String gamemode) {
        if (ConfigData.GAMEMODES.get().stream().noneMatch(gamemode2 -> gamemode2.equalsIgnoreCase(gamemode))) {
            Messages.EXCEPT_MODE.send(player);
            return;
        }
        Arena arena = ArenaManager.getInstance().getArenaCreateIfNotExist(player, gamemode);

        //If WaveController is null, arena is just created and in cooldown. Start will be handled automatically
        if (arena != null && arena.getWaveController() != null)
            arena.getWaveController().start();
    }
}
