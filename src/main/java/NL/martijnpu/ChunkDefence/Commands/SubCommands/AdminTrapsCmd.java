package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Types.GuiTrapList;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

public final class AdminTrapsCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Opens the trap GUI.";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"traps"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_TRAP;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        GuiManager.getInstance().createInventory(player, GuiTrapList.class).openGui();
    }
}
