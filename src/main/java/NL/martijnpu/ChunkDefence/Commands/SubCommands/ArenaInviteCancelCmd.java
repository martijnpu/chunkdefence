package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Players.PlayerData;
import NL.martijnpu.ChunkDefence.Players.PlayerDataManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class ArenaInviteCancelCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "arena";
    }

    @Override
    protected String getDescription() {
        return "Invoke the invite of a player to your arena";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"invite", "cancel"};
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected int getPlayerArguments() {
        return 1;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + " <player>";
    }

    @Override
    protected List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(ChunkDefence.get().getServer().getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList()));
        return bigList;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Player target = ChunkDefence.get().getServer().getPlayer(args[0]);
        if (target == null) {
            Messages.EXCEPT_OFFLINE.send(player);
            return;
        }

        PlayerData playerData = PlayerDataManager.getInstance().getUserData(target.getUniqueId());
        Arena arena = ArenaManager.getInstance().getArena(player);
        if (arena == null) {
            Messages.ARENA_INVITE_NONE.send(player);
            return;
        }

        if (!ConfigData.GM_ARENA_ALLOW_INVITE.get(arena.getArenaData().GAMEMODE.get())) {
            Messages.ARENA_INVITE_GAMEMODE.send(player);
            return;
        }

        long elapsedTime = playerData.removeInvite(arena);
        if (elapsedTime == -1 || elapsedTime > 120)
            Messages.ARENA_INVITE_NOT.send(player);
        else {
            Messages.ARENA_INVITE_INVOKED.send(target, player.getName());
            Messages.ARENA_INVITE_REMOVED.send(player);
        }
    }
}
