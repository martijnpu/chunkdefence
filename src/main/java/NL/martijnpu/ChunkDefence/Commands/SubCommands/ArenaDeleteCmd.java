package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Gui.GuiInventory;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Types.GuiArenaDelete;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public final class ArenaDeleteCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "arena";
    }

    @Override
    protected String getDescription() {
        return "Delete your arena";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"delete"};
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        arenaDeleteConfirm(player);
    }

    public static void arenaDeleteConfirm(Player player) {
        {
            Arena arena = ArenaManager.getInstance().getArena(player);
            if (arena == null) {
                Messages.ARENA_NONE.send(player);
                return;
            }

            Messages.DEBUG.sendConsole("Found UUID: " + arena.getArenaData().getMembers().get(0));
            Messages.DEBUG.sendConsole("Current UUID: " + player.getUniqueId());

            if (!arena.getArenaData().getMembers().get(0).equals(player.getUniqueId())) {
                Messages.CUSTOM.send(player, "This action can only be done by the owner of the Arena!");
                return;
                //Todo Message you are not the owner
            }

            GuiInventory inv = GuiManager.getInstance().createInventory(player, GuiArenaDelete.class);
            inv.changeItem(1, new GuiSlot(Material.LIME_CONCRETE_POWDER, Messages.GUI_DELETE_ARENA_QUESTION.get())
                    .setLore(Messages.GUI_DELETE_ARENA_CONFIRM.get())
                    .setLeftClick(() -> {
                        GuiManager.getInstance().closeInventory(player);
                        ArenaManager.getInstance().removeArena(arena);
                    }));
            inv.openGui();
        }
    }
}
