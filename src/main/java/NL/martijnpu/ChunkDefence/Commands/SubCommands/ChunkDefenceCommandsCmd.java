package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.CommandManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ChunkDefenceCommandsCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefence";
    }

    @Override
    protected String getDescription() {
        return "Shows all available commands";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"commands"};
    }

    @Override
    protected List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(Collections.singletonList("<page>"));
        return bigList;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        try {
            if (args.length == 0)
                Messages.sendBigMessage(player, CommandManager.getInstance().onHelpCommand(player, "", 1));
            else
                Messages.sendBigMessage(player, CommandManager.getInstance().onHelpCommand(player, "", Integer.parseInt(args[0])));
        } catch (Exception ex) {
            Messages.EXCEPT_NUMBER.send(player, args[0]);
        }
    }
}
