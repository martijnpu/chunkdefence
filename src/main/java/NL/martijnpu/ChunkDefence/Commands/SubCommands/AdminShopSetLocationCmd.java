package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public final class AdminShopSetLocationCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Set the location for /shop";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"shop", "setlocation"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_SHOP;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Location location = Statics.roundToHalf(player.getLocation());
        ConfigData.SHOP_LOCATION.set(Statics.getStringFromLocation(location));
        Messages.SHOP_LOC_SAVED.send(player);

        Statics.teleportToShop(player);
    }
}
