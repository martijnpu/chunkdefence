package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Mobs.DefinedMobData;
import NL.martijnpu.ChunkDefence.Mobs.MobManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Waves.SpawnWaves;
import net.kyori.adventure.inventory.Book;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.event.HoverEvent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.entity.Player;

import java.util.Map;

public final class ArenaWaveInfoCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "arena";
    }

    @Override
    protected String getDescription() {
        return "Get mob info about the current wave";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"waveinfo"};
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        onCommand(player);
    }

    public static void onCommand(Player player) {
        Arena arena = ArenaManager.getInstance().getArena(player);
        if (arena == null) {
            Messages.ARENA_NONE.send(player);
            return;
        }
        int wave = arena.getArenaData().WAVE.get();
        TextComponent.Builder page = Component.text();

        try {
            Map<Integer, DefinedMobData> spawnEntities = MobManager.getInstance().getDefinedWave(wave);
            page.append(Component.text("Wave " + wave, NamedTextColor.GOLD, TextDecoration.BOLD)
                    .appendNewline());

            if (spawnEntities.isEmpty())
                spawnEntities = SpawnWaves.getGeneratedWave(wave);

            for (Map.Entry<Integer, DefinedMobData> entry : spawnEntities.entrySet()) {
                page.append(Component.text("  - " + entry.getKey() % 100 + " " + entry.getValue().getEntityType().name().toLowerCase().replace("_", " "), NamedTextColor.BLACK))
                        .hoverEvent(HoverEvent.showText(AdminWaveListCmd.getMobData(entry.getValue(), entry.getKey() % 100, Component.empty())))
                        .appendNewline();
            }

        } catch (Exception ex) {
            page.append(Component.text("Wave " + wave, NamedTextColor.DARK_RED, TextDecoration.BOLD)
                    .appendNewline()
                    .append(Component.text("Unable to generate."))
                    .appendNewline()
                    .append(Component.text(ex.getMessage())));
        }

        Book book = Book.book(Component.text("Waves"),
                Component.text("ChunkDefence"),
                page.build());

        player.openBook(book);
    }
}
