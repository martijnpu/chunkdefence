package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class ArenaInfoOthersCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "arena";
    }

    @Override
    protected String getDescription() {
        return "Get arena info of the given player or the arena you're in";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"info"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_INFO;
    }

    @Override
    protected int getPlayerArguments() {
        return 1;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + " <Player>";
    }

    @Override
    protected List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(ChunkDefence.get().getServer().getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList()));
        return bigList;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Player p = ChunkDefence.get().getServer().getPlayer(args[0]);
        if (p == null) {
            Messages.EXCEPT_OFFLINE.send(player);
            return;
        }
        Arena arena = ArenaManager.getInstance().getArena(p);
        if (arena == null) {
            Messages.ARENA_NONE.send(player);
            return;
        }
        Messages.sendBigMessage(player, Arrays.asList(
                Messages.WAVE_CURRENT.get(String.valueOf(arena.getArenaData().WAVE.get())),
                arena.getWaveController().getStatus()));
    }
}
