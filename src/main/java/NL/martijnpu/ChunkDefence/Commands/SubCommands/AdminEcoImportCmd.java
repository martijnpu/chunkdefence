package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Players.EconomyManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

public final class AdminEcoImportCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Import the economy from Vault to local";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"economy", "import"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_ECONOMY;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        if (EconomyManager.getInstance().importFromVault())
            Messages.ECO_IMPORT.send(player);
        else
            Messages.ECO_NO_VAULT.send(player);
    }
}
