package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Schematics.SchematicManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

public final class AdminSchematicListCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Get a list of all available Arenas";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"schematic", "list"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_SCHEMATIC;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected String getUsage() {
        return super.getUsage();
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Messages.sendBigMessage(player, SchematicManager.getInstance().getAllSchematics());
    }
}
