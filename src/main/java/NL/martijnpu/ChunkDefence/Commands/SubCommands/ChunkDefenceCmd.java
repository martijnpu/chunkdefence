package NL.martijnpu.ChunkDefence.Commands.SubCommands;


import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public final class ChunkDefenceCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefence";
    }

    @Override
    protected String getDescription() {
        return "Plugin information";
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        List<String> messages = new ArrayList<>();
        messages.add("&a" + ChunkDefence.get().getDescription().getFullName());
        messages.add("&f" + ChunkDefence.get().getDescription().getDescription());
        messages.add("");
        messages.add("&aAuthors: &f" + ChunkDefence.get().getDescription().getAuthors());
        messages.add("&aWiki: &f" + ChunkDefence.get().getDescription().getWebsite());
        messages.add("");
        messages.add("&aAvailable commands: &f/cd commands");
        Messages.sendBigMessage(player, messages);
    }
}
