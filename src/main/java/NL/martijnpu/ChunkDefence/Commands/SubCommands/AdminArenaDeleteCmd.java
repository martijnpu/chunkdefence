package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class AdminArenaDeleteCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Delete the arena of others";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"deleteArena"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_ARENA;
    }

    @Override
    protected int getPlayerArguments() {
        return 1;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + " <player>";
    }

    @Override
    protected List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(ChunkDefence.get().getServer().getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList()));
        return bigList;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Player target = ChunkDefence.get().getServer().getPlayer(args[0]);
        if (target == null) {
            Messages.EXCEPT_OFFLINE.send(player);
            return;
        }
        Arena arena = ArenaManager.getInstance().getArena(target);
        if (arena == null) {
            Messages.ARENA_NONE.send(player);
            return;
        }
        ArenaManager.getInstance().removeArena(arena);
        Messages.CMD_SUCCESS.send(player);
    }
}
