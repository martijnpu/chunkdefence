package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

public final class AdminRestartCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Restart the whole plugin! All waves will restart!";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"restart"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_RESTART;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        if (ChunkDefence.get().onReload(true)) {
            Messages.CONFIG_SUCCESS.send(player);
        } else
            Messages.CONFIG_ERROR.send(player);
    }
}
