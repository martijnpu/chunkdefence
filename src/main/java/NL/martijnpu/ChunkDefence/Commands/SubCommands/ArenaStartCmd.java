package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Types.GuiGameModes;
import org.bukkit.entity.Player;

public final class ArenaStartCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "arena";
    }

    @Override
    protected String getDescription() {
        return "Start or force-start the next wave";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"start"};
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Arena arena = ArenaManager.getInstance().getArena(player);
        if (arena == null)
            GuiManager.getInstance().createInventory(player, GuiGameModes.class).openGui();
        else
            arena.getWaveController().start();
    }
}
