package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Players.PlayerData;
import NL.martijnpu.ChunkDefence.Players.PlayerDataManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class ArenaInviteAcceptCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "arena";
    }

    @Override
    protected String getDescription() {
        return "Accept the invite of a player";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"invite", "accept"};
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected int getPlayerArguments() {
        return 1;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + " <player>";
    }

    @Override
    protected List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(ChunkDefence.get().getServer().getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList()));
        return bigList;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        PlayerData playerData = PlayerDataManager.getInstance().getUserData(player.getUniqueId());

        Player inviter = ChunkDefence.get().getServer().getPlayer(args[0]);
        if (inviter == null) {
            Messages.EXCEPT_OFFLINE.send(player);
            return;
        }

        Arena arena = ArenaManager.getInstance().getArena(inviter);
        if (arena == null) {
            Messages.ARENA_INVITE_NONE_OTHER.send(player);
            return;
        }

        if (!ConfigData.GM_ARENA_ALLOW_INVITE.get(arena.getArenaData().GAMEMODE.get())) {
            Messages.ARENA_INVITE_GAMEMODE.send(player);
            return;
        }

        long elapsedTime = playerData.removeInvite(arena);
        if (elapsedTime == -1) {
            Messages.ARENA_INVITE_UNKNOWN.send(player);
            return;
        } else if (elapsedTime > 120) {
            Messages.ARENA_INVITE_EXPIRED.send(player);
            return;
        }

        boolean canBigger = false;
        if (Permission.MAX_PLAYERS_NOLIMIT.hasPermission(inviter))
            canBigger = true;
        else
            for (int i = arena.getArenaData().getMembers().size(); i < 100; i++) {
                if (Permission.CUSTOM.hasPermission(inviter, "chunkdefence.max_players." + i)) {
                    canBigger = true;
                    break;
                }
            }

        if (!canBigger) {
            Messages.ARENA_INVITE_SIZE.send(player);
            return;
        }

        arena.playerJoin(player);
        Messages.ARENA_INVITE_JOINED.send(player);
    }
}
