package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Players.EconomyManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class AdminEcoInfoCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Get the money amount of a player";
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_ECONOMY;
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"economy", "info"};
    }

    @Override
    protected int getPlayerArguments() {
        return 1;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + " <player>";
    }

    @Override
    protected List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(ChunkDefence.get().getServer().getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList()));
        return bigList;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        OfflinePlayer target = ChunkDefence.get().getServer().getOfflinePlayerIfCached(args[0]);
        if (target == null) {
            Messages.EXCEPT_OFFLINE.send(player);
            return;
        }

        Messages.ECO_TOT.send(player, target.getName(), String.valueOf(EconomyManager.getInstance().getBalance(target)));
    }
}
