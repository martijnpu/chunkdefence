package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Shops.Shop;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

public final class AdminShopEditCostCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Edit the shop costs you're looking at.";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"shop", "edit", "cost"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_SHOP;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected int getPlayerArguments() {
        return 1;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + "<cost>";
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Shop shop = ShopManager.getInstance().getShop(player);

        if (shop == null) {
            Messages.SHOP_NOT_FOUND.send(player);
            return;
        }

        try {
            double cost = Double.parseDouble(args[0]);

            shop.setCost(cost);
            Messages.SHOP_EDIT_SUCCESS.send(player);
        } catch (NumberFormatException ex) {
            Messages.EXCEPT_NUMBER.send(player);
        }
    }
}
