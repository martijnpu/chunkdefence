package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Mobs.DefinedMobData;
import NL.martijnpu.ChunkDefence.Mobs.MobManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Waves.SpawnWaves;
import net.kyori.adventure.inventory.Book;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.event.HoverEvent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class AdminWaveListCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Shows a list of waves.";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"wave", "list"};
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_WAVE;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + " [number]";
    }

    private static final int bookSize = 20; //Max pages (minus mainPage)
    private static final int pageSize = 14; //Max lines per page

    @Override
    protected void onCommand(Player player, String[] args) {
        try {
            Integer.parseInt(args[0]);
        } catch (IllegalArgumentException | IndexOutOfBoundsException ex) {
            args = new String[]{"0"};
        }

        int currBook = Integer.parseInt(args[0]);
        int linesTotal = 0;
        int linesWave = 0;
        int currPage = 0;
        int wave = 1;
        int minWave = Integer.MAX_VALUE;

        List<Component> pages = new ArrayList<>();
        TextComponent.Builder page = Component.text();

        if (currBook < 0) {
            Messages.EXCEPT_NUMBER.send(player, currBook + "");
            return;
        }

        try {
            while (pages.size() < bookSize) {
                if (minWave > wave && currPage >= (currBook * bookSize)) //Update minimum displayed wave
                    minWave = wave;

                TextComponent.Builder waveInfo = Component.text();
                Map<Integer, DefinedMobData> spawnEntities = MobManager.getInstance().getDefinedWave(wave);
                if (spawnEntities.isEmpty())
                    spawnEntities = SpawnWaves.getGeneratedWave(wave);

                linesWave += 2;
                waveInfo.append(Component.text("Wave " + wave, NamedTextColor.GOLD, TextDecoration.BOLD)
                        .appendNewline());

                for (Map.Entry<Integer, DefinedMobData> entry : spawnEntities.entrySet()) {
                    linesWave++;
                    waveInfo.append(Component.text("  - " + entry.getKey() % 100 + " " + entry.getValue().getEntityType().name().toLowerCase().replace("_", " "), NamedTextColor.BLACK)
                                    .hoverEvent(HoverEvent.showText(getMobData(entry.getValue(), entry.getKey() % 100, Component.empty()))))
                            .appendNewline();
                }

                if (linesTotal + linesWave <= pageSize) { //Stay on same page if it fits
                    linesTotal += linesWave;
                } else {
                    if (currPage >= (currBook * bookSize)) //Only show waves in desired book
                        pages.add(page.build());

                    page = Component.text();
                    linesTotal = linesWave - 1; //First starts without empty line
                    currPage++;
                }
                page.append(waveInfo).appendNewline();
                linesWave = 0;

                /*
                if (spawnEntities.size() + linesWritten + 2 > pageSize) {
                    if (currPage >= startPage) { //Skip pages before startPage
                        pages.add(waveInfo.build());
                        if (minWave > wave) //Update minWave
                            minWave = wave;
                    }

                    currPage++;
                    waveInfo = Component.text();
                    linesWritten = 0;
                }
*/
                wave++;
            }
        } catch (Exception ex) { //Generation error
            pages.add(Component.text("Error during wave " + wave, NamedTextColor.DARK_RED, TextDecoration.BOLD).appendNewline()
                    .append(Component.text(ex.getMessage(), NamedTextColor.GRAY, TextDecoration.ITALIC)));
        }
        wave -= 2; //Last wave didn't make it to the page


        TextComponent.Builder mainPage = Component.text()
                .append(Component.text("   List of waves", NamedTextColor.BLACK, TextDecoration.BOLD))
                .appendNewline().appendNewline();

        if (minWave == Integer.MAX_VALUE) {
            mainPage.append(Component.text("Unable to generate waves.", NamedTextColor.RED)).appendNewline()
                    .append(Component.text("Invalid config OR wave can't be generated (Too big)", NamedTextColor.RED)).appendNewline();
            mainPage.append(Component.text("How to check the config:", NamedTextColor.BLACK)).appendNewline()
                    .append(Component.text("1.'/cda reload'", NamedTextColor.BLACK)).appendNewline()
                    .append(Component.text("2. Check the console for errors", NamedTextColor.BLACK));
        } else {
            mainPage.append(Component.text("Waves " + minWave + " till " + wave))
                    .appendNewline().appendNewline();

            if (minWave > 1)
                mainPage.append(Component.text("Previous waves", NamedTextColor.GOLD)
                                .hoverEvent(HoverEvent.showText(Component.text("Click to generate waves before " + minWave, NamedTextColor.WHITE)))
                                .clickEvent(ClickEvent.runCommand("/cda wave list " + (currBook - 1))))
                        .appendNewline().appendNewline();

            mainPage.append(Component.text("More waves", NamedTextColor.GOLD)
                    .hoverEvent(HoverEvent.showText(Component.text("Click to generate waves after " + wave, NamedTextColor.WHITE)))
                    .clickEvent(ClickEvent.runCommand("/cda wave list " + (currBook + 1))));
        }

        pages.add(0, mainPage.build());
        if (pages.size() == bookSize + 1)
            pages.set(bookSize, pages.get(bookSize)
                    .append(Component.text("More waves", NamedTextColor.GOLD)
                            .hoverEvent(HoverEvent.showText(Component.text("Click to open the next book", NamedTextColor.WHITE)))
                            .clickEvent(ClickEvent.runCommand("/cda wave list " + (currBook + 1)))
                            .appendNewline()));

        Book book = Book.book(Component.text("Waves"),
                Component.text("ChunkDefence"),
                pages);

        player.openBook(book);
    }

    static Component getMobData(DefinedMobData mobData, int amount, Component prefix) {
        TextComponent.Builder builder = Component.text();
        builder.append(prefix).append(Component.text("Type: ", NamedTextColor.GOLD)).append(Component.text(mobData.getEntityType().toString().toLowerCase(), NamedTextColor.WHITE)).appendNewline();
        builder.append(prefix).append(Component.text("Coins: ", NamedTextColor.GOLD)).append(Component.text(mobData.getCoins(), NamedTextColor.WHITE)).appendNewline();
        builder.append(prefix).append(Component.text("Amount: ", NamedTextColor.GOLD)).append(Component.text(mobData.getAmount() * amount, NamedTextColor.WHITE)).appendNewline();
        builder.append(prefix).append(Component.text("Points: ", NamedTextColor.GOLD)).append(Component.text(mobData.getWeight(), NamedTextColor.WHITE));
        if (mobData.getEntityHealth() != -1)
            builder.appendNewline().append(prefix).append(Component.text("Health: ", NamedTextColor.GOLD)).append(Component.text(mobData.getEntityHealth(), NamedTextColor.WHITE));
        if (mobData.getCustomName() != null)
            builder.appendNewline().append(prefix).append(Component.text("Custom name: ", NamedTextColor.GOLD)).append(Messages.formatLegacy(mobData.getCustomName()));
        if (mobData.getEntityType() == EntityType.SLIME)
            builder.appendNewline().append(prefix).append(Component.text("Size: ", NamedTextColor.GOLD)).append(Component.text(mobData.getSize(), NamedTextColor.WHITE));
        if (mobData.getEntityType() == EntityType.CREEPER)
            builder.appendNewline().append(prefix).append(Component.text("ExplosionRadius: ", NamedTextColor.GOLD)).append(Component.text(mobData.getExplosionRadius(), NamedTextColor.WHITE));
        if (mobData.getPowered())
            builder.appendNewline().append(prefix).append(Component.text("Powered: ", NamedTextColor.GOLD)).append(Component.text("true", NamedTextColor.WHITE));
        if (mobData.getColor() != null)
            builder.appendNewline().append(prefix).append(Component.text("Color: ", NamedTextColor.GOLD)).append(Component.text(mobData.getColor().toLowerCase(), NamedTextColor.WHITE));
        if (mobData.getSilent())
            builder.appendNewline().append(prefix).append(Component.text("Silent: ", NamedTextColor.GOLD)).append(Component.text("true", NamedTextColor.WHITE));
        if (mobData.getGlowing())
            builder.appendNewline().append(prefix).append(Component.text("Glowing: ", NamedTextColor.GOLD)).append(Component.text("true", NamedTextColor.WHITE));
        if (mobData.getGliding())
            builder.appendNewline().append(prefix).append(Component.text("Gliding: ", NamedTextColor.GOLD)).append(Component.text("true", NamedTextColor.WHITE));
        if (mobData.getEntityMainHand() != null)
            builder.appendNewline().append(prefix).append(Component.text("Hand: ", NamedTextColor.GOLD)).append(Component.text(mobData.getEntityMainHand().getType().toString().toLowerCase(), NamedTextColor.WHITE));
        if (mobData.getEntityHelmet() != null)
            builder.appendNewline().append(prefix).append(Component.text("Helmet: ", NamedTextColor.GOLD)).append(Component.text(mobData.getEntityHelmet().getType().toString().toLowerCase(), NamedTextColor.WHITE));
        if (mobData.getEntityChestPlate() != null)
            builder.appendNewline().append(prefix).append(Component.text("Chestplate: ", NamedTextColor.GOLD)).append(Component.text(mobData.getEntityChestPlate().getType().toString().toLowerCase(), NamedTextColor.WHITE));
        if (mobData.getEntityLeggings() != null)
            builder.appendNewline().append(prefix).append(Component.text("Leggings: ", NamedTextColor.GOLD)).append(Component.text(mobData.getEntityLeggings().getType().toString().toLowerCase(), NamedTextColor.WHITE));
        if (mobData.getEntityBoots() != null)
            builder.appendNewline().append(prefix).append(Component.text("Boots: ", NamedTextColor.GOLD)).append(Component.text(mobData.getEntityBoots().getType().toString().toLowerCase(), NamedTextColor.WHITE));
        if (mobData.getPassenger() != null)
            builder.appendNewline().append(prefix).append(Component.text("Passenger: ", NamedTextColor.GOLD)).appendNewline()
                    .append(getMobData(mobData.getPassenger(), amount, prefix.append(Component.text("> ", NamedTextColor.WHITE))));

        return builder.build();
    }
}
