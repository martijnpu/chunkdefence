package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public final class AdminSpawnSetCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Set the first spawn location";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"spawn", "set"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_SPAWN;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Location location = Statics.roundToHalf(player.getLocation());
        ConfigData.SPAWN_LOCATION.set(Statics.getStringFromLocation(location));
        Messages.SPAWN_SAVED.send(player);

        Statics.teleportToSpawn(player, true);
    }
}
