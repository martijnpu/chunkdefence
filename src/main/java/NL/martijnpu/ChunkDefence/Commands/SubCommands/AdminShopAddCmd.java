package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Types.GuiShopAdd;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

public final class AdminShopAddCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Add a new shop where you look";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"shop", "add"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_SHOP;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        if (!ShopManager.getInstance().canAddShop(player))
            return;

        if (ShopManager.getInstance().getShop(player) == null)
            GuiManager.getInstance().createInventory(player, GuiShopAdd.class).openGui();
        else
            Messages.SHOP_ALREADY.send(player);
    }
}
