package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.entity.Player;

public final class ArenaTeleportCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "arena";
    }

    @Override
    protected String getDescription() {
        return "Teleport to your Arena";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"teleport"};
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Arena arena = ArenaManager.getInstance().getArena(player);
        if (arena == null)
            Messages.ARENA_NONE.send(player);
        else
            arena.teleportHome(player, false);
    }
}
