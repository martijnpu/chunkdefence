package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Gui.GuiInventory;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Types.GuiArenaLeave;
import NL.martijnpu.ChunkDefence.Gui.Types.GuiArenaSave;
import NL.martijnpu.ChunkDefence.Gui.Util.GuiSlot;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public final class ArenaLeaveCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "arena";
    }

    @Override
    protected String getDescription() {
        return "Save & leave your current arena";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"leave"};
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        arenaLeaveConfirm(player);
    }

    public static void arenaLeaveConfirm(Player player) {
        Arena arena = ArenaManager.getInstance().getArena(player);
        if (arena == null) {
            Messages.ARENA_NONE.send(player);
            return;
        }

        if (arena.getArenaData().getMembers().size() > 1) {
            if (arena.getArenaData().getMembers().get(0) == player.getUniqueId()) {
                Messages.CUSTOM.send(player, "All players need to leave before you can save en leave your arena!");
                //Todo Message you can not leave if you're the owner and have other players
                return;
            }

            GuiInventory inv = GuiManager.getInstance().createInventory(player, GuiArenaLeave.class);
            inv.changeItem(1, new GuiSlot(Material.LIME_CONCRETE_POWDER, Messages.GUI_LEAVE_ARENA_QUESTION.get())
                    .setLore(Messages.GUI_LEAVE_ARENA_CONFIRM.get())
                    .setLeftClick(() -> {
                        GuiManager.getInstance().closeInventory(player);
                        arena.playerRemove(player, true);
                    }));
            inv.openGui();
        } else {
            GuiInventory inv = GuiManager.getInstance().createInventory(player, GuiArenaSave.class);
            inv.changeItem(1, new GuiSlot(Material.LIME_CONCRETE_POWDER, Messages.GUI_SAVE_ARENA_QUESTION.get())
                    .setLore(Messages.GUI_SAVE_ARENA_CONFIRM.get())
                    .setLeftClick(() -> {
                        GuiManager.getInstance().closeInventory(player);
                        arena.deactivateArena();
                    }));
            inv.openGui();
        }
    }
}
