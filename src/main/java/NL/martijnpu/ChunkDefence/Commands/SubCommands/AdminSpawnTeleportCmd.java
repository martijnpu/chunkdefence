package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import org.bukkit.entity.Player;

public final class AdminSpawnTeleportCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Teleport to the spawn";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"spawn", "teleport"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_SPAWN;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Statics.teleportToSpawn(player, false);
    }
}
