package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapDataManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

import java.util.stream.Collectors;

public final class AdminTrapsListCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "List of all available Traps.";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"traps", "list"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_TRAP;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Messages.sendBigMessage(player, TrapDataManager.getInstance().getAllTrapNames().stream().map(x -> "&f" + x + ": " + TrapDataManager.getInstance().getTrapBase(x).NAME.get()).collect(Collectors.toList()));
    }
}
