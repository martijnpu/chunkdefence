package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import org.bukkit.entity.Player;

public final class SpawnTeleportCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "spawn";
    }

    @Override
    protected String getDescription() {
        return "Teleport to the spawn";
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected Permission getPermission() {
        return Permission.SPAWN;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Statics.teleportToSpawn(player, false);
    }
}
