package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public final class ArenaInfoCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "arena";
    }

    @Override
    protected String getDescription() {
        return "Show the info about the current wave";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"info"};
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Arena arena = ArenaManager.getInstance().getArena(player.getLocation());
        if (arena == null)
            arena = ArenaManager.getInstance().getArena(player);
        if (arena == null) {
            Messages.ARENA_NONE.send(player);
            return;
        }

        List<String> list = new ArrayList<>();

        list.add(Messages.WAVE_CURRENT.get(String.valueOf(arena.getArenaData().WAVE.get())));
        list.add(Messages.WAVE_MEMBERS.get(arena.getArenaMembers()));
        Messages.sendBigMessage(player, list);
    }
}
