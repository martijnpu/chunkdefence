package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.CommandManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class AdminHelpCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Shows all available admin commands";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"commands"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN;
    }

    @Override
    protected List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(Collections.singletonList("<page>"));
        return bigList;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        try {
            if (args.length == 0)
                Messages.sendBigMessage(player, CommandManager.getInstance().onHelpCommand(player, getCommand(), 1));
            else
                Messages.sendBigMessage(player, CommandManager.getInstance().onHelpCommand(player, getCommand(), Integer.parseInt(args[0])));
        } catch (Exception ex) {
            Messages.EXCEPT_NUMBER.send(player, args[0]);
        }
    }
}
