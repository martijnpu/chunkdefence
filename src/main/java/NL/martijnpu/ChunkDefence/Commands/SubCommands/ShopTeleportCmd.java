package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Utils.Statics;
import NL.martijnpu.ChunkDefence.Waves.WaveController;
import org.bukkit.entity.Player;

public final class ShopTeleportCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "shop";
    }

    @Override
    protected String getDescription() {
        return "Teleport to the shop";
    }

    @Override
    protected Permission getPermission() {
        return Permission.SHOP;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        teleport(player);
    }

    public static void teleport(Player player) {
        Arena arena = ArenaManager.getInstance().getArena(player);
        if (arena != null) {
            WaveController wc = arena.getWaveController();
            if (wc != null && wc.isInWave() && !Permission.BYPASS_TELEPORT.hasPermission(player)) {
                Messages.SHOP_LOC_WAVE.send(player);
                return;
            }
        }
        Statics.teleportToShop(player);
    }
}
