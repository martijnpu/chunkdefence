package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Arenas.Schematics.SchematicEditor;
import NL.martijnpu.ChunkDefence.Arenas.Schematics.SchematicManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.ArrayList;
import java.util.List;

public final class AdminSchematicLoadCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Load an Arena";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"schematic", "load"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_SCHEMATIC;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected int getPlayerArguments() {
        return 1;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + " <name>";
    }

    @Override
    protected List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(SchematicManager.getInstance().getAllSchematics());
        return bigList;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Location location = player.getLocation();

        Location delta = ArenaManager.getInstance().getArenaDelta(args[0]);
        location.setX(location.getBlockX() + (delta.getX() % 1));
        location.setY(location.getBlockY() + (delta.getY() % 1));
        location.setZ(location.getBlockZ() + (delta.getZ() % 1));
        location.setYaw(delta.getYaw());
        location.setPitch(delta.getPitch());

        if (SchematicEditor.getInstance().pasteArenaAtLocation(location, args[0])) {
            Messages.SCHEM_LOADED.send(player, args[0]);
            player.teleport(location, PlayerTeleportEvent.TeleportCause.PLUGIN);
        } else
            Messages.SCHEM_UNKNOWN.send(player, args[0]);
    }
}
