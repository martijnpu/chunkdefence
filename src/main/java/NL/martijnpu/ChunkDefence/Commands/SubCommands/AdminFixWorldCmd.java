package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Arenas.Schematics.SchematicEditor;
import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Utils.TimeHandler;
import NL.martijnpu.ChunkDefence.Worlds.WorldManager;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class AdminFixWorldCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Fix the world if by accident chunks are generated";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"fixWorld"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN;
    }


    private Iterator<BoundingBox> boxIterator = null;
    private final List<BoundingBox> excludes = new ArrayList<>();

    private final int MAX_SIZE = 1000000; //1 million blocks per iteration to prevent the server from becoming unresponsive
    private final int TIME_BETWEEN = 1; //seconds between iterations

    @Override
    protected void onCommand(Player player, String[] args) {
        if (player != null) {
            Messages.PLUGIN.send(player, "&cThis command can only be run from the console due to its extreme measures. It is not recommended to have players online and playing while rendering due to the lag spikes!");
            return;
        }

        if (boxIterator != null) {
            if (args.length == 0 || !"cancel".equals(args[0])) {
                Messages.PLUGIN.send(player, "&cWorld regeneration in progress...\n&cTo cancel write: '&f" + getUsage() + " cancel&c'");
                return;
            }

            Messages.PLUGIN.send(player, "&cCancelling the regeneration");
            boxIterator = null;
            return;
        }

        if (args.length == 0 || !"confirm".equals(args[0])) {
            Messages.PLUGIN.send(player, "&cThis command requires a lot of processing power. It is not recommended to have players online and playing while rendering due to the lag spikes!\n&cTo continue with this command write: '&f" + getUsage() + " confirm&c'");
            return;
        }

        excludes.clear();
        World world = WorldManager.getInstance().getArenaWorld();
        int arenaSpace = ConfigData.ARENA_SPACE.get();
        int halfSpace = arenaSpace / 2;

        //exclude arenas/spawn
        excludes.add(new BoundingBox(-halfSpace, world.getMinHeight(), -halfSpace,
                halfSpace, world.getMaxHeight(), halfSpace));

        for (Arena arena : ArenaManager.getInstance().getArenas()) {
            String schematic = arena.getArenaData().SCHEMATIC.get();
            Vector start = arena.getArenaData().getSpawnLocation().toVector();
            Vector dimensions = SchematicEditor.getInstance().getDimensions(schematic);
            Vector delta = ArenaManager.getInstance().getArenaDelta(schematic).toVector();

            if (dimensions == null) { //Unable to determinate arena size
                start.subtract(new Vector(arenaSpace / 2, 0, arenaSpace / 2));
                start.setY(world.getMinHeight());
                dimensions = new Vector(arenaSpace, world.getMaxHeight(), arenaSpace);
            } else {
                start.subtract(delta);
            }

            excludes.add(BoundingBox.of(start, start.clone().add(dimensions)));
        }

        //Calc big box
        int arenaRings = 0;
        while (Math.pow((arenaRings * 2 + 1), 2) <= excludes.size())
            arenaRings++;

        BoundingBox totalBox = new BoundingBox(arenaRings * arenaSpace + halfSpace, world.getMinHeight(), arenaRings * arenaSpace + halfSpace,
                -arenaRings * arenaSpace - halfSpace, world.getMaxHeight(), -arenaRings * arenaSpace - halfSpace);

        int numSubBoxes = (int) Math.ceil(totalBox.getVolume() / MAX_SIZE);
        int subBoxWidth = (int) Math.ceil(totalBox.getWidthX() / Math.cbrt(numSubBoxes));
        int subBoxHeight = (int) Math.ceil(totalBox.getHeight() / Math.cbrt(numSubBoxes));
        int subBoxDepth = (int) Math.ceil(totalBox.getWidthZ() / Math.cbrt(numSubBoxes));
        List<BoundingBox> subBoxes = new ArrayList<>();

        // Split the main box into sub-boxes
        for (int x = 0; x < Math.cbrt(numSubBoxes); x++) {
            for (int y = (int) Math.cbrt(numSubBoxes); y > 0; y--) { //From top to bottom
                for (int z = 0; z < Math.cbrt(numSubBoxes); z++) {
                    int startX = (int) totalBox.getMinX() + x * subBoxWidth;
                    int startY = (int) totalBox.getMaxY() - y * subBoxHeight;
                    int startZ = (int) totalBox.getMinZ() + z * subBoxDepth;
                    int endX = (int) Math.min(startX + subBoxWidth, totalBox.getMaxX());
                    int endY = (int) Math.max(startY + subBoxHeight, totalBox.getMinY());
                    int endZ = (int) Math.min(startZ + subBoxDepth, totalBox.getMaxZ());

                    subBoxes.add(new BoundingBox(startX, startY, startZ, endX, endY, endZ));
                }
            }
        }

        //Messages.PLUGIN.send(player, "&a" + (totalBox.getVolume() - excludes.stream().mapToDouble(BoundingBox::getVolume).sum()) + " blocks to check divided into " + subBoxes.size() + " sub action to prevent lag.");
        Messages.PLUGIN.send(player, "&aCalculated time is &f" + subBoxes.size() * TIME_BETWEEN + "&a seconds.\n&aTo cancel write: '&f" + getUsage() + " cancel&a'");

        boxIterator = subBoxes.iterator();
        processNextSubBox(0, subBoxes.size());
    }

    private void processNextSubBox(int processed, int total) {
        if (boxIterator == null || !boxIterator.hasNext()) {
            Messages.PLUGIN.sendConsole("&fWorld fixing done!");
            return;
        }

        BoundingBox subBox = boxIterator.next();
        boolean skipDelay = true; //Skip delay if no blocks are changed

        for (int x = (int) subBox.getMinX(); x < subBox.getMaxX(); x++) {
            for (int y = (int) subBox.getMaxY(); y > subBox.getMinY(); y--) { //Top to bottom for fluids
                for (int z = (int) subBox.getMinZ(); z < subBox.getMaxZ(); z++) {
                    boolean exclude = false;
                    for (BoundingBox arenaBox : excludes) {
                        if (arenaBox.contains(x, y, z)) {
                            exclude = true;
                            break;
                        }
                    }

                    if (!exclude) {
                        Block block = WorldManager.getInstance().getArenaWorld().getBlockAt(x, y, z);
                        if (block.getType() != Material.AIR) {
                            block.setType(Material.AIR);
                            skipDelay = false;
                        }
                    }
                }
            }
        }


        if ((int) ((processed + 1.0) / total * 100.0) != (int) ((processed * 1.0) / total * 100.0)) //Increase >1%
            Messages.PLUGIN.sendConsole("&fWorld fixing: " + String.format("%.2f", (processed + 1.0) / total * 100.0) + "%");

        new BukkitRunnable() {
            @Override
            public void run() {
                processNextSubBox(processed + 1, total);
            }
        }.runTaskLater(ChunkDefence.get(), skipDelay ? 1L : TIME_BETWEEN * TimeHandler.SEC);
    }
}
