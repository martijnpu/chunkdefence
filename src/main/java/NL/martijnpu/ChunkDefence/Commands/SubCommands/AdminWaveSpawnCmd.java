package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import NL.martijnpu.ChunkDefence.Waves.SpawnWaves;
import org.bukkit.entity.Player;

public final class AdminWaveSpawnCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Summon a specific wave";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"wave", "spawn"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_WAVE;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected int getPlayerArguments() {
        return 1;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + " <wave number>";
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        try {
            int wave = Integer.parseInt(args[0]);
            Arena arena = ArenaManager.getInstance().getArena(player.getLocation());

            if (arena == null) {
                Messages.ARENA_NONE.send(player);
                return;
            }

            if (arena.getWaveController() != null && arena.getWaveController().isInWave()) {
                Messages.WAVE_PROGRESS.send(player);
                return;
            }

            SpawnWaves.spawnWave(wave, arena);
        } catch (NumberFormatException ex) {
            Messages.EXCEPT_NUMBER.send(player, args[0]);
        }
    }
}
