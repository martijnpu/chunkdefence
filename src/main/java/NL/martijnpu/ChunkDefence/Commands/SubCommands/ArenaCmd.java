package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Gui.Types.GuiMainMenu;
import org.bukkit.entity.Player;

public final class ArenaCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "arena";
    }

    @Override
    protected String getDescription() {
        return "Shows the main ChunkDefence menu.";
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        GuiManager.getInstance().createInventory(player, GuiMainMenu.class).openGui();
    }
}
