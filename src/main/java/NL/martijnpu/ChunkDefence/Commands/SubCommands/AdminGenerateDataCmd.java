package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Data.FileHandler;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public final class AdminGenerateDataCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Save the item you hold as data for the mobs.yml";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"saveitem"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_WAVE;
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        FileHandler.MOBS_FILE.get().set("saved-item", null);
        ItemStack stack;

        stack = player.getInventory().getItemInMainHand();
        if (!stack.getType().isAir())
            FileHandler.MOBS_FILE.get().set("saved-item.equipment.mainhand", stack);

        stack = player.getInventory().getItemInOffHand();
        if (!stack.getType().isAir())
            FileHandler.MOBS_FILE.get().set("saved-item.equipment.offhand", stack);

        stack = player.getInventory().getHelmet();
        if (stack != null && !stack.getType().isAir())
            FileHandler.MOBS_FILE.get().set("saved-item.equipment.helmet", stack);

        stack = player.getInventory().getChestplate();
        if (stack != null && !stack.getType().isAir())
            FileHandler.MOBS_FILE.get().set("saved-item.equipment.chestplate", stack);

        stack = player.getInventory().getLeggings();
        if (stack != null && !stack.getType().isAir())
            FileHandler.MOBS_FILE.get().set("saved-item.equipment.leggings", stack);

        stack = player.getInventory().getBoots();
        if (stack != null && !stack.getType().isAir())
            FileHandler.MOBS_FILE.get().set("saved-item.equipment.boots", stack);

        FileHandler.MOBS_FILE.save();
        Messages.CONFIG_ITEM.send(player);
    }
}
