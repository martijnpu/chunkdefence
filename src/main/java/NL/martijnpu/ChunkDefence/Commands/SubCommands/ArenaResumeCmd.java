package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

public final class ArenaResumeCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "arena";
    }

    @Override
    protected String getDescription() {
        return "Resume a paused wave in your arena";
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_PAUSE;
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"resume"};
    }

    @Override
    protected boolean playerOnly() {
        return true;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        Arena arena = ArenaManager.getInstance().getArena(player);
        if (arena == null)
            Messages.ARENA_NONE.send(player);
        else if (arena.getOnlinePlayers().stream().noneMatch((x) -> arena.isInsideArena(x.getLocation())))
            Messages.ARENA_AWAY.send(player);
        else if (arena.getWaveController().resume())
            Messages.WAVE_RESUME.send(player);
        else
            Messages.WAVE_RESUMED.send(player);
    }
}
