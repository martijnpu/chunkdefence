package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Players.EconomyManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class AdminEcoAddCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Add money to a player";
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_ECONOMY;
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"economy", "add"};
    }

    @Override
    protected int getPlayerArguments() {
        return 2;
    }

    @Override
    protected String getUsage() {
        return super.getUsage() + " <player> <amount>";
    }

    @Override
    protected List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(ChunkDefence.get().getServer().getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList()));
        bigList.add(IntStream.rangeClosed(0, 9).mapToObj(String::valueOf).collect(Collectors.toList()));
        return bigList;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        try {
            OfflinePlayer target = ChunkDefence.get().getServer().getOfflinePlayerIfCached(args[0]);
            if (target == null) {
                Messages.EXCEPT_OFFLINE.send(player);
                return;
            }
            double money = Double.parseDouble(args[1]);

            EconomyManager.getInstance().depositPlayer(target, money);
            Messages.ECO_TOT.send(player, target.getName(), String.valueOf(EconomyManager.getInstance().getBalance(target)));
            Messages.ECO_ADD.send(target, money + "");
        } catch (NumberFormatException ex) {
            Messages.EXCEPT_NUMBER.send(player, args[1]);
        }
    }
}
