package NL.martijnpu.ChunkDefence.Commands.SubCommands;

import NL.martijnpu.ChunkDefence.Commands.SubCommand;
import NL.martijnpu.ChunkDefence.Scores.ScoreManager;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

public final class AdminUpdatePlaceholdersCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Force update placeholders";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"updatePlaceholders"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN_RELOAD;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        ScoreManager.getInstance().updateHighScores();
    }
}
