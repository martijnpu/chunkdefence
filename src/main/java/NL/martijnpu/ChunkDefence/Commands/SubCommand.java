package NL.martijnpu.ChunkDefence.Commands;


import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.util.List;

public abstract class SubCommand {

    protected abstract void onCommand(Player player, String[] args);

    protected abstract String getCommand();

    protected abstract String getDescription();

    protected String[] getArgs() {
        return new String[]{};
    }

    protected boolean playerOnly() {
        return false;
    }

    protected Permission getPermission() {
        return Permission.NONE;
    }

    protected int getPlayerArguments() {
        return 0;
    }

    @Nullable
    protected List<List<String>> getTabComplete() {
        return null;
    }

    protected String getUsage() {
        StringBuilder sb = new StringBuilder("/");
        sb.append(getCommand());
        for (String arg : getArgs())
            sb.append(" ").append(arg);
        return sb.toString();
    }
}
