package NL.martijnpu.ChunkDefence.Commands;

import NL.martijnpu.ChunkDefence.Players.EconomyManager;
import NL.martijnpu.ChunkDefence.Players.PlayerDataManager;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import NL.martijnpu.ChunkDefence.Utils.Permission;
import org.bukkit.entity.Player;

public final class AdminTestCmd extends SubCommand {

    @Override
    protected String getCommand() {
        return "chunkdefenceadmin";
    }

    @Override
    protected String getDescription() {
        return "Test command";
    }

    @Override
    protected String[] getArgs() {
        return new String[]{"test"};
    }

    @Override
    protected Permission getPermission() {
        return Permission.ADMIN;
    }

    @Override
    protected void onCommand(Player player, String[] args) {
        if ("save".equals(args[0])) {
            EconomyManager.getInstance().saveBalance(player, "default");
            PlayerDataManager.getInstance().getUserData(player.getUniqueId()).saveInventoryGamemode("default");

            Messages.CMD_SUCCESS.send(player);
        } else if ("retrieve".equals(args[0])) {
            EconomyManager.getInstance().retrieveBalance(player, "default");
            PlayerDataManager.getInstance().getUserData(player.getUniqueId()).retrieveInventoryGamemode("default");

            Messages.CMD_SUCCESS.send(player);
        } else {
            Messages.DEBUG.send(player, args[0]);
        }
    }
}
