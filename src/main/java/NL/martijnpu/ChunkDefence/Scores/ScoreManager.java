package NL.martijnpu.ChunkDefence.Scores;

import NL.martijnpu.ChunkDefence.Arenas.Arena;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Data.FileHandler;
import NL.martijnpu.ChunkDefence.Scores.ScoreTypes.ArenaHighscore;
import NL.martijnpu.ChunkDefence.Scores.ScoreTypes.MobKillsScore;
import NL.martijnpu.ChunkDefence.Scores.ScoreTypes.iScore;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public final class ScoreManager {
    private static ScoreManager instance;

    //Todo add PlayerDeaths
    public final ScoreView mobKills = new ScoreView();
    public final HashMap<String, ScoreView> arenaWaveHighscore = new HashMap<>();

    private ScoreManager() {
        Set<String> usersMobKill = loadUsersFromFile(MobKillsScore.KEY);
        for (String string : usersMobKill) {
            try {
                mobKills.addUUID(new MobKillsScore(UUID.fromString(string)));
            } catch (IllegalArgumentException ex) {
                Messages.WARN.sendConsole("Unable to load valid UUID in highscores: " + MobKillsScore.KEY + "." + string);
            }
        }

        for (String gamemode : ConfigData.GAMEMODES.get()) {
            ScoreView view = new ScoreView();

            Set<String> usersArenaWave = loadUsersFromFile(ArenaHighscore.KEY(gamemode));
            for (String string : usersArenaWave) {
                try {
                    view.addUUID(new ArenaHighscore(UUID.fromString(string), gamemode));
                } catch (IllegalArgumentException ex) {
                    Messages.WARN.sendConsole("Unable to load valid UUID in highscores: " + ArenaHighscore.KEY(gamemode) + "." + string);
                }
            }
            arenaWaveHighscore.put(gamemode, view);
        }
    }

    public static ScoreManager getInstance() {
        if (instance == null)
            instance = new ScoreManager();
        return instance;
    }

    public static void resetInstance() {
        instance = null;
        FileHandler.SCORES_FILE.reload();
    }

    public void printInitData() {
        Messages.PLUGIN.sendConsole("Loading highscores from file");
        Messages.PLUGIN.sendConsole(" > Loaded " + mobKills.getListSize() + " scores for mobkills");
        arenaWaveHighscore.forEach((x, y) -> Messages.PLUGIN.sendConsole(" > Loaded " + y.getListSize() + " scores for " + x));
    }

    public void addMobKill(UUID uuid) {
        iScore score = mobKills.getScore(uuid);

        if (score == null) {
            score = new MobKillsScore(uuid);
            mobKills.addUUID(score);
        }

        score.updateScore(1);
    }

    public boolean isValidGamemode(String gamemode) {
        return arenaWaveHighscore.containsKey(gamemode);
    }

    public void updateArenaScore(Arena arena) {
        if (arena.getArenaData().getMembers().isEmpty())
            return;

        String gamemode = arena.getArenaData().GAMEMODE.get();
        ScoreView scoreView = arenaWaveHighscore.get(gamemode);
        int wave = arena.getArenaData().WAVE.get();

        if (scoreView == null) {
            scoreView = new ScoreView();
            arenaWaveHighscore.put(gamemode, scoreView);
            Messages.DEBUG.sendConsole("Highscore list for gamemode '" + gamemode + "' not found. Creating new list...");
        }

        for (Player player : arena.getOnlinePlayers()) {
            iScore score = scoreView.getScore(player.getUniqueId());

            if (score == null) {
                score = new ArenaHighscore(player.getUniqueId(), gamemode);
                scoreView.addUUID(score);
            }

            score.updateScore(wave);
        }
    }

    /**
     * Method to update all highscores async
     */
    public void updateHighScores() {
        mobKills.updateListsAsync();
        arenaWaveHighscore.forEach((key, list) -> list.updateListsAsync());
    }

    private Set<String> loadUsersFromFile(String key) {
        ConfigurationSection cs = FileHandler.SCORES_FILE.get().getConfigurationSection(key);
        if (cs == null)
            return new HashSet<>();
        return cs.getKeys(false);
    }
}
