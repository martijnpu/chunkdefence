package NL.martijnpu.ChunkDefence.Scores.ScoreTypes;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import org.bukkit.OfflinePlayer;

import java.util.UUID;

public class ScoreValue {
    private final UUID uuid;
    private final int value;

    private ScoreValue(UUID uuid, int value) {
        this.uuid = uuid;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public UUID getUuid() {
        return uuid;
    }

    public OfflinePlayer getOfflinePlayer() {
        return ChunkDefence.get().getServer().getOfflinePlayer(uuid);
    }

    public static ScoreValue getDailyScore(iScore score) {
        return new ScoreValue(score.uuid, score.getDaily());
    }

    public static ScoreValue getWeeklyScore(iScore score) {
        return new ScoreValue(score.uuid, score.getWeekly());
    }

    public static ScoreValue getMonthlyScore(iScore score) {
        return new ScoreValue(score.uuid, score.getMonthly());
    }

    public static ScoreValue getForeverScore(iScore score) {
        return new ScoreValue(score.uuid, score.getForever());
    }
}
