package NL.martijnpu.ChunkDefence.Scores.ScoreTypes;

import java.util.UUID;

public class MobKillsScore extends iScore {
    public static String KEY = "mobkill";

    public MobKillsScore(UUID uuid) {
        super(KEY, uuid, true);
    }
}

