package NL.martijnpu.ChunkDefence.Scores.ScoreTypes;

import NL.martijnpu.ChunkDefence.Data.FileHandler;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.bukkit.configuration.ConfigurationSection;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public abstract class iScore {
    public final UUID uuid;
    public final String key;
    private final boolean isSum; //Calculate sum or max value
    private final HashMap<LocalDate, Integer> scores = new HashMap<>();

    protected iScore(String key, UUID uuid, boolean isSum) {
        this.key = key;
        this.uuid = uuid;
        this.isSum = isSum;

        String path = String.format("%s.%s", key, uuid);
        if (FileHandler.SCORES_FILE.get().isSet(path)) {
            ConfigurationSection config = FileHandler.SCORES_FILE.get().getConfigurationSection(path);

            for (String configKey : config.getKeys(false)) {
                LocalDate date = getDateFormatted(configKey);
                if (date == null) {
                    Messages.WARN.sendConsole("Invalid date found in scores " + path + ":" + configKey);
                    continue;
                }

                int value = config.getInt(configKey, -99);
                if (value <= 0) {
                    Messages.WARN.sendConsole("Invalid value found in scores " + path + "." + configKey + ":" + config.get(configKey));
                    continue;
                }

                scores.put(date, value);
            }
        }
    }

    /***
     * Sum or update new value for today. Based on #isSum value
     * @param value
     */
    public void updateScore(int value) {
        if (isSum)
            value += getDaily();

        scores.put(LocalDate.now(), value); //Save in local cache

        String path = String.format("%s.%s.%s", key, uuid, getTodayFormatted());

        int result = FileHandler.SCORES_FILE.get().getInt(path, -999);
        if (value > result) {
            FileHandler.SCORES_FILE.get().set(path, value);
            FileHandler.SCORES_FILE.save();
        }
    }

    private String getTodayFormatted() {
        return DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now());
    }

    private LocalDate getDateFormatted(String date) {
        try {
            return LocalDate.parse(date);
        } catch (DateTimeParseException ex) {
            return null;
        }
    }

    public int getDaily() {
        LocalDate today = LocalDate.now();
        if (scores.containsKey(today))
            return scores.get(today);
        return 0;
    }

    public int getWeekly() {
        LocalDate today = LocalDate.now();
        int dayOfWeek = today.getDayOfWeek().getValue();
        int foundValue = 0;

        LocalDate firstDay = today.minusDays(dayOfWeek);
        LocalDate lastDay = today.plusDays(1);

        if (dayOfWeek == 1) //Monday
            return getDaily();

        for (Map.Entry<LocalDate, Integer> entry : scores.entrySet()) {
            if (entry.getKey().isAfter(firstDay) && entry.getKey().isBefore(lastDay))
                if (isSum) {
                    foundValue += entry.getValue();
                } else {
                    if (entry.getValue() > foundValue)
                        foundValue = entry.getValue();
                }
        }

        return foundValue;
    }

    public int getMonthly() {
        LocalDate today = LocalDate.now();
        int dayOfMonth = today.getDayOfMonth();
        int foundValue = 0;

        LocalDate firstDay = today.minusDays(dayOfMonth);
        LocalDate lastDay = today.plusDays(1);

        for (Map.Entry<LocalDate, Integer> entry : scores.entrySet()) {
            if (entry.getKey().isAfter(firstDay) && entry.getKey().isBefore(lastDay))
                if (isSum) {
                    foundValue += entry.getValue();
                } else {
                    if (entry.getValue() > foundValue)
                        foundValue = entry.getValue();
                }
        }

        return foundValue;
    }

    public int getYearly() {
        LocalDate today = LocalDate.now();
        int dayOfYear = today.getDayOfYear();
        int foundValue = 0;

        LocalDate firstDay = today.minusDays(dayOfYear);
        LocalDate lastDay = today.plusDays(1);

        for (Map.Entry<LocalDate, Integer> entry : scores.entrySet()) {
            if (entry.getKey().isAfter(firstDay) && entry.getKey().isBefore(lastDay))
                if (isSum) {
                    foundValue += entry.getValue();
                } else {
                    if (entry.getValue() > foundValue)
                        foundValue = entry.getValue();
                }
        }

        return foundValue;
    }

    public int getForever() {
        int foundValue = 0;

        for (Map.Entry<LocalDate, Integer> entry : scores.entrySet()) {
            if (isSum) {
                foundValue += entry.getValue();
            } else {
                if (entry.getValue() > foundValue)
                    foundValue = entry.getValue();
            }
        }

        return foundValue;
    }
}
