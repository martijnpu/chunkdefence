package NL.martijnpu.ChunkDefence.Scores.ScoreTypes;

import java.util.UUID;

public class ArenaHighscore extends iScore {
    public static String KEY(String arenaName) {
        return "arena." + arenaName;
    }

    public ArenaHighscore(UUID uuid, String arenaName) {
        super(KEY(arenaName), uuid, false);
    }
}
