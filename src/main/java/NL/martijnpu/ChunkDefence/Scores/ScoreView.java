package NL.martijnpu.ChunkDefence.Scores;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Scores.ScoreTypes.ScoreValue;
import NL.martijnpu.ChunkDefence.Scores.ScoreTypes.iScore;
import NL.martijnpu.ChunkDefence.Utils.Messages;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public class ScoreView {
    private final List<iScore> rootList;

    private final List<ScoreValue> daily = new ArrayList<>();
    private final List<ScoreValue> weekly = new ArrayList<>();
    private final List<ScoreValue> monthly = new ArrayList<>();
    private final List<ScoreValue> forever = new ArrayList<>();

    private boolean updateInterlock = false;

    public ScoreView() {
        rootList = new ArrayList<>();
        updateListsAsync();
    }

    public void addUUID(iScore newScore) {
        rootList.add(newScore);
    }

    public int getListSize() {
        return rootList.size();
    }

    public @Nullable iScore getScore(UUID uuid) {
        return rootList.stream().filter(iScore -> iScore.uuid.equals(uuid)).findFirst().orElse(null);
    }

    public @Nullable ScoreValue getDaily(int position) {
        if (updateInterlock || position <= 0 || position > daily.size())
            return null;
        return daily.get(position - 1);
    }

    public @Nullable ScoreValue getWeeky(int position) {
        if (updateInterlock || position <= 0 || position > weekly.size())
            return null;
        return weekly.get(position - 1);
    }

    public @Nullable ScoreValue getMonthy(int position) {
        if (updateInterlock || position <= 0 || position > monthly.size())
            return null;
        return monthly.get(position - 1);
    }

    public @Nullable ScoreValue getForever(int position) {
        if (updateInterlock || position <= 0 || position > forever.size())
            return null;
        return forever.get(position - 1);
    }

    /***
     * Sorts the lists. Should be called Async to prevent any lag
     */
    public void updateListsAsync() {
        if (updateInterlock)
            return;

        ChunkDefence.get().getServer().getScheduler().runTaskAsynchronously(ChunkDefence.get(), () -> {
            long time = System.currentTimeMillis();
            updateInterlock = true;

            daily.clear();
            weekly.clear();
            monthly.clear();
            forever.clear();

            rootList.forEach(x -> daily.add(ScoreValue.getDailyScore(x)));
            rootList.forEach(x -> weekly.add(ScoreValue.getWeeklyScore(x)));
            rootList.forEach(x -> monthly.add(ScoreValue.getMonthlyScore(x)));
            rootList.forEach(x -> forever.add(ScoreValue.getForeverScore(x)));

            daily.sort(Comparator.comparingInt(ScoreValue::getValue).reversed());
            weekly.sort(Comparator.comparingInt(ScoreValue::getValue).reversed());
            monthly.sort(Comparator.comparingInt(ScoreValue::getValue).reversed());
            forever.sort(Comparator.comparingInt(ScoreValue::getValue).reversed());

            updateInterlock = false;
            time = System.currentTimeMillis() - time;
            if (time > 100) // sort > 0.1 sec
                Messages.WARN.sendConsole("Updating scoreboard took " + time + " ms for " + rootList.get(0).key);
        });
    }
}
