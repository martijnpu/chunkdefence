package NL.martijnpu.ChunkDefence.Utils;


import NL.martijnpu.ChunkDefence.ChunkDefence;
import org.bukkit.entity.Player;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public final class Updater {
    private static double currVersion;
    private static double newVersion;

    public static double getCurrVersion() {
        return currVersion;
    }

    public static double getNewVersion() {
        return newVersion;
    }

    public static void onPlayerJoin(Player player) {
        if ((newVersion > currVersion) && Permission.ADMIN.hasPermission(player)) {
            List<String> list = new ArrayList<>();
            list.add("&eNew version of ChunkDefence available!");
            list.add("Current version: &f" + currVersion);
            list.add("&eNew version: &f" + newVersion);
            Messages.sendBigMessage(player, list);
        }
    }

    public static void checkForUpdate() {
        currVersion = Double.parseDouble(ChunkDefence.get().getDescription().getVersion());
        try {
            getSpigotVersion();
            if (newVersion > currVersion) {
                Messages.PLUGIN.sendConsole("------------------------------------------------");
                Messages.PLUGIN.sendConsole("A new version of ChunkDefence is available.");
                Messages.PLUGIN.sendConsole("New version: " + newVersion + ". Current version: " + currVersion + ".");
                Messages.PLUGIN.sendConsole("URL: https://www.spigotmc.org/resources/chunkdefence.84958/");
                Messages.PLUGIN.sendConsole("------------------------------------------------");
            } else if (newVersion < currVersion) {
                Messages.PLUGIN.sendConsole("&bYour version of ChunkDefence (v" + currVersion + ") is ahead of the official release!");
            } else {
                Messages.PLUGIN.sendConsole("There is not a new version available for ChunkDefence.");
            }
        } catch (Exception e) {
            Messages.PLUGIN.sendConsole("------------------------------------------------");
            Messages.PLUGIN.sendConsole("Unable to receive version from Spigot.");
            Messages.PLUGIN.sendConsole("Current version: " + currVersion + ".");
            Messages.PLUGIN.sendConsole("URL: https://www.spigotmc.org/resources/chunkdefence.84958/");
            Messages.PLUGIN.sendConsole("------------------------------------------------");
            newVersion = 0;
        }
    }

    private static void getSpigotVersion() throws IOException {
        final HttpsURLConnection connection = (HttpsURLConnection) new URL("https://api.spigotmc.org/legacy/update.php?resource=84958").openConnection();
        connection.setRequestMethod("GET");
        newVersion = Double.parseDouble(new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine());
    }
}
