package NL.martijnpu.ChunkDefence.Utils;


import NL.martijnpu.ChunkDefence.ChunkDefence;
import org.bukkit.NamespacedKey;

public final class Keys {
    //Changing those keys will result in a complete reset of ChunkDefence!
    public static final NamespacedKey TRAP_KEY = new NamespacedKey(ChunkDefence.get(), "ChunkDefence-TrapBase");
    public static final NamespacedKey TRAP_DURABILITY = new NamespacedKey(ChunkDefence.get(), "ChunkDefence-TrapDurability");
    public static final NamespacedKey TRAP_OWNER = new NamespacedKey(ChunkDefence.get(), "ChunkDefence-TrapOwnerUUID");
    public static final NamespacedKey ENTITY_LAST_ATTACKED = new NamespacedKey(ChunkDefence.get(), "ChunkDefence-LastAttackedUUID");
    public static final NamespacedKey SHOP_KEY = new NamespacedKey(ChunkDefence.get(), "ChunkDefence-ShopName");
    public static final String GUI_INV_KEY = "ChunkDefence-GuiInv";
    public static final String GUI_TRAP_EDIT_KEY = "ChunkDefence-GuiTrap";
    public static final String GUI_SIGN_KEY = "ChunkDefence-GuiSign";
    public static final String MOB_KILLCOINS_KEY = "killCoins";
}
