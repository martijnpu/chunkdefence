package NL.martijnpu.ChunkDefence.Utils;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.TreeMap;
import java.util.stream.Collectors;

public final class Statics {
    private static final TreeMap<Integer, String> map = new TreeMap<>();
    public static boolean isPAPIEnabled = false;
    public static boolean isProtocolLibAvailable = false;

    static {
        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");
    }

    /**
     * Convert the number to Roman numbering
     */
    public static String toRoman(int number) {
        int l = map.floorKey(number);
        if (number == l) {
            return map.get(number);
        }
        return map.get(l) + toRoman(number - l);
    }

    /**
     * Convert the text to Capital and remove any underscores
     */
    public static String toCapital(String input) {
        return Arrays.stream(input.replace('_', ' ').split(" "))
                .map(word -> word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase())
                .collect(Collectors.joining(" "));
    }

    public static void teleportToSpawn(Player player, boolean silence) {
        Location spawn = getLocationFromString(ConfigData.SPAWN_LOCATION.get());

        if (spawn == null) {
            Messages.SPAWN_NOT_SET.send(player);
            return;
        }

        player.teleport(spawn, PlayerTeleportEvent.TeleportCause.PLUGIN);

        if (!silence) {
            if (player.getLocation().equals(spawn))
                Messages.SPAWN_TP.send(player);
            else
                Messages.SPAWN_ERROR.send(player);
        }
    }

    public static void teleportToShop(Player player) {
        Location shop = getLocationFromString(ConfigData.SHOP_LOCATION.get());

        if (shop == null) {
            Messages.SHOP_LOC_NOT_SET.send(player);
            return;
        }

        player.teleport(shop, PlayerTeleportEvent.TeleportCause.PLUGIN);
        Messages.SHOP_LOC_WELCOME.send(player);
    }

    public static Location getLocationFromString(String s) {
        if (s == null || s.trim().isEmpty())
            return null;
        s += " ";
        String[] parts = s.split(" ");

        World w;
        double x, y, z;
        float yaw = 0, pitch = 180;

        try {
            switch (parts.length) {
                case 6:
                    yaw = Float.parseFloat(parts[4]);
                    pitch = Float.parseFloat(parts[5]);
                case 4:
                    w = ChunkDefence.get().getServer().getWorld(parts[0]);
                    x = Double.parseDouble(parts[1]);
                    y = Double.parseDouble(parts[2]);
                    z = Double.parseDouble(parts[3]);
                    break;
                case 5:
                    yaw = Float.parseFloat(parts[3]);
                    pitch = Float.parseFloat(parts[4]);
                case 3:
                    w = null;
                    x = Double.parseDouble(parts[0]);
                    y = Double.parseDouble(parts[1]);
                    z = Double.parseDouble(parts[2]);
                    break;
                default:
                    return null;
            }
        } catch (NumberFormatException ex) {
            Messages.WARN.sendConsole("Unable to parse location to coordinates: " + s);
            return null;
        }

        return new Location(w, x, y, z, yaw, pitch);
    }

    /**
     * Get a String from a Location
     * <i>Example: "World 10 200 15 90 180"</i>
     */
    public static String getStringFromLocation(Location loc) {
        String string = getStringFromLocationExcludeWorld(loc);
        if (string.isEmpty())
            return string;
        return (loc.getWorld() == null ? "null" : loc.getWorld().getName()) + ' ' + string;
    }

    /**
     * Get a String from a Location
     * <i>Example: "10 200 15 90 180"</i>
     */
    public static String getStringFromLocationExcludeWorld(Location loc) {
        if (loc == null)
            return "";
        if (loc.getYaw() == 0 && loc.getPitch() == 0)
            return getStringFromXYZ(loc.toVector());
        return getStringFromXYZ(loc.toVector())
                + ' ' + ((int) loc.getYaw())
                + ' ' + ((int) loc.getPitch()); //Cast to int to remove decimals
    }

    /**
     * Return vector from a String
     * <i>Example: "10 200 15"</i>
     *
     * @return Null if string is invalid
     */
    @Nullable
    public static Vector getXYZFromString(String string) {
        if (string == null || string.trim().isEmpty())
            return null;
        string += " ";
        String[] parts = string.split(" ");
        return parts.length != 3 ? new Vector(0, 0, 0) : new Vector(Double.parseDouble(parts[0]), Double.parseDouble(parts[1]), Double.parseDouble(parts[2]));
    }

    public static Location roundToHalf(Location location) {
        Location tmpLocation = location.clone();
        tmpLocation.setX(roundToHalf(tmpLocation.getX()));
        tmpLocation.setY(roundToHalf(tmpLocation.getY()));
        tmpLocation.setZ(roundToHalf(tmpLocation.getZ()));
        tmpLocation.setYaw((int) tmpLocation.getYaw());
        tmpLocation.setPitch((int) tmpLocation.getPitch());
        return tmpLocation;
    }

    /**
     * Return String from a Vector
     * <i>Example: "10 200 15"</i>
     */
    public static String getStringFromXYZ(Vector v) {
        return v == null ? "" : (v.getX() % 1 == 0 ? ((int) v.getX()) : v.getX()) + " " +
                (v.getY() % 1 == 0 ? ((int) v.getY()) : v.getY()) + " " +
                (v.getZ() % 1 == 0 ? ((int) v.getZ()) : v.getZ());
    }

    public static double roundToHalf(double d) {
        return Math.round(d * 2) / 2.0;
    }

    public static double roundToFull(double value) {
        int scale = (int) Math.pow(10, 1);
        return (double) Math.round(value * scale) / scale;
    }
}
