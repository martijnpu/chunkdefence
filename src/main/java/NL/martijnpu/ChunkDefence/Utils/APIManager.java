package NL.martijnpu.ChunkDefence.Utils;

import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Mobs.MobManager;
import NL.martijnpu.ChunkDefence.Players.PlayerDataManager;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Traps.TrapManager;

@SuppressWarnings("unused") //Not used internally
public class APIManager {

    /**
     * The ArenaManger handles all data regarding the Arenas.
     * Manager can be used for the creation, deletion of Arenas. When retrieving an arena it's possible to get or manipulate the waveController.
     */
    public ArenaManager getArenaManager() {
        return ArenaManager.getInstance();
    }

    /**
     * The GuiManager keeps track of the open GUI's.
     * Manager can be used to open, change or close GUI's.
     */
    public GuiManager getGuiManager() {
        return GuiManager.getInstance();
    }

    /**
     * The MobManager is in charge of the mobs per wave. Whether those are defined or generated.
     * Manager can be used to customize different waves.
     */
    public MobManager getMobManager() {
        return MobManager.getInstance();
    }

    /**
     * The PlayerDataManager keeps track of all personal playerData.
     * Manager can be used to manipulate the economy.
     */
    public PlayerDataManager getPlayerDataManager() {
        return PlayerDataManager.getInstance();
    }

    /**
     * The ShopManager keeps track of all shops and their interactions.
     * Manager can be used to create, remove or manipulate shops.
     */
    public ShopManager getShopManager() {
        return ShopManager.getInstance();
    }

    /**
     * The TrapManager keeps track of all available Traps and keeps them activated.
     * Manager can be used to place or destroy traps. The creation of new defined Traps is not possible via API, but should be directly via the file.
     */
    public TrapManager getTrapManager() {
        return TrapManager.getInstance();
    }
}
