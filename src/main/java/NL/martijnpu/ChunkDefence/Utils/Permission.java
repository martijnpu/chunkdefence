package NL.martijnpu.ChunkDefence.Utils;

import org.bukkit.entity.Player;

public enum Permission {
    NONE(""),
    CUSTOM(""),
    ADMIN("chunkdefence.admin"),
    ADMIN_ARENA("chunkdefence.admin.arena"),
    ADMIN_DESTROY("chunkdefence.admin.destroy"),
    ADMIN_ECONOMY("chunkdefence.admin.economy"),
    ADMIN_INFO("chunkdefence.admin.info"),
    ADMIN_PAUSE("chunkdefence.admin.pause"),
    ADMIN_SCHEMATIC("chunkdefence.admin.schematic"),
    ADMIN_RELOAD("chunkdefence.admin.reload"),
    ADMIN_RESTART("chunkdefence.admin.restart"),
    ADMIN_SHOP("chunkdefence.admin.shop"),
    ADMIN_SPAWN("chunkdefence.admin.spawn"),
    ADMIN_TELEPORT("chunkdefence.admin.teleport"),
    ADMIN_TRAP("chunkdefence.admin.trap"),
    ADMIN_TRAP_PLACE("chunkdefence.admin.trap_place"),
    ADMIN_WAVE("chunkdefence.admin.wave"),
    BYPASS_BUILD("chunkdefence.bypass.build"),
    BYPASS_COOLDOWN("chunkdefence.bypass.arena.cooldown"),
    BYPASS_DIRECTSTART("chunkdefence.bypass.directstart"),
    BYPASS_GAMEMODE("chunkdefence.bypass.gamemode"),
    BYPASS_TELEPORT("chunkdefence.bypass.teleport"),
    MAX_PLAYERS_NOLIMIT("chunkdefence.max_players.*"),
    SHOP("chunkdefence.shop"),
    SHOP_NOLIMIT("chunkdefence.shop.*"),
    SPAWN("chunkdefence.spawn");

    private final String path;

    Permission(String path) {
        this.path = path;
    }

    /**
     * Checks if the sender has a custom permission
     */
    public boolean hasPermission(Object sender, String string) {
        if (!(sender instanceof Player player) || equals(NONE))
            return true;
        return player.hasPermission(string);
    }

    /**
     * Checks if the sender has the permission
     */
    public boolean hasPermission(Object sender) {
        if (sender == null || equals(NONE))
            return true;
        return hasPermission(sender, path);
    }

    /**
     * Checks perm and sends the "NO_PERM_CMD" message to the @sender
     */
    public boolean hasPermissionMessage(Object sender) {
        boolean perm = hasPermission(sender);
        if (!perm)
            Messages.CMD_NO_PERM.send(sender);
        return perm;
    }

    public String getPath() {
        return path;
    }
}
