package NL.martijnpu.ChunkDefence.Utils;

import NL.martijnpu.ChunkDefence.Arenas.ArenaManager;
import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Data.FileHandler;
import NL.martijnpu.ChunkDefence.Events.ProtocolLib.PackagesListener;
import NL.martijnpu.ChunkDefence.Gui.GuiManager;
import NL.martijnpu.ChunkDefence.Scores.ScoreManager;
import NL.martijnpu.ChunkDefence.Shops.ShopManager;
import NL.martijnpu.ChunkDefence.Traps.TrapData.TrapDataManager;
import NL.martijnpu.ChunkDefence.Traps.TrapManager;
import NL.martijnpu.ChunkDefence.Waves.FireWorks.FireworksManager;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import java.util.ArrayList;
import java.util.List;

public final class TimeHandler {
    public static final long SEC = 20L;
    private static final TimeHandler instance = new TimeHandler();
    private long currSec = 0;
    private int particleTask, secondTask, engineTask, fileSaveTask, scoreboardTask, shopResetTask;
    private final List<Runnable> runnableQueue = new ArrayList<>();

    private TimeHandler() {}

    public static TimeHandler getInstance() {
        return instance;
    }

    /**
     * Get the current seconds
     */
    public static long getCurrSec() {
        return getInstance().currSec;
    }

    public void start() {
        int frequency = ConfigData.PARTICLE_FREQUENCY.get();
        particleTask = ChunkDefence.get().getServer().getScheduler().scheduleSyncRepeatingTask(ChunkDefence.get(), this::particleRunnable,
                SEC * 3, frequency);
        Messages.PLUGIN.sendConsole("Started particle updates at every " + frequency + " ticks. Lower can cause more lag");

        secondTask = ChunkDefence.get().getServer().getScheduler().scheduleSyncRepeatingTask(ChunkDefence.get(), this::secondRunnable,
                SEC * 3, SEC);

        engineTask = ChunkDefence.get().getServer().getScheduler().scheduleSyncRepeatingTask(ChunkDefence.get(), this::engineRunnable,
                SEC * 3, 3);

        fileSaveTask = ChunkDefence.get().getServer().getScheduler().runTaskTimerAsynchronously(ChunkDefence.get(), this::fileSaveRunnable,
                SEC, SEC * 5).getTaskId();

        shopResetTask = ChunkDefence.get().getServer().getScheduler().scheduleSyncRepeatingTask(ChunkDefence.get(), this::shopResetRunnable,
                SEC * 60 * 60 * 4, SEC * 60 * 60 * 4); //Run every 4 hours

        if (Statics.isPAPIEnabled) {
            scoreboardTask = ChunkDefence.get().getServer().getScheduler().scheduleSyncRepeatingTask(ChunkDefence.get(), this::scoreboardRunnable,
                    SEC * ConfigData.FREQUENCY_SCOREBOARD.get(), SEC * ConfigData.FREQUENCY_SCOREBOARD.get());
            Messages.PLUGIN.sendConsole("Started placeholder reloading at every " + ConfigData.FREQUENCY_SCOREBOARD.get() + " second(s). Faster can cause more lag");
        }
    }

    public void stop() {
        if (particleTask != 0)
            ChunkDefence.get().getServer().getScheduler().cancelTask(particleTask);
        if (secondTask != 0)
            ChunkDefence.get().getServer().getScheduler().cancelTask(secondTask);
        if (engineTask != 0)
            ChunkDefence.get().getServer().getScheduler().cancelTask(engineTask);
        if (scoreboardTask != 0)
            ChunkDefence.get().getServer().getScheduler().cancelTask(scoreboardTask);
        if (fileSaveTask != 0)
            ChunkDefence.get().getServer().getScheduler().cancelTask(fileSaveTask);
        if (shopResetTask != 0)
            ChunkDefence.get().getServer().getScheduler().cancelTask(shopResetTask);

        particleTask = 0;
        secondTask = 0;
        engineTask = 0;
        scoreboardTask = 0;
        fileSaveTask = 0;
        shopResetTask = 0;

        fileSaveRunnable(); //Run once to save last moment changes
    }

    public void addRunnableQueue(Runnable runnable) {
        runnableQueue.add(runnable);
    }

    private void removeDrops() {
        for (World world : ChunkDefence.get().getServer().getWorlds())
            for (Entity entity : world.getEntities())
                if (entity.getType() == EntityType.DROPPED_ITEM)
                    entity.remove();
    }

    private void particleRunnable() {
        TrapManager.getInstance().handleEngineTick();
        TrapDataManager.getInstance().trapDataTick();
        GuiManager.getInstance().guiTick();
        FireworksManager.updateTick();
    }

    private void secondRunnable() {
        ArenaManager.getInstance().updateSec();
        currSec++;
    }

    private void engineRunnable() {
        removeDrops();
        PackagesListener.checkQue();

        runnableQueue.removeIf((runnable) -> {
            runnable.run();
            return true;
        });
    }

    private void fileSaveRunnable() {
        FileHandler.get().saveAllFiles();
    }

    private void scoreboardRunnable() {
        ScoreManager.getInstance().updateHighScores();
    }

    private void shopResetRunnable() { //Todo check why the shop entities disappear after some time
        ShopManager.resetInstance();
        ShopManager.getInstance().printInitData();
    }
}
