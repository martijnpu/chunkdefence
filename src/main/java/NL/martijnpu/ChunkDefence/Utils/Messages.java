package NL.martijnpu.ChunkDefence.Utils;

import NL.martijnpu.ChunkDefence.ChunkDefence;
import NL.martijnpu.ChunkDefence.Data.ConfigData;
import NL.martijnpu.ChunkDefence.Data.MessageData;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.minimessage.MiniMessage;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

public enum Messages {
    //Custom, not translatable messages
    CUSTOM("custom-does-not-exist", "custom-does-not-exist"), //Only used while developing
    DEBUG(CUSTOM.path, CUSTOM.placeholders.get(0)), //Used for debug messages
    WARN(CUSTOM.path, CUSTOM.placeholders.get(0)), //Always check for config & permission
    PLUGIN(CUSTOM.path, CUSTOM.placeholders.get(0)), //Information from plugin
    LIST(CUSTOM.path, CUSTOM.placeholders.get(0)), //Used be the list command
    FOOTER("footer"),
    HEADER("header"),
    PREFIX("prefix"),
    INV_RESET("inventory.reset"),

    ARENA_COOLDOWN("arena.cooldown", "%AMOUNT%"),
    ARENA_CHANGE("arena.change"),
    ARENA_CREATE("arena.create", "%TIME%"),
    ARENA_DELETE("arena.deletion"),
    ARENA_AWAY("arena.away"),
    ARENA_NONE("arena.none"),
    ARENA_JOIN("arena.join", "%NAME%"),
    ARENA_ADD("arena.add", "%NAME%"),
    ARENA_LEFT("arena.left", "%NAME%"),
    ARENA_QUIT("arena.quit", "%NAME%"),
    ARENA_WELCOME("arena.welcome"),
    ARENA_WELCOME_OTHER("arena.welcome-other", "%NAME%"),
    ARENA_TELEPORT("arena.teleport"),
    ARENA_INSIDE("arena.inside"),
    ARENA_RESUME("arena.resume"),
    ARENA_ACTIVATE("arena.activated", "%NAME%"),
    ARENA_DEACTIVATE("arena.deactivated", "%NAME%"),

    ARENA_INVITE_EXPIRED("arena.invite.expired"),
    ARENA_INVITE_GAMEMODE("arena.invite.gamemode"),
    ARENA_INVITE_INVOKED("arena.invite.revoked", "%NAME%"),
    ARENA_INVITE_JOINED("arena.invite.joined"),
    ARENA_INVITE_NONE("arena.invite.none-arena"),
    ARENA_INVITE_NONE_OTHER("arena.invite.none-other"),
    ARENA_INVITE_NOT("arena.invite.not-invited"),
    ARENA_INVITE_REMOVED("arena.invite.removed"),
    ARENA_INVITE_SIZE("arena.invite.max-size"),
    ARENA_INVITE_SUCCESS("arena.invite.success", "%NAME%"),
    ARENA_INVITE_TARGET("arena.invite.target", "%NAME%"),
    ARENA_INVITE_UNKNOWN("arena.invite.unknown"),

    BOSS_INACTIVE("bossbar.wave-inactive"),
    BOSS_COUNTDOWN("bossbar.wave-countdown", "%AMOUNT%"),
    BOSS_START("bossbar.wave-start"),
    BOSS_ACTIVE("bossbar.wave-active", "%WAVE%"),
    BOSS_END("bossbar.wave-finish"),

    CMD_FORMAT("command.format", "%FORMAT%"),
    CMD_NO_PERM("command.no-permission"),
    CMD_PLAYER("command.player-only"),
    CMD_SUCCESS("command.succeeded"),
    CMD_UNKNOWN("command.unknown", "%COMMAND%"),
    CMD_HELP_CMD("command.help.command", "%COMMAND%"),
    CMD_HELP_DESC("command.help.description", "%DESCRIPTION%"),
    CMD_HELP_PAGE("command.help.page", "%AMOUNT%", "%TOTAL%"),

    CONFIG_ERROR("config.error"),
    CONFIG_EXCEPTION("config.exception"),
    CONFIG_ITEM("config.item-saved"),
    CONFIG_SUCCESS("config.success"),

    ECO_ADD("economy.add", "%AMOUNT%"),
    ECO_ADD_KILL("economy.add-mobkill", "%PLAYER%", "%MOB%", "%COINS%"),
    ECO_ADD_TRAP("economy.add-trapkill", "%PLAYER%", "%MOB%", "%COINS%"),
    ECO_REMOVE("economy.remove", "%AMOUNT%"),
    ECO_TOT("economy.total", "%NAME%", "%AMOUNT%"),
    ECO_NO_VAULT("economy.no-vault"),
    ECO_EXPORT("economy.export"),
    ECO_IMPORT("economy.import"),

    EXCEPT_ADMIN("exception.administrator"),
    EXCEPT_ARENA_CHANGE("exception.arena-change"),
    EXCEPT_ARENA_SPAWN_LOC("exception.spawnlocations"),
    EXCEPT_FULL("exception.full"),
    EXCEPT_MODE("exception.gamemode"),
    EXCEPT_NUMBER("exception.number", "%NUMBER%"),
    EXCEPT_OFFLINE("exception.offline"),
    EXCEPT_SELF("exception.yourself"),
    EXCEPT_ARGUMENT("exception.argument", " %ARGUMENT%"),
    EXCEPT_OWNER("exception.owner"),

    //region GUI
    GUI_CANCEL("gui.cancel"),

    GUI_BUY_ARENA_NAME("gui.arena-buy.name"),
    GUI_BUY_ARENA_CONFIRM("gui.arena-buy.confirm"),
    GUI_BUY_ARENA_QUESTION("gui.arena-buy.question"),

    GUI_DELETE_ARENA_NAME("gui.arena-delete.name"),
    GUI_DELETE_ARENA_CONFIRM("gui.arena-delete.confirm"),
    GUI_DELETE_ARENA_QUESTION("gui.arena-delete.question"),

    GUI_SAVE_ARENA_NAME("gui.arena-save.name"),
    GUI_SAVE_ARENA_CONFIRM("gui.arena-save.confirm"),
    GUI_SAVE_ARENA_QUESTION("gui.arena-save.question"),

    GUI_LEAVE_ARENA_NAME("gui.arena-leave.name"),
    GUI_LEAVE_ARENA_CONFIRM("gui.arena-leave.confirm"),
    GUI_LEAVE_ARENA_QUESTION("gui.arena-leave.question"),

    GUI_MAIN_NAME("gui.main-menu.name"),
    GUI_MAIN_EXIT("gui.main-menu.exit"),
    GUI_MAIN_TOP("gui.main-menu.top"),
    GUI_MAIN_HELP("gui.main-menu.help"),
    GUI_MAIN_BACK("gui.main-menu.back"),
    GUI_MAIN_SAVE("gui.main-menu.save"),
    GUI_MAIN_LEAVE("gui.main-menu.leave"),
    GUI_MAIN_DELETE("gui.main-menu.delete"),
    GUI_MAIN_WAVE_INFO("gui.main-menu.wave-info"),
    GUI_MAIN_CREATE("gui.main-menu.create-arena"),
    GUI_MAIN_START("gui.main-menu.start-wave"),
    GUI_MAIN_SHOP("gui.main-menu.teleport-shop"),
    GUI_MAIN_ARENA("gui.main-menu.teleport-arena"),

    GUI_SHOP_NAME("gui.shop-add.name"),
    GUI_SHOP_EDIT("gui.shop-add.edit"),

    GUI_GM_NAME("gui.gamemodes.name"),
    GUI_GM_LORE("gui.gamemodes.gamemode-lore"),

    GUI_TOP_EMPTY("gui.scoreboard.empty"),
    GUI_TOP_FORMAT("gui.scoreboard.player-format", "%POSITION%", "%NAME%"),
    GUI_TOP_ARENA_NAME("gui.scoreboard.arena-name"),
    GUI_TOP_GM_NAME("gui.scoreboard.gamemode-name", "%GAMEMODE%"),
    GUI_TOP_GM_LORE("gui.scoreboard.gamemode-lore"),
    GUI_TOP_KILL_NAME("gui.scoreboard.kills-name"),
    GUI_TOP_KILL_LORE("gui.scoreboard.kills-lore"),
    //endregion GUI

    PLACEHOLDER_NONE_ARENA("placeholders.none-arena"),
    PLACEHOLDER_NONE_GAMEMODE("placeholders.none-gamemode"),
    PLACEHOLDER_TOP_EMPTY("placeholders.top.empty", "%INDEX%"),
    PLACEHOLDER_TOP_KILL("placeholders.top.kill", "%INDEX%", "%NAME%", "%AMOUNT%"),
    PLACEHOLDER_TOP_WAVE("placeholders.top.wave", "%INDEX%", "%GAMEMODE%", "%WAVE%", "%MEMBERS%"),
    PLACEHOLDER_WAVE_COUNT("placeholders.wave.countdown", "%AMOUNT%"),
    PLACEHOLDER_WAVE_NONE("placeholders.wave.none"),
    PLACEHOLDER_WAVE_PAUSE("placeholders.wave.pause"),
    PLACEHOLDER_WAVE_REMAIN("placeholders.wave.remaining", "%AMOUNT%"),
    PLACEHOLDER_WAVE_SPAWN("placeholders.wave.spawning"),

    SCHEM_NO_WE("schematic.no-worldedit"),
    SCHEM_PROCESS("schematic.processing", "%VAR%"),
    SCHEM_REMOVED("schematic.removed"),
    SCHEM_LOADED("schematic.loaded", "%NAME%"),
    SCHEM_SAVED("schematic.saved", "%NAME%"),
    SCHEM_SELECT("schematic.selection"),
    SCHEM_UNKNOWN("schematic.not-found", "%NAME%"),

    SHOP_ADD("shop.added"),
    SHOP_ALREADY("shop.already-set"),
    SHOP_ACTION_BAR("shop.hover-actionbar", "%SHOP%"),
    SHOP_INVALID("shop.invalid"),
    SHOP_LOOK("shop.look"),
    SHOP_MOVED("shop.moved"),
    SHOP_EDIT_SUCCESS("shop.edit-success"),
    SHOP_NOT_FOUND("shop.not-found"),
    SHOP_REMOVED("shop.removed"),
    SHOP_BUY_ARENA_ALREADY_OWN("shop.buying.arena-already-own"),
    SHOP_BUY_ARENA_LACK("shop.buying.arena-lack-own"),
    SHOP_BUY_COOLDOWN("shop.buying.cooldown", "%AMOUNT%"),
    SHOP_BUY_NO_MONEY("shop.buying.not-enough"),
    SHOP_BUY_NO_PERM("shop.buying.no-permission"),
    SHOP_BUY_NO_SPACE("shop.buying.no-space"),
    SHOP_BUY_SUCCESS("shop.buying.success"),
    SHOP_LOC_NOT_SET("shop.location.not-set"),
    SHOP_LOC_SAVED("shop.location.saved"),
    SHOP_LOC_WAVE("shop.location.during-wave"),
    SHOP_LOC_WELCOME("shop.location.welcome"),

    SPAWN_ERROR("spawn.unable"),
    SPAWN_FIRST("spawn.first-time"),
    SPAWN_NOT_SET("spawn.not-set"),
    SPAWN_SAVED("spawn.saved"),
    SPAWN_TP("spawn.teleport"),
    SPAWN_WELCOME("spawn.welcome"),

    TRAP_UNKNOWN("trap.unknown", "%NAME%"),
    TRAP_RECEIVE("trap.receive", "%NAME%", "%AMOUNT%"),
    TRAP_AMPLIFY("trap.amplify", "%AMPLIFY%"),
    TRAP_BUILD_TIME("trap.build-time", "%AMOUNT%"),
    TRAP_DAMAGE("trap.damage", "%DAMAGE%", "%AMOUNT%"),
    TRAP_DAMAGE_FIRE("trap.damage-aesthetic"),
    TRAP_DIVIDER("trap.divider"),
    TRAP_DURABILITY("trap.durability", "%AMOUNT%", "%MAX_AMOUNT%"),
    TRAP_DURABILITY_INF("trap.durability-infinity"),
    TRAP_DURABILITY_INF_SHOP("trap.shop-display.durability-infinity"),
    TRAP_EFFECT("trap.effect", "%EFFECT%"),
    TRAP_FIRE("trap.fire"),
    TRAP_INVALID_NEXTTO("trap.invalid-place-nextToTrap"),
    TRAP_INVALID_OUTSIDE("trap.outside-arena"),
    TRAP_INVALID_OWNER("trap.invalid-owner", "%NAME%"),
    TRAP_INVALID_PLACE("trap.invalid-place"),
    TRAP_MOBS("trap.mobs"),
    TRAP_MOBS_ALL("trap.mobs-all"),
    TRAP_MOBS_ALL_SHOP("trap.shop-display.mobs-all"),
    TRAP_MOBS_NL("trap.mobs-next-line"),
    TRAP_RANGE("trap.range", "%AMOUNT%"),
    TRAP_TARGET("trap.target", "%AMOUNT%"),

    WAVE_CANCEL("wave.cancel"),
    WAVE_CURRENT("wave.current", "%AMOUNT%"),
    WAVE_FINISH("wave.finish"),
    WAVE_MEMBERS("wave.members", "%MEMBERS%"),
    WAVE_NEXT_MULTI("wave.next-multiple", "%AMOUNT%"),
    WAVE_NEXT_SINGLE("wave.next-single", "%AMOUNT%"),
    WAVE_PROGRESS("wave.progress"),
    WAVE_RESET("wave.reset"),
    WAVE_SKIP_TIMER("wave.skip-timer"),
    WAVE_START("wave.starting"),
    WAVE_SURVIVE("wave.survived", "%AMOUNT%"),
    WAVE_FORCE_ALREADY("wave.force.already"),
    WAVE_FORCE_SUCCESS("wave.force.succeed"),
    WAVE_FORCE_WAVE("wave.force.in-wave"),
    WAVE_PAUSE("wave.pause.pause"),
    WAVE_PAUSED("wave.pause.already-paused"),
    WAVE_RESUME("wave.pause.resume"),
    WAVE_RESUMED("wave.pause.already-resumed");

    private final String path;
    private final List<String> placeholders;

    Messages(String path, String... placeholders) {
        this.path = path;
        this.placeholders = Arrays.asList(placeholders);
    }

    public static void sendBigMessage(Player player, List<String> messageLines) { //Todo update as much as possible to Components
        if (player == null)
            LIST.sendConsole(colorMessageHeadFooter(messageLines));
        else
            LIST.send(player, colorMessageHeadFooter(messageLines));
    }

    public static void sendBigMessage(Player player, Component component) {
        if (player == null)
            ChunkDefence.get().getServer().getConsoleSender().sendMessage(colorMessageHeadFooter(component));
        else
            player.sendMessage(colorMessageHeadFooter(component));
    }

    @SuppressWarnings("deprecation")
    public static String format(String string) {
        if (string == null)
            return null;
        return ChatColor.translateAlternateColorCodes('&', string);
    }

    public static Component formatLegacy(String string) {
        if (string == null || string.isBlank())
            return Component.empty();
        if (string.startsWith("[mm]"))
            return MiniMessage.miniMessage().deserialize(string.substring(4));
        return LegacyComponentSerializer.legacyAmpersand().deserialize(string);
    }

    private static String colorMessageHeadFooter(List<String> messageLine) {
        if (messageLine.isEmpty())
            return "";
        StringBuilder sb = new StringBuilder("\n" + HEADER.getReplacedMessage() + "&r\n");
        messageLine.forEach(x -> sb.append(LIST.getReplacedMessage(x)).append("\n"));
        sb.append(FOOTER.getReplacedMessage()).append("\n");
        return sb.toString();
    }

    private static Component colorMessageHeadFooter(Component component) {
        return Component.newline()
                .append(HEADER.getMiniMessageFormat()).appendNewline()
                .append(component).appendNewline()
                .append(FOOTER.getMiniMessageFormat()).appendNewline();
    }

    public void sendActionBarMessage(Player player, String... replacements) {
        player.sendActionBar(getMiniMessageFormat(replacements));
    }

    public String getPath() {
        return path;
    }

    /**
     * Send to player
     */
    public void send(Object sender, String... replacements) {
        if (equals(DEBUG) && !ConfigData.DEBUG.get())
            return;
        if (equals(WARN) && ConfigData.HIDE_WARNING.get() && !Permission.ADMIN.hasPermission(sender))
            return;

        if (sender instanceof Player player) {
            Component message = (equals(LIST) ? Component.empty() : PREFIX.getMiniMessageFormat()).append(getMiniMessageFormat(replacements));

            player.sendMessage(message);
        } else
            PLUGIN.sendConsole(getReplacedMessage(replacements));
    }

    /**
     * Send to console
     */
    @SuppressWarnings("deprecation")
    public void sendConsole(String... replacements) {
        if (equals(WARN)) {
            ChunkDefence.get().getLogger().warning(ChatColor.stripColor(getReplacedMessage(replacements)));
            return;
        }

        String prefix = "&6[ChunkDefence] &r";
        if (equals(DEBUG)) {
            if (ConfigData.DEBUG.get())
                prefix += "&4[DEBUG] ";
            else
                return;
        }

        ChunkDefence.get().getServer().getConsoleSender().sendMessage(formatLegacy(prefix).append(getMiniMessageFormat(replacements)));
    }

    /**
     * Get formatted message
     */
    public String get(String... replacements) {
        return format(getReplacedMessage(replacements));
    }

    /**
     * Get MM formatted message
     */
    public Component getMiniMessageFormat(String... replacements) {
        return formatLegacy(getReplacedMessage(replacements));
    }

    private String getReplacedMessage(String... replacements) {
        String message = MessageData.getMessage(path);
        if (replacements.length < placeholders.size())
            WARN.sendConsole("There is a placeholder missing in key 'messages." + path + "'. Check the documentation or report this to the developer! This was provided: '" + Arrays.toString(replacements) + "'");
        if (replacements.length > placeholders.size())
            WARN.sendConsole("There are too many placeholder found in key 'messages." + path + "'. Check the documentation or report this to the developer! This was provided: '" + Arrays.toString(replacements) + "'");
        if (!placeholders.isEmpty())
            for (int i = 0; i < placeholders.size(); i++)
                message = message.replace(placeholders.get(i), replacements[i]);
        return message;
    }
}
